from setuptools import find_namespace_packages, find_packages, setup

setup(
    name = "divertor_spectroscopy",
    author = "frhe",
    version = "1.0",
    description = "All sorts of packages used for qss divertorspectroscopy at w7x",
    packages=find_packages() + find_namespace_packages(),
    install_requires=["periodictable"],
    #python_requires=">=3.6.*",
    #zip_safe=False,
)
