
"""The idea is to get away from jupyter notebooks for analysis
The analysis is supposed to be more automated
Notebooks will then be used to visualize some of the results
"""

### for automated analysis some things need to be taken into account:

import numpy as np
import matplotlib.pyplot as plt
import json
import codecs
import copy
import importlib
from scipy import constants
from tqdm import tqdm
from functools import partial
from multiprocess import Pool
import scipy.io as sio
import logging
logger = logging.getLogger(__name__)

# todo: do I need to catch these errors here? The entire thing is not functional anyways without these imports - 
import archivedb
import w7x_overviewplot.query_programs as qp
import w7x_overviewplot.vmec as vmec
import w7x_overviewplot.logbookapi as LB
import w7x_overviewplot._datasources._datasources_cdx as cdx
import w7x_overviewplot.w7x_overviewplot
import w7xdia.ecrh

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import datasource_qss
from divertor_spectroscopy import wavelength_calibration
from divertor_spectroscopy import analysis
from divertor_spectroscopy import instrument
from divertor_spectroscopy import spectrum
from divertor_spectroscopy import atomic_data
from divertor_spectroscopy import qsq_juice
from hebeam_analysis import crm_solver as crm
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]
class Analysis_active_qss():
    
    def __init__(self,pid,halfmodule="H51",gas="He",valves=None,instrument_function_mode="gauss",zeeman=False):
        self.pid = pid
        self.halfmodule = halfmodule
        self.gas = gas
        self.instrument_function_mode = instrument_function_mode
        self.zeeman = zeeman
        self.campaign = utils.get_campaign_from_pid_or_day(self.pid)
        if valves is not None:
            self.valves = valves
        else:
            logger.info("Using the default valves")
            if self.halfmodule == "H51":
                if self.campaign == "OP2.2":
                    self.valves = ["H51v2", "H51v3", "H51v4", "H51v5"] # valve 1 is broken
                elif self.campaign == "OP2.1":
                    self.valves = ["H51v2", "H51v3", "H51v4", "H51v5"]
                else:
                    self.valves = ["H51v1", "H51v2", "H51v3", "H51v4", "H51v5"]
                    self.valves = ["H51v3"]
            if self.halfmodule == "H30":
                if self.campaign == "OP2.2":
                    self.valves = ["H30v1", "H30v2", "H30v3", "H30v4", "H30v5"]
                if self.campaign == "OP2.1":
                    self.valves = ["H30v1", "H30v2", "H30v3", "H30v4","H30v5"]
                    #valves = ["H30v1","H30v5"]
        if gas == "He":
            if self.halfmodule == "H30":
                self.camids = ["IP160_2"]
            if halfmodule == "H51":
                self.camids = ["IP160_4"]
        if gas == "N2":
            if self.halfmodule == "H51":
                self.camids = ["IP320_4"]
        if gas == "Ne":
            if self.halfmodule == "H51":
                self.camids = ["IP320_3"]
        self.ds_qsq, self.valve_names = qsq_juice.get_qsq_valve_voltages(self.pid,valves=self.valves,timeout=10)
    
    def analysis_active_main(self,debug=False,save=False):
        # loading the data: 
        self.debug = debug
        self.get_qsq_active_valve_names_and_times(debug=self.debug)
        self.ds_qss = {}
        for camid in self.camids:
            if self.instrument_function_mode == "convolution_entire_spectrum":
                numeric_instrument_function = True
            else:
                numeric_instrument_function = False
            self.ds_qss[camid] = datasource_qss.Datasource_qss(time_id=self.pid,camid=camid,instrument_cal=True,
                                    numeric_instrument_function=numeric_instrument_function,debug=True,correct_smearing=True)
            kwargs = {"zeeman":self.zeeman}
            model = spectrum.load_model(camid=camid,central_wavelength=self.ds_qss[camid].central_wavelength,**kwargs)
            wavelength_calibration.calibrate_spectrometer_using_datasource_qss_class(
                        instrument_function_mode=self.instrument_function_mode,ds_qss=self.ds_qss[camid],
                        model=model,wavelength_coefficients_guess=self.ds_qss[camid].wavelength_coefficients_guess,
                        lower_bounds_wl=self.ds_qss[camid].lower_bounds_wl,
                        upper_bounds_wl=self.ds_qss[camid].upper_bounds_wl,wcal=True,
                        signal_type="signal_photons",save_path=None,debug=self.debug)
        self.get_active_spectra(debug=self.debug)
        self.fit_active_spectra(debug=self.debug)
        if self.gas == "He":
            self.get_ne_Te_from_line_ratios()
        if save:
            self.save_fit_parameters()
    def get_qsq_active_valve_names_and_times(self,debug=False):
        self.puffs = {}
        valve_names_puffs = []
        opening_times_puffs = []
        closing_times_puffs = []
        if debug:
            plt.figure(figsize=(8,6))
        for valve_name in self.valve_names:
            if debug:
                plt.plot(self.ds_qsq[valve_name][0],self.ds_qsq[valve_name][1],label=valve_name)
            if np.nanmax(self.ds_qsq[valve_name][1]) > 50:
                peaks = utils.detect_peaks(spectrum = self.ds_qsq[valve_name][1],number_peaks=5,clearance=100)
                time = self.ds_qsq[valve_name][0]
                voltage_signal = self.ds_qsq[valve_name][1]
                threshold = (np.max(voltage_signal) - np.min(voltage_signal)) / 2
                # Step 2: Find the indices where the voltage crosses the threshold
                switch_on_indices = np.where(np.diff((voltage_signal > threshold).astype(int)) == 1)[0]
                switch_off_indices = np.where(np.diff((voltage_signal > threshold).astype(int)) == -1)[0]

                # Step 3: Find the corresponding switch-on and switch-off times
                switch_on_times = time[switch_on_indices]
                switch_off_times = time[switch_off_indices]
                for switch_on_time in switch_on_times:
                    if debug:
                        plt.axvline(x=switch_on_time, color='g', linestyle='--')
                    valve_names_puffs.append(valve_name)
                    opening_times_puffs.append(switch_on_time)
                for switch_off_time in switch_off_times:
                    if debug:
                        plt.axvline(x=switch_off_time, color='r', linestyle='--')
                    closing_times_puffs.append(switch_off_time)
            #for peak in peaks:
            #    plt.axvline(x=ds_qsq[valve_name][0][peak], color='r', linestyle='--')
        if debug:
            plt.legend()
            plt.xlim(left=0)
            plt.xlabel("Time [s]")
            plt.ylabel("Voltage [V]")
            plt.show()

        sorting = np.argsort(opening_times_puffs)
        self.puffs["valve_names"] = np.array(valve_names_puffs)[sorting]
        self.puffs["opening_times"] = np.array(opening_times_puffs)[sorting]
        self.puffs["closing_times"] = np.array(closing_times_puffs)[sorting]
    def get_active_spectra(self,window_size=3,debug=False):
    
        self.puffs["active_spectra"] = {}
        
        for camid in self.camids:
            if self.gas == "He":
                strongest_line = 667.815
            if self.gas == "N2":
                strongest_line = 399.5
            if self.gas == "Ne":
                strongest_line = 369.421
            w_idx = utils.val2idx(np.nanmean(self.ds_qss[camid].data["wavelength_nm"],axis=0),strongest_line)
            w_slice = slice(w_idx-15,w_idx+15) # here is the strongest He line - todo: This actually can vary between spectra and should be done via the wavelength calibration
            plt.figure(figsize=(8,6))
            signal_mean = np.nanmean(self.ds_qss[camid].data["signal_photons"][:,:,w_slice], axis=(1,2))
            plt.plot(self.ds_qss[camid].data["time_s"], signal_mean)
            self.puffs["active_spectra"][camid] = []
            for idx, opening_time in enumerate(self.puffs["opening_times"]):
                t_idx_opening = utils.val2idx(self.ds_qss[camid].data["time_s"], opening_time)
                search_width = 2  # Search range before and after the expected max
                search_width_background = 2

                # Search in the range around expected max signal (opening + 2)
                search_range = slice(t_idx_opening+2-search_width, t_idx_opening+2+search_width+1)
                signal_window_sums = np.array([
                    np.nansum(signal_mean[i:i+window_size]) for i in range(search_range.start, search_range.stop-window_size+1)
                ])

                # Get the start index of the highest sum window
                max_signal_idx_start = search_range.start + np.argmax(signal_window_sums)
                max_signal_idx_end = max_signal_idx_start + window_size - 1

                # **Fix: Background selection takes one left and one right from the signal window**
                search_range_background_left = slice(max_signal_idx_start-search_width_background, max_signal_idx_start)
                search_range_background_right = slice(max_signal_idx_end, max_signal_idx_end+search_width_background+1)

                t_idx_start_background = max_signal_idx_start - search_width_background + np.nanargmin(signal_mean[search_range_background_left])
                t_idx_end_background = max_signal_idx_end + np.nanargmin(signal_mean[search_range_background_right])

                # Plot highlighting signal and background ranges
                plt.axvspan(xmin=self.ds_qss[camid].data["time_s"][max_signal_idx_start],
                            xmax=self.ds_qss[camid].data["time_s"][max_signal_idx_end],
                            color="green", alpha=0.5)
                
                plt.axvline(self.ds_qss[camid].data["time_s"][t_idx_start_background], c="r", ls="-", alpha=0.8)
                plt.axvline(self.ds_qss[camid].data["time_s"][t_idx_end_background], c="orange", ls="--", alpha=0.8)

                t_idx_closing = utils.val2idx(self.ds_qss[camid].data["time_s"], self.puffs["closing_times"][idx])

                # Compute active spectrum over strongest consecutive signal indices
                active_spectrum = np.nanmean(self.ds_qss[camid].data["signal_photons"][max_signal_idx_start:max_signal_idx_end+1], axis=0)

                # Compute background spectrum using one left and one right index
                background_spectrum = np.nanmean(self.ds_qss[camid].data["signal_photons"][[t_idx_start_background, t_idx_end_background]], axis=0)

                # Store the difference
                self.puffs["active_spectra"][camid].append(active_spectrum - background_spectrum)

            plt.title(self.pid + " "+camid)
            plt.xlabel("Time [s]")
            plt.ylabel("Time averaged intensity entire chip [calibrated]")
            plt.grid()
            plt.legend()
            plt.show()
            if self.debug:
                for camid in self.camids:
                    for idx,opening_time in enumerate(self.puffs["opening_times"]):
                        plt.figure(figsize=(8,6))
                        plt.title(camid+" "+self.puffs["valve_names"][idx]+" "+str(opening_time))
                        plt.imshow(self.puffs["active_spectra"][camid][idx],aspect="auto",origin="lower",interpolation="none")
                        plt.colorbar()
                        plt.show()
    def fit_active_spectra(self,debug=False):
        for camid in self.camids:
            if self.gas == "He":
                self.puffs["fit_parameters"] = {}
                self.puffs["fit_parameters"][camid] = {}
                self.puffs["fit_parameters"][camid]["Intensity_HeI2p1P_3d1D_667.815"] = []
                self.puffs["fit_parameters"][camid]["Intensity_HeI2p3P_3s3S_706.519"] = []
                self.puffs["fit_parameters"][camid]["Intensity_HeI2p1P_3s1s_728.135"] = []
                special_tags = ["HeI2p1P_3d1D","HeI2p3P_3s3S","HeI2p1P_3s1s"]
                kwargs = {"extra":"he_beam","zeeman":self.zeeman}
                legend_loc = "upper right"
            if self.gas == "N2":
                self.puffs["fit_parameters"] = {}
                self.puffs["fit_parameters"][camid] = {}
                self.puffs["fit_parameters"][camid]["Intensity_NII_399.500"] = []
                self.puffs["fit_parameters"][camid]["Intensity_NII_404.098"] = []
                kwargs = {"extra":"he_beam","zeeman":self.zeeman}
                legend_loc = "upper right"
            if self.gas == "Ne":
                self.puffs["fit_parameters"] = {}
                self.puffs["fit_parameters"][camid] = {}
                self.puffs["fit_parameters"][camid]["Intensity_NeII3s2D_3p2F_357.111"] = []
                self.puffs["fit_parameters"][camid]["Intensity_NeII3s2P_3p2D_371.315"] = []
                self.puffs["fit_parameters"][camid]["Intensity_NeII3s4P_3p4P_371.046"] = []
                special_tags = ["NeII3s2D_3p2F","NeII3s2P_3p2D","NeII3s4P_3p4P"]
                kwargs = {"extra":"he_beam","zeeman":self.zeeman}
                legend_loc = "upper center"
            model = self.ds_qss[camid].model = spectrum.load_model(camid=camid,central_wavelength=self.ds_qss[camid].central_wavelength,**kwargs)
            for idx,opening_time in enumerate(self.puffs["opening_times"]):
            #for idx in [0,1]:
            #idx = 4
            #opening_time = 5.6
            #idx = utils.val2idx(puffs["opening_times"],opening_time)
                for fit_parameter in self.puffs["fit_parameters"][camid].keys():
                    self.puffs["fit_parameters"][camid][fit_parameter].append([])
                for channel in range(27):
                    if channel in self.ds_qss[camid].broken_channels:
                        logger.info("Channel {} is broken".format(channel))
                        for fit_parameter in list(self.puffs["fit_parameters"][camid])[:3]:
                            self.puffs["fit_parameters"][camid][fit_parameter][-1].append(np.nan)
                    else:
                        experiment_data = self.puffs["active_spectra"][camid][idx][channel]
                        wavelength = self.ds_qss[camid].data["wavelength_nm"][channel]
                        #inst = instrument.Instrument(wavelength=ds_qss[camid].data["wavelength_nm"][channel],width=ds_qss[camid].instrument_function[channel]*(np.abs(wavelength[0]-wavelength[-1])/len(wavelength)),instrument_function_mode=instrument_function_mode,numeric_instrument_function=ds_qss[camid].numeric_instrument_function[channel])
                        inst = instrument.Instrument(wavelength=self.ds_qss[camid].data["wavelength_nm"][channel],width=self.ds_qss[camid].instrument_function[channel]*(np.abs(wavelength[0]-wavelength[-1])/len(wavelength)),instrument_function_mode=self.instrument_function_mode)
                        analyse = analysis.Analysis(model=model,instrument=inst)
                        analyse.fit_spectrum(experiment_data=experiment_data,exclude_wavelength_from_fit=None)
                        highlight_tags =self.ds_qss[camid].model.all_tags
                        title_box = "Pid: {}\nCamid: {}\nPort: {}\nTime: {:.2f} to {:.2f}s\nChannel: {}\n bg subtracted".format(self.pid, camid, self.ds_qss[camid].port, self.puffs["opening_times"][idx], self.puffs["closing_times"][idx], channel)
                        title_box_placement = "upper left"
                        analyse.format_fit_output()
                        if debug:
                            if self.zeeman:
                                show_positions = True
                            else:
                                show_positions = False
                            intensity_label=r"Intensity [Photons/($\mathrm{m}^2$ nm sr s)]"
                            analyse.display_spectrum(show_positions=show_positions,guess=False,legend_loc=legend_loc,background_guess=False,fit=False,highlight_tags=highlight_tags,special_tags=special_tags,title_box=title_box,title_box_placement=title_box_placement,ylabel=intensity_label)
                        for fit_parameter in list(self.puffs["fit_parameters"][camid].keys())[:3]:
                            p_idx = np.where(np.array(analyse.fit_description) == fit_parameter)[0][0]
                            self.puffs["fit_parameters"][camid][fit_parameter][-1].append(analyse.fit_parameters_linear[p_idx])    
    def get_ne_Te_from_line_ratios(self):
        for camid in self.camids:
            if self.gas == "He":
                ratio_668_728 = np.array(self.puffs["fit_parameters"][camid]["Intensity_HeI2p1P_3d1D_667.815"])/np.array(self.puffs["fit_parameters"][camid]["Intensity_HeI2p1P_3s1s_728.135"])
                ratio_707_728 = np.array(self.puffs["fit_parameters"][camid]["Intensity_HeI2p3P_3s3S_706.519"])/np.array(self.puffs["fit_parameters"][camid]["Intensity_HeI2p1P_3s1s_728.135"])
                crm_solver = crm.CRM_solver(ratio_707_728, ratio_668_728, model = "Burgos_HR")
                self.puffs["fit_parameters"][camid]["Electron_temperature"],self.puffs["fit_parameters"][camid]["Electron_density"] = crm_solver.solve()
                self.puffs["fit_parameters"][camid]["Electron_density"] = self.puffs["fit_parameters"][camid]["Electron_density"]/10
    def save_fit_parameters(self):
        if self.halfmodule == "H30":
            port = "AEI30"
        if self.halfmodule == "H51":
            port = "AEI51"
        ground_path = "//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/"
        path = ground_path + "qss_analysis/discharges/{}/{}/{}".format(self.campaign,self.pid[:8],self.pid)
        version = "1"
        name = "active_spectroscopy_port{}_version{}".format(port,version)
        json.dump(self.puffs, codecs.open(path+"/{}.json".format(name), 'w', encoding='utf-8'),
                                cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)  