import numpy as np
import pandas as pd
import archivedb
import w7x_overviewplot.query_programs as qp
import w7x_overviewplot.vmec as vmec
import w7x_overviewplot.logbookapi as LB
import w7xdia.ecrh
import json
import codecs
import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import automated_analysis_qss
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

# continue loading entire op1.2b - then pick detachment experiments later!
# todo - still keep this clean


class Qss_search():
    """Overview QSS spectrometers, cameras and seeding of divertor gas inlet!"""

    def __init__(self,campaign="OP2.2"):
        """This was originally quite hardcoded, here it is actually smarter to read the path from the datasource settings file
        """
        self.campaign = campaign
        self.ground_path = "http://archive-webapi.ipp-hgw.mpg.de/"
        self.qss_path = "/raw/W7X/QSS_DivertorSpectroscopy/"
        camids = ["IP320_1","IP320_6","IP320_9","IP320_3","IP320_4","IP320_5","IP320_2","IP320_8","IP160_2","IP160_3","IP160_4"]
        with open(__location__+"/datasource_settings.json") as f:
            self.datasource_settings = json.load(f)[self.campaign]
        self.camerapath_of_specs = {} # always useful
        for camid in camids:
            self.camerapath_of_specs[camid] = self.datasource_settings[camid]["archive_path"]

    
    
    
    def get_qss_settings_op2_1(self,camids=["IP320_1","IP320_2","IP320_3","IP320_4"],signals = [],days=None,debug=False,average=200,version="0"):
        if days is None:
            days = ["2022-11-22",
                    "2022-11-23",
                    "2022-11-29",
                    "2022-11-30",
                    "2022-12-01",
                    "2022-12-06",
                    "2022-12-07",
                    "2022-12-13",
                    "2022-12-14",
                    "2022-12-15",
                    "2023-01-17",
                    "2023-01-18",
                    "2023-01-19",
                    "2023-01-25",
                    "2023-01-26",
                    "2023-01-31",
                    "2023-02-09",
                    "2023-02-14",
                    "2023-02-15",
                    "2023-02-16",
                    "2023-02-22",
                    "2023-02-23",
                    "2023-03-07",
                    "2023-03-14",
                    "2023-03-15",
                    "2023-03-16",
                    "2023-03-23",
                    "2023-03-28",
                    "2023-03-30",
                    ]
        self.signal_name = "_PARLOG"
        self.qss_settings = {}
        self.qss_settings["pids"] = []
        self.qss_settings["discharge_length"] = []
        
        for camid in camids:
            self.qss_settings[camid] = {}
            for signal in signals:
                self.qss_settings[camid][signal]=[]
        
        for day in days:
            campaign = utils.get_campaign_from_pid_or_day(day)
            if day[:4]=="2022":
                self.database_path = "Test"
            else:
                self.database_path = "ArchiveDB"
            shot_list = archivedb.get_program_list_for_day(day,timeout=10)
            lb = LB.LogbookAPI()
            path = ground_path + "qss_analysis/discharges/{}/".format(campaign)+ day[:4]+day[5:7]+day[8:]
            if os.path.exists(path):
                pass
            else:
                os.mkdir(path)
            for shot in shot_list:
                # pulse trains should be excluded:
                try:
                    info = lb.getprograminfo(program=utils.pid_format_to_logbook(shot["id"]))
                    if "train" in info["_source"]["name"].lower():
                        logger.debug("Pulse Train")
                    else:
                        logger.debug(shot["id"])
                        
                        settings = automated_analysis_qss.get_settings_for_shot(pid=shot["id"],version=version,debug=debug)
                        if settings["discharge_length"] > 3:
                            self.qss_settings["pids"].append("W7X"+shot["id"])
                            self.qss_settings["discharge_length"].append(settings["discharge_length"])
                            for camid in camids:
                                for signal in signals:
                                    try:
                                        signal_path = self.ground_path+self.database_path+self.qss_path+self.camerapath_of_specs[camid]+signal
                                        print(signal_path)
                                        data = archivedb.get_signal_for_program(signal_path,shot["id"],useCache=True)[1][1]
                                        self.qss_settings[camid][signal].append(data)
                                            # read central wavelength of spectrometer
                                            # append the central wavelength
                                    except:
                                        self.qss_settings[camid][signal].append(np.nan)
                                
                                
                except:
                    logger.debug(shot["id"]+" failed early, is there anything?")
    
    
    '''
    # this was OP1.2b - probably you never need it again, but just to be save keep it here
    def __init__(self, timeout=1, spectrometers=["IP160_1", "IP160_2",
                                                 "IP160_3", "IP160_4", "Triax320_1", "Triax320_2", "IP320_2","SP2750"], database_path="Test", save=False, cache=False):
        """Creates qss_object
        should in general just load everything and then programs can be picked
        """

        ground_path = "http://archive-webapi.ipp-hgw.mpg.de/"
        qss_path = "/raw/W7X/QSS_DivertorSpectroscopy/"
        camerapath_of_specs = {}
        specpath_of_specs = {}
        specpath_of_specs["IP160_1"] = "IsoPlane160_1_1-QSS60OC139"
        specpath_of_specs["IP160_2"] = "IsoPlane160_2_1-QSS60OC140"
        specpath_of_specs["IP160_3"] = "IsoPlane160_3_1-QSS60OC141"
        specpath_of_specs["IP160_4"] = "IsoPlane160_4_1-QSS60OC142"
        specpath_of_specs["Triax320_1"] = "Triax320_1_1-QSS60OC004"
        specpath_of_specs["Triax320_2"] = "Triax320_2_1-QSS60OC005"
        specpath_of_specs["IP320_2"] = "IsoPlane320_2_1-QSS60OC144"
        camerapath_of_specs["IP160_1"] = "PI_CCD_01_1-QSS60OC089"
        camerapath_of_specs["IP160_2"] = "PI_CCD_02_1-QSS60OC090"
        camerapath_of_specs["IP160_3"] = "PI_CCD_03_1-QSS60OC091"
        camerapath_of_specs["IP160_4"] = "PI_CCD_04_1-QSS60OC092"
        camerapath_of_specs["Triax320_1"] = "PI_CCD_05_1-QSS60OC093"
        camerapath_of_specs["Triax_320_2"] = "PI_CCD_06_1-QSS60OC095"
        camerapath_of_specs["IP320_2"] = "PI_CCD_06_1-QSS60OC095"
        camerapath_of_specs["SP2750"] = ["PI_CCD_FZJ_01_1-QSS60OC125"]
        if cache:
            # how to do this flexible? just take the keys and read it completely
            path = ground_path_pc + "qss_analysis/json_files/qss_overview.json"
            with open(path) as f:
                cache_data = json.load(f)
                self.data = {}
                self.data["pid"] = np.array(cache_data["pid"])
                for spec in spectrometers:
                    self.data[spec] = {}
                    self.data[spec]["central_wavelength"] = np.array(
                        cache_data[spec]["central_wavelength"])
                self.data["gas_type"] = {}
                self.data["gas_type"]["AEH30"] = np.array(
                    cache_data["gas_type"]["AEH30"])
                self.data["gas_type"]["AEH51"] = np.array(
                    cache_data["gas_type"]["AEH51"])
        else:
            self.data = {}
            self.data["pid"] = []
            for spec in spectrometers:
                self.data[spec] = {}
                self.data[spec]["central_wavelength"] = []
            self.data["gas_type"] = {}
            self.data["gas_type"]["AEH30"] = []
            self.data["gas_type"]["AEH51"] = []

            url_gas_type_30 = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/QSQ_Hebeam/Gas_pressures_AEH30_PARLOG/V1/parms/gasType/"
            url_gas_type_51 = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/QSQ_Hebeam/Gas_pressures_AEH51_PARLOG/V1/parms/gasType/"
            url_piezo_30 = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/QSQ_Hebeam/Piezo_valve_voltage_AEH30_DATASTREAM/V1/"
            valves_30 = ["0/AEH30_1", "1/AEH30_2",
                         "2/AEH30_3", "3/AEH30_4", "4/AEH30_5"]
            url_piezo_51 = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/QSQ_Hebeam/Piezo_valve_voltage_AEH51_DATASTREAM/V1/"

            self.cam_parameters = ["ReadoutControlMode", "AdcQuality", "AdcSpeed",
                                   "AdcEMGain", "AdcAnalogGain", "ExposureTime", "ReadoutTimeCalculation", "Rois_configString", "enableROIs"]
            self.spec_parameters = ["Central_Wavelength", "Wavelength_Calib_Coeff_0th",
                                    "Wavelength_Calib_Coeff_1th", "Wavelength_Calib_Coeff_2nd", "Wavelength_Calib_Coeff_3nd"]
            for spec in spectrometers:
                for param in self.cam_parameters:
                    self.data[spec][param] = []
            valves_51 = ["0/AEH51_1", "1/AEH51_2",
                         "2/AEH51_3", "3/AEH51_4", "4/AEH51_5"]

            if True:  # this is only false for testing that it doesn't take that long!
                daylist = ["2018-07-18",
                           "2018-07-19",
                           "2018-07-24",
                           "2018-07-25",
                           "2018-07-26",
                           "2018-07-31",
                           "2018-08-01",
                           "2018-08-07",
                           "2018-08-08",
                           "2018-08-09",
                           "2018-08-14",
                           "2018-08-16",
                           "2018-08-21",
                           "2018-08-22",
                           "2018-08-23",
                           "2018-08-28",
                           "2018-08-29",
                           "2018-09-04",
                           "2018-09-05",
                           "2018-09-06",
                           "2018-09-11",
                           "2018-09-12",
                           "2018-09-18",
                           "2018-09-19",
                           "2018-09-20",
                           "2018-09-25",
                           "2018-09-27",
                           "2018-10-02",
                           "2018-10-04",
                           "2018-10-09",
                           "2018-10-10",
                           "2018-10-11",
                           "2018-10-16",
                           "2018-10-17",
                           "2018-10-18"]
            else:
                daylist = ["2018-10-16"]
            for date in daylist:
                try:
                    pidlist = archivedb.get_program_list_for_day(
                        date, timeout=timeout)
                except:
                    print("Error on day:", date)
                for pid in pidlist:
                    self.data["pid"].append(pid["id"])
                    try:
                        max_voltage = 0
                        for valve in valves_30:
                            if np.max(archivedb.get_signal_for_program(url_piezo_30 + valve, pid["id"], timeout=timeout)[1]) > max_voltage:
                                max_voltage = np.max(archivedb.get_signal_for_program(
                                    url_piezo_30 + valve, pid["id"], timeout=timeout)[1])
                        if max_voltage > 0:
                            self.data["gas_type"]["AEH30"].append(
                                archivedb.get_signal_for_program(url_gas_type_30, pid["id"], timeout=timeout)[1][0])
                        else:
                            self.data["gas_type"]["AEH30"].append("No seeding")
                    except:
                        self.data["gas_type"]["AEH30"].append("Reading error")
                    try:
                        max_voltage = 0
                        for valve in valves_51:
                            if np.max(archivedb.get_signal_for_program(url_piezo_51 + valve, pid["id"], timeout=timeout)[1]) > max_voltage:
                                max_voltage = np.max(archivedb.get_signal_for_program(
                                    url_piezo_51 + valve, pid["id"], timeout=timeout)[1])
                        if max_voltage > 0:
                            self.data["gas_type"]["AEH51"].append(
                                archivedb.get_signal_for_program(url_gas_type_51, pid["id"], timeout=timeout)[1][0])
                        else:
                            self.data["gas_type"]["AEH51"].append("No seeding")
                    except:
                        self.data["gas_type"]["AEH51"].append("Reading error")
                    for spec in spectrometers:
                        if spec == "SP2750": # there is no central wavelength saved, so we are loading the wavelength calibration and take the central pixel
                            try:
                                path_wcal = "Test/raw/W7X/QSS_DivertorSpectroscopy/PI_CCD_FZJ_01_1-QSS60OC125_MetaData_PARLOG/"
                                wavelength_dict = archivedb.get_parameters_box_for_program(path_wcal,pid["id"],timeout=10,useCache=True)["values"][0]["wavelengthCalibration"]["Wavelength"]
                                wavelength = np.zeros(1024)*np.nan
                                for p in range(1024):
                                    wavelength[p]=wavelength_dict["["+str(p)+"]"]
                                self.data[spec]["central_wavelength"].append(np.round(wavelength[512]))
                            except:
                                self.data[spec]["central_wavelength"].append(
                                    np.nan)
                        else:
                            try:
                                path = ground_path + database_path + qss_path + \
                                    specpath_of_specs[spec] + \
                                    "_PARLOG/V1/parms/" + "Central_Wavelength/"
                                self.data[spec]["central_wavelength"].append(
                                    archivedb.get_signal_for_program(path, pid["id"], timeout=timeout)[1][0])
                            except:
                                print(path)
                                self.data[spec]["central_wavelength"].append(
                                    np.nan)
                    for spec in spectrometers:
                        for param in self.cam_parameters:
                            try:
                                self.data[spec][param].append(
                                    archivedb.get_signal_for_program(ground_path + database_path + qss_path + camerapath_of_specs[spec] + "_PARLOG/parms/PICam/config/" + param, pid["id"], timeout=timeout)[1][0])
                            except:
                                self.data[spec][param].append(
                                    np.nan)
            
            self.data["pid"] = np.array(self.data["pid"])
            for spec in spectrometers:
                self.data[spec]["central_wavelength"] = np.array(
                    self.data[spec]["central_wavelength"])
            self.data["gas_type"]["AEH30"] = np.array(
                self.data["gas_type"]["AEH30"])
            self.data["gas_type"]["AEH51"] = np.array(
                self.data["gas_type"]["AEH51"])
            if save:
                path = ground_path_pc + "qss_analysis/json_files/qss_overview.json"
                json.dump(self.data, codecs.open(path, 'w', encoding='utf-8'),
                          cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)
        # the ground thing will stay the json file - but let's also introduce a pandas self.df for easier handling
        # there is also some additional data added, but this is just processing, not reading it from the archive!
        self.flattened_data = {}
        self.flattened_data["pid"] = self.data["pid"]
        for spec in spectrometers:
            self.flattened_data[spec+"_cw"] = self.data[spec]["central_wavelength"]
        for port in self.data["gas_type"].keys():
            self.flattened_data[port+"_gas_type"] = self.data["gas_type"][port]
        self.df = pd.DataFrame(self.flattened_data)
        self.df.set_index("pid",inplace=True)
        detached_discharges = ["20180814.007",
                                "20180814.008",
                                "20180814.009",
                                "20180814.010",
                                "20180814.015",
                                "20180814.016",
                                "20180814.023",
                                "20180814.024",
                                "20180814.025",
                                "20180814.048",
                                "20180821.019",
                                "20180821.022",
                                "20180823.011",
                                "20180823.015",
                                "20180823.020",
                                "20180823.022",
                                "20180823.039",
                                "20180829.008",
                                "20180829.017",
                                "20180829.023",
                                "20180829.031",
                                "20180904.011",
                                "20180904.014",
                                "20180904.015",
                                "20180904.016",
                                "20180904.017",
                                "20180904.018",
                                "20180905.010",
                                "20180905.012",
                                "20180905.019",
                                "20180905.022",
                                "20180905.027",
                                "20180905.028",
                                "20180919.014",
                                "20180919.016",
                                "20180919.022",
                                "20180919.043",
                                "20180919.045",
                                "20180919.048",
                                "20180919.051",
                                "20180920.019",
                                "20180920.026",
                                "20180920.027",
                                "20180920.028",
                                "20180920.038",
                                "20180920.040",
                                "20180920.041",
                                "20180920.042",
                                "20180920.043",
                                "20180920.044",
                                "20180920.045",
                                "20180920.049",
                                "20180927.042",
                                "20180927.043",
                                "20180927.045",
                                "20180927.046",
                                "20180927.050",
                                "20181004.045",
                                "20181009.031",
                                "20181009.037",
                                "20181009.039",
                                "20181010.027",
                                "20181010.028",
                                "20181010.029",
                                "20181010.030",
                                "20181010.031",
                                "20181010.032",
                                "20181010.033",
                                "20181010.034",
                                "20181010.035",
                                "20181010.036",
                                "20181010.037",
                                "20181010.038",
                                "20181010.040",
                                "20181011.012",
                                "20181011.013",
                                "20181011.014",
                                "20181011.034",
                                "20181016.006",
                                "20181016.007",
                                "20181016.008",
                                "20181016.009",
                                "20181016.010",
                                "20181016.011",
                                "20181016.012",
                                "20181016.013",
                                "20181016.014",
                                "20181016.015",
                                "20181016.016",
                                "20181016.017",
                                "20181016.018",
                                "20181016.019",
                                "20181016.021",
                                "20181016.022",
                                "20181016.023",
                                "20181016.024",
                                "20181017.025",
                                "20181017.026",
                                "20181017.039"]
        detached = np.full(len(self.df["IP160_1_cw"]),False)
        for i in range(len(self.data["pid"])):
            if self.data["pid"][i] in detached_discharges: 
                detached[i] = True
        self.df["detached"] = detached
            '''