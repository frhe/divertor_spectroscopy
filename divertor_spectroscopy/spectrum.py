'''
Super class to process spectra
This class is made for creating forward models, which then can be used for analysis
Todo: 
As currently designed for every usecase (normal fit, wavelength calibration, 
instrument function invesigation etc) there is a build_spectrum and fit_spectrum function.
This is extremely much uselessly repeated code - the normal build spectrum and fit spectrum could be included
in all of these and only the additional fit parameters be used
this is kind of already done with the guess spectrum function, which works for all of these
'''
# official python packages
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import curve_fit
from scipy.interpolate import interp2d
from scipy.signal import convolve
import importlib
import json
import periodictable

# my python packages
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import json
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]
    
ground_path = settings["ground_path"]
special_colors = ["darkmagenta","darkred","darkblue","darkgreen"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]
elements = ["H","He","B","C","N","O","Ne","Ar","W","Hg"]
element_colors = {}
for i in range(len(elements)):
    element_colors[elements[i]] = colors[i+1]
styles = ["solid","dashed","dotted","dashdot","solid","dashed","dotted","dashdot"]
ion_stages = ["I","II","III","IV","V","VI","VII","VIII"]
ion_styles = {}
for i in range(len(ion_stages)):
    ion_styles[ion_stages[i]] = styles[i]

"""
todo:
the average option does not properly include the valid shit
I want to exclude it from the fit, when it is not valid anywhere!
"""

class Spectrum():
    """
    This class is used to create and fit spectra
    It can be used to quickly produce spectra by using the constructor but it is also a superclass for predefined more complex spectra, which are used for analysis, which includes more physics

    can you use a constructor of the superclass
    you want the model only to be a model! - so it shouldn't need a wavelength vector or something

    No wavelength array belongs to a spectral model, this belongs to the respective datasource
    No instrument functions belong to spectrum models - this has to happen in an instrument class
    Todo:
    A nice output of fitted parameters with a debug option would be helpful!
    the ...spectrum methods: build_spectrum, fit_spectrum, guess_spectrum should be variable. The fitted parameters for each line should be chosen

    The intensity calibration should be done from a central pixel
    There is a lot of the old functionality in this before separation of spectrum and instrument. This will be deleted at some point but kept for now because some of the functionality is not implemented in the new version yet. 
    """
    def __init__(self,peaks,tags):
        """
        I don't want to produce an extra class for every spectrum, so this is a quick constructor to fit spectra
        """
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        for i in range(len(peaks)):
            self.add_multiplet_to_model(positions=[peaks[i]],branching_ratios=[1],tag=tags[i],intensity=1)


    def add_multiplet_to_model(self, positions, branching_ratios, intensity=1, doppler_temperature=0, tag="None",fit_broadening=False):
        """
        Adds a multiplet to a model of a spectrum.
        Multiplets are useful to fix branching ratios (fine-structure). This reduces the parameters to fit, and directly adds up the intensity of all components of the multiplet (these all belong to the same transition, so they should be added up)
        You always have to fit the intensity!
        Optionally you can fit the broadening
        """
        new_multiplet = spectral_line_classes.Multiplet(
            positions=positions, branching_ratios=branching_ratios, doppler_temperature=doppler_temperature, tag=tag, intensity=intensity,fit_broadening=fit_broadening)
        self.multiplets.append(new_multiplet)
        self.all_tags.append(tag)

    def add_lorentzian_to_model(self, position=397.007, intensity=1, broadening=0.04, tag=None):
        """A Lorentzian function can be added to a spectrum, but isn't really used at the moment - because you usually also need the gaussian component and the convolution then is a voigtian (next function)"""
        new_lorentzian = spectral_line_classes.Lorentzian(
            position=position, intensity=intensity, broadening=broadening, tag=tag)
        self.lorentzians.append(new_lorentzian)
        self.all_tags.append(tag)

    def add_voigtian_to_model(self, position=397.007, intensity=1, width=0.04, broadening=0.04, tag=None):
        """A Voigtian function is used to fit the shape of Balmer lines"""
        new_voigtian = spectral_line_classes.Voigtian(
            position=position, intensity=intensity, width=width, broadening=broadening, tag=tag)
        self.voigtians.append(new_voigtian)
        self.all_tags.append(tag)
        
    def add_balmer_lines_to_model(self,lines=["H_epsilon"],intensities=[1],electron_density=1e20,electron_temperature=3,doppler_temperature=3,tag="HI",magnetic_field=None,angle_to_magnetic_field=90,fit_broadening=False,fit_doppler_shift=False,doppler_shift=0):
        """This method is supposed to be used for modeling a or multiple hydrogen lines and fit it's density and optionally temperature.
        If the same density and temperature is supposed to be fitted using multiple lines, input a list longer than 1 of lines and intensities
        """
        new_balmer_line = spectral_line_classes.Balmer_lines(lines=lines,intensities=intensities,doppler_temperature=doppler_temperature,electron_density=electron_density,electron_temperature=electron_temperature,tag=tag,magnetic_field=magnetic_field,angle_to_magnetic_field=angle_to_magnetic_field,fit_broadening=fit_broadening,fit_doppler_shift=fit_doppler_shift,doppler_shift=doppler_shift)
        self.balmer_lines.append(new_balmer_line)
        self.all_tags.append(tag)
    def add_pec_lines_to_model(self,multiplets,element="n",ion=1,indices=None,ne_range=[1e19,1e21],Te_range=[1,10],ne_count=100,Te_count=101,electron_density=1e20,electron_temperature=4,impurity_density=1e19,emission_length=0.1,fit_electron_density=True,fit_electron_temperature=True,fit_impurity_density=True,cache=False,save=False,debug=False):
        """This is supposed to add together some multiplets, but vary the line_parameters and line ratios using PECs and therefore plasma parameters
        For this to work we probably need to load the parameterspace and everything in this class
        """
        ### how am I currently fitting a spectrum? I can do a width for example
        ### now this method has to create the line_parameters only from the input parameters and be able to fit it in the fitting method
        # create fractional abundance
        # create PECs
        # create line_parameters dependent on plasma parameters
        new_pec_line = spectral_line_classes.Pec_line(multiplets=multiplets,element=element,ion=ion,indices=indices,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=emission_length,fit_electron_density=fit_electron_density,fit_electron_temperature=fit_electron_temperature,fit_impurity_density=fit_impurity_density,save=save,cache=cache,debug=debug)
        self.pec_lines.append(new_pec_line)
        for m in multiplets:
            self.all_tags.append(m.tag)
   
    def get_lines_of_model(self):
        lines = []
        for m in self.multiplets:
            for p in m.positions:
                lines.append(p)
        return lines

    def clear_tag_multiplicity(self):
        all_tags = []
        for tag in self.all_tags:
            if not tag in all_tags:
                if not tag == None:
                    all_tags.append(tag)
        self.all_tags=all_tags
    def get_all_model_parameters(self):
        """When I do fits, I used to save only the fit paramters. Here I would like to save the entire information of the model. 
        Also I want to be able to quickly display this in a dataframe.
        """
        self.all_model_parameters = {}
        self.all_model_parameters["exclude_wavelength_from_fit"] = self.exclude_wavelength_from_fit
        try:
            self.all_model_parameters["zeeman"] = self.zeeman
        except:
            self.all_model_parameters["zeeman"] = None
        try:
            self.all_model_parameters["magnetic_field"] = self.magnetic_field
        except:
            self.all_model_parameters["magnetic_field"] = None
        try:
            self.all_model_parameters["angle_to_magnetic_field"] = self.angle_to_magnetic_field
        except:
            self.all_model_parameters["angle_to_magnetic_field"] = None
        self.all_model_parameters["multiplets"] = {}
        self.all_model_parameters["balmer_lines"] = {}
        for m in self.multiplets:
            self.all_model_parameters["multiplets"]["{}_{:.3f}".format(m.tag,np.sum(np.array(m.positions)*(m.branching_ratios)))]=m.doppler_temperature
        for bl in self.balmer_lines:
            self.all_model_parameters["balmer_lines"][(bl.lines[0])] = bl.doppler_temperature

def load_model(camid, central_wavelength, *args, **kwargs):
    # load correct model
    camid_name_model_class = camid.lower().split("_")[0] 
    camid_name_model_constructor = camid_name_model_class.capitalize()
    if "extra" in kwargs and kwargs["extra"] != "":
        name_model = f"{camid_name_model_class}_cw_{int(np.round(central_wavelength))}_{kwargs['extra']}"
        name_constructor = f"{camid_name_model_constructor}_cw_{int(central_wavelength)}_{kwargs['extra']}"
        path_model = f"divertor_spectroscopy.spectrum_models.{name_model}"
    else:
        name_model = f"{camid_name_model_class}_cw_{int(np.round(central_wavelength))}"
        name_constructor = f"{camid_name_model_constructor}_cw_{int(np.round(central_wavelength))}"
        path_model = f"divertor_spectroscopy.spectrum_models.{name_model}"
    model_class = importlib.import_module(name = path_model)
    constructor = getattr(model_class, name_constructor)
    
    # delete "extra" from kwargs
    kwargs.pop("extra", None)
    model = constructor(**kwargs)
    return model
"""
def load_model(camid,central_wavelength,extra="",zeeman=False,stark_density=False,special_tags=False):
    camid_name_model_class = camid.lower().split("_")[0] # the name how it will be found model clases
    camid_name_model_constructor = ""
    camid_name_model_constructor += camid_name_model_class[0].upper()
    for letter in camid_name_model_class[1:]:
        camid_name_model_constructor += letter
    name = "divertor_spectroscopy.spectrum_models.{}_cw_{}{}".format(camid_name_model_class,str(int(central_wavelength)),extra)
    model_class = importlib.import_module(name = name)
    constructor_name = "{}_cw_{}{}".format(camid_name_model_constructor,str(int(central_wavelength)),extra)
    constructor = getattr(model_class,constructor_name)
    model = constructor(special_tags=special_tags,stark_density=stark_density,zeeman=zeeman)
    return model
"""
def redistribute_branching_ratios(predicted_positions,predicted_ratios,measured_positions,measured_ratios):
    """Sometimes the measurements of the branching ratios between fine structure components differ from predictions from atomic physics
    If we want to apply the Zeeman effect using all it's components as output by the Zeeman code from John Hey, we need to renormalize the components of each multiplet
    the lines can be really unordered
    Args:
        predicted_positions (np.array): all positions as predicted by the zeeman code - these are a lot
        predicted_ratios (np.array): branching ratios as predicted by the zeeman code
        measured_positions(np.array): these positions usually do not resolve the zeeman effect
        measured_ratios (np.array): experimentally measured ratios
    """
    sorting_predicted = np.argsort(predicted_positions)
    sorting_measured = np.argsort(measured_positions)
    predicted_positions_sorted = predicted_positions[sorting_predicted]
    predicted_ratios_sorted = predicted_ratios[sorting_predicted]
    measured_positions_sorted = measured_positions[sorting_measured]
    measured_ratios_sorted = measured_ratios[sorting_measured]
    clearance_between_measured_lines = measured_positions_sorted[1]-measured_positions_sorted[0]
    for i in range(len(measured_positions_sorted)-2):
        if measured_positions_sorted[i+2]-measured_positions_sorted[i+1]<clearance_between_measured_lines:
            clearance_between_measured_lines = measured_positions_sorted[i+2]-measured_positions_sorted[i+1]
    # now loop over 
    new_ratios = {}
    new_ratios["positions"] = {}
    new_ratios["ratios_old"] = {}
    new_ratios["ratios_new"] = {}
    new_ratios["sum_ratios_old"] = {}
    new_ratios["sum_ratios_new"] = {}
    output = {}
    
    for n,component in enumerate(measured_positions_sorted):
        new_ratios["positions"][str(component)]=[]
        new_ratios["ratios_old"][str(component)]=[]
        new_ratios["ratios_new"][str(component)]=[]
        new_ratios["sum_ratios_old"][str(component)]=[]
        new_ratios["sum_ratios_new"][str(component)]=[]
        for i,p in enumerate(predicted_positions_sorted):
            if np.abs(p-component)<clearance_between_measured_lines/2:
                new_ratios["positions"][str(component)].append(p)
                new_ratios["ratios_old"][str(component)].append(predicted_ratios_sorted[i])
        new_ratios["sum_ratios_old"][str(component)]=np.sum(new_ratios["ratios_old"][str(component)])
        new_ratios["ratios_new"][str(component)]=new_ratios["ratios_old"][str(component)]/new_ratios["sum_ratios_old"][str(component)]*measured_ratios_sorted[n]
        new_ratios["sum_ratios_new"][str(component)]=np.sum(new_ratios["ratios_new"][str(component)])
        new_ratios["positions"][str(component)]=np.array(new_ratios["positions"][str(component)])
    #return pd.DataFrame(new_ratios) # this is meant for debugging
    output["positions"] = []
    output["ratios"] = []
    for component in measured_positions_sorted:
        output["positions"]+=list(new_ratios["positions"][str(component)])
        output["ratios"]+=list(new_ratios["ratios_new"][str(component)])
    return np.array(output["positions"]),np.array(output["ratios"])
            
    