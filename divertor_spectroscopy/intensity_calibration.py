"""this module is meant to read raw data acquired for the intensity calibration
-> second step would be to read intensity calibrations from this
"""

import matplotlib.pyplot as plt
import numpy as np
import archivedb
import json
import scipy.constants as const
import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import utilities as utils


import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Intensity_calibration():
    """This class provides some helper methods to produce intensity calibrations
    But the routine production is done via a java script
    The python routines are used to investigate the intensity calibration itself!
    Moreover reading routines for the calibration coefficients are provided
    """
    def __init__(self):
        """Production of the calibration coefficients is rather done in a Jupyter notebook
        Until this changes, this is just some shell
        """
        pass

    

    def display_background(self, camid):
        """Used to analyse systematic errors in background measurements

        Args:
            camid (_type_): _description_
        """
        gains = []
        means = []
        stds = []
        for gain in self.raw_data[camid]["background"].keys():
            gains.append(int(gain))
            means.append(np.mean(self.raw_data[camid]["background"][gain]))
            stds.append(np.std(self.raw_data[camid]["background"][gain]))
        fig, axs = plt.subplots(1, 2, figsize=(10, 5))
        axs = axs.flatten()
        fig.suptitle("Background test of {}".format(camid))
        axs[0].plot(gains, means, "+")
        axs[1].plot(gains, stds, "+")
        for ax in axs:
            ax.set_xlabel("Gain")
        axs[0].set_ylabel("Mean")
        axs[1].set_ylabel("Standard deviation")
        plt.tight_layout()
        plt.show()

    def display_raw_data(self, camid, central_wavelength):
        """Used to analyse systematic errors in calibration measurements

        Args:
            camid (_type_): _description_
            central_wavelength (_type_): _description_
        """
        gains = []
        means = []
        stds = []
        for gain in self.raw_data[camid][central_wavelength].keys():
            gains.append(int(gain))
            means.append(
                np.mean(self.raw_data[camid][central_wavelength][gain]))
            stds.append(np.std(self.raw_data[camid][central_wavelength][gain]))
        fig, axs = plt.subplots(1, 2, figsize=(10, 5))
        axs = axs.flatten()
        fig.suptitle("Raw data test of {}".format(camid))
        axs[0].plot(gains, means, "+")
        axs[1].plot(gains, stds, "+")
        for ax in axs:
            ax.set_xlabel("Gain")
        axs[0].set_ylabel("Mean")
        axs[1].set_ylabel("Standard deviation")
        plt.tight_layout()
        plt.show()


def get_intensity_calibration(camid, grating, ical_comment, central_wavelength,broken_channels=[],campaign="OP2.1",debug=False):
    """This method reads intensity calibration files generated using my own python scripts
    Meant for analyisis of the calibration itself, automated writing of calibration files should run via a java script

    Args:
        camid (String): name of the spectrometer + camera system i.e. IP320_1
        grating (int): some spectrometers have multiple gratings
        central_wavelength (String): some spectrometers have movable gratings
        gain (int): recommended use one and rely on linearity of gain, but some measurements (UV) might have weak signal. Then also higher gain calibration measurements have been made
        debug (bool, optional):  Debugging option. Defaults to False.

    Returns:
        numpy array: matrix with intensity calibration [channels,pixels]
    """
    path = ground_path + "qss_analysis/json_files/intensity_calibration_factors_"+campaign+".json"
    with open(path) as f:
        ical = json.load(f)
    #try:
    calibration_matrix = np.array(ical[camid][str(
        grating)][ical_comment][str(central_wavelength)])
    for c in broken_channels:
        calibration_matrix[c] = calibration_matrix[c]*np.nan
#except:
    #    print("your settings are probably not available - interpolation?")
    if debug:
        plt.figure(figsize=(8,6))
        title = camid + " " + str(central_wavelength) + " " + ical_comment
        plt.imshow(calibration_matrix,aspect="auto",origin="lower",interpolation="none")
        plt.colorbar()
        plt.title(title)
        plt.show()
    return calibration_matrix


def get_intensity_calibration_from_archive(pid,camid,database="Sandbox",version=None,useLastVersion=True,broken_channels=[],convert_to_photons=False, smooth_window=0,wavelength=None, debug=False):
    """ Calibration is saved to the archive as a PARLOG
    this method converts that weird structure to a nice np array
    """
    return_data = {}
    return_data["calibration_factors"] = np.zeros((27,1024))*np.nan
    camname = {"IP320_2":"PI_CCD_06_1-QSS60OC095","IP160_1":"PI_CCD_01_1-QSS60OC089","IP160_2":"PI_CCD_02_1-QSS60OC090","IP160_3":"PI_CCD_03_1-QSS60OC091","IP160_4":"PI_CCD_04_1-QSS60OC092"}
    path =database+"/raw/W7XAnalysis/QSS_DivertorSpectroscopy/"+ camname[camid]+"_IntensityCalibration_PARLOG/"
    calibration_dict = archivedb.get_parameters_box_for_program(path,pid,version=version,useLastVersion=useLastVersion,timeout=10,useCache=True)["values"][0]["IntensityCalibrationFactors"]
    wavelength_dict = archivedb.get_parameters_box_for_program(path,pid,version=version,useLastVersion=useLastVersion,timeout=10,useCache=True)["values"][0]["wavelength"]
    return_data["wavelength"] = np.zeros(1024)*np.nan
    # the wavelength_calibration in the archive is really bad and in some cases just wrong - I want to use for now local wavelength calibrations. I think the wavelength resolution is not important here, so I'll average over the the entire chip to keep the wavelength vector 1-dimensional
    if wavelength is not None:
        return_data["wavelength"] = np.nanmean(wavelength,0) # average over all channels to get dimension right. Here 1 nm shouldn't make a big difference - hopefully
    else:
        for p in range(1024):
            return_data["wavelength"][p] = wavelength_dict["["+str(p)+"]"]
    if convert_to_photons:
        for c in range(27):
            if not c in broken_channels:
                for p in range(1024):
                    return_data["calibration_factors"][c,p]=calibration_dict["["+str(c)+"]"]["["+str(p)+"]"]*return_data["wavelength"][p]*1e-9/(const.h*const.c*1e9)
    else:
        for c in range(27):
            if not c in broken_channels:
                for p in range(1024):
                    return_data["calibration_factors"][c,p]=calibration_dict["["+str(c)+"]"]["["+str(p)+"]"]
    if debug:
        extent = [return_data["wavelength"][0],return_data["wavelength"][-1],-0.5,26.5]
        plt.figure(figsize=(8,6))
        plt.imshow(return_data["calibration_factors"],aspect="auto",origin="lower",interpolation="none",extent=extent)
        plt.title("Calibration_factors {} pid = {}".format(camid,pid))
        plt.xlabel("Wavelength [nm]")
        plt.ylabel("Channel")
        cbar = plt.colorbar()
        if convert_to_photons:
            cbar.ax.set_ylabel(r"Calibration factor [Photons/(s m^2 nm sr)]")
        else:
            cbar.ax.set_ylabel("Something close to Watts/m^3 sr")
        plt.show()
    if smooth_window > 0:
        return_data["calibration_factors_smoothed"] = np.zeros((27,1024))*np.nan
        for c in range(27):
            if not c in broken_channels:
                return_data["calibration_factors_smoothed"][c] = utils.smooth_noisy_data(return_data["calibration_factors"][c],smooth_window)
        if debug:
            plt.figure(figsize=(8,6))
            plt.plot(return_data["wavelength"],return_data["calibration_factors"].T)
            plt.plot(return_data["wavelength"],return_data["calibration_factors_smoothed"].T)
            plt.title("Smoothing intensity calibration {} pid = {}".format(camid,pid))
            plt.xlabel("Wavelength [nm]")
            plt.ylabel("Channel")
            plt.show()
    return return_data

def get_intensity_calibration_from_archive_julich(pid,camid="SP2750",version=None,useLastVersion=True,broken_channels=[], smooth_window=0, debug=False):
    """ Calibration is saved to the archive as a PARLOG
    this method converts that weird structure to a nice np array
    OP1.2b implementation
    
    Special case Jülicher spectrometer (SP2750 is first implementation)
    """
    number_channels = 28
    return_data = {}
    return_data["calibration_factors"] = np.zeros((number_channels,1024))*np.nan
    return_data["wavelength"] = np.zeros((number_channels,1024))*np.nan
    path_wcal = "Test/raw/W7X/QSS_DivertorSpectroscopy/PI_CCD_FZJ_01_1-QSS60OC125_MetaData_PARLOG/"
    path_ical = "Test/raw/W7X/QSS_DivertorSpectroscopy/PI_CCD_FZJ_01_1-QSS60OC125_MetaData2_PARLOG/"
    calibration_dict = archivedb.get_parameters_box_for_program(path_ical,pid,version=version,useLastVersion=useLastVersion,timeout=10,useCache=True)["values"][0]["absoluteIntensityCalibration"]["absoluteIntensityCalibrationCoefficient"]
    wavelength_dict = archivedb.get_parameters_box_for_program(path_wcal,pid,version=version,useLastVersion=useLastVersion,timeout=10,useCache=True)["values"][0]["wavelengthCalibration"]["Wavelength"]
    for c in range(number_channels):
        for p in range(1024):
            return_data["wavelength"][c,p]=wavelength_dict["["+str(p)+"]"]

    for c in range(number_channels):
            if not c in broken_channels:
                for p in range(1024):
                    return_data["calibration_factors"][c,p]=calibration_dict["ch"+str(c+1)]["["+str(p)+"]"]
    
    if smooth_window > 0:
        return_data["calibration_factors_smoothed"] = np.zeros((27,1024))*np.nan
        for c in range(27):
            if not c in broken_channels:
                return_data["calibration_factors_smoothed"][c] = utils.smooth_noisy_data(return_data["calibration_factors"][c],smooth_window)
        if debug:
            plt.figure(figsize=(8,6))
            plt.plot(return_data["wavelength"],return_data["calibration_factors"].T)
            plt.plot(return_data["wavelength"],return_data["calibration_factors_smoothed"].T)
            plt.title("Smoothing intensity calibration {} pid = {}".format(camid,pid))
            plt.xlabel("Wavelength [nm]")
            plt.ylabel("Channel")
            plt.show()
    return return_data



