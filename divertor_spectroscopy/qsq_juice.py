"""
This class is meant to provide infrastructure to write data into juice!

The Juice file for OP2.1 did not have detailed gas injection methods
If there was a neon injection (even diagnostic) from the divertor gas system the entire discharge was thrown out
I want to squeeze this information using Matts qsq contribution to the overviewplot https://git.ipp-hgw.mpg.de/astechow/w7x-overviewplot/-/blob/master/w7x_overviewplot/_datasources/_datasources_qsq.py?ref_type=heads
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import PillowWriter
import json
import pandas as pd
from scipy.optimize import curve_fit
import codecs
import logging
logger = logging.getLogger(__name__)
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]

from divertor_spectroscopy import utilities as utils
import w7x_overviewplot.query_programs as qp
import w7x_overviewplot._datasources._datasources_qsq as qsq
import archivedb

archive_path = "http://archive-webapi.ipp-hgw.mpg.de"

# divertor valves volages urls
urls_div_v = {"OP2.1":{"H10":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/32/Control%20voltage%20-%20H10/',
                        "H11":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/30/Control%20voltage%20-%20H11/',
                        "H20":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/36/Control%20voltage%20-%20H20/',
                        "H21":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/34/Control%20voltage%20-%20H21/',
                        "H31":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/38/Control%20voltage%20-%20H31/',
                        "H40":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/42/Control%20voltage%20-%20H40/',
                        "H41":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/40/Control%20voltage%20-%20H41/',
                        "H30v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/20/Control%20voltage%20-%20H30v1/',
                        "H30v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/21/Control%20voltage%20-%20H30v2/',
                        "H30v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/22/Control%20voltage%20-%20H30v3/',
                        "H30v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/23/Control%20voltage%20-%20H30v4/',
                        "H30v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/24/Control%20voltage%20-%20H30v5/',
                        "H50v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/10/Control%20voltage%20-%20H50v1/',
                        "H50v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/11/Control%20voltage%20-%20H50v2/',
                        "H50v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/12/Control%20voltage%20-%20H50v3/',
                        "H50v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/13/Control%20voltage%20-%20H50v4/',
                        "H50v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/14/Control%20voltage%20-%20H50v5/',
                        "H51v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/0/Control%20voltage%20-%20H51v1/',
                        "H51v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/1/Control%20voltage%20-%20H51v2/',
                        "H51v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/2/Control%20voltage%20-%20H51v3/',
                        "H51v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/3/Control%20voltage%20-%20H51v4/',
                        "H51v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/4/Control%20voltage%20-%20H51v5/'},
                "OP2.2": {"H10":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/32/Control%20voltage%20-%20H10/',
                        "H11":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/30/Control%20voltage%20-%20H11/',
                        "H20":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/36/Control%20voltage%20-%20H20/',
                        "H21":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/34/Control%20voltage%20-%20H21/',
                        "H31":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/38/Control%20voltage%20-%20H31/',
                        "H40":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/42/Control%20voltage%20-%20H40/',
                        "H41":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/40/Control%20voltage%20-%20H41/',
                        "H30v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/20/Control%20voltage%20-%20H30v1/',
                        "H30v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/21/Control%20voltage%20-%20H30v2/',
                        "H30v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/22/Control%20voltage%20-%20H30v3/',
                        "H30v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/23/Control%20voltage%20-%20H30v4/',
                        "H30v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/24/Control%20voltage%20-%20H30v5/',
                        "H50v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/10/Control%20voltage%20-%20H50v1/',
                        "H50v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/11/Control%20voltage%20-%20H50v2/',
                        "H50v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/12/Control%20voltage%20-%20H50v3/',
                        "H50v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/13/Control%20voltage%20-%20H50v4/',
                        "H50v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/14/Control%20voltage%20-%20H50v5/',
                        "H51v1":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/0/Control%20voltage%20-%20H51v1/',
                        "H51v2":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/1/Control%20voltage%20-%20H51v2/',
                        "H51v3":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/2/Control%20voltage%20-%20H51v3/',
                        "H51v4":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/3/Control%20voltage%20-%20H51v4/',
                        "H51v5":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/4/Control%20voltage%20-%20H51v5/',
                        "A21":'/ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletMeasuredValues_DATASTREAM/44/Control%20voltage%20-%20A21/'}}
urls_div_p = {"OP2.1":  {"H10":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/657/QSQ70BG710CP12%20Value/',
                         "H11":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/666/QSQ70BG711CP12%20Value/',
                         "H20":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/792/QSQ70BG720CP11%20Value/',
                         "H21":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/804/QSQ70BG721CP12%20Value/',
                         "H30":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/933/QSQ70BG730CP12%20Value/',
                         "H31":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/942/QSQ70BG731CP12%20Value/',
                         "H40":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1071/QSQ70BG740CP12%20Value/',
                         "H41":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1080/QSQ70BG741CP12%20Value/',
                         "H50":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1209/QSQ70BG750CP12%20Value/',
                         "H51":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1218/QSQ70BG751CP12%20Value/'},
              "OP2.2":  {"H10":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/657/QSQ70BG710CP12%20Value/',
                         "H11":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/666/QSQ70BG711CP12%20Value/',
                         "H20":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/792/QSQ70BG720CP11%20Value/',
                         "H21":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/804/QSQ70BG721CP12%20Value/',
                         "H30":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/933/QSQ70BG730CP12%20Value/',
                         "H31":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/942/QSQ70BG731CP12%20Value/',
                         "H40":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1071/QSQ70BG740CP12%20Value/',
                         "H41":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1080/QSQ70BG741CP12%20Value/',
                         "H50":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1209/QSQ70BG750CP12%20Value/',
                         "H51":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1218/QSQ70BG751CP12%20Value/',
                         "A21":'/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1239/QSQ80BG721CP12%20Value/'}}
urls_div_p = {"OP2.1":{"H10":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/657/QSQ70BG710CP12%20Value/',
                        "H11":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/666/QSQ70BG711CP12%20Value/',
                        "H20":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/792/QSQ70BG720CP11%20Value/',
                        "H21":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/804/QSQ70BG721CP12%20Value/',
                        "H30":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/933/QSQ70BG730CP12%20Value/',
                        "H31":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/942/QSQ70BG731CP12%20Value/',
                        "H40":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1071/QSQ70BG740CP12%20Value/',
                        "H41":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1080/QSQ70BG741CP12%20Value/',
                        "H50":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1209/QSQ70BG750CP12%20Value/',
                        "H51":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1218/QSQ70BG751CP12%20Value/',
                        "A21":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1239/QSQ80BG721CP12%20Value/'},
                        "OP2.2":{"H10":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/657/QSQ70BG710CP12%20Value/',
                        "H11":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/666/QSQ70BG711CP12%20Value/',
                        "H20":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/792/QSQ70BG720CP11%20Value/',
                        "H21":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/804/QSQ70BG721CP12%20Value/',
                        "H30":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/933/QSQ70BG730CP12%20Value/',
                        "H31":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/942/QSQ70BG731CP12%20Value/',
                        "H40":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1071/QSQ70BG740CP12%20Value/',
                        "H41":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1080/QSQ70BG741CP12%20Value/',
                        "H50":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1209/QSQ70BG750CP12%20Value/',
                        "H51":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1218/QSQ70BG751CP12%20Value/',
                        "A21":    '/ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/1239/QSQ80BG721CP12%20Value/'}}
div_gas_types = {'2': 'unknown',
                     '3': 'evacuated',
                     '8': 'He',
                     '10': 'H2',
                     '11': 'N2', 
                     '12': 'Ne',
                     '13': 'Ar'}
# divertor gas valves
BASEURL = '/ArchiveDB/views/KKS/DCH_GasInlet/Divertor/Measured/'
archive_signal_dict = {}
# Piezo valve voltages
archive_signal_dict['QSQ_H10_V'] = f'{BASEURL}32/'
archive_signal_dict['QSQ_H11_V'] = f'{BASEURL}30/'
archive_signal_dict['QSQ_H20_V'] = f'{BASEURL}36/'
archive_signal_dict['QSQ_H21_V'] = f'{BASEURL}34/'
archive_signal_dict['QSQ_A21_V'] = f'{BASEURL}44/'
archive_signal_dict['QSQ_H31_V'] = f'{BASEURL}38/'
archive_signal_dict['QSQ_H40_V'] = f'{BASEURL}42/'
archive_signal_dict['QSQ_H41_V'] = f'{BASEURL}40/'
archive_signal_dict['QSQ_H30v1_V'] = f'{BASEURL}20/'
archive_signal_dict['QSQ_H30v2_V'] = f'{BASEURL}21/'
archive_signal_dict['QSQ_H30v3_V'] = f'{BASEURL}22/'
archive_signal_dict['QSQ_H30v4_V'] = f'{BASEURL}23/'
archive_signal_dict['QSQ_H30v5_V'] = f'{BASEURL}24/'
archive_signal_dict['QSQ_H50v1_V'] = f'{BASEURL}10/'
archive_signal_dict['QSQ_H50v2_V'] = f'{BASEURL}11/'
archive_signal_dict['QSQ_H50v3_V'] = f'{BASEURL}12/'
archive_signal_dict['QSQ_H50v4_V'] = f'{BASEURL}13/'
archive_signal_dict['QSQ_H50v5_V'] = f'{BASEURL}14/'
archive_signal_dict['QSQ_H51v1_V'] = f'{BASEURL}0/'
archive_signal_dict['QSQ_H51v2_V'] = f'{BASEURL}1/'
archive_signal_dict['QSQ_H51v3_V'] = f'{BASEURL}2/'
archive_signal_dict['QSQ_H51v4_V'] = f'{BASEURL}3/'
archive_signal_dict['QSQ_H51v5_V'] = f'{BASEURL}4/'

BASEURL = '/ArchiveDB/views/KKS/DCH_GasInlet/PLC_Data/'
# Gas type
archive_signal_dict['QSQ_H10_gastype'] = f'{BASEURL}409/'
archive_signal_dict['QSQ_H11_gastype'] = f'{BASEURL}410/'
archive_signal_dict['QSQ_H20_gastype'] = f'{BASEURL}411/'
archive_signal_dict['QSQ_H21_gastype'] = f'{BASEURL}412/'
archive_signal_dict['QSQ_A21_gastype'] = f'{BASEURL}415/'
archive_signal_dict['QSQ_H30_gastype'] = f'{BASEURL}413/'
archive_signal_dict['QSQ_H31_gastype'] = f'{BASEURL}414/'
archive_signal_dict['QSQ_H40_gastype'] = f'{BASEURL}416/'
archive_signal_dict['QSQ_H41_gastype'] = f'{BASEURL}417/'
archive_signal_dict['QSQ_H50_gastype'] = f'{BASEURL}418/'
archive_signal_dict['QSQ_H51_gastype'] = f'{BASEURL}419/'
# Gas box pressure (high pressure gauges, OP2.1)
archive_signal_dict['QSQ_H10_pressure_cp11'] = f'{BASEURL}644/'
archive_signal_dict['QSQ_H11_pressure_cp11'] = f'{BASEURL}653/'
archive_signal_dict['QSQ_H20_pressure_cp11'] = f'{BASEURL}782/'
archive_signal_dict['QSQ_H21_pressure_cp11'] = f'{BASEURL}791/'
archive_signal_dict['QSQ_H30_pressure_cp11'] = f'{BASEURL}920/'
archive_signal_dict['QSQ_H31_pressure_cp11'] = f'{BASEURL}929/'
archive_signal_dict['QSQ_H40_pressure_cp11'] = f'{BASEURL}1058/'
archive_signal_dict['QSQ_H41_pressure_cp11'] = f'{BASEURL}1067/'
archive_signal_dict['QSQ_H50_pressure_cp11'] = f'{BASEURL}1196/'
archive_signal_dict['QSQ_H51_pressure_cp11'] = f'{BASEURL}1205/'
# Gas box pressure (high pressure gauges, OP2.2)
archive_signal_dict['QSQ_H10_pressure_cp11_OP22'] = f'{BASEURL}654/'
archive_signal_dict['QSQ_H11_pressure_cp11_OP22'] = f'{BASEURL}663/'
archive_signal_dict['QSQ_H20_pressure_cp11_OP22'] = f'{BASEURL}792/'
archive_signal_dict['QSQ_H21_pressure_cp11_OP22'] = f'{BASEURL}801/'
archive_signal_dict['QSQ_A21_pressure_cp11_OP22'] = f'{BASEURL}1236/'
archive_signal_dict['QSQ_H30_pressure_cp11_OP22'] = f'{BASEURL}930/'
archive_signal_dict['QSQ_H31_pressure_cp11_OP22'] = f'{BASEURL}939/'
archive_signal_dict['QSQ_H40_pressure_cp11_OP22'] = f'{BASEURL}1068/'
archive_signal_dict['QSQ_H41_pressure_cp11_OP22'] = f'{BASEURL}1077/'
archive_signal_dict['QSQ_H50_pressure_cp11_OP22'] = f'{BASEURL}1206/'
archive_signal_dict['QSQ_H51_pressure_cp11_OP22'] = f'{BASEURL}1215/'
# Gas box pressure (low pressure gauges, OP2.1)
archive_signal_dict['QSQ_H10_pressure_cp12'] = f'{BASEURL}647/'
archive_signal_dict['QSQ_H11_pressure_cp12'] = f'{BASEURL}656/'
archive_signal_dict['QSQ_H20_pressure_cp12'] = f'{BASEURL}785/'
archive_signal_dict['QSQ_H21_pressure_cp12'] = f'{BASEURL}794/'
archive_signal_dict['QSQ_H30_pressure_cp12'] = f'{BASEURL}923/'
archive_signal_dict['QSQ_H31_pressure_cp12'] = f'{BASEURL}933/'
archive_signal_dict['QSQ_H40_pressure_cp12'] = f'{BASEURL}1062/'
archive_signal_dict['QSQ_H41_pressure_cp12'] = f'{BASEURL}1070/'
archive_signal_dict['QSQ_H50_pressure_cp12'] = f'{BASEURL}1199/'
archive_signal_dict['QSQ_H51_pressure_cp12'] = f'{BASEURL}1208/'
# Gas box pressure (low pressure gauges, OP2.2)
archive_signal_dict['QSQ_H10_pressure_cp12_OP22'] = f'{BASEURL}657/'
archive_signal_dict['QSQ_H11_pressure_cp12_OP22'] = f'{BASEURL}666/'
archive_signal_dict['QSQ_H20_pressure_cp12_OP22'] = f'{BASEURL}795/'
archive_signal_dict['QSQ_H21_pressure_cp12_OP22'] = f'{BASEURL}804/'
archive_signal_dict['QSQ_A21_pressure_cp12_OP22'] = f'{BASEURL}1239/'
archive_signal_dict['QSQ_H30_pressure_cp12_OP22'] = f'{BASEURL}933/'
archive_signal_dict['QSQ_H31_pressure_cp12_OP22'] = f'{BASEURL}942/'
archive_signal_dict['QSQ_H40_pressure_cp12_OP22'] = f'{BASEURL}1071/'
archive_signal_dict['QSQ_H41_pressure_cp12_OP22'] = f'{BASEURL}1080/'
archive_signal_dict['QSQ_H50_pressure_cp12_OP22'] = f'{BASEURL}1209/'
archive_signal_dict['QSQ_H51_pressure_cp12_OP22'] = f'{BASEURL}1218/'

def get_qsq_valve_voltages(shotname,valves=None,campaign="OP2.2",timeout=10):
    if valves is None:
        valves = urls_div_v[campaign].keys()
    valve_names = []
    valve_voltages = {}
    if True:
        for valve in valves:
            valve_names.append("GasInlet_qsq"+str(valve)+"_"+"V")
            #path = archive_path+urls_div_v[campaign][valve] # Alias
            path = archive_path+archive_signal_dict["QSQ_{}_V".format(valve)] # Controlstation
            valve_voltages[valve_names[-1]] = archivedb.get_signal_for_program(path,shotname,timeout=timeout,useCache=True)
            list_valve_voltages = list(valve_voltages[valve_names[-1]])
            try: # when there is nothing saved under one stream, this will avoid errors
                list_valve_voltages[1] = np.array(list_valve_voltages[1],dtype=float)*26
            except:
                pass
            valve_voltages[valve_names[-1]] = tuple(list_valve_voltages)
    return valve_voltages,valve_names
def squeeze_qsq_valve_voltages(shotname, time_base, timestamps, common_base_method,
              timeout, settings={"campaign":"OP2.2"}):
    
    valve_voltages,valve_names = get_qsq_valve_voltages(shotname,campaign=settings["campaign"])
    qsq_squeeze = {}
    for name in valve_names:
        qsq_tb = common_base_method(valve_voltages[name][0], valve_voltages[name][1], time_base)
        qsq_squeeze[name] = qsq_tb[:,0]
    return pd.DataFrame(qsq_squeeze, index = timestamps)
def get_qsq_gastypes_and_pressures(shotname,campaign="OP2.2",valves=None):
    
    if valves is None:
        if campaign == "OP2.2":
            valves = ["H10", "H11", "H20", "H21", "H30", "H31", "H40", "H41", "H50", "H51",'A21']
        if campaign == "OP2.1":
            valves = ["H10", "H11", "H20", "H21", "H30", "H31", "H40", "H41", "H50", "H51"]
    valve_names = []
    gas_types_and_pressures = {}
    for valve in valves:
        valve_names.append("GasInlet_qsq"+str(valve)+"_"+"gas_type")
        gas_types_and_pressures[valve_names[-1]]=qp.get_gastypes(shotname, system='qsq', ids=[valve])[0]
        valve_names.append("GasInlet_qsq"+str(valve)+"_"+"P")
        gas_types_and_pressures[valve_names[-1]]=qp.get_qsq_gas_box_pressure(shotname, ids=[valve])[0] 
    return gas_types_and_pressures,valve_names
def squeeze_qsq_gas_types_and_pressures(shotname, time_base, timestamps, common_base_method,
              timeout, settings={"campaign":"OP2.2"}):
    gas_types_and_pressures,valve_names = get_qsq_gastypes_and_pressures(shotname,campaign=settings["campaign"])
    qsq_squeeze = {}
    for name in valve_names:
        qsq_squeeze[name] = gas_types_and_pressures[name]
    return pd.DataFrame(qsq_squeeze, index = timestamps)
def get_qsq_voltages_types_pressures_and_gas_amount(shotname,campaign="OP2.2",time_resolution=None,debug=False):
    gas_types_and_pressures,valve_names_gas_types_and_pressures = get_qsq_gastypes_and_pressures(shotname,campaign=campaign)
    valve_voltages,valve_names_voltages = get_qsq_valve_voltages(shotname,campaign=campaign)
    processor = QSQProcessor(campaign=campaign)
    gas_amounts = {}
    valve_names_gas_amounts = []
    for valve in valve_names_voltages:
        valve = valve[12:-2]
        halfmodule = valve[:3]
        gas_type = gas_types_and_pressures["GasInlet_qsq{}_gas_type".format(halfmodule)]
        pressure = gas_types_and_pressures["GasInlet_qsq{}_P".format(halfmodule)]
        if gas_type in ["N2","Ne"]:
            try:
                calibration_factor = processor.get_calibration_factor(gas=gas_type,valve=valve,pressure=pressure)
                time_resolution = valve_voltages["GasInlet_qsq{}_V".format(valve)][0][11]-valve_voltages["GasInlet_qsq{}_V".format(valve)][0][10]
                gas_amounts["GasInlet_qsq{}_gas_amount".format(valve)] = list(valve_voltages["GasInlet_qsq{}_V".format(valve)])
                gas_amounts["GasInlet_qsq{}_gas_amount".format(valve)][1] = gas_amounts["GasInlet_qsq{}_gas_amount".format(valve)][1]*calibration_factor/120*1000*pressure*time_resolution
                valve_names_gas_amounts.append("GasInlet_qsq{}_gas_amount".format(valve))
            except:
                pass
    if debug:
        plt.figure(figsize=(8,6))
        for key in gas_amounts.keys():
            try:
                plt.plot(gas_amounts[key][0],gas_amounts[key][1],label=key+" "+ gas_types_and_pressures[key[:16]+"gas_type"])
            except:
                logger.warning(key+" not found")
        plt.xlim(-1,11)
        plt.title(shotname)
        plt.legend()
        plt.xlabel("Time [s]")
        plt.ylabel("Gas flow [Atoms/s]")
        plt.grid()
        plt.show()
    return valve_voltages,valve_names_voltages, gas_types_and_pressures,valve_names_gas_types_and_pressures, gas_amounts, valve_names_gas_amounts
def squeeze_qsq_voltages_types_pressures_and_gas_amount(shotname, time_base, timestamps, common_base_method,
              timeout, settings={"campaign":"OP2.2"}):
    
    valve_voltages,valve_names_voltages, gas_types_and_pressures,valve_names_gas_types_and_pressures, gas_amounts, valve_names_gas_amounts = get_qsq_voltages_types_pressures_and_gas_amount(shotname,campaign=settings["campaign"])
    qsq_squeeze = {}
    for name in valve_names_voltages:
        qsq_tb = common_base_method(valve_voltages[name][0], valve_voltages[name][1], time_base)
        qsq_squeeze[name] = qsq_tb[:,0]
    for name in valve_names_gas_types_and_pressures:
        qsq_squeeze[name] = gas_types_and_pressures[name]
    for name in valve_names_gas_amounts:
        qsq_tb = common_base_method(gas_amounts[name][0], gas_amounts[name][1], time_base)
        qsq_squeeze[name] = qsq_tb[:,0]
    return pd.DataFrame(qsq_squeeze, index = timestamps)
def sum_up_voltage_times_pressure_for_gases(row,gas_type,campaign="OP2.2"):
    """_summary_

    Args:
        df (dataframe): your 
    """
    if campaign == "OP2.2":
        halfmodules = ['H10', 'H11', 'H20', 'H21', 'H30', 'H31', 'H40', 'H41', 'H50', 'H51','A21']
        valves = ['H10', 'H11', 'H20', 'H21', 'H30v1', 'H30v2', 'H30v3', 'H30v4', 'H30v5', 'H31', 'H40', 'H41', 'H50v1', 'H50v2', 'H50v3', 'H50v4', 'H50v5', 'H51v1', 'H51v2', 'H51v3', 'H51v4', 'H51v5','A21']
        name = "qsq"
    else:
        halfmodules = ['H10', 'H11', 'H20', 'H21', 'H30', 'H31', 'H40', 'H41', 'H50', 'H51']
        valves = ['H10', 'H11', 'H20', 'H21', 'H30v1', 'H30v2', 'H30v3', 'H30v4', 'H30v5', 'H31', 'H40', 'H41', 'H50v1', 'H50v2', 'H50v3', 'H50v4', 'H50v5', 'H51v1', 'H51v2', 'H51v3', 'H51v4', 'H51v5']
        name = "qsq"
    voltage_times_pressure = 0
    for halfmodule in halfmodules:
        if gas_type == row["GasInlet_"+name+halfmodule+"_gas_type"]:
            for valve in valves:
                if halfmodule in valve:
                    if row["GasInlet_"+name+valve+"_V"] > 1:
                        voltage_times_pressure += row["GasInlet_"+name+valve+"_V"]*row["GasInlet_"+name+halfmodule+"_P"]/1000 # this delivers Bar * Voltage
    return voltage_times_pressure  
def get_gas_amounts_from_pressure_and_voltages(row,valve,time_average=0.2,campaign="OP2.2"):
    """This is applied to an existing juice and evaluated per row. The gas amount per valve is returned

    Args:
        row (_type_): _description_
        campaign (str, optional): _description_. Defaults to "OP2.2".
    """
    processor = QSQProcessor(campaign=campaign)
    halfmodule = valve[:3]
    gas_type = row["GasInlet_qsq{}_gas_type".format(halfmodule)]
    pressure = row["GasInlet_qsq{}_P".format(halfmodule)]
    valve_voltage = row["GasInlet_qsq{}_V".format(valve)]
    if gas_type in ["N2","Ne"]:
        try:
            calibration_factor = processor.get_calibration_factor(gas=gas_type,valve=valve,pressure=pressure)
            return valve_voltage*calibration_factor/120*1000*pressure*time_average
        except:
            return np.nan
def sum_up_gas_amounts(row,gas_type,campaign="OP2.2"):
    """_summary_

    Args:
        df (dataframe): your 
    """
    if campaign == "OP2.2":
        halfmodules = ['H10', 'H11', 'H20', 'H21', 'H30', 'H31', 'H40', 'H41', 'H50', 'H51','A21']
        valves = ['H10', 'H11', 'H20','H30v1', 'H30v2', 'H30v3', 'H30v4', 'H30v5', 'H21',  'H31', 'H40', 'H41', "H50v5",]
        #valves = ['H50v1', 'H50v2', 'H50v3', 'H50v5', 'H51v1', 'H51v2', 'H51v3', 'H51v4', 'H51v5''A21'] took these out, because there are no calibration factors for the single ones and therefore I did not include them in the first run. Need to talk to Anastasios
        name = "qsq"
    else:
        halfmodules = ['H10', 'H11', 'H20', 'H21', 'H30', 'H31', 'H40', 'H41', 'H50', 'H51']
        valves = ['H10', 'H11', 'H20', 'H21', 'H30v1', 'H30v2', 'H30v3', 'H30v4', 'H30v5', 'H31', 'H40', 'H41', 'H50v1', 'H50v2', 'H50v3', 'H50v4', 'H50v5', 'H51v1', 'H51v2', 'H51v3', 'H51v4', 'H51v5']
        name = "qsq"
    gas_amount_total = 0
    for halfmodule in halfmodules:
        if gas_type == row["GasInlet_"+name+halfmodule+"_gas_type"]:
            for valve in valves:
                if halfmodule in valve:
                    if row["GasInlet_"+name+valve+"_gas_amount"] > 1:
                        gas_amount_total += row["GasInlet_"+name+valve+"_gas_amount"]
    return gas_amount_total

# Existing class (directly integrated with calibration methods)
class QSQProcessor:
    # Existing __init__, URLs, and other methods are unchanged
    def __init__(self, campaign="OP2.2"):
        self.campaign = campaign

        # Calibration data for OP1.2, OP2.1, and OP2.2
        self.calib_OP12 = {}      #calibration constant [atoms/ms/mbar]
        self.calib_OP12['He'] = np.array([[50, 250, 500, 1000],
                            [1.2e14, 7.0e14, 1.0e15, 1.2e15]]).T
        self.calib_OP12['N2'] = np.array([[100, 250, 500, 1000],
                            [6.0e14, 1.0e15, 1.0e15, 1.0e15]]).T
        self.calib_OP12['H2'] = np.array([[100, 250, 500, 1000, 2000],
                            [8.0e14, 2.6e15, 3.0e15, 3.6e15, 3.6e15]]).T
        self.calib_OP12['CH4'] = np.array([[50, 100, 250, 500, 1000],
                                [5.5e14, 9.0e14, 9.0e14, 9.0e14, 1.15e15]]).T
        self.calib_OP12['Ne'] = np.array([[100, 250, 500, 1000],
                            [2.0e14, 3.5e14, 5.0e14, 5.0e14]]).T
        self.calib_OP12['Ar'] = np.array([[100, 250, 500],
                            [1.5e14, 3.0e14, 3.0e14]]).T


        self.calib_OP21 = {}      #calibration constant [atoms/ms/mbar]
        self.calib_OP21['He'] = np.array([[90, 230, 1470],
                            [3.1e14, 5.4e14, 1.3e15]]).T
        self.calib_OP21['N2'] = np.array([[110, 270],
                            [6.9e14, 1.2e15]]).T
        self.calib_OP21['H2'] = np.array([[100, 250, 500, 1000, 2000],
                            [8.0e14, 2.6e15, 3.0e15, 3.6e15, 3.6e15]]).T
        self.calib_OP21['CH4'] = np.array([[50, 100, 250, 500, 1000],
                                [5.5e14, 9.0e14, 9.0e14, 9.0e14, 1.15e15]]).T
        self.calib_OP21['Ne'] = np.array([[40, 180],
                            [2.7e14, 4.5e14]]).T
        self.calib_OP21['Ar'] = np.array([[100, 220, 660],
                            [2.1e14, 2.0e14, 4.2e14]]).T




        self.calib_OP22 = {'Ne': {'H10': [],'H11': [],'H20': [],'H21':[],'H30':[],'H31':[],'H40':[],'H41':[],'H50':[],'H51':[]},
                'N2': {'H10': [],'H11': [],'H20': [],'H21':[],'H30':[],'H31':[],'H40':[],'H41':[],'H50':[],'H51':[]}}      #self.calib_OP22ration constant [atoms/ms/mbar]

        self.calib_OP22['Ne']['H10'] = np.array([[67, 100, 200],
                                    [1.17e14, 1.84e14, 2.02e14]]).T
        self.calib_OP22['Ne']['H11'] = np.array([[37, 100, 180],
                                    [1.84e14, 3.51e14, 4.98e14]]).T
        self.calib_OP22['Ne']['H20'] = np.array([[37, 78, 180],
                                    [1.93e14, 2.55e14, 3.93e14]]).T
        self.calib_OP22['Ne']['H21'] = np.array([[50, 100, 180],
                                    [1.14e14, 1.87e14, 3.20e14]]).T
        self.calib_OP22['Ne']['H30v1'] = np.array([[50, 100, 200], # Attention, all the v1 - v5s are just copied. Anastasios did not resovle this!
                                    [4.35e13, 7.76e13, 1.28e14]]).T
        self.calib_OP22['Ne']['H30v2'] = np.array([[50, 100, 200],
                                    [4.35e13, 7.76e13, 1.28e14]]).T
        self.calib_OP22['Ne']['H30v3'] = np.array([[50, 100, 200],
                                    [4.35e13, 7.76e13, 1.28e14]]).T
        self.calib_OP22['Ne']['H30v4'] = np.array([[50, 100, 200],
                                    [4.35e13, 7.76e13, 1.28e14]]).T
        self.calib_OP22['Ne']['H30v5'] = np.array([[50, 100, 200],
                                    [4.35e13, 7.76e13, 1.28e14]]).T
        self.calib_OP22['Ne']['H31'] = np.array([[12, 50, 200],
                                    [4.2e14, 3.21e14, 2.79e14]]).T
        self.calib_OP22['Ne']['H40'] = np.array([[50, 100, 200],
                                    [9.82e13, 1.72e14, 2.97e14]]).T
        self.calib_OP22['Ne']['H41'] = np.array([[50, 78, 180],
                                    [1.21e14, 2.07e14, 3.43e14]]).T
        self.calib_OP22['Ne']['H50v1'] = np.array([[50, 100, 200],
                                    [1.34e14, 2.50e14, 3.94e14]]).T
        self.calib_OP22['Ne']['H50v2'] = np.array([[50, 100, 200],
                                    [1.34e14, 2.50e14, 3.94e14]]).T
        self.calib_OP22['Ne']['H50v3'] = np.array([[50, 100, 200],
                                    [1.34e14, 2.50e14, 3.94e14]]).T
        self.calib_OP22['Ne']['H50v4'] = np.array([[50, 100, 200],
                                    [1.34e14, 2.50e14, 3.94e14]]).T
        self.calib_OP22['Ne']['H50v5'] = np.array([[50, 100, 200],
                                    [1.34e14, 2.50e14, 3.94e14]]).T
        self.calib_OP22['Ne']['H51v1'] = np.array([[100],
                                    [2.41e14]]).T
        self.calib_OP22['Ne']['H51v2'] = np.array([[100],
                                    [2.41e14]]).T
        self.calib_OP22['Ne']['H51v3'] = np.array([[100],
                                    [2.41e14]]).T
        self.calib_OP22['Ne']['H51v4'] = np.array([[100],
                                    [2.41e14]]).T
        self.calib_OP22['Ne']['H51v5'] = np.array([[100],
                                    [2.41e14]]).T
        self.calib_OP22['N2']['H10'] = np.array([[50, 100, 200],
                                    [1.5e14, 2.64e14, 2.02e14]]).T
        self.calib_OP22['N2']['H11'] = np.array([[50, 100],
                                    [1.8e14, 3.23e14]]).T
        self.calib_OP22['N2']['H20'] = np.array([[67, 180],
                                    [3.27e14, 5.1e14,]]).T
        self.calib_OP22['N2']['H21'] = np.array([[78, 100, 180],
                                    [1.89e14, 2.18e14, 4.38e14]]).T
        self.calib_OP22['N2']['H30v1'] = np.array([[67, 100, 200],
                                    [7.64e13, 1.13e14, 1.28e14]]).T
        self.calib_OP22['N2']['H30v2'] = np.array([[67, 100, 200],
                                    [7.64e13, 1.13e14, 1.28e14]]).T
        self.calib_OP22['N2']['H30v3'] = np.array([[67, 100, 200],
                                    [7.64e13, 1.13e14, 1.28e14]]).T
        self.calib_OP22['N2']['H30v4'] = np.array([[67, 100, 200],
                                    [7.64e13, 1.13e14, 1.28e14]]).T
        self.calib_OP22['N2']['H30v5'] = np.array([[67, 100, 200],
                                    [7.64e13, 1.13e14, 1.28e14]]).T
        self.calib_OP22['N2']['H31'] = np.array([[67, 100, 200],
                                    [1.48e14, 2.20e14, 2.79e14]]).T
        self.calib_OP22['N2']['H40'] = np.array([[67, 100, 200],
                                    [1.52e14, 2.50e14, 2.97e14]]).T
        self.calib_OP22['N2']['H41'] = np.array([[37, 67],
                                    [2.31e14,3.61e14]]).T
        self.calib_OP22['N2']['H50v1'] = np.array([[67, 100, 180, 200],
                                    [2.1e14, 3.14e14, 5.00e14, 3.94e14]]).T
        self.calib_OP22['N2']['H50v2'] = np.array([[67, 100, 180, 200],
                                    [2.1e14, 3.14e14, 5.00e14, 3.94e14]]).T
        self.calib_OP22['N2']['H50v3'] = np.array([[67, 100, 180, 200],
                                    [2.1e14, 3.14e14, 5.00e14, 3.94e14]]).T
        self.calib_OP22['N2']['H50v4'] = np.array([[67, 100, 180, 200],
                                    [2.1e14, 3.14e14, 5.00e14, 3.94e14]]).T
        self.calib_OP22['N2']['H50v5'] = np.array([[67, 100, 180, 200],
                                    [2.1e14, 3.14e14, 5.00e14, 3.94e14]]).T
        self.calib_OP22['N2']['H51v1'] = np.array([[67, 100, 180],
                                    [2.09e14, 3.14e14, 5.19e14]]).T
        self.calib_OP22['N2']['H51v2'] = np.array([[67, 100, 180],
                                    [2.09e14, 3.14e14, 5.19e14]]).T
        self.calib_OP22['N2']['H51v3'] = np.array([[67, 100, 180],
                                    [2.09e14, 3.14e14, 5.19e14]]).T
        self.calib_OP22['N2']['H51v4'] = np.array([[67, 100, 180],
                                    [2.09e14, 3.14e14, 5.19e14]]).T
        self.calib_OP22['N2']['H51v5'] = np.array([[67, 100, 180],
                                    [2.09e14, 3.14e14, 5.19e14]]).T
        

    def get_calibration_factor(self, gas, valve=None, pressure=None):
        """
        Get the calibration factor for the given campaign, gas, valve, and pressure.

        Args:
            gas (str): The type of gas (e.g., "Ne", "N2").
            valve (str, optional): The valve identifier (required for OP2.2).
            pressure (float): The pressure value in mbar.

        Returns:
            float: The calibration factor (atoms/mbar/ms).
        """
        if self.campaign == "OP1.2":
            cal_dict = self.calib_OP12
        elif self.campaign == "OP2.1":
            cal_dict = self.calib_OP21
        elif self.campaign == "OP2.2":
            cal_dict = self.calib_OP22
        else:
            raise ValueError(f"Invalid campaign: {self.campaign}")
        # Check if OP2.2 requires a valve-specific calibration
        if self.campaign != 'OP2.2':
            cal_factor = np.interp(pressure,
                                cal_dict[gas][:,0],
                                cal_dict[gas][:,1])
        else:
            cal_factor = np.interp(pressure,
                                cal_dict[gas][valve][:,0],
                                cal_dict[gas][valve][:,1])
        return cal_factor

    def plot_calibration(self, gas, valve=None):
        """
        Plot calibration factors for the given campaign, gas, and valve.

        Args:
            gas (str): The type of gas (e.g., "Ne", "N2").
            valve (str, optional): The valve identifier (required for OP2.2).
        """
        if self.campaign == "OP1.2":
            cal_dict = self.self.calib_OP12
        elif self.campaign == "OP2.1":
            cal_dict = self.self.calib_OP21
        elif self.campaign == "OP2.2":
            cal_dict = self.self.calib_OP22.get(gas, {})
        else:
            raise ValueError(f"Invalid campaign: {self.campaign}")

        plt.figure()
        if self.campaign != "OP2.2":
            plt.plot(
                cal_dict[gas][:, 0],
                cal_dict[gas][:, 1],
                "--o",
                label=f"{self.campaign} {gas}",
            )
        else:
            if valve not in cal_dict:
                raise ValueError(f"Valve {valve} is not available for gas {gas} in OP2.2")
            plt.plot(
                cal_dict[valve][:, 0],
                cal_dict[valve][:, 1],
                "--o",
                label=f"OP2.2 {gas} {valve}",
            )

        plt.xlabel("Pressure [mbar]")
        plt.ylabel("Calibration Factor [atoms/mbar/ms]")
        plt.title(f"Calibration Factors for {self.campaign} - {gas}")
        plt.legend()
        plt.grid()
        plt.show()
