"""
An Ulbricht sphere is used to acquire data for absolute calibrations
This module mimics the wavelength dependent intensity of an Ulbricht sphere and should be used for intensity calibrations
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import utilities as utils

class Ulbricht_sphere():
    def __init__(self, wavelength_array, intensity_array, fit_mode="full", debug=False, resolution=5e-11):
        """
        given the calibrated measurements of intensity against wavelength, a temperature can be estimated and the radiation can be described as a black body

        input:
        wavelength_array -> wavelength in m
        intensity_array -> intensity in a.u.
        fit_mode -> "full": tries to fit the entire array with a Planck-emission spectrum
                    "partial": fits
        user_wavelength -> here you want the datapoints

        """
        guess = [2000, 1e-9]
        if fit_mode == "full":
            popt, pcov = curve_fit(
                utils.planck_law, wavelength_array, intensity_array, guess)
            self.wavelength = np.linspace(wavelength_array[0], wavelength_array[-1], int(
                (wavelength_array[-1] - wavelength_array[0]) / resolution))
            self.intensity = utils.planck_law(self.wavelength, *popt)

        if fit_mode == "partial":
            popt = np.zeros((len(wavelength_array), 2))
            self.wavelength = []
            self.intensity = []
            for w in range(len(wavelength_array) - 1):
                popt[w], pcov = curve_fit(
                    utils.planck_law, wavelength_array[w:w + 2], intensity_array[w: w + 2], guess)
                wavelength = np.linspace(wavelength_array[w], wavelength_array[w + 1], int((
                    wavelength_array[w + 1] - wavelength_array[w]) / resolution))
                intensity = utils.planck_law(np.array(wavelength), *popt[w])
                for i in range(len(wavelength) - 1):
                    self.wavelength.append(wavelength[i])
                    self.intensity.append(intensity[i])
        if debug:
            plt.figure(figsize=(8, 6))
            plt.plot(wavelength_array, intensity_array, "+", label="data")
            if fit_mode == "full":
                plt.plot(np.array(self.wavelength), np.array(
                    self.intensity), label="fit {} points".format(len(self.wavelength)))
            if fit_mode == "partial":
                plt.plot(np.array(self.wavelength), np.array(
                    self.intensity), label="fit")
            plt.legend()
            if fit_mode == "full":
                plt.title("Temperature T = {} K, a = {}".format(*popt))
            plt.xlabel("Wavelength [m]")
            plt.ylabel("Emissivity [W/(m$^3$s)]")
            plt.legend()
            plt.show()
