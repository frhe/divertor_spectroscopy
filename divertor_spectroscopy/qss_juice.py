"""
This class is meant to provide infrastructure to write data into juice!

The idea of juice is, that this will all be read from the archive!
Currently (October 2022) there is only raw data in the archive.
Data will be created in analysis scripts and written to json files. 
These will be read here and squeezed into juice!

Every "get_"-method should have debug option, so you can visually look into what the method is making
Every "squeeze_"-method should follow immediately after the "get_"-method

Currently I am reading the fit paramters from json files saved on my local machine. There are two approaches:
- fitting every channel of a spectrometer
    - this is required for profile analyis
    - this is what mid term should also be uploaded and processed automatically
- fitting only the channel per strikeline with the maximum intensity
    - this is faster and therefore can be used a bit more flexible
    - this will stay for now in my hands as it is more in detail analysis
"""

import numpy as np
import matplotlib.pyplot as plt
import json
import pandas as pd
from scipy.optimize import curve_fit
import codecs
import logging
logger = logging.getLogger(__name__)
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import analysis # this is only planned for the colors and linestyles in the plots

"""
Here will be methods that process the fits of all channels of a discharge
"""

def get_qss_max_intensities(shotname,strikeline="horizontal",central_wavelengths=["364","405"],
                            camids=["IP320_1","IP320_2"],port="AEF51",use_line_for_same_ions=["NII_399.500","NeII_371.046"],average="200",version="_neon1",
                            debug=False,debug_parameters=None,plot_line_ratios = [],plot_channels=False,xlim=None):
    """
    Max intensity of all fitted lines in a discharge is read from a saved json file
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with average intensity of every fitted line ["Intensity [Photons/($\mathrm{m}^2$ sr s)]"]: list of the average instensities
    list of line names
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    
    paths = []
    for camid in camids:
        for central_wavelength in central_wavelengths:
            paths.append("{}qss_analysis/discharges/{}/{}/{}/fit_parameters_camid{}_cw{}_port{}_average{}ms_version{}.json".format(ground_path,campaign,shotname[:8],shotname,camid,central_wavelength,port,average,version))
    channels = np.arange(27)
    if port[:3] == "AEF":
        if strikeline == "horizontal":
            channels = np.arange(15)
        if strikeline == "vertical":
            channels = np.arange(15,27)
    if port[:3] == "AEI":
        strikeline = "horizontal" 
    if strikeline == "no_strikeline":
        channels = np.arange(27)
    emission_lines = []
    qss_max_intensities = {}
    qss_max_channels = {}
    fit_parameters = {}
    for path in paths:
        try:
            with open(path) as f:
                data = json.load(f)
                fit_parameters = data["fit_parameters"]
                time_interval = data["time_s"][1]-data["time_s"][0]
                if data["time_s"][0]<time_interval/4:# my fits are a bit off timing wise - this will have to be corrected in the automated_analysis, but for old fits, we have to correct it here. The solution is pretty much, if the first time stamp is 0, we shift everything by half a timepoint to have the center of the timepoint
                    fit_parameters["time_s"] = np.array(data["time_s"]) + time_interval/2
                else: # timing is already correct
                    fit_parameters["time_s"] = data["time_s"] 
                if type(data["fit_description"]) is list:
                    fit_parameters["fit_description"] = data["fit_description"]
                else:
                    fit_parameters["fit_description"] = list(data["fit_parameters_intensity_in_watts_unit"].keys())
            for key in fit_parameters.keys():
                fit_parameters[key] = np.array(fit_parameters[key])
        except:
            fit_parameters["fit_description"] = []
        for element in fit_parameters["fit_description"]:
            if "Intensity_" in element:
                #p_idx = np.where(element == np.array(fit_parameters["fit_parameters_description"]))[0][0]
                emission_lines.append(element)
                qss_max_intensities[element] = []
                qss_max_channels[element] = []
                qss_max_intensities[element].append(fit_parameters["time_s"][:-1])
                qss_max_channels[element].append(fit_parameters["time_s"][:-1])
                qss_max_intensities[element].append([])
                qss_max_channels[element].append([])
                for i in range(len(fit_parameters["time_s"][:-1])):
                    #try:   
                    channel = np.nanargmax(fit_parameters[element][i][channels])
                    for line in use_line_for_same_ions:
                        if line[:-7] in element:
                            channel = np.nanargmax(fit_parameters["Intensity_"+line][i][channels])
                    qss_max_intensities[element][1].append(fit_parameters[element][i][channels][channel])
                    qss_max_channels[element][1].append(channel+channels[0])
                        
                    #except:
                    #    qss_max_intensities[element][1].append(np.nan)
                    #    qss_max_channels[element][1].append(np.nan)
                qss_max_intensities[element][0] = np.array(qss_max_intensities[element][0])
                qss_max_intensities[element][1] = np.array(qss_max_intensities[element][1])
                qss_max_channels[element][0] = np.array(qss_max_channels[element][0])
                qss_max_channels[element][1] = np.array(qss_max_channels[element][1])
            if "Stark_density_" in element:
                #p_idx = np.where(element == np.array(fit_parameters["fit_parameters_description"]))[0][0]
                emission_lines.append(element)
                qss_max_intensities[element] = []
                qss_max_channels[element] = []
                qss_max_intensities[element].append(fit_parameters["time_s"][:-1])
                qss_max_channels[element].append(fit_parameters["time_s"][:-1])
                qss_max_intensities[element].append([])
                qss_max_channels[element].append([])
                for i in range(len(fit_parameters["time_s"][:-1])):
                    #try:
                    channel = np.nanargmax(fit_parameters["Intensity_"+element[14:]][i][channels])+channels[0]
                    qss_max_intensities[element][1].append(fit_parameters[element][i][channel])
                    qss_max_channels[element][1].append(channel)    
                    #except:
                    #    qss_max_intensities[element][1].append(np.nan)
                    #    qss_max_channels[element][1].append(np.nan)
                qss_max_intensities[element][0] = np.array(qss_max_intensities[element][0])
                qss_max_intensities[element][1] = np.array(qss_max_intensities[element][1])
                qss_max_channels[element][0] = np.array(qss_max_channels[element][0])
                qss_max_channels[element][1] = np.array(qss_max_channels[element][1])
    if debug: 
        if debug_parameters == None:
            debug_parameters = emission_lines
        fig, ax1 = plt.subplots(figsize=(8,6))
        if len(plot_line_ratios) > 0:
            ax2 = ax1.twinx()
            #ax2.grid(True, which='both', axis='both')  # Enable grid for both x and y on ax1
        if plot_channels:
            ax3 = ax1.twinx()
            ax3.spines['right'].set_position(('outward', 60))
        
        for parameter in debug_parameters:
            x = qss_max_intensities[parameter][0]
            y = qss_max_intensities[parameter][1]
            if "Intensity" in parameter:
                element, ion = utils.split_tag(parameter[10:])            
                ax1.plot(x,y,label=parameter[10:],c=analysis.element_colors[element], ls=analysis.ion_styles[ion],lw=3)
            if "Stark_density" in parameter:
                ax1.plot(x,y,label=parameter,c = colors[debug_parameters.index(parameter) % len(colors)])
        if plot_channels:
            for parameter in debug_parameters:
                x = qss_max_channels[parameter][0]
                y = qss_max_channels[parameter][1]
                if "Intensity" in parameter:
                    ax3.plot(x,y,c = colors[debug_parameters.index(parameter) % len(colors)],ls="-.",label="c")
                if "Stark_density_" in parameter:
                    ax3.plot(x,y,c = colors[debug_parameters.index(parameter) % len(colors)],ls="-.",label="c")
            ax2.legend(loc="upper right")
        for line_ratio in plot_line_ratios:
            x = qss_max_intensities[line_ratio[0]][0]
            y = qss_max_intensities[line_ratio[0]][1]/qss_max_intensities[line_ratio[1]][1]
            ax2.plot(x,y,label="{}/{}".format(line_ratio[0][-7:],line_ratio[1][-7:]),ls="--")
        plt.title("{} {} max channel {}".format(shotname,port,strikeline))
        ax1.legend(loc="upper left")
        
        ax1.set_xlabel("Time [s]")
        #ax1.set_ylabel("Intensity "+data["fit_parameters_unit"][parameter])
        ax1.set_ylabel("Intensity [Photons/($\mathrm{m}^2$ sr s)]")
        if plot_channels:
            ax3.set_ylabel("Channel")
            ax3.legend("upper right")
        if len(plot_line_ratios)>0:
            ax2.set_ylabel("Line ratio")
            ax2.legend(loc="upper center")
            ax2.set_ylim(0.2,0.5) #Ne # todo: next level hardcoding
            #ax2.set_ylim(0.1,0.2) # N
        
        ax1.grid(True, which='both', axis='both')  # Enable grid for the x-axis on ax2
        if xlim is not None:
            ax1.set_xlim(*xlim)
        #ax1.grid()
        save_path = "{}qss_analysis/discharges/{}/{}/{}/".format(ground_path,campaign,shotname[:8],shotname)
        utils.create_folder(save_path+"Plots")
        utils.create_folder(save_path+"Plots/"+port)
        plt.savefig(save_path+"Plots/"+port+"/"+"QSS_max_intensities_{}".format(shotname)+".png",format="png")
        plt.show()
    qss_max_intensities_correct_keys = {}
    for i,emission_line in enumerate(emission_lines):
        new_name = port+"_"+strikeline+"_v"+version+"_"+emission_line
        emission_lines[i] = new_name
        qss_max_intensities_correct_keys[new_name] = qss_max_intensities[emission_line]
    return qss_max_intensities_correct_keys, emission_lines

def squeeze_qss_max_intensities(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_max_intensities, emission_lines = get_qss_max_intensities(shotname,strikeline=settings["strikeline"],central_wavelengths=settings["central_wavelengths"],camids=settings["camids"],port=settings["port"],average=settings["average"],version=settings["version"]) # todo: settings need to be corrected!
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_max_intensities[name][0], qss_max_intensities[name][1], time_base)
        qss_squeeze['qss_max_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)

def get_qss_average_intensities(shotname,central_wavelengths=["364","405"],
                            camids=["IP320_1","IP320_2"],port="AEF51",average="200",version="1",
                            debug=False,debug_parameters=None):
    """
    Max intensity of all fitted lines in a discharge is read from a saved json file
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with average intensity of every fitted line ["Intensity [Photons/($\mathrm{m}^2$ sr s)]"]: list of the average instensities
    list of line names
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    paths = []
    for camid in camids:
        for central_wavelength in central_wavelengths:
            paths.append("{}qss_analysis/discharges/{}/{}/{}/fit_parameters_camid{}_cw{}_port{}_average{}ms_version{}.json".format(ground_path,campaign,shotname[:8],shotname,camid,central_wavelength,port,average,version))
    emission_lines = []
    qss_average_intensities = {}
    fit_parameters = {}
    for path in paths:
        try:
            with open(path) as f:
                data = json.load(f)
                fit_parameters = data["fit_parameters"]
                time_interval = data["time_s"][1]-data["time_s"][0]
                if data["time_s"][0]<time_interval/4:# my fits are a bit off timing wise - this will have to be corrected in the automated_analysis, but for old fits, we have to correct it here. The solution is pretty much, if the first time stamp is 0, we shift everything by half a timepoint to have the center of the timepoint
                    fit_parameters["time_s"] = np.array(data["time_s"]) + time_interval/2
                else: # timing is already correct
                    fit_parameters["time_s"] = data["time_s"] 
                if type(data["fit_description"]) is list:
                    fit_parameters["fit_description"] = data["fit_description"]
                else:
                    fit_parameters["fit_description"] = list(data["fit_parameters_intensity_in_watts_unit"].keys())
            for key in fit_parameters.keys():
                fit_parameters[key] = np.array(fit_parameters[key])
        except:
            fit_parameters["fit_description"] = []
        for element in fit_parameters["fit_description"]:
            if "Intensity_" in element:
                #p_idx = np.where(element == np.array(fit_parameters["fit_parameters_description"]))[0][0]
                emission_lines.append(element)
                qss_average_intensities[element] = []
                qss_average_intensities[element].append(fit_parameters["time_s"][:-1])
                qss_average_intensities[element].append([])
                for i in range(len(fit_parameters["time_s"][:-1])):
                    try:
                        qss_average_intensities[element][1].append(np.nanmean(fit_parameters[element][i]))
                    except:
                        qss_average_intensities[element][1].append(np.nan)
                qss_average_intensities[element][0] = np.array(qss_average_intensities[element][0])
                qss_average_intensities[element][1] = np.array(qss_average_intensities[element][1])
    if debug: 
        if debug_parameters == None:
            debug_parameters = emission_lines
        fig, ax1 = plt.subplots(figsize=(8,6))
        ax2 = ax1.twinx()
        
        for parameter in debug_parameters:
            x = qss_average_intensities[parameter][0]
            y = qss_average_intensities[parameter][1]
            if "Intensity" in parameter:
                element, ion = utils.split_tag(parameter[10:])            
                ax1.plot(x,y,label=parameter[10:],c=analysis.element_colors[element], ls=analysis.ion_styles[ion],lw=3)
            if "density" in parameter:
                ax2.plot(x,y,label=parameter,c = colors[debug_parameters.index(parameter) % len(colors)])
        plt.title("{} {} channel average".format(shotname,port))
        ax1.legend(loc="upper left")
        #ax2.legend(loc="upper right")
        ax1.set_xlabel("Time [s]")
        #ax1.set_ylabel("Intensity "+data["fit_parameters_unit"][parameter])
        ax1.set_ylabel("Intensity [Photons/($\mathrm{m}^2$ sr s)]")
        #ax2.set_ylabel("Density")
        ax1.grid(True, which='both', axis='both')
        save_path = "{}qss_analysis/discharges/{}/{}/{}/".format(ground_path,campaign,shotname[:8],shotname)
        utils.create_folder(save_path+"Plots")
        utils.create_folder(save_path+"Plots/"+port)
        plt.savefig(save_path+"Plots/"+port+"/"+"QSS_average_intensities_{}".format(shotname)+".png",format="png")
        plt.show()
        plt.show()
    qss_average_intensities_correct_keys = {}
    for i,emission_line in enumerate(emission_lines):
        new_name = port+"_v"+version+"_"+emission_line
        emission_lines[i] = new_name
        qss_average_intensities_correct_keys[new_name] = qss_average_intensities[emission_line]
    return qss_average_intensities_correct_keys, emission_lines

def squeeze_qss_average_intensities(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_average_intensities, emission_lines = get_qss_average_intensities(shotname,central_wavelengths=settings["central_wavelengths"],camids=settings["camids"],port=settings["port"],average=settings["average"],version=settings["version"]) # todo: settings need to be corrected!
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_average_intensities[name][0], qss_average_intensities[name][1], time_base)
        qss_squeeze['qss_average_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)

def get_qss_radiation_front(shotname,central_wavelengths=["364","405","462"],
                            camids=["IP320_3"],port="AEI51",average="100",version="1",
                            debug=False,xlim=None,debug_parameters=None,debug_profiles=False,normalize=True,save_profiles=False):

    """
    The parameters of the radiation zone are fitted reading a profile from already existing fits
    Returned are the position (channel), intensity and width of these radiation fronts
    
    """
    print(shotname)
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    paths = []
    for camid in camids:
        for central_wavelength in central_wavelengths:
            paths.append("{}qss_analysis/discharges/{}/{}/{}/fit_parameters_camid{}_cw{}_port{}_average{}ms_version{}.json".format(ground_path,campaign,shotname[:8],shotname,camid,central_wavelength,port,average,version))
    emission_lines = []
    qss_positions_radiation_front = {}
    qss_intensities_radiation_front = {}
    qss_widths_radiation_front = {}
    qss_decays_exp = {}
    qss_intensities_exp = {}
    fit_parameters_all = {}
    # here we load everything
    file_counter = 0
    for path in paths:
        try:
            with open(path) as f:
                data = json.load(f)
                fit_parameters = data["fit_parameters"]
                fit_parameters["time_s"] = data["time_s"]
                fit_parameters["fit_description"] = data["fit_description"]
            for key in fit_parameters.keys():
                fit_parameters[key] = np.array(fit_parameters[key])
            file_counter += 1
            print(path)
            for element in fit_parameters["fit_description"]:
                if "Intensity_" in element:
                    emission_lines.append(element)
                    fit_parameters_all[element]=fit_parameters[element]
                    qss_positions_radiation_front[element]=[]
                    qss_positions_radiation_front[element].append(fit_parameters["time_s"])
                    qss_positions_radiation_front[element].append([])
                    qss_intensities_radiation_front[element] = []
                    qss_intensities_radiation_front[element].append(fit_parameters["time_s"])
                    qss_intensities_radiation_front[element].append([])
                    qss_widths_radiation_front[element] = []
                    qss_widths_radiation_front[element].append(fit_parameters["time_s"])
                    qss_widths_radiation_front[element].append([])
                    qss_decays_exp[element] = []
                    qss_decays_exp[element].append(fit_parameters["time_s"])
                    qss_decays_exp[element].append([])
                    qss_intensities_exp[element] = []
                    qss_intensities_exp[element].append(fit_parameters["time_s"])
                    qss_intensities_exp[element].append([])
        except:
            pass
            #logger.warning("Does your file exist? "+path)
    if file_counter == 0:
        print("No files found for {}".format(shotname))
    if xlim is None:
        xlim = [fit_parameters["time_s"][0],fit_parameters["time_s"][-1]]
    save_paths = []
    if save_profiles:
        save_path = "{}qss_analysis/discharges/{}/{}/{}/".format(ground_path,campaign,shotname[:8],shotname)
        utils.create_folder(save_path+"Plots")
        utils.create_folder(save_path+"Plots/"+port)
    for i,time in enumerate(fit_parameters["time_s"]):
        if debug_profiles and xlim[0] <= time and xlim[1]>=time:
            plt.figure(figsize=(8,6))
            title = "{} Profile {}, Time {:.3f}".format(shotname,port,float(average)/1000*i)
            plt.title(title)
        for element in emission_lines:
            #qss_positions_radiation_front[element][1].append(np.nanargmax(fit_parameters[camid][i,:,len(emission_lines)-1]))
            #try:
            ydata = np.array(fit_parameters_all[element][i])
            xdata = np.arange(27)
            valid = np.isfinite(ydata)
            guess = [np.min(ydata[valid]),xdata[valid][np.argmax(ydata[valid][10:])+10],ydata[valid][0],np.sqrt(2*np.pi)*2.5*ydata[valid][np.argmax(ydata[valid][10:])+10],.3,2.5]
            bounds = ([0.5*np.min(ydata[valid]),3,0.5*ydata[valid][0],0,0,1],[np.min(ydata[valid]),27,ydata[valid][0],np.sqrt(2*np.pi)*10*np.max(ydata[valid]),10,10])
            fit_parameters_radiation_front,pcov_radiation_front = curve_fit(f=radiation_front,xdata=xdata[valid],ydata=ydata[valid],p0=guess,bounds=bounds)
            if debug_profiles and xlim[0] <= time and xlim[1] >= time:
                if element in debug_parameters:
                    if normalize:
                        plt.plot(xdata[valid],ydata[valid]/np.nanmax(ydata[valid]),"+-",label=element[10:],c=colors[debug_parameters.index(element)])
                        plt.plot(xdata[valid],radiation_front(xdata[valid],*fit_parameters_radiation_front)/np.nanmax(ydata[valid]),c=colors[debug_parameters.index(element)],ls="--")
                        plt.plot(xdata[valid],utils.gauss(xdata[valid],*[0,fit_parameters_radiation_front[1],fit_parameters_radiation_front[3],fit_parameters_radiation_front[5]])/np.nanmax(ydata[valid]),c=colors[debug_parameters.index(element)],ls="--",alpha=0.5)
                        plt.plot(xdata[valid],fit_parameters_radiation_front[2]/np.exp(xdata[valid]*fit_parameters_radiation_front[4])/np.nanmax(ydata[valid]),c=colors[debug_parameters.index(element)],ls="--",alpha=0.5)
                    else:    
                        plt.plot(xdata[valid],ydata[valid],"+-",label=element,c=colors[debug_parameters.index(element)])
                        plt.plot(xdata[valid],radiation_front(xdata[valid],*fit_parameters_radiation_front),c=colors[debug_parameters.index(element)],ls="--")
            intensity_gauss = fit_parameters_radiation_front[3]
            intensity_exp = fit_parameters_radiation_front[2]/fit_parameters_radiation_front[4]*(1-np.exp(-27*fit_parameters_radiation_front[4]))
            #if intensity_gauss > intensity_exp:
            qss_positions_radiation_front[element][1].append(fit_parameters_radiation_front[1])
            qss_widths_radiation_front[element][1].append(fit_parameters_radiation_front[5])
            #else:
            #    qss_positions_radiation_front[element][1].append(0)
            #    qss_widths_radiation_front[element][1].append(0)
            qss_intensities_radiation_front[element][1].append(intensity_gauss)
            qss_intensities_exp[element][1].append(intensity_exp)
            qss_decays_exp[element][1].append(fit_parameters_radiation_front[4])
                #except:
                #    qss_positions_radiation_front[element][1].append(np.nan)
                #    qss_intensities_radiation_front[element][1].append(np.nan)
            #qss_positions_radiation_front[element][0] = np.array(qss_positions_radiation_front[element][0])
            #qss_positions_radiation_front[element][1] = np.array(qss_positions_radiation_front[element][1])
            #qss_intensities_radiation_front[element][0] = np.array(qss_intensities_radiation_front[element][0])
            #qss_intensities_radiation_front[element][1] = np.array(qss_intensities_radiation_front[element][1])
        if debug_profiles and xlim[0] <= time and xlim[1]>=time:
            plt.legend()
            plt.xlabel("Channel")
            if normalize:
                plt.ylabel("Intensity [normalized]")
            else:
                plt.ylabel(r"Intensity [Photons/($\mathrm{m}^2$ nm sr s)]")
            #plt.ylim(0,800)
            if save_profiles:
                plt.savefig(save_path+"Plots/"+port+"/"+title+".png",format="png")
                save_paths.append(save_path+"Plots/"+port+"/"+title+".png")
            plt.show()    
    if debug: 
        ylabels = ["Position [Channel]","Intensity Gauss","Width [Channels]","Intensity Exp.","Exp. decay [Channels]"]
        title = "Emission fronts"
        for measurement_idx,measurement in enumerate([qss_positions_radiation_front,qss_intensities_radiation_front,qss_widths_radiation_front,qss_intensities_exp,qss_decays_exp]):
            if debug_parameters == None:
                debug_parameters = emission_lines
            fig, ax1 = plt.subplots(figsize=(8,6))
            #ax2 = ax1.twinx()
            for parameter in debug_parameters:
                x = measurement[parameter][0]
                y = measurement[parameter][1]
                element, ion = utils.split_tag(parameter[10:])            
                c=analysis.element_colors[element] 
                ls=analysis.ion_styles[ion]
                lw=3
                ax1.plot(x,y,label=parameter[10:],c=c,ls=ls,lw=lw)
            plot_arrows = True
            if plot_arrows:
                if "Position" in ylabels[measurement_idx]:
                    plt.arrow(0.8, 0.2, 0, -0.1, head_width=0.05, head_length=0.05, fc='red', ec='red', width=0.02,
                        transform=plt.gca().transAxes)
                    plt.text(0.63, 0.22, 'Target', fontsize=12, color='red',
                        transform=plt.gca().transAxes)
                    # Todo: this arror is supposed to be at in the upper center of the plot pointing up
                    plt.arrow(0.55, 0.8, 0., 0.1, head_width=0.05, head_length=0.05, fc='blue', ec='blue', width=0.02,
                            transform=plt.gca().transAxes)
                    plt.text(0.57, 0.85, 'Confined Plasma', fontsize=12, color='blue',
                        transform=plt.gca().transAxes)
                    plt.ylim(0,26)
            plt.title("{} {} {}".format(title,shotname,port))
            ax1.legend(loc="upper left")
            #ax2.legend(loc="upper right")
            ax1.set_xlabel("Time [s]")
            ax1.set_ylabel(ylabels[measurement_idx])
            ax1.set_xlim(xlim)
            plt.grid()
            if save_profiles:
                plt.savefig(save_path+"Plots/"+port+"/"+title+"_"+ylabels[measurement_idx]+".png",format="png")
            
            plt.show()
    if save_profiles:
        save_file = {"qss_positions_radiation_front":qss_positions_radiation_front,"qss_intensities_radiation_front":qss_intensities_radiation_front,"qss_widths_radiation_front":qss_widths_radiation_front,"emission_lines":emission_lines}
        json.dump(save_file, codecs.open(save_path+"Plots/"+port+"/"+"{} {} {}".format(title,shotname,port)+".json", 'w', encoding='utf-8'),
                cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)     
    #print(qss_positions_radiation_front)
    return qss_positions_radiation_front, qss_intensities_radiation_front,qss_widths_radiation_front,qss_intensities_exp,qss_decays_exp, emission_lines

def radiation_front(xdata,offset,peak_position,intensity0,intensity,width0,width):
    """
    Defining the radiation front seems to be not trivial: there are the following problems:
    - channel one is often brightest until very deep detachment
    - double peaks need to be implemented
    Idea is to look if there is a second peak (not on the first two channels) and then allow two peak positions
    """
    # one peak is always fixed at channel 0
    front = np.zeros(len(xdata)) + offset
    front += 1/np.exp(xdata*width0)*intensity0
    front += utils.gauss(xdata,0,peak_position,intensity,width)
    return front

def squeeze_qss_radiation_front(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_positions_radiation_front, qss_intensities_radiation_front, qss_widths_radiation_front,qss_intensities_exp,qss_decays_exp,emission_lines = get_qss_radiation_front(shotname,central_wavelengths=settings["central_wavelengths"],
                            camids=settings["camids"],port=settings["port"],average=settings["average"],version=settings["version"])
    qss_squeeze = {}
    print(shotname)
    for name in emission_lines:
        qss_tb = common_base_method(np.array(qss_positions_radiation_front[name][0]), np.array(qss_positions_radiation_front[name][1]), time_base)
        qss_squeeze['qss_positions_radiation_front_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_intensities_radiation_front[name][0]), np.array(qss_intensities_radiation_front[name][1]), time_base)
        qss_squeeze['qss_intensities_radiation_front_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_widths_radiation_front[name][0]), np.array(qss_widths_radiation_front[name][1]), time_base)
        qss_squeeze['qss_widths_radiation_front_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_intensities_exp[name][0]), np.array(qss_intensities_exp[name][1]), time_base)
        qss_squeeze['qss_intensities_exp_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_decays_exp[name][0]), np.array(qss_decays_exp[name][1]), time_base)
        qss_squeeze['qss_decays_exp_{name}'.format(name=name)]=qss_tb[:,0]
        
    return pd.DataFrame(qss_squeeze, index = timestamps)

def get_qss_strikeline(shotname,strikeline="horizontal",central_wavelengths=["364","405","462"],
                            camids=["IP320_3"],average="100",version="1",port="AEF51",
                            debug=False,xlim=None,debug_parameters=None,debug_profiles=False,
                            normalize=True,save_profiles=False):

    """
    The parameters of the radiation at the strikelines is fitted reading a profile from already existing fits
    Returned are the position (channel), intensity and width of these strikelines
    
    This method will have to know which strikeline it has to fit - so for one standard config discharge you need two function evaluations
    This decision is made because it will be handled quite similar to the get_max_intensities method!
    
    """
    # this should maybe be solved by the lightpath database
    
    # todo: this should be handled by some function to make it less ugly
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    if campaign == "OP2.1":
        shifts = {"IP320_1":0,"IP320_2":-0.5,"IP160_1":0}
    if campaign == "OP2.2":
        shifts = {"IP320_1":0,"IP320_6":-0.5,"IP160_1":0}
    paths = [] # getting all possible combinations of paths, most of them will not work
    for camid in camids:
        for central_wavelength in central_wavelengths:
            paths.append("{}qss_analysis/discharges/{}/{}/{}/fit_parameters_camid{}_cw{}_port{}_average{}ms_version{}.json".format(ground_path,campaign,shotname[:8],shotname,camid,central_wavelength,port,average,version))
    emission_lines = []
    qss_positions_strikeline = {}
    qss_intensities_strikeline = {}
    qss_widths_strikeline = {}
    fit_parameters_all = {}
    camids = {}
    # here we load everything
    for path in paths:
        try:
            with open(path) as f:
                data = json.load(f)
                fit_parameters = data["fit_parameters"]
                fit_parameters["time_s"] = data["time_s"]
                fit_parameters["fit_description"] = data["fit_description"]
                number_channels = len(fit_parameters[fit_parameters["fit_description"][0]][0])
            for key in fit_parameters.keys():
                fit_parameters[key] = np.array(fit_parameters[key])

            for element in fit_parameters["fit_description"]:
                if "Intensity_" in element:
                    camids[element] = data["camid"]
                    emission_lines.append(element)
                    fit_parameters_all[element]=fit_parameters[element]
                    qss_positions_strikeline[element]=[]
                    qss_positions_strikeline[element].append(fit_parameters["time_s"])
                    qss_positions_strikeline[element].append([])
                    qss_intensities_strikeline[element] = []
                    qss_intensities_strikeline[element].append(fit_parameters["time_s"])
                    qss_intensities_strikeline[element].append([])
                    qss_widths_strikeline[element] = []
                    qss_widths_strikeline[element].append(fit_parameters["time_s"])
                    qss_widths_strikeline[element].append([])
        except:
            pass
    if xlim is None: 
        xlim = [fit_parameters["time_s"][0],fit_parameters["time_s"][-1]]
    save_paths = [] # this is only for displaying and debugging
    if save_profiles:
        save_path = "{}qss_analysis/discharges/{}/{}/{}/".format(ground_path,campaign,shotname[:8],shotname)
        utils.create_folder(save_path+"Plots")
        utils.create_folder(save_path+"Plots/"+port)
        
    for i,time in enumerate(fit_parameters["time_s"]):
        if debug_profiles and xlim[0] <= time and xlim[1]>=time:
            plt.figure(figsize=(8,6))
            title = "{} Profile AEF, Time {:.1f}".format(shotname,float(average)/1000*i)
            plt.title(title)
        for element in emission_lines:
            #qss_positions_strikeline[element][1].append(np.nanargmax(fit_parameters[camid][i,:,len(emission_lines)-1]))
            #try:    if port[:3] == "AEF":
            # we want to look only on a certain target. But that channel slice might be different dependent on port and camera
            if strikeline == "horizontal":
                channels = np.arange(17)
            if strikeline == "vertical":
                channels = np.arange(17,27)
            ydata = np.array(fit_parameters_all[element][i,channels])
            xdata = channels-shifts[camids[element]]
            valid = np.isfinite(ydata)
            guess_offset = np.min(ydata[valid])
            guess_position = xdata[np.nanargmax(ydata)]
            guess_width = 2.5
            guess_intensity = np.sqrt(2*np.pi)*guess_width*ydata[int(utils.val2idx(xdata,guess_position))]
            guess = [guess_offset,guess_position,guess_intensity,guess_width]
            bounds = ([0,np.nanmin(xdata),0,0.1],[2*guess_offset,np.nanmax(xdata),10*guess_intensity,10])
            try:
                fit_parameters_strikeline,pcov_strikeline = curve_fit(f=utils.gauss,xdata=xdata[valid],ydata=ydata[valid],p0=guess,bounds=bounds)
            except:
                fit_parameters_strikeline = np.array(guess)*np.nan
            if debug_profiles and xlim[0] <= time and xlim[1] >= time:
                if element in debug_parameters:
                    if normalize:
                        plt.plot(xdata[valid],ydata[valid]/np.nanmax(ydata[valid]),"+-",label=element[10:],c=colors[debug_parameters.index(element)])
                        plt.plot(xdata[valid],utils.gauss(xdata[valid],*fit_parameters_strikeline)/np.nanmax(ydata[valid]),c=colors[debug_parameters.index(element)],ls="--")
                    else:    
                        plt.plot(xdata[valid],ydata[valid],"+-",label=element,c=colors[debug_parameters.index(element)])
                        plt.plot(xdata[valid],utils.gauss(xdata[valid],*fit_parameters_strikeline),c=colors[debug_parameters.index(element)],ls="--")
                    broken_colors = {"IP320_1":"red","IP320_2":"orange","IP160_1":"brown","IP320_6":"orange"}
                    for channel,y in enumerate(ydata):
                        if np.isnan(y):
                            plt.axvline(xdata[channel],c=broken_colors[camids[element]],ls="--")
                            
            position = fit_parameters_strikeline[1]
            intensity = fit_parameters_strikeline[2]
            width = fit_parameters_strikeline[3]
            qss_positions_strikeline[element][1].append(position)
            qss_widths_strikeline[element][1].append(width)
            qss_intensities_strikeline[element][1].append(intensity)
        if debug_profiles and xlim[0] <= time and xlim[1]>=time:
            plt.legend()
            plt.xlabel("Channel")
            if normalize:
                plt.ylabel("Intensity [normalized]")
            else:
                plt.ylabel(r"Intensity [Photons/($\mathrm{m}^2$ nm sr s)]")
            #plt.ylim(0,800)
            if save_profiles:
                plt.savefig(save_path+"Plots/"+port+"/"+title+".png",format="png")
                save_paths.append(save_path+"Plots/"+port+"/"+title+".png")
            plt.grid()
            plt.show()    
    if debug: 
        title = "Emission fronts"
        ylabels = ["Position [channel]","Intensity","Width"]
        measurements = [qss_positions_strikeline,qss_intensities_strikeline,qss_widths_strikeline]
        for i in range(3):
            if debug_parameters == None:
                debug_parameters = emission_lines
            fig, ax1 = plt.subplots(figsize=(8,6))
            #ax2 = ax1.twinx()
            for parameter in debug_parameters:
                x = measurements[i][parameter][0]
                y = measurements[i][parameter][1]
                ax1.plot(x,y,label=parameter[10:-8],c = colors[debug_parameters.index(parameter) % len(colors)])
            plot_arrows = True
            if plot_arrows:
                if "Position" in ylabels[i]:
                    ax1.set_ylim(0,27)
                    plt.arrow(0.5, 0.45, 0., -0.1, head_width=0.05, head_length=0.05, fc='red', ec='red', width=0.02,
                        transform=plt.gca().transAxes)
                    plt.text(0.4, 0.47, 'Horizontal Target', fontsize=12, color='red',
                        transform=plt.gca().transAxes)
                    plt.arrow(0.5, 0.55, 0., 0.1, head_width=0.05, head_length=0.05, fc='blue', ec='blue', width=0.02,
                            transform=plt.gca().transAxes)
                    plt.text(0.4, 0.51, 'Vertical Target', fontsize=12, color='blue',
                            transform=plt.gca().transAxes)
            plt.title("Emission Fronts {} {}".format(shotname,"AEF"))
            ax1.legend(loc="upper left")
            #ax2.legend(loc="upper right")
            ax1.set_xlabel("Time [s]")
            ax1.set_ylabel(ylabels[i])
            if i == 0:
                if port == "AEF51":
                    plt.ylim(8,27)
                if port == "AEF30":
                    plt.ylim(0,26)
            ax1.set_xlim(xlim)
            plt.grid()
            if save_profiles:
                plt.savefig(save_path+"Plots/"+port+"/"+"{} {} {} {}".format(title,ylabels[i],shotname,port)+".png",format="png")
            plt.show()    
    if save_profiles:
        save_file = {
            "qss_positions_strikeline":qss_positions_strikeline,
            "qss_intensities_strikeline":qss_intensities_strikeline,
            "qss_widths_strikeline":qss_widths_strikeline,
            }
        json.dump(save_file, codecs.open(save_path+"Plots/"+ports[camid]+"/"+"{} {} {}".format(title,shotname,port)+".json", 'w', encoding='utf-8'),
                cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)     
    return qss_positions_strikeline, qss_intensities_strikeline,qss_widths_strikeline, emission_lines

def squeeze_qss_strikeline(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_horizontal_positions_strikeline, qss_horizontal_intensities_strikeline, qss_horizontal_widths_strikeline,emission_lines = get_qss_strikeline(shotname,central_wavelengths=settings["central_wavelengths"],
                            camids=settings["camids"],strikeline=settings["strikeline"],average=settings["average"],version=settings["version"])
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(np.array(qss_horizontal_positions_strikeline[name][0]), np.array(qss_horizontal_positions_strikeline[name][1]), time_base)
        qss_squeeze['qss_positions_strikeline_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_horizontal_intensities_strikeline[name][0]), np.array(qss_horizontal_intensities_strikeline[name][1]), time_base)
        qss_squeeze['qss_intensities_strikeline_{name}'.format(name=name)]=qss_tb[:,0]
        qss_tb = common_base_method(np.array(qss_horizontal_widths_strikeline[name][0]), np.array(qss_horizontal_widths_strikeline[name][1]), time_base)
        qss_squeeze['qss_widths_strikeline_{name}'.format(name=name)]=qss_tb[:,0]
        
    return pd.DataFrame(qss_squeeze, index = timestamps)
"""
Here will be methods that process the fits of only the maximum channel
"""
def get_qss_max_intensities_brightest_channel(shotname,camid="IP320_6", strongest_lines=["410.174"], 
                                       strikeline="horizontal", analysis_step="0.1", 
                                       port="AEF51",debug=False,version="1",debug_parameters=None):
    """
    This function reads fit parameters of every channel with maximum intensity of a specific spectral line.
    It is the equivalent to get data produced by analysis.fit_discharge_brightest_channel into Juice
    Args:
        shotname (str): The name of the shot, e.g., "20181016.023".
        strongest_lines (list of str, optional): The fits are saved for the channel where this line is strongest. Defaults to ["410.174"].
        strikeline (str, optional): The type of strikeline. Defaults to "horizontal".
        analysis_step (str, optional): The analysis step. Defaults to "0.1".
        port (str, optional): The port identifier. Defaults to "AEF51".
    
    Returns:
        tuple: A tuple containing:
            - dict: dictionary with the fit values for every fit_parameter
            - list: List of names of the fitted parameters
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    qss_max_intensities_strikeline = {}
    fit_description = []
    
    # Iterate over each line that 
    for strongest_line in strongest_lines:
        path = "{}qss_analysis/discharges/{}/{}/{}/Fit_of_channels_max_intensity_intensities_{}_{}_{}ms{}.json".format(ground_path,campaign,shotname[:8],shotname,strongest_line,port,analysis_step,version)
        
        # Open the JSON file and load data
        with open(path) as f:
            data = json.load(f)
            fit_parameters = data[strikeline]["fit_parameters"]
            time = data[strikeline]["time"]
            fit_description.extend(data[strikeline]["fit_description"])
        # Iterate over each fit parameter
        for parameter in data[strikeline]["fit_description"]:
            qss_max_intensities_strikeline[parameter] = []
            qss_max_intensities_strikeline[parameter].append(time)
            qss_max_intensities_strikeline[parameter].append(10**np.array(fit_parameters[parameter]))
            qss_max_intensities_strikeline[parameter] = np.array(qss_max_intensities_strikeline[parameter])
    if debug:
        if debug_parameters == None:
            debug_parameters = fit_description
        fig, ax1 = plt.subplots(figsize=(8,6))
        ax2 = ax1.twinx()
        for parameter in debug_parameters:
            x = qss_max_intensities_strikeline[parameter][0]
            y = qss_max_intensities_strikeline[parameter][1]
            if "Intensity" in parameter:
                ax1.plot(x,y,label=parameter,c=colors[debug_parameters.index(parameter)])
            if "density" in parameter:
                ax2.plot(x,y,label=parameter,c=colors[debug_parameters.index(parameter)])
        plt.title("{} {} {}".format(shotname,port,strikeline))
        ax1.legend(loc="upper left")
        ax2.legend(loc="upper right")
        ax1.set_xlabel("Time [s]")
        ax1.set_ylabel("Intensity")
        ax2.set_ylabel("Density")
        plt.show()
    return qss_max_intensities_strikeline, fit_description


def squeeze_qss_max_intensities_brightest_line(shotname, time_base, timestamps, common_base_method, timeout, settings=None):
    """
    Squeeze the maximum intensities of QSS data for each fit parameter onto a common time base.
    
    Args:
        shotname (str): The name of the shot.
        time_base (array_like): The common time base to which fit_parameters will be squeezed.
        timestamps (array_like): Timestamps corresponding to QSS data.
        common_base_method (function): A function to squeeze QSS data onto a common time base.
        timeout (float): Timeout for squeezing operation.
        settings (dict, optional): Additional settings. Defaults to None.
    
    Returns:
        pandas.DataFrame: DataFrame containing squeezed QSS maximum intensities for each emission line.
    """
    # Retrieve QSS maximum intensities for each emission line
    qss_max_intensities, emission_lines = get_qss_max_intensities_brightest_channel(shotname, port=settings["port"], strikeline=settings["strikeline"], strongest_lines=settings["strongest_lines"],version=settings["version"],analysis_step=settings["analysis_step"])
    qss_squeeze = {}
    
    # Iterate over each emission line
    for name in emission_lines:
        qss_tb = common_base_method(qss_max_intensities[name][0], qss_max_intensities[name][1], time_base)
        qss_squeeze['qss_brightest_channel_strikeline_{name}'.format(name=name)] = qss_tb[:,0]
    
    return pd.DataFrame(qss_squeeze, index=timestamps)

def get_qss_average_intensities_full_discharge_fixed_channel(shotname, strikeline="horizontal", 
                                       port="AEF51",debug=False,version="1",debug_parameters=None):
    """
    This function reads fit parameters of every channel with maximum intensity of a specific spectral line.
    It is the equivalent to get data produced by analysis.fit_discharge_brightest_channel into Juice
    Args:
        shotname (str): The name of the shot, e.g., "20181016.023".
        strongest_lines (list of str, optional): The fits are saved for the channel where this line is strongest. Defaults to ["410.174"].
        strikeline (str, optional): The type of strikeline. Defaults to "horizontal".
        analysis_step (str, optional): The analysis step. Defaults to "0.1".
        port (str, optional): The port identifier. Defaults to "AEF51".
    
    Returns:
        tuple: A tuple containing:
            - dict: dictionary with the fit values for every fit_parameter
            - list: List of names of the fitted parameters
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    qss_average_intensities_full_discharge = {}
    fit_description = []
    
    # Iterate over each line that 

    path = "{}qss_analysis/discharges/{}/{}/{}/Fixed_channel_full_discharge_average_intensities_{}_{}.json".format(ground_path,campaign,shotname[:8],shotname,port,version)
    
    # Open the JSON file and load data
    with open(path) as f:
        data = json.load(f)
        fit_parameters = data[strikeline]["fit_parameters"]
        time = data[strikeline]["time"]
        fit_description.extend(data[strikeline]["fit_description"])
    # Iterate over each fit parameter
    for p_idx,parameter in enumerate(data[strikeline]["fit_description"]):
        qss_average_intensities_full_discharge[parameter] = []
        qss_average_intensities_full_discharge[parameter].append(time)
        qss_average_intensities_full_discharge[parameter].append(np.array([10**fit_parameters[p_idx]]))
        qss_average_intensities_full_discharge[parameter] = np.array(qss_average_intensities_full_discharge[parameter])
    if debug:
        if debug_parameters == None:
            debug_parameters = fit_description
        fig, ax1 = plt.subplots(figsize=(8,6))
        ax2 = ax1.twinx()
        values = []
        categories = []
        for parameter in debug_parameters:
            values.append(qss_average_intensities_full_discharge[parameter][1][0])
            categories.append(parameter[10:])
        plt.bar(categories, values)
        plt.title("{} {} {}".format(shotname,port,strikeline))
        ax1.set_ylabel("Intensity")
        plt.show()
    return qss_average_intensities_full_discharge, fit_description

def squeeze_qss_average_intensities_full_discharge_fixed_channel(shotname, time_base, timestamps, common_base_method, timeout, settings=None):
    """
    Squeeze the maximum intensities of QSS data for each fit parameter onto a common time base.
    
    Args:
        shotname (str): The name of the shot.
        time_base (array_like): The common time base to which fit_parameters will be squeezed.
        timestamps (array_like): Timestamps corresponding to QSS data.
        common_base_method (function): A function to squeeze QSS data onto a common time base.
        timeout (float): Timeout for squeezing operation.
        settings (dict, optional): Additional settings. Defaults to None.
    
    Returns:
        pandas.DataFrame: DataFrame containing squeezed QSS maximum intensities for each emission line.
    """
    # Retrieve QSS maximum intensities for each emission line
    qss_max_intensities, emission_lines = get_qss_average_intensities_full_discharge_fixed_channel(shotname, strikeline=settings["strikeline"],port=settings["port"], version=settings["version"])
    qss_squeeze = {}
    
    # Iterate over each emission line
    for name in emission_lines:
        qss_tb = common_base_method(qss_max_intensities[name][0], qss_max_intensities[name][1], time_base)
        qss_squeeze['qss_average_intensity_full_discharge_{name}'.format(name=name)] = qss_tb[:,0]
    
    return pd.DataFrame(qss_squeeze, index=timestamps)
def get_qss_active_intensities_valve_and_channel(shotname,camid="IP160_2",port="AEI30",
                               valve="H30v4",
                               channel=0,version="1",debug=False,debug_parameters=None,
                               plot_line_ratios = [],
                               plot_channels=False,xlim=None):
    """
    reads the fitted intensities of active puffs
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    path = "{}qss_analysis/discharges/{}/{}/{}/active_spectroscopy_port{}_version{}.json".format(ground_path,campaign,shotname[:8],shotname,port,version)
    qss_active_intensities = {}
    fit_description = []
    with open(path) as f:
        data = json.load(f)
    for fit_parameter in data["fit_parameters"][camid]:
        fit_description.append(fit_parameter)
        qss_active_intensities[fit_parameter] = [[],[]]
    for valve_idx,valve_name in enumerate(data["valve_names"]):
        if valve in valve_name:
            for fit_parameter in fit_description:
                qss_active_intensities[fit_parameter][0].append((data["opening_times"][valve_idx]+data["closing_times"][valve_idx])/2)
                qss_active_intensities[fit_parameter][1].append(data["fit_parameters"][camid][fit_parameter][valve_idx][channel])
    for fit_parameter in data["fit_parameters"][camid]:
        qss_active_intensities[fit_parameter] = np.array(qss_active_intensities[fit_parameter])
    if debug:
        fig, ax1 = plt.subplots(figsize=(8,6))
        for fit_parameter in fit_description[:3]:
            plt.plot(qss_active_intensities[fit_parameter][0],qss_active_intensities[fit_parameter][1],label=fit_parameter)
        if len(plot_line_ratios) > 0:
            ax2 = ax1.twinx()
            for line_ratio in plot_line_ratios:
                ax2.plot(qss_active_intensities[line_ratio[0]][0],qss_active_intensities[line_ratio[0]][1]/qss_active_intensities[line_ratio[1]][1],label=line_ratio,ls="--")
        plt.grid()
        plt.legend()
        plt.show()
    return qss_active_intensities,fit_description
def squeeze_qss_active_intensities_valve_and_channel(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_active_intensities, emission_lines = get_qss_active_intensities_valve_and_channel(shotname,camid=settings["camid"],channel=settings["channel"],valve=settings["valve"],port=settings["port"],version=settings["version"]) # todo: settings need to be corrected!
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_active_intensities[name][0], qss_active_intensities[name][1], time_base)
        qss_squeeze['qss_active_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)  

"""From here on we have old stuff, that I just don't want to delete yet in case I need it at some point"""

def get_qss_average_intensities_old(shotname,camid="IP320_2",ground_path="//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/",debug=True):
    """
    Average intensity of all fitted lines in a discharge is read from a saved json file
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with average intensity of every fitted line ["Intensity [Photons/($\mathrm{m}^2$ sr s)]"]: list of the average instensities
    list of line names
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    paths = ["{}qss_analysis/discharges/OP2.1/{}/{}/fit_parameters_camidIP320_2_cw405_portAEF51_average200ms_version1.json".format(ground_path,shotname[:8],shotname),
             "{}qss_analysis/discharges/OP2.1/{}/{}/fit_parameters_camidIP320_1_cw364_portAEF51_average200ms_version1.json".format(ground_path,shotname[:8],shotname)]
    emission_lines = []
    qss_average_intensities = {}
    for path in paths:
        try:
            with open(path) as f:
                data = json.load(f)
                fit_parameters = data["fit_parameters"]
                fit_parameters["time_s"] = data["time_s"]
                fit_parameters["fit_parameters_description"] = data["fit_description"]
            for key in fit_parameters.keys():
                fit_parameters[key] = np.array(fit_parameters[key])
        except:
            fit_parameters["fit_parameters_description"] = []
        for element in fit_parameters["fit_parameters_description"]:
            if "Intensity_" in element:
                #p_idx = np.where(element == np.array(fit_parameters["fit_parameters_description"]))[0][0]
                emission_lines.append(element)
                qss_average_intensities[element] = []
                qss_average_intensities[element].append(fit_parameters["time_s"][:-1])
                qss_average_intensities[element].append([])
                for i in range(len(fit_parameters["time_s"][:-1])):
                    try:
                        qss_average_intensities[element][1].append(np.nanmean(fit_parameters[element][i]))
                    except:
                        qss_average_intensities[element][1].append(np.nan)
                qss_average_intensities[element][0] = np.array(qss_average_intensities[element][0])
                qss_average_intensities[element][1] = np.array(qss_average_intensities[element][1])
    return qss_average_intensities, emission_lines


def get_qss_positions_max_intensities_old(shotname,camid,ground_path="//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/",debug=True):
    """
    The parameters of the radiation zone are fitted reading a profile from already existing fits
    Returned are the position (channel), intensity and width of these radiation fronts
    
    """
    campaign = utils.get_campaign_from_pid_or_day(shotname)
    path = "{}qss_analysis/discharges/{}/{}/{}/fit_parameters_{}.json".format(ground_path,campaign,shotname[:8],shotname,camid)     
    try:
        with open(path) as f:
            fit_parameters = json.load(f)
        for key in fit_parameters.keys():
            fit_parameters[key] = np.array(fit_parameters[key])
    except:
        logger.debug("File doesn't exist")
    emission_lines = []
    qss_positions_max_intensities = {}
    qss_max_intensities = {}
    qss_width_max_intensities = {}
    for element in fit_parameters["fit_parameters_description"]:
        if "Intensity_" in element:
            emission_lines.append(element)
            qss_positions_max_intensities[element]=[]
            qss_positions_max_intensities[element].append(fit_parameters["time_s"])
            qss_positions_max_intensities[element].append([])
            qss_max_intensities[element] = []
            qss_max_intensities[element].append(fit_parameters["time_s"])
            qss_max_intensities[element].append([])
            for i in range(len(fit_parameters["time_s"])):
                #qss_positions_max_intensities[element][1].append(np.nanargmax(fit_parameters[camid][i,:,len(emission_lines)-1]))
                try:
                    ydata = 10**fit_parameters[camid][i,:,len(emission_lines)-1]
                    xdata = np.arange(27)
                    valid = np.isfinite(ydata)
                    guess = [np.min(ydata[valid]),xdata[valid][np.argmax(ydata[valid][10:])+10],ydata[valid][0],ydata[valid][np.argmax(ydata[valid][10:])+10],.3,2.5]
                    bounds = ([0.5*np.min(ydata[valid]),0,0.5*ydata[valid][0],0,0,1],[np.min(ydata[valid]),27,ydata[valid][0],np.max(ydata[valid]),10,10])
                    fit_parameters_radiation_front,pcov_radiation_front = curve_fit(f=radiation_front,xdata=xdata[valid],ydata=ydata[valid],p0=guess,bounds=bounds)
                    if debug:
                        if "CIII_" in element:
                            logger.debug(guess)
                            logger.debug(fit_parameters_radiation_front)
                            plt.figure(figsize=(8,6))
                            title = "Profile {}, Time {:.1f}".format(element,0.2*i)
                            plt.title(title)
                            plt.plot(xdata[valid],ydata[valid],"+-",label="Data")
                            plt.plot(xdata[valid],radiation_front(xdata[valid],*fit_parameters_radiation_front),label="Fit")
                            plt.legend()
                            plt.xlabel("Channel")
                            plt.ylabel(r"Intensity [Photons/($\mathrm{m}^2$ nm sr s)]")
                            #plt.ylim(0,800)
                            plt.savefig("images//"+title+".png",format="png")
                            plt.show()
                    qss_positions_max_intensities[element][1].append(fit_parameters_radiation_front[1])
                    qss_max_intensities[element][1].append(fit_parameters_radiation_front[3])
                except:
                    qss_positions_max_intensities[element][1].append(np.nan)
                    qss_max_intensities[element][1].append(np.nan)
            qss_positions_max_intensities[element][0] = np.array(qss_positions_max_intensities[element][0])
            qss_positions_max_intensities[element][1] = np.array(qss_positions_max_intensities[element][1])
            qss_max_intensities[element][0] = np.array(qss_max_intensities[element][0])
            qss_max_intensities[element][1] = np.array(qss_max_intensities[element][1])
    return qss_positions_max_intensities, qss_max_intensities, emission_lines

def squeeze_qss_position_max_intensities_radiation_front_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_positions_max_intensities, qss_max_intensities, emission_lines = get_qss_positions_max_intensities(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_positions_max_intensities[name][0], qss_positions_max_intensities[name][1], time_base)
        qss_squeeze['qss_positions_max_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)


    
def get_qss_ratio_balmer_delta_epsilon_old(shotname,camid,ground_path="//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/"):
    """
    Intensity ratio of Balmer epsilon and delta line (fitted) in a discharge is read from a saved json file
    Under discussion is which stark density I want to use
        - maximim intensity?
        - maximum density?
        - average? for now starting with this over all 27 channels!
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with ratio of balmer epsilon and delta line: list of the ratios
    list of line names
    """
    path = "{}qss_analysis/discharges/OP1.2b/{}/{}/fit_parameters_{}.json".format(ground_path,shotname[:8],shotname,camid)     
    try:
        with open(path) as f:
            fit_parameters = json.load(f)
        for key in fit_parameters.keys():
            fit_parameters[key] = np.array(fit_parameters[key])
    except:
        logger.debug("File doesn't exist")
    balmer_lines = []
    ratio_balmer_delta_epsilon = {}
    idx_delta = list(fit_parameters["fit_parameters_description"]).index("Intensity_Balmer_H_delta")
    idx_epsilon = list(fit_parameters["fit_parameters_description"]).index("Intensity_Balmer_H_epsilon")
    element = "ratio_balmer_delta_epsilon"
    balmer_lines.append(element)
    ratio_balmer_delta_epsilon[element]=[]
    ratio_balmer_delta_epsilon[element].append(fit_parameters["time_s"])
    ratio_balmer_delta_epsilon[element].append([])
    for i in range(len(fit_parameters["time_s"])):
        ratio_balmer_delta_epsilon[element][1].append(10**np.nanmean(fit_parameters[camid][i,:15,idx_delta])/10**np.nanmean(fit_parameters[camid][i,:15,idx_epsilon]))
    ratio_balmer_delta_epsilon[element][0] = np.array(ratio_balmer_delta_epsilon[element][0])
    ratio_balmer_delta_epsilon[element][1] = np.array(ratio_balmer_delta_epsilon[element][1])
    return ratio_balmer_delta_epsilon, balmer_lines

def get_qss_stark_density_old(shotname,camid="IP320_2",intensity_cutoff=16,debug=False,ground_path="//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/"):
    """
    Stark density of fitted Balmer lines in a discharge is read from a saved json file
    Under discussion is which stark density I want to use
        - maximim intensity?
        - maximum density?
        - average? for now starting with this over all 27 channels!
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with stark denisty: 
    list of line names
    """
    #path = "{}qss_analysis/discharges/OP1.2b/{}/{}/fit_parameters_{}.json".format(ground_path,shotname[:8],shotname,camid)     
    path = "{}qss_analysis/discharges/OP2.1/{}/{}/fit_parameters_camidIP320_2_cw405_portAEF51_average200ms_version1.json".format(ground_path,shotname[:8],shotname)     
    try:
        with open(path) as f:
            data = json.load(f)
            fit_parameters = data["fit_parameters"]
            fit_parameters["time_s"] = data["time_s"]
            fit_parameters["fit_parameters_description"] = data["fit_description"]
        for key in fit_parameters.keys():
            fit_parameters[key] = np.array(fit_parameters[key])
    except:
        logger.debug("File doesn't exist")
    strikelines = ["horizontal","vertical"]
    element = "Electron_density Balmer_lines"
    intensity = "Intensity_Balmer_H_delta"
    stark_density = {}
    for strikeline in strikelines:
        stark_density[element]=[]
        stark_density[element].append(fit_parameters["time_s"])
        stark_density[element].append([])
        for i in range(len(fit_parameters["time_s"])):
            stark_array_linear = np.array(fit_parameters[element][i])
            
            intensity_array_linear = np.array(fit_parameters[intensity][i])
            for c in range(len(intensity_array_linear)):
                if stark_array_linear[c] <= 5e18 or stark_array_linear[c]>=5e21:
                    stark_array_linear[c] = np.nan
            valid = np.where(intensity_array_linear>intensity_cutoff)
            stark_density[element][1].append(np.nanmean(stark_array_linear[valid])) 
        stark_density[element][0] = np.array(stark_density[element][0])
        stark_density[element][1] = np.array(stark_density[element][1])
        if debug:
            plt.figure(figsize=(8,6))
            plt.plot(stark_density[element][0],stark_density[element][1])
            plt.show()
    return stark_density, [element]

def get_qss_stark_density_strikelines_standard_old(shotname,camid="IP320_2",intensity_cutoff=16,debug=False,ground_path="//share.ipp-hgw.mpg.de/Documents/frhe/Documents/phd/"):
    """
    Stark density of fitted Balmer lines in a discharge is read from a saved json file
    Under discussion is which stark density I want to use
        - maximim intensity?
        - maximum density?
        - average? for now starting with this over all 27 channels!
    
    Input:
    pid i.e. "20181016.023"
    camid i.e. "IP320_2"
    
    Returns:
    dictionary with stark denisty: 
    list of line names
    """
    #path = "{}qss_analysis/discharges/OP1.2b/{}/{}/fit_parameters_{}.json".format(ground_path,shotname[:8],shotname,camid)     
    path = "{}qss_analysis/discharges/OP2.1/{}/{}/fit_parameters_camidIP320_2_cw405_portAEF51_average200ms_version1.json".format(ground_path,shotname[:8],shotname)     
    try:
        with open(path) as f:
            data = json.load(f)
            fit_parameters = data["fit_parameters"]
            fit_parameters["time_s"] = data["time_s"]
            fit_parameters["fit_parameters_description"] = data["fit_description"]
        for key in fit_parameters.keys():
            fit_parameters[key] = np.array(fit_parameters[key])
    except:
        logger.debug("File doesn't exist")
    element = "Electron_density Balmer_lines"
    idx_density = np.where(np.array(fit_parameters["fit_parameters_description"])==element)[0][0]
    intensity = "Intensity_Balmer_H_delta"
    idx_intensity = np.where(np.array(fit_parameters["fit_parameters_description"])==intensity)[0][0]
    stark_density = {}
    
    stark_density[element].append(fit_parameters["time_s"])
    stark_density["horizontal"]=[]
    stark_density["vertical"].append([])
    for i in range(len(fit_parameters["time_s"])):
        stark_array_linear_horizontal = np.array(fit_parameters[element][i])
        intensity_array_linear_horizontal = np.array(fit_parameters[intensity][i])
        for c in range(len(intensity_array_linear_horizontal)):
            if stark_array_linear_horizontal[c] <= 5e18 or stark_array_linear_horizontal[c]>=5e21:
                stark_array_linear_horizontal[c] = np.nan
        valid = np.where(intensity_array_linear_horizontal>intensity_cutoff)
        stark_density[element][1].append(np.nanmean(stark_array_linear_horizontal[valid])) 
    stark_density[element][0] = np.array(stark_density[element][0])
    stark_density[element][1] = np.array(stark_density[element][1])
    if debug:
        plt.figure(figsize=(8,6))
        plt.plot(stark_density[element][0],stark_density[element][1])
        plt.show()
    return stark_density, ["horizontal","vertical"]



def squeeze_qss_max_intensities_radiation_front_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_positions_max_intensities, qss_max_intensities, emission_lines = get_qss_positions_max_intensities(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_max_intensities[name][0], qss_max_intensities[name][1], time_base)
        qss_squeeze['qss_max_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)




def squeeze_qss_average_intensities_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    qss_average_intensities, emission_lines = get_qss_average_intensities(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in emission_lines:
        qss_tb = common_base_method(qss_average_intensities[name][0], qss_average_intensities[name][1], time_base)
        qss_squeeze['qss_average_intensities_{name}'.format(name=name)]=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)

def squeeze_qss_stark_density_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    stark_density, balmer_lines = get_qss_stark_density(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in balmer_lines:
        qss_tb = common_base_method(stark_density[name][0], stark_density[name][1], time_base)
        qss_squeeze['Electron_density Balmer_lines']=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)

def squeeze_qss_stark_density_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    stark_density, balmer_lines = get_qss_stark_density(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in balmer_lines:
        qss_tb = common_base_method(stark_density[name][0], stark_density[name][1], time_base)
        qss_squeeze['Electron_density Balmer_lines']=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)

def squeeze_qss_ratio_balmer_delta_epsilon_old(shotname, time_base, timestamps, common_base_method,
              timeout, settings=None):
    ratio_balmer, balmer_lines = get_qss_ratio_balmer_delta_epsilon(shotname,camid="IP320_2")
    qss_squeeze = {}
    for name in balmer_lines:
        qss_tb = common_base_method(ratio_balmer[name][0], ratio_balmer[name][1], time_base)
        qss_squeeze['ratio_balmer_delta_epsilon']=qss_tb[:,0]
    return pd.DataFrame(qss_squeeze, index = timestamps)
