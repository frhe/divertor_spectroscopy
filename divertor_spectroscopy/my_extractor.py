import numpy as np
import pandas as pd
import w7x_overviewplot.logbookapi as LB
import re
from divertor_spectroscopy import utilities as utils

def get_lbo_times(pid,debug=False):
    lb = LB.LogbookAPI(kks="QSL")
    try:
        comments = lb.getcomponentcomments(program=utils.pid_format_to_logbook(pid))
        # Since comments is a dictionary, extract the first value
        # Assuming the structure of comments is {'some_id': 'comment_text'}
        comment_text = next(iter(comments.values()))

        # Extract planned injection times using a regular expression
        match = re.search(r'planned injection times: \[(.*?)\]', comment_text)
        if match:
            lbo_times = np.array([int(x) for x in match.group(1).split(', ')])/1000
        else:
            lbo_times = []  # Empty list if no match found
        if debug:
            # Print or assign the list as needed
            print("Planned LBO Injections:", lbo_times)
    except:
        lbo_times = []
    return lbo_times
def get_tespel_time(pid,debug=False):
    lb = LB.LogbookAPI(kks="QSH")
    componenttags = lb.getcomponenttags(program=utils.pid_format_to_logbook(pid))
    try:
        tespel_info = componenttags[0]["TESPEL"]

        # Use regular expression to extract the time in seconds
        match = re.search(r'@ ([\d.]+)s', tespel_info)
        if match:
            tespel_time = float(match.group(1))  # Convert to float for numerical use
            if debug:
                print("Time in seconds:", tespel_time)
        else:
            tespel_time = None
            if debug: 
                print("No time found in TESPEL info")
    except:
        tespel_time = None
    return tespel_time

        