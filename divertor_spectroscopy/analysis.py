import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from numpy import convolve
import json
import os
import copy
import pandas as pd
import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import atomic_data
from divertor_spectroscopy import instrument

# This is my solution for locating this package, but there should be a much more elegant solution
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

# as this class is responsible for displaying the spectra, colors and line styles are chosen here to always be consistent
special_colors = ["darkmagenta","darkred","darkblue","darkgreen"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]
elements = ["H","He","B","C","N","O","Ne","Ar","Hg","W"]
element_colors = {}
for i_element,i_color in enumerate([1,2,4,5,9,6,3,8,7,7]):
    element_colors[elements[i_element]] = colors[i_color]
styles = ["solid","dashed","dotted","dashdot","solid","dashed","dotted","dashdot"]
ion_stages = ["I","II","III","IV","V","VI","VII","VIII"]
ion_styles = {}
for i in range(len(ion_stages)):
    ion_styles[ion_stages[i]] = styles[i]
line_width = 2.5
class Analysis():
    def __init__(self,model,instrument):
        """This function represents a spectrum observed on a single channel of an instrument
        Args:
            model (spectrum object): contains all atomic and plasma physics related information
            instrument (instrument object): contains all the instrument related information 
                - attention, this is supposed to be one channel of an instrument only!
        """
        self.model = model
        self.instrument = instrument
        self.get_default_fit_parameters()
        self.set_fit_description()
        
    def guess_spectrum(self,experiment_data,valid):
        """Guesses the fit parameters connected to the model - also background level

        Args:
            experiment_data (np.array): Experiment data you want to fit (without background correction)
        """
        # we need a background offset - this can now be taken and averaged at different positions. Important new feature is, that it is now also saved in the fits of the model
        if type(self.model.recommended_background_position) is list:
            self.background_position_indices = []
            self.background_guesses = []
            for background_position in self.model.recommended_background_position: 
                self.background_position_indices.append(utils.val2idx(self.instrument.wavelength[valid],background_position))
                self.background_guesses.append(np.nanmean(experiment_data[self.background_position_indices[-1]-2:self.background_position_indices[-1]+3]))
            self.background_guess = np.nanmean(self.background_guesses)
        else:
            self.background_position_idx = utils.val2idx(self.instrument.wavelength[valid],self.model.recommended_background_position)
            self.background_guess = np.nanmean(experiment_data[self.background_position_idx-2:self.background_position_idx+3])
        # the guess
        self.guess = []
        self.lower_bounds = []
        self.upper_bounds = []
        if self.instrument.shift_guess is None:
            wavelength = self.instrument.wavelength[valid]
        else:
            wavelength = self.instrument.wavelength[valid] - self.instrument.shift_guess
        # multiplets have broadening from the instrument and from a temperature component - temperature is not always fittet - intensity is always fitted
        if self.instrument.instrument_function_mode == "gauss" or self.instrument.instrument_function_mode == "numeric" or self.instrument.instrument_function_mode == "convolution_entire_spectrum":
            for m in self.model.multiplets:                
                averaged_positions, summed_branching_ratios = sum_close_branching_ratios(m.positions,m.branching_ratios,5*np.abs(self.instrument.wavelength[1]-self.instrument.wavelength[0]),debug=False)
                dominant_idx = np.argmax(summed_branching_ratios)
                position_idx = utils.val2idx(wavelength, averaged_positions[dominant_idx])
                if m.fit_broadening: # guess for temperature and intensity has to be appended
                    intensity_guess = np.log10((experiment_data[position_idx] - self.background_guess)/summed_branching_ratios[dominant_idx] *self.instrument.width * np.sqrt(2 * np.pi))
                    if np.isfinite(intensity_guess):
                        self.guess.append(intensity_guess) # todo: add broadening from temperature!
                    else:
                        self.guess.append(np.log10(np.nanmean(experiment_data))-2)
                    self.guess.append(m.doppler_temperature)
                else:
                    intensity_guess = np.log10((experiment_data[position_idx] - self.background_guess)/summed_branching_ratios[dominant_idx] *self.instrument.width * np.sqrt(2 * np.pi))
                    if np.isfinite(intensity_guess):
                        self.guess.append(intensity_guess)
                    else:
                        logger.debug("Intensity guess not finite")
                        self.guess.append(np.log10(np.nanmean(experiment_data)*self.instrument.width* np.sqrt(2 * np.pi))-2)
                self.lower_bounds.append(np.log10(np.nanmean(experiment_data)*self.instrument.width* np.sqrt(2 * np.pi))-2)
                self.upper_bounds.append(np.log10(np.nanmax(experiment_data)* self.instrument.width * np.sqrt(2 * np.pi))+2)# todo: upper bound and instrument function a bit weird
                    
                    
                if m.fit_broadening:
                    self.lower_bounds.append(0)
                    self.upper_bounds.append(10*self.guess[-1])
            for bl in self.model.balmer_lines: 
                # guess the intensity as if these had only instrument function broadening: todo - implement this for sopra case
                # todo: how do I properly handle zeeman effect?
                
                for nl in range(len(bl.lines)):
                    averaged_positions, summed_branching_ratios = sum_close_branching_ratios(bl.positions[nl],bl.branching_ratios[nl],5*np.abs(self.instrument.wavelength[1]-self.instrument.wavelength[0]))
                    dominant_idx = np.argmax(summed_branching_ratios)
                    position_idx = utils.val2idx(wavelength, averaged_positions[dominant_idx])
                    intensity_guess = np.log10((experiment_data[position_idx] - self.background_guess)/summed_branching_ratios[dominant_idx] *self.instrument.width * np.sqrt(2 * np.pi))
                    if np.isfinite(intensity_guess):
                        self.guess.append(intensity_guess) # todo: add broadening from temperature!
                    else:
                        self.guess.append(np.log10(np.nanmean(experiment_data))-2)
                    self.lower_bounds.append(np.log10(np.nanmean(experiment_data))-2)
                    self.upper_bounds.append(np.log10(np.nanmax(experiment_data)*self.instrument.width * np.sqrt(2 * np.pi))+2)# todo: upper bound and instrument function a bit weird
                self.guess.append(np.log10(bl.electron_density))
                self.lower_bounds.append(18)
                self.upper_bounds.append(np.log10(1e21))
                if bl.fit_broadening:
                    self.guess.append(bl.doppler_temperature)
                    self.lower_bounds.append(0)
                    self.upper_bounds.append(10*self.guess[-1])
                if bl.fit_doppler_shift:
                    self.guess.append(bl.doppler_shift)
                    self.lower_bounds.append(-0.1)
                    self.upper_bounds.append(0.1)
            for pec in self.model.pec_lines:
                if pec.fit_electron_density:
                    self.guess.append(np.log10(pec.electron_density))
                    self.lower_bounds.append(np.log10(pec.ne_range[0]))
                    self.upper_bounds.append(np.log10(pec.ne_range[1]))
                if pec.fit_electron_temperature:
                    self.guess.append(pec.electron_temperature)
                    self.lower_bounds.append(pec.Te_range[0])
                    self.upper_bounds.append(pec.Te_range[1])
                if pec.fit_impurity_density:
                    self.guess.append(np.log10(pec.impurity_density))
                    self.lower_bounds.append(np.log10(pec.impurity_density)-3)
                    self.upper_bounds.append(np.log10(pec.impurity_density)+3)
        for i in range(len(self.guess)):
            if not np.isfinite(self.guess[i]):
                self.guess[i] = 1
            if not np.isfinite(self.lower_bounds[i]):
                self.lower_bounds[i] = self.guess[i]/10
            if not np.isfinite(self.upper_bounds[i]):
                self.upper_bounds[i] = self.guess[i]*10
            if self.guess[i] > self.upper_bounds[i]:
                self.guess[i] = self.upper_bounds[i]
            if self.guess[i] < self.lower_bounds[i]:
                self.guess[i] = self.lower_bounds[i]     

    def get_default_fit_parameters(self):
        """Reads from model and instrument what is meant to be fitted and saves it in the object
        """
        self.default_fit_parameters = []
        if self.instrument.wavelength_coefficients_guess is not None:
            self.default_fit_parameters = copy.deepcopy(self.instrument.wavelength_coefficients_guess)
        if self.instrument.shift_guess is not None:
            self.default_fit_parameters.append(copy.deepcopy(self.instrument.shift_guess))
        if self.instrument.width_guess is not None:
            self.default_fit_parameters.append(copy.deepcopy(self.instrument.width_guess))
        for m in self.model.multiplets:
            self.default_fit_parameters.append(np.log10(m.intensity))
            if m.fit_broadening:
                self.default_fit_parameters.append(m.doppler_temperature)
        for bl in self.model.balmer_lines:
            for i in range(len(bl.lines)):
                self.default_fit_parameters.append(np.log10(bl.intensities[i]))
            self.default_fit_parameters.append(np.log10(bl.electron_density))
            if bl.fit_broadening:
                self.default_fit_parameters.append(bl.doppler_temperature)
            if bl.fit_doppler_shift:
                self.default_fit_parameters.append(bl.doppler_shift)
        for pec in self.model.pec_lines:
            if pec.fit_electron_density:
                self.default_fit_parameters.append(np.log10(pec.electron_density))
            if pec.fit_electron_temperature:
                self.default_fit_parameters.append(pec.electron_temperature)
            if pec.fit_impurity_density:
                self.default_fit_parameters.append(np.log10(pec.impurity_density))
        
    def build_spectrum(self,wavelength,*fit_parameters,filter=None):
        """Central method which calculates a spectrum
        This method is supposed to be used in all applications

        Args:
            wavelength (np.array): Wavelength arry of the instrument channel
            filter (str): String used to filter which ions should be highlighted while displaying. Defaults to None.
            *fit_parameters (list): these are the variable (meant to fit) parameters of your analysis object
        Returns:
            np.array: Spectrum
        """
        signal = np.zeros(len(wavelength))
        current_fit_parameter = 0
        
        for m in self.model.multiplets:
            mass = atomic_data.get_mass_of_element_from_tag(m.tag)
            
            for p_idx,p in enumerate(m.positions):
                if m.fit_broadening:
                    doppler_width = utils.doppler_broadening_nist(fit_parameters[current_fit_parameter+1],mass,p)
                else:
                    doppler_width = utils.doppler_broadening_nist(m.doppler_temperature,mass,p)
                    m.doppler_width = doppler_width
                if self.instrument.instrument_function_mode == "gauss":
                    width = np.sqrt(self.instrument.width**2+doppler_width**2)
                    if filter is None or m.tag == filter:
                        signal += utils.gauss(x=wavelength, offset=0, position=p,
                                            intensity=10**fit_parameters[current_fit_parameter] * m.branching_ratios[p_idx],width=width)
                if self.instrument.instrument_function_mode == "numeric":
                    if filter is None or m.tag == filter:
                        gauss_part = utils.gauss(x=wavelength, offset=0, position=p,
                                                intensity=10**fit_parameters[current_fit_parameter] * m.branching_ratios[p_idx],width=doppler_width)
                        signal += convolve(gauss_part,self.instrument.numeric_instrument_function,mode="same")/sum(self.instrument.numeric_instrument_function)
                if self.instrument.instrument_function_mode == "convolution_entire_spectrum":
                    if m.doppler_width < self.instrument.dispersion*1.25/2.35:# this is the point found in how_to_instrument_function.ipynb
                        #logger.info("Doppler width {} is small, to avoid numerical errors in convolution set it to {}".format(doppler_width,self.instrument.dispersion))
                        m.doppler_width = self.instrument.dispersion*1.25/2.35
                    if filter is None or m.tag == filter:
                        signal += utils.gauss(x=wavelength, offset=0, position=p,
                                                intensity=10**fit_parameters[current_fit_parameter] * m.branching_ratios[p_idx],width=m.doppler_width)
            if m.fit_broadening:
                current_fit_parameter += 2
            else:
                current_fit_parameter += 1
        for bl in self.model.balmer_lines:
            number_lines = len(bl.lines)
            electron_density = 10**fit_parameters[current_fit_parameter+number_lines]
            for nl in range(number_lines):
                for p_idx,p in enumerate(bl.positions[nl]):
                    mass = atomic_data.get_mass_of_element_from_tag(bl.tag)
                    if bl.fit_broadening:
                        doppler_width = utils.doppler_broadening_nist(fit_parameters[current_fit_parameter+number_lines+1],mass,p)
                    else:
                        doppler_width = utils.doppler_broadening_nist(bl.doppler_temperature,mass,p)
                    bl.get_fwhm(electron_density,bl.electron_temperature)
                    if bl.fit_doppler_shift & bl.fit_broadening:
                        doppler_shift = fit_parameters[current_fit_parameter+number_lines+2]
                    elif bl.fit_doppler_shift:
                        doppler_shift = fit_parameters[current_fit_parameter+number_lines+1]
                    else:
                        doppler_shift = bl.doppler_shift
                    if self.instrument.instrument_function_mode == "gauss":
                        bl.width_gauss = np.sqrt(self.instrument.width**2+doppler_width**2)
                        if filter is None or bl.tag == filter:
                            signal += utils.lomanowskigauss(x=wavelength,offset=0,position=bl.positions[nl,p_idx]+doppler_shift,intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],width_gauss=bl.width_gauss,fwhm_lorentz=bl.fwhm[nl])
                            #signal += utils.lomanowski(x=wavelength,offset=0,position=bl.positions[nl,p_idx],intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],fwhm=bl.fwhm[nl])
                    if self.instrument.instrument_function_mode == "numeric":
                        bl.width_gauss = doppler_width
                        if filter is None or bl.tag == filter:
                            lomanowski = utils.lomanowskigauss(x=wavelength,offset=0,position=bl.positions[nl,p_idx]+doppler_shift,intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],width_gauss=doppler_width,fwhm_lorentz=bl.fwhm[nl])
                            #lomanowski = utils.lomanowski(x=wavelength,offset=0,position=bl.positions[nl,p_idx],intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],fwhm=bl.fwhm[nl])
                            signal += convolve(lomanowski,self.instrument.numeric_instrument_function,mode="same")/sum(self.instrument.numeric_instrument_function)
                    
                    if self.instrument.instrument_function_mode == "convolution_entire_spectrum":
                        bl.width_gauss = doppler_width
                        if filter is None or bl.tag == filter:
                            #signal += utils.lomanowski(x=wavelength,offset=0,position=bl.positions[nl,p_idx],intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],fwhm=bl.fwhm[nl])
                            signal += utils.lomanowskigauss(x=wavelength,offset=0,position=bl.positions[nl,p_idx]+doppler_shift,intensity=10**fit_parameters[current_fit_parameter+nl]*bl.branching_ratios[nl,p_idx],width_gauss=bl.width_gauss,fwhm_lorentz=bl.fwhm[nl])
                            
            if bl.fit_broadening and bl.fit_doppler_shift:
                current_fit_parameter += number_lines + 3
            elif bl.fit_broadening:
                current_fit_parameter += number_lines + 2
            elif bl.fit_doppler_shift:
                current_fit_parameter += number_lines + 2
            else:
                current_fit_parameter += number_lines + 1 
        for pec in self.model.pec_lines:
            # I want to be able to fix any of the parameters: 
            i = 0
            if pec.fit_electron_density:
                electron_density = 10**fit_parameters[current_fit_parameter]
                i += 1
            else:
                electron_density = pec.electron_density
            if pec.fit_electron_temperature:
                electron_temperature = fit_parameters[current_fit_parameter+i]
                i += 1
            else:
                electron_temperature = pec.electron_temperature
            if pec.fit_impurity_density:
                impurity_density = 10**fit_parameters[current_fit_parameter+i]
                i += 1
            else:
                impurity_density = pec.impurity_density
            # if I chose indices as a next step, the program can't vary the temperature - therefore some sort of interpolation might be necessary
            for m_idx,m in enumerate(pec.multiplets):
                m.intensity = pec.interpolated_line_intensity_functions[m_idx](electron_density,electron_temperature) * electron_density *impurity_density * pec.emission_length / (4*np.pi)
                mass = atomic_data.get_mass_of_element_from_tag(m.tag)
                lambda_0 = np.mean(np.array(m.positions)*np.array(m.branching_ratios))
                doppler_width = utils.doppler_broadening_nist(m.doppler_temperature,mass,lambda_0)
                m.doppler_width = doppler_width
                for p_idx,p in enumerate(m.positions):
                    if self.instrument.instrument_function_mode == "gauss":
                        width = np.sqrt(self.instrument.width**2+doppler_width**2)
                        if filter is None or m.tag == filter:
                            signal += utils.gauss(x=wavelength, offset=0, position=p,
                                                intensity=m.intensity * m.branching_ratios[p_idx],width=width)
                    if self.instrument.instrument_function_mode == "numeric":
                        if filter is None or m.tag == filter:
                            gauss_part = utils.gauss(x=wavelength, offset=0, position=p,
                                                    intensity=m.intensity * m.branching_ratios[p_idx],width=doppler_width)
                            signal += convolve(gauss_part,self.instrument.numeric_instrument_function,mode="same")/sum(self.instrument.numeric_instrument_function)
                    if self.instrument.instrument_function_mode == "convolution_entire_spectrum":
                        if m.doppler_width < self.instrument.dispersion*1.25/2.35:# this is the point found in how_to_instrument_function.ipynb
                            #logger.info("Doppler width {} is small, to avoid numerical errors in convolution set it to {}".format(doppler_width,self.instrument.dispersion))
                            m.doppler_width = self.instrument.dispersion*1.25/2.35
                        if filter is None or m.tag == filter:
                            signal += utils.gauss(x=wavelength, offset=0, position=p,
                                                    intensity=m.intensity * m.branching_ratios[p_idx],width=m.doppler_width)
            current_fit_parameter += i
            
        if self.instrument.instrument_function_mode == "convolution_entire_spectrum":
            signal = convolve(signal,self.instrument.numeric_instrument_function,mode="same")/sum(self.instrument.numeric_instrument_function)
        return signal
    
    def build_spectrum_wavelength_shift(self,wavelength,*fit_parameters,filter=None):
        return self.build_spectrum(wavelength-fit_parameters[0],*fit_parameters[1:],filter=filter)
    
    def build_spectrum_variable_width_and_shift(self,wavelength,*fit_parameters,filter=None):
        self.instrument.width = fit_parameters[1]
        return self.build_spectrum(wavelength-fit_parameters[0],*fit_parameters[2:],filter=filter)
    
    def build_spectrum_wavelength_calibration(self,x,*fit_parameters,filter=None):
        """First application of "build_spectrum" - use build spectrum to do a wavelength calibration 

        Args:
            x (np.array): pixels on the camera - usually this is np.linspace(-511.5,511.5,1024)
            filter (str): String used to filter which ions should be highlighted while displaying. Defaults to None.
            *fit_parameters (list): these are the variable (meant to fit) parameters of your analysis object
        Returns:
            np.array: Spectrum
        """
        order = len(self.instrument.wavelength_coefficients_guess)-1
        if order == 0: # this is only a wavelength shift!
            wavelength = self.instrument.wavelength-fit_parameters[0]
        else:
            wavelength = utils.polynominal(x,*fit_parameters[:order+1])
        return self.build_spectrum(wavelength,*fit_parameters[order+1:],filter=filter)
    
    def fit_spectrum(self,experiment_data,wavelength_to_fit=None,exclude_wavelength_from_fit=None,use_sigma=True,mode="default"):
        """Fitting a spectrum of an analysis-object
        Which parameters of the model should be fitted is defined in the model class
        Instrument fits like a wavelength calibration or a x shift are defined by the mode

        Args:
            experiment_data (np.array): Experiment data for the analysis of the spectrum
            mode (str, optional): Mode of fitting - options are "default" and "wavelength_calibration". Defaults to "default".
        """
        self.mode = mode
        self.experiment_data = experiment_data
        self.valid = utils.get_valid_pixels(self.experiment_data)
        if len(self.valid) < len(self.experiment_data):
            logger.warning("Attention, there are non finite pixels in your spectrum")
        self.guess_spectrum(experiment_data=self.experiment_data[self.valid],valid=self.valid)
        ydata = self.experiment_data[self.valid] - self.background_guess
        if use_sigma:
            sigma = np.ones(1024)
            #sigma = ydata**2
        else:
            sigma = None
        if mode == "default":
            if wavelength_to_fit is not None:
                self.wavelength_to_fit = wavelength_to_fit
                w_indices_include = np.arange(*np.sort(utils.val2idx(self.instrument.wavelength,wavelength_to_fit)))
                valid_w_indices_include = np.intersect1d(np.arange(1024),w_indices_include)
                self.valid = np.intersect1d(self.valid,valid_w_indices_include)
                ydata = self.experiment_data[self.valid] - self.background_guess
            if exclude_wavelength_from_fit is not None:
                for exclude in exclude_wavelength_from_fit:
                    w_indices_exclude = np.arange(*np.sort(utils.val2idx(self.instrument.wavelength,exclude)))
                    valid_w_indices = np.setdiff1d(np.arange(1024),w_indices_exclude)
                    print(len(valid_w_indices))
                    print(len(self.valid))
                    self.valid = np.intersect1d(self.valid,valid_w_indices)
                    print(len(self.valid))
                ydata = self.experiment_data[self.valid] - self.background_guess
            if use_sigma:
                sigma = ydata
                sigma = np.sqrt(np.sqrt(ydata))
                #sigma = ydata
                sigma = None
                # I think this might be a bit numerically unstable with a huge sigma, so I'll normalize it
                #sigma = np.sqrt((ydata/np.nanmax(ydata)))
                #sigma = None
            else:
                sigma = None
            f = self.build_spectrum
            self.fit_parameters,self.pcov = curve_fit(f=f,xdata=self.instrument.wavelength[self.valid],ydata=ydata,p0=self.guess,bounds=(self.lower_bounds,self.upper_bounds),sigma=sigma)
        if mode == "wavelength_shift":
            sigma = None # otherwise the wavelength calibration can be negatively influenced by incorrectly fitted shifts
            if exclude_wavelength_from_fit is not None:
                for exclude in exclude_wavelength_from_fit:
                    w_indices_exclude = np.arange(*np.sort(utils.val2idx(self.instrument.wavelength,exclude)))
                    valid_w_indices = np.setdiff1d(np.arange(1024),w_indices_exclude)
                    print(len(valid_w_indices))
                    print(len(self.valid))
                    self.valid = np.intersect1d(self.valid,valid_w_indices)
                    print(len(self.valid))
                ydata = self.experiment_data[self.valid] - self.background_guess
            self.guess = [self.instrument.shift_guess] + self.guess
            self.lower_bounds = self.instrument.lower_bounds+ self.lower_bounds
            self.upper_bounds = self.instrument.upper_bounds + self.upper_bounds
            logger.debug("guess {}".format(self.guess))
            logger.debug("lower_bounds: {}".format(self.lower_bounds))
            logger.debug("upper_bounds: {}".format(self.upper_bounds))
            f = self.build_spectrum_wavelength_shift
            self.fit_parameters,self.pcov = curve_fit(f=f,xdata=self.instrument.wavelength[self.valid],ydata=ydata,p0=self.guess,bounds=(self.lower_bounds,self.upper_bounds),sigma=sigma)
        if mode == "variable_width_and_shift":
            self.guess = [self.instrument.shift_guess,self.instrument.width_guess] + self.guess
            self.lower_bounds = self.instrument.lower_bounds + self.lower_bounds
            self.upper_bounds = self.instrument.upper_bounds + self.upper_bounds
            #logger.debug("guess {}".format(self.guess))
            #logger.debug("lower_bounds: {}".format(self.lower_bounds))
            #logger.debug("upper_bounds: {}".format(self.upper_bounds))
            f = self.build_spectrum_variable_width_and_shift
            self.fit_parameters,self.pcov = curve_fit(f=f,xdata=self.instrument.wavelength[self.valid],ydata=ydata,p0=self.guess,bounds=(self.lower_bounds,self.upper_bounds),sigma=sigma)
        
        if mode == "wavelength_calibration":
            sigma = None# otherwise the wavelength calibration can be negatively influenced by incorrectly fitted shifts
            self.guess = self.instrument.wavelength_coefficients_guess + self.guess
            self.lower_bounds = self.instrument.lower_bounds + self.lower_bounds
            self.upper_bounds = self.instrument.upper_bounds + self.upper_bounds
            if exclude_wavelength_from_fit is not None:                
                for exclude in exclude_wavelength_from_fit:
                    w_indices_exclude = np.arange(*np.sort(utils.val2idx(self.instrument.wavelength,exclude)))
                    valid_w_indices = np.setdiff1d(np.arange(1024),w_indices_exclude)
                    print(len(valid_w_indices))
                    print(len(self.valid))
                    self.valid = np.intersect1d(self.valid,valid_w_indices)
                    print(len(self.valid))
                ydata = self.experiment_data[self.valid] - self.background_guess
            #logger.debug("guess {}".format(self.guess))
            #logger.debug("lower_bounds: {}".format(self.lower_bounds))
            #logger.debug("upper_bounds: {}".format(self.upper_bounds))
            f = self.build_spectrum_wavelength_calibration
            self.fit_parameters,self.pcov = curve_fit(f=f,xdata=self.instrument.x[self.valid],ydata=ydata,p0=self.guess,bounds=(self.lower_bounds,self.upper_bounds),sigma=sigma)
        return self.fit_parameters
        def display_spectrum(self,show=True,experiment=True,guess=True,fit=True,background_guess=True,highlight_tags=[],special_tags=[],debug_tags=None,debug_fit_parameters=None,show_positions=False,
                            scale="linear",fontsize=14,xlim=None,ylim=None,xlabel="Wavelength [nm]",ylabel="Intensity [a.u.]",title=None,title_box=None,title_box_placement="upper left",legend_loc="best",ncol=1,save_path=None):
            """Displaying the data saved in the analysis-object
            
            Args:
                experiment (bool, optional): Should experiment data be shown?. Defaults to True.
                guess (bool, optional): Should guess be shown?. Defaults to True.
                fit (bool, optional): Should fit be shown?. Defaults to True.
                background_guess (bool, optional): Should background guess position be shown?. Defaults to True.
                highlight_tags (list, optional): Which ions should be highlighted in spectrum?. Defaults to [].
                fontsize (int, optional): Make your fontsizes nice and big. Defaults to 14.
                xlabel (str, optional): Label on x axis. Defaults to "Wavelength [nm]".
                ylabel (str, optional): Label on y axis. Defaults to "Intensity [a.u.]".
                title (_type_, optional): Title over graph. Defaults to None.
            """
            plt.rcParams.update({'font.size': fontsize})
            fig,ax = plt.subplots(1,1,figsize=(8,6)) #plt.figure(figsize=(8,6))
            
            if self.mode == "default":
                f = self.build_spectrum
                x = self.instrument.wavelength
                wavelength = self.instrument.wavelength
            if self.mode == "wavelength_calibration":
                f = self.build_spectrum_wavelength_calibration
                x = self.instrument.x
                wavelength = self.instrument.wavelength
            if self.mode == "wavelength_shift":
                f = self.build_spectrum_wavelength_shift
                wavelength = self.instrument.wavelength - self.instrument.shift_guess
                x = self.instrument.wavelength
            if self.mode == "variable_width_and_shift":
                f = self.build_spectrum_variable_width_and_shift
                wavelength = self.instrument.wavelength - self.fit_parameters[0]
                x = self.instrument.wavelength
            
            if background_guess:
                if type(self.model.recommended_background_position) is list:
                    for background_position in self.model.recommended_background_position:
                        plt.axvline(background_position,ls="--",c="r",label="BG")
                else:
                    plt.axvline(self.model.recommended_background_position,ls="--",c="r",label="BG")
            if experiment:
                plt.plot(wavelength[self.valid],self.experiment_data[self.valid],"+-",lw=1,ms=10,label="Meas.",c="black")
            if guess:
                plt.plot(wavelength,self.background_guess+f(x,*self.guess),label="Guess",c="grey")
            if fit:
                plt.plot(wavelength,self.background_guess+f(x,*self.fit_parameters),label="Sum. Fit",c=colors[0])        
            if not debug_tags is None:
                for i_debug,debug_tag in enumerate(debug_tags):
                    element,ion=utils.split_tag(debug_tag)
                    plt.plot(wavelength,self.background_guess+f(x,*debug_fit_parameters[i_debug],filter=debug_tag),c=element_colors[element],ls=ion_styles[ion],lw=line_width)
            special_tag_counter = 0
            for highlight in highlight_tags:
                element,ion=utils.split_tag(highlight)
                if highlight in special_tags:
                    plt.plot(wavelength,self.background_guess+f(x,*self.fit_parameters,filter=highlight),label=highlight,c=special_colors[special_tag_counter],ls=ion_styles[ion],lw=line_width)
                    special_tag_counter+=1
                else:
                    plt.plot(wavelength,self.background_guess+f(x,*self.fit_parameters,filter=highlight),label=highlight,c=element_colors[element],ls=ion_styles[ion],lw=line_width)
            if show_positions:
                for m in self.model.multiplets:
                    for i,p in enumerate(m.positions):
                        plt.axvline(ymin=0,ymax=m.branching_ratios[i],x=p)
                for bl in self.model.balmer_lines:
                    number_lines = len(bl.lines)
                    for nl in range(number_lines):
                        for i,p in enumerate(bl.positions[nl]):
                            plt.axvline(ymin=0,ymax=bl.branching_ratios[nl,i],x=p)
            for i_gap in range(len(self.valid)-1):
                if np.abs(self.valid[i_gap] -self.valid[i_gap+1]) > 1:
                    print(self.valid[i_gap],self.valid[i_gap+1])
                    xmin = np.nanmin([wavelength[self.valid[i_gap]],wavelength[self.valid[i_gap+1]]])
                    xmax = np.nanmax([wavelength[self.valid[i_gap]],wavelength[self.valid[i_gap+1]]])
                    print(xmin,xmax)
                    plt.axvspan(xmin=xmin,xmax=xmax,color="grey",alpha=0.2)
                
            if xlim is not None:
                if self.instrument.wavelength[0]>self.instrument.wavelength[1]:
                    xlim_indices = slice(*list(np.flip(utils.val2idx(self.instrument.wavelength,xlim))))
                else:
                    xlim_indices = slice(*utils.val2idx(self.instrument.wavelength,xlim))
                plt.xlim(*xlim)
                ylim_from_xlim = [0.9*self.background_guess,1.1*np.nanmax(self.experiment_data[xlim_indices])]
                plt.ylim(*ylim_from_xlim)
                if ylim is not None:
                    plt.ylim(*ylim)
            else: 
                plt.xlim(np.nanmin(wavelength),np.nanmax(wavelength))
                ylim_default = [0.9*self.background_guess,1.1*np.nanmax(self.experiment_data[self.valid])]
                plt.ylim(*ylim_default)
                if ylim is not None:
                    if ylim[0] is None:
                        ylim[0] = ylim_default[0]
                    if ylim[1] is None:
                        ylim[1] = ylim_default[1]
                    plt.ylim(*ylim)                          
            plt.title(title)
            if title_box is not None:
                if title_box_placement == "upper left":
                    box_coord = [0.03, 0.96]
                if title_box_placement == "upper right":
                    box_coord = [0.68, 0.96]
                props = dict(boxstyle='round', facecolor='grey', alpha=0.5)
                ax.text(*box_coord, title_box, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            
            
            # Create a dictionary to store unique labels
            handles, labels = plt.gca().get_legend_handles_labels()
            unique_labels = dict()

            # Filter out duplicate labels
            for handle, label in zip(handles, labels):
                if label not in unique_labels:
                    unique_labels[label] = handle
            plt.legend(unique_labels.values(),unique_labels.keys(),loc=legend_loc,ncol=ncol,columnspacing=0.5)
            plt.grid()
            plt.tight_layout()
            plt.yscale(scale)
            if save_path is not None:
                plt.tight_layout()
                plt.savefig(save_path+"_"+scale+".pdf")
                plt.savefig(save_path+"_"+scale+".png",dpi=400)
            if show:
                    plt.show()
            else:
                plt.close()
    def display_spectrum(self, show=True, experiment=True, guess=True, fit=True, background_guess=True, highlight_tags=[], special_tags=[], debug_tags=None, debug_fit_parameters=None, show_positions=False,
                        scale="linear", fontsize=14, xlim=None, ylim=None, xlabel="Wavelength [nm]", ylabel="Intensity [a.u.]", title=None, title_box=None, title_box_placement="upper left", legend_loc="best", ncol=1, save_path=None,add_spectra=None):
        """Displaying the data saved in the analysis-object

        Args:
            experiment (bool, optional): Should experiment data be shown?. Defaults to True.
            guess (bool, optional): Should guess be shown?. Defaults to True.
            fit (bool, optional): Should fit be shown?. Defaults to True.
            background_guess (bool, optional): Should background guess position be shown?. Defaults to True.
            highlight_tags (list, optional): Which ions should be highlighted in spectrum?. Defaults to [].
            fontsize (int, optional): Make your fontsizes nice and big. Defaults to 14.
            xlabel (str, optional): Label on x axis. Defaults to "Wavelength [nm]".
            ylabel (str, optional): Label on y axis. Defaults to "Intensity [a.u.]".
            title (_type_, optional): Title over graph. Defaults to None.
        """
        plt.rcParams.update({'font.size': fontsize})
        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        
        if self.mode == "default":
            f = self.build_spectrum
            x = self.instrument.wavelength
            wavelength = self.instrument.wavelength
        elif self.mode == "wavelength_calibration":
            f = self.build_spectrum_wavelength_calibration
            x = self.instrument.x
            wavelength = self.instrument.wavelength
        elif self.mode == "wavelength_shift":
            f = self.build_spectrum_wavelength_shift
            wavelength = self.instrument.wavelength - self.instrument.shift_guess
            x = self.instrument.wavelength
        elif self.mode == "variable_width_and_shift":
            f = self.build_spectrum_variable_width_and_shift
            wavelength = self.instrument.wavelength - self.fit_parameters[0]
            x = self.instrument.wavelength
        
        if background_guess:
            if isinstance(self.model.recommended_background_position, list):
                for background_position in self.model.recommended_background_position:
                    ax.axvline(background_position, ls="--", c="r", label="BG")
            else:
                ax.axvline(self.model.recommended_background_position, ls="--", c="r", label="BG")
        
        if experiment:
            ax.plot(wavelength, self.experiment_data, "+-", lw=1, ms=10, label="Meas.", c="black")
        try:
            if self.wavelength_to_fit is not None:
                #xmin = utils.val2idx(wavelength,self.wavelength_to_fit[0])
                #xmax = utils.val2idx(wavelength,self.wavelength_to_fit[1])
                xmin = self.wavelength_to_fit[0]
                xmax = self.wavelength_to_fit[1]
                print(xmin,xmax)
                ax.axvspan(xmin=xmin, xmax=xmax, color="green", alpha=0.2)
        except:
            pass
        if guess:
            ax.plot(wavelength, self.background_guess + f(x, *self.guess), label="Guess", c="grey")
        
        if fit:
            ax.plot(wavelength, self.background_guess + f(x, *self.fit_parameters), label="Sum. Fit", c=colors[0])        
        
        if debug_tags is not None:
            for i_debug, debug_tag in enumerate(debug_tags):
                element, ion = utils.split_tag(debug_tag)
                ax.plot(wavelength, self.background_guess + f(x, *debug_fit_parameters[i_debug], filter=debug_tag), c=element_colors[element], ls=ion_styles[ion], lw=line_width)
        
        special_tag_counter = 0
        for highlight in highlight_tags:
            element, ion = utils.split_tag(highlight)
            color = special_colors[special_tag_counter] if highlight in special_tags else element_colors[element]
            if highlight in special_tags:
                special_tag_counter += 1
            ax.plot(wavelength, self.background_guess + f(x, *self.fit_parameters, filter=highlight), label=highlight, c=color, ls=ion_styles[ion], lw=line_width)
        
        if show_positions:
            for m in self.model.multiplets:
                for i, p in enumerate(m.positions):
                    ax.axvline(ymin=0, ymax=m.branching_ratios[i], x=p)
            for bl in self.model.balmer_lines:
                number_lines = len(bl.lines)
                for nl in range(number_lines):
                    for i, p in enumerate(bl.positions[nl]):
                        ax.axvline(ymin=0, ymax=bl.branching_ratios[nl, i], x=p)
        
        for i_gap in range(len(self.valid) - 1):
            if np.abs(self.valid[i_gap] - self.valid[i_gap + 1]) > 1:
                xmin = np.nanmin([wavelength[self.valid[i_gap]], wavelength[self.valid[i_gap + 1]]])
                xmax = np.nanmax([wavelength[self.valid[i_gap]], wavelength[self.valid[i_gap + 1]]])
                ax.axvspan(xmin=xmin, xmax=xmax, color="grey", alpha=0.2)
            
        if xlim is not None:
            xlim_indices = slice(*list(np.flip(utils.val2idx(self.instrument.wavelength, xlim))) if self.instrument.wavelength[0] > self.instrument.wavelength[1] else utils.val2idx(self.instrument.wavelength, xlim))
            ax.set_xlim(*xlim)
            ylim_from_xlim = [0.9 * self.background_guess, 1.1 * np.nanmax(self.experiment_data[xlim_indices])]
            ax.set_ylim(*ylim_from_xlim)
            if ylim is not None:
                ax.set_ylim(*ylim)
        else:
            ax.set_xlim(np.nanmin(wavelength), np.nanmax(wavelength))
            ylim_default = [0.9 * self.background_guess, 1.1 * np.nanmax(self.experiment_data[self.valid])]
            ax.set_ylim(*ylim_default)
            if ylim is not None:
                if ylim[0] is None:
                    ylim[0] = ylim_default[0]
                if ylim[1] is None:
                    ylim[1] = ylim_default[1]
                ax.set_ylim(*ylim)                          
        
        ax.set_title(title)
        if title_box is not None:
            box_coord = [0.03, 0.96] if title_box_placement == "upper left" else [0.68, 0.96]
            props = dict(boxstyle='round', facecolor='grey', alpha=0.5)
            ax.text(*box_coord, title_box, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)
        
        if add_spectra is not None:
            for i in range(len(add_spectra)):
                plt.plot(wavelength,add_spectra[i][0],label=add_spectra[i][1])
        
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        
        # Create a dictionary to store unique labels
        handles, labels = ax.get_legend_handles_labels()
        unique_labels = dict()

        # Filter out duplicate labels
        for handle, label in zip(handles, labels):
            if label not in unique_labels:
                unique_labels[label] = handle
        ax.legend(unique_labels.values(), unique_labels.keys(), loc=legend_loc, ncol=ncol, columnspacing=0.5)
        ax.grid()
        fig.tight_layout()
        ax.set_yscale(scale)
        
        if save_path is not None:
            fig.tight_layout()
            fig.savefig(save_path + "_" + scale + ".pdf")
            fig.savefig(save_path + "_" + scale + ".png", dpi=400)
        
        if show:
            plt.show()
        else:
            plt.close()

        
    def set_fit_description(self):
        """This method sets the fit_description of the analysis-object
        This is a list of strings, which describes the variable parameters of the analysis
        """
        self.fit_description = []
        if self.instrument.wavelength_coefficients_guess is not None:
            for i in range(len(self.instrument.wavelength_coefficients_guess)):
                self.fit_description.append("wavelength_coefficient_"+str(i))
        if self.instrument.shift_guess is not None:
            self.fit_description.append("Wavelength_shift")
        if self.instrument.width_guess is not None:
            self.fit_description.append("Width instrument")
        for m in self.model.multiplets:
            self.fit_description.append("Intensity_{}_{:.3f}".format(m.tag,np.sum(np.array(m.positions)*(m.branching_ratios))))
            if m.fit_broadening:
                self.fit_description.append("Doppler_temperature_{}_{:.3f}".format(m.tag,np.sum(np.array(m.positions)*(m.branching_ratios))))
        for bl in self.model.balmer_lines:
            for i in range(len(bl.lines)):
                self.fit_description.append("Intensity_"+bl.lines[i])
            if len(bl.lines)>1:
                self.fit_description.append("Stark_density")
            else:
                self.fit_description.append("Stark_density_"+bl.lines[i])
            if bl.fit_broadening:
                self.fit_description.append("Doppler_temperature"+bl.lines[i])
            if bl.fit_doppler_shift:
                self.fit_description.append("Doppler_shift_"+bl.lines[i])
        for pec in self.model.pec_lines:
            if pec.fit_electron_density:
                self.fit_description.append("Electron_density_"+pec.tag)
            if pec.fit_electron_temperature:
                self.fit_description.append("Electron_temperature_"+pec.tag)
            if pec.fit_impurity_density:
                self.fit_description.append("Impurity_density_"+pec.tag)
    def format_fit_output(self):
        table = {}
        table["fit_description"] = self.fit_description
        table["fit_parameters"] = self.fit_parameters
        table["fit_parameters_linear"] = []
        for i,parameter in enumerate(self.fit_description):
            if "temperature" in parameter or "wavelength" in parameter or "Wavelength" in parameter or "Width" in parameter or "shift" in parameter:
                table["fit_parameters_linear"].append(self.fit_parameters[i])
            else:
                table["fit_parameters_linear"].append(10**self.fit_parameters[i])
        self.fit_parameters_linear = table["fit_parameters_linear"]
        table["guess"] = self.guess
        table["lower_bounds"] = self.lower_bounds
        table["upper_bounds"] = self.upper_bounds
        self.fit_output = pd.DataFrame(table)
    
def fit_discharge_brightest_channel(model,ds_qss,pid,camid,port,special_tags,xlim,intensity_label,exclude_channels,strikeline,signal_type,analysis_start,analysis_end,analysis_step,ylim=None,scales=["linear","log"],instrument_function_mode="gauss",numeric_instrument_function=None,strongest_line=None,c_idx_fixed=None,channel_shift=None,show=True,highlight_tags=[],debug=False,save_path=None):
    output = {}
    output["time"] = []
    output["channels"] = []
    output["fit_parameters"] = {}
    inst = instrument.Instrument(wavelength=ds_qss.data["wavelength_nm"][10],width=0.04)
    analyse = Analysis(model=model,instrument=inst)
    output["fit_description"] = analyse.fit_description
    for parameter in output["fit_description"]:
        output["fit_parameters"][parameter] = []
    for i in range(int((analysis_end-analysis_start)/analysis_step)):
        progress_percentage = (i + 1) / int((analysis_end-analysis_start)/analysis_step) * 100

        # Log the progress in 10 percent steps
        if progress_percentage % 10 == 0:
            logger.info(f"Progress: {progress_percentage:.0f}%")
        t = analysis_start+i*analysis_step
        t_to = analysis_start+(i+1)*analysis_step
        t_idx = utils.val2idx(ds_qss.data["time_s"],t)
        t_idx_to = utils.val2idx(ds_qss.data["time_s"],t_to)
        experiment_data_all_channels = np.zeros(27)*np.nan
        if c_idx_fixed is None: # searches channel with strongest signal at "strongest_line" position
            for c_idx in range(len(ds_qss.data["wavelength_nm"])):
                if c_idx in ds_qss.broken_channels or c_idx in exclude_channels[strikeline]:
                    experiment_data_all_channels[c_idx] = np.nan
                else:
                    w_idx = utils.val2idx(ds_qss.data["wavelength_nm"][c_idx],strongest_line)
                    experiment_data_all_channels[c_idx] = np.nanmean(ds_qss.data[signal_type][t_idx:t_idx_to+1,c_idx,w_idx-10:w_idx+11])
            c_idx = np.nanargmax(experiment_data_all_channels)
            if channel_shift is not None:
                c_idx = c_idx + channel_shift
                    
        else:
            c_idx = c_idx_fixed
        output["channels"].append(c_idx) 
        output["time"].append((t+t_to)/2)
        if c_idx in ds_qss.broken_channels or c_idx < 0 or c_idx > 26:
            for parameter_idx,parameter in enumerate(output["fit_description"]):
                output["fit_parameters"][parameter].append(np.nan)
            logger.debug("Skipping channel {}".format(c_idx))
        else:
            experiment_data=np.nanmean(ds_qss.data[signal_type][t_idx:t_idx_to+1,c_idx],0)
            wavelength=ds_qss.data["wavelength_nm"][c_idx]
            width = ds_qss.instrument_function[c_idx]*(np.abs(wavelength[0]-wavelength[-1])/len(wavelength))
            if numeric_instrument_function is not None:
                    numeric_instrument_function_channel = numeric_instrument_function[c_idx]
            else:
                numeric_instrument_function_channel = None
                
            inst = instrument.Instrument(wavelength=wavelength,width=width,instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function_channel)
            analyse = Analysis(model=model,instrument=inst)
            t_idx = utils.val2idx(ds_qss.data["time_s"],t)
            t_idx_to = utils.val2idx(ds_qss.data["time_s"],t_to)
            title = "{} {} {} {:0.1f} - {:0.1f} s Ch. {} {} {}".format(pid,camid,port,t,t_to,c_idx,strikeline[:3],instrument_function_mode[:3])
            experiment_data=np.nanmean(ds_qss.data[signal_type][t_idx:t_idx_to+1,c_idx],0)
            analyse.fit_spectrum(experiment_data=experiment_data)
            show_positions = False
            for scale in scales:
                analyse.display_spectrum(highlight_tags=highlight_tags,special_tags=special_tags,show_positions=show_positions,guess=False,xlim=xlim,ylabel=intensity_label,title=title,ylim=ylim,scale=scale,show=show,save_path=save_path+title+".png")
            for parameter_idx,parameter in enumerate(output["fit_description"]):
                output["fit_parameters"][parameter].append(analyse.fit_parameters[parameter_idx])
    for parameter in output["fit_description"]:
        output["fit_parameters"][parameter] = np.array(output["fit_parameters"][parameter])
    output["channels"] = np.array(output["channels"])
    output["time"] = np.array(output["time"])
    return output

def fit_profile(model,ds_qss,pid,camid,port,special_tags,xlim,intensity_label,scale,channels,signal_type,analysis_start,analysis_end,analysis_step,debug=False,instrument_function_mode="gauss",numeric_instrument_function=None,):
    output = {}
    output["time"] = []
    output["channels"] = channels
    output["fit_parameters"] = {}
    inst = instrument.Instrument(wavelength=ds_qss.data["wavelength_nm"][10],width=0.04)
    analyse = Analysis(model=model,instrument=inst)
    output["fit_description"] = analyse.fit_description
    
    for i in range(int((analysis_end-analysis_start)/analysis_step)):
        t = analysis_start+i*analysis_step
        t_to = analysis_start+(i+1)*analysis_step
        output["time"].append((t+t_to)/2)
    output["time"] = np.array(output["time"])
    for parameter in output["fit_description"]:
        output["fit_parameters"][parameter] = np.zeros((len(output["time"]),len(output["channels"])))*np.nan
    for i in range(int((analysis_end-analysis_start)/analysis_step)):
        t = analysis_start+i*analysis_step
        t_to = analysis_start+(i+1)*analysis_step
        t_idx = utils.val2idx(ds_qss.data["time_s"],t)
        t_idx_to = utils.val2idx(ds_qss.data["time_s"],t_to)
        for c_idx in channels: 
            if c_idx in ds_qss.broken_channels:
                pass
            else:
                experiment_data=np.nanmean(ds_qss.data[signal_type][t_idx:t_idx_to+1,c_idx],0)
                wavelength=ds_qss.data["wavelength_nm"][c_idx]
                width = ds_qss.instrument_function[c_idx]*(np.abs(wavelength[0]-wavelength[-1])/len(wavelength))
                if numeric_instrument_function is not None:
                    numeric_instrument_function_channel = numeric_instrument_function[c_idx]
                else:
                    numeric_instrument_function_channel = None
                inst = instrument.Instrument(wavelength=wavelength,width=width,instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function_channel)
                analyse = Analysis(model=model,instrument=inst)
                t_idx = utils.val2idx(ds_qss.data["time_s"],t)
                t_idx_to = utils.val2idx(ds_qss.data["time_s"],t_to)
                title = "{} {} {} {:0.1f} - {:0.1f} s Ch. {} ".format(pid,camid,port,t,t_to,c_idx)
                highlight_tags = model.all_tags
                experiment_data=np.nanmean(ds_qss.data[signal_type][t_idx:t_idx_to+1,c_idx],0)
                analyse.fit_spectrum(experiment_data=experiment_data)
                show_positions = True
                if debug:
                    analyse.display_spectrum(highlight_tags=highlight_tags,special_tags=special_tags,show_positions=show_positions,guess=False,xlim=xlim,ylabel=intensity_label,title=title,scale=scale)
                for parameter_idx,parameter in enumerate(output["fit_description"]):
                    output["fit_parameters"][parameter][i,c_idx] = analyse.fit_parameters[parameter_idx]    
    for parameter in output["fit_description"]:
        output["fit_parameters"][parameter] = np.array(output["fit_parameters"][parameter])
    output["channels"] = np.array(output["channels"])
    return output

def sum_close_branching_ratios(positions, branching_ratios, difference,debug=False):
    """This function takes a intensity distribution of a multiplet and sums multiplets together, when they single peaks are closer than the differnce paramter

    Args:
        positions (list): positions of the multiplet
        branching_ratios (list): intensity branching ratios of the multiplet
        difference (float): clearance between all parts of the multiplet

    Returns:
        averaged_positions (list): the wavelength that represents the center of mass for the summed multiplet
        summed_branching_ratios (list): the summed intensity branching ratios that are close enough together
    """
    if debug:
        print(positions,branching_ratios,difference)
    # initialize all parameters
    sorted_positions = sorted(zip(positions,branching_ratios))
    grouped_branching_ratios = []
    grouped_positions = []
    previous_position = sorted_positions[0][0]
    current_group = []
    for position, branching_ratio in sorted_positions:
        if np.abs(position-previous_position)<difference:
            current_group.append((position,branching_ratio))
        else:
            grouped_branching_ratios.append([br[1] for br in current_group])
            grouped_positions.append(np.average([br[0] for br in current_group],weights=[br[1] for br in current_group]))
            current_group = [(position,branching_ratio)]
        previous_position = position
    grouped_branching_ratios.append([br[1] for br in current_group])
    grouped_positions.append(np.average([br[0] for br in current_group],weights=[br[1] for br in current_group]))
    summed_branching_ratios = [np.sum(group) for group in grouped_branching_ratios]
    averaged_positions = np.array(grouped_positions)
    return averaged_positions,summed_branching_ratios

def load_fits_of_discharge(pid,camid,cw,port,average,version):
    data = {}
    campaign = "OP2.1"
    path = "{}qss_analysis/discharges/{}/{}/{}/fit_parameters_camid{}_cw{}_port{}_average{}ms_version{}.json".format(ground_path,campaign,pid[:8],pid,camid,cw,port,average,version)
    with open(path) as f:
            data = json.load(f)
            data["time_s"] = np.array(data["time_s"])
            for parameter in data["fit_description"]:
                data["fit_parameters"][parameter] = np.array(data["fit_parameters"][parameter])
    return data
def plot_fit_parameter2d(data,fit_parameter):
    plt.figure(figsize=(8,6))
    extent = [data["time_s"][0],data["time_s"][-1],-0.5,26.5]
    plt.imshow(data["fit_parameters"][fit_parameter].T,aspect="auto",interpolation="none",origin="lower",extent=extent)
    plt.xlabel("Time [s]")
    plt.ylabel("Channels")
    plt.title("{} {} {} {}".format(fit_parameter,data["pid"],data["camid"],data["port"]))
    cbar = plt.colorbar()
    cbar.set_label('Intensity [Photons/s/m$^2$/sr]', rotation=90,labelpad=15)
    plt.show()