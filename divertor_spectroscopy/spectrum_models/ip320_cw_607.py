import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_607(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman,stark_density=True,variable_width=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.03
        self.variable_width = variable_width
        if False: # use instrument function calibration
            with open(ground_path + "qss_analysis/json_files/instrument_function_calibration.json") as f:
                self.instrument_function = json.load(f)["IP320_2"]["30um"]["width"]
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 649
        # Multiplets
        # how do I display the fulcher band?
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()