import importlib
import json

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Soprawega_cw_434(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self,zeeman=False,stark_density=False,exclude_wavelength_from_fit=None,special_tags=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.recommended_background_position = 434.45
        self.strongest_line = 434.047
        #self.strongest_line = 441.4905
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.3
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets

        #self.add_multiplet_to_model(
        #    positions=[432.556], branching_ratios=[1], intensity=1e17, doppler_temperature = 6, tag="CIII")
        #line_name = "O-II-4341"
        #self.add_multiplet_to_model(
        #    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        #line_name = "O-II-4350"
        #self.add_multiplet_to_model(
        #    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        #line_name = "O-II-4418"
        
        #self.add_multiplet_to_model(
        #    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        #self.add_multiplet_to_model(
        #    positions=[433.70705 ], branching_ratios=[1], intensity=1e17, doppler_temperature = 3, tag="ArII")
        #self.add_multiplet_to_model(
        #    positions=[434.5560], branching_ratios=[1], intensity=1e17, doppler_temperature = 3, tag="OII")
        if not stark_density:
            line_name = "H-I-4341"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 1, tag="HI")
        # Balmer lines
        if zeeman:
            magnetic_field = self.magnetic_field
        else:
            magnetic_field = None
        if stark_density:
            self.add_balmer_lines_to_model(
                lines=["H_gamma"], intensities=[1e18], electron_density=1e19, electron_temperature=3, doppler_temperature=1, tag="HI", magnetic_field=magnetic_field, angle_to_magnetic_field=self.angle_to_magnetic_field,fit_doppler_shift=False,fit_broadening=False)

        self.clear_tag_multiplicity()
        self.get_all_model_parameters()