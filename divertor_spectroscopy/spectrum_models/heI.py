import matplotlib.pyplot as plt
import numpy as np
from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import spectrum


class HeI(spectrum.Spectrum):
    """
    Spectrum is the used to analyze spectra
    Expects calibrated spectra which can now be analyzed
    """

    def __init__(self, wavelength, width=1, background=0, debug=False, cache=False, save=False):
        lines = np.array([
            # 2723.19,
            # 2763.80,
            # 2818.2,
            # 2945.11,
            # 3013.7,
            # 3354.55,
            # 3447.59,
            # 3587.27,
            # 3613.64,
            # 3634.23,
            # 3705.00,
            # 3732.86,
            3819.607,
            3888.6046,
            3964.729,
            # 4009.27,
            # 4026.191,
            4026.36,
            4120.82,
            # 4120.99,
            4143.76,
            4387.929,
            4437.55,
            4471.479,
            # 4471.68,
            4713.146,
            # 4713.38,
            4921.931,
            5015.678,
            5047.74,
            5875.6148,
            # 5875.6404,
            # 5875.9663,
            6678.1517,
            # 6867.48,
            # 7065.1771,  # to close resolve
            # 7065.2153,
            7065.7086,
            7281.35,
            # 7816.15,
            # 8361.69,
            # 9063.27,
            # 9210.34,
            # 9463.61,
            # 9516.60,
            # 9526.17,
            # 9529.27,
            # 9603.42,
            # 9702.60
        ]) / 10
        self.lines = lines
        self.background = background
        self.wavelength = wavelength
        self.multiplets = []
        self.lorentzians = []
        self.voigtians = []
        self.width = width
        for line in lines:
            self.add_multiplet_to_model(positions=[line], branching_ratios=[
                                        1], intensity=1, width=self.width, tag="HeI")
