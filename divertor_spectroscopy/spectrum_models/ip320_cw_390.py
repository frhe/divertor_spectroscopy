import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_390(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman,helium=True):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.045
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        # He
        if helium:
            self.add_multiplet_to_model(
                positions=[383.3554], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
            self.add_multiplet_to_model(
                positions=[388.8648], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
            self.add_multiplet_to_model(
                positions=[396.47291], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
            
        # C
        self.add_multiplet_to_model(
            positions=[383.1726], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[387.619], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[387.641], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[387.666], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[391.898], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[392.069], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        # N
        self.add_multiplet_to_model(
            positions=[399.5], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[383.837], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[391.900], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[395.585], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[399.863, 400.358], branching_ratios=[0.375, 0.625], intensity=1e17, width=self.width, tag="NIII")
        # O
        line_name = "O-II-3967"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, width=self.width, tag="OII")
    
        # Balmer lines
        if zeeman:
            magnetic_field = self.magnetic_field
        else:
            magnetic_field = None
        self.add_balmer_lines_to_model(
            lines=["H_eta", "H_zeta", "H_epsilon"], intensities=[0.1e17, 0.2e17, 0.5e17], electron_density=1e20, electron_temperature=3, width_gauss=self.width, tag="HI", magnetic_field=magnetic_field, angle_to_magnetic_field=self.angle_to_magnetic_field)

        self.build_popt_description()
        self.clear_tag_multiplicity()