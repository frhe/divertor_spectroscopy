import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320fzj_cw_405(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman,he_main_gas=False,argon_seeding=False,pec_lines=True,stark_density=True,variable_width=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.03
        self.variable_width = variable_width
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 401.68
        # Multiplets
        
        # He
        # There are different amounts of Helium in the spectrum. This can range from very little, over He-Beam up to he-main-gas plasmas
        # For he-main gas you might want to switch all available lines on!
        if False:
            self.add_multiplet_to_model(
                positions=[396.47291], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
        line_name = "He-I-4026"
        if he_main_gas:
             self.add_multiplet_to_model(
                positions=[397.2015], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"],
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
            intensity=1e17,width=self.width, tag="HeI")
        if he_main_gas:
            line_name = "He-I-4121"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"],
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,width=self.width, tag="HeI")
            self.add_multiplet_to_model(
                positions=[414.3761], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
        
            # B
        if False:
            line_name = "B-II-4122"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="BII")
            
        # C
        # double excited CII line - branching ratios calculated by statistical weights
        self.add_multiplet_to_model(
                positions=[400.9882,401.7272,402.1166], branching_ratios=[0.5021,0.3324,0.1655], intensity=1e17, width=self.width, tag="CII")
        line_name = "C-II-4075" 
        # core electrons have one excited electron, which I had to workaround in John Heys Zeeman code
        # validated with Sopra in experiment 20181018.038
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="CII")
        line_name = "C-III-4069"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="CIII")
        
        # N
        # The NI line at 410 and 411 nm is prominent but not seen in the spectrum. Come back to this, when nitrogen is in the machine!
        if not pec_lines:
            self.add_multiplet_to_model(positions=[399.5], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
            self.add_multiplet_to_model(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, width=self.width, tag="NII")
        if False: # double excited line: ratio is measured, statistical weigths are similar: 0.2019,0.3336,0.4620
            self.add_multiplet_to_model(positions=[412.408,413.367,414.577],branching_ratios=[0.192,0.303,0.505],intensity=1e17,width=self.width,tag="NII") # double excited
        #self.add_multiplet_to_model(
        #        positions=[407.304, 407.691, 408.227,408.730], branching_ratios=[0.403, 0.131, 0.371,0.095],
        #        intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[399.863, 400.358], branching_ratios=[0.375, 0.625], intensity=1e17, width=self.width, tag="NIII")
        line_name = "N-III-4099"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="NIII")
        if False:
            self.add_multiplet_to_model(
                positions=[413.163,413.383,414.613,414.678], branching_ratios=[0.157,0.509,0.208,0.126],
                intensity=1e17, width=self.width, tag="NIII")
        
            # O
        if False:
            line_name = "O-II-3967"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        line_name = "O-II-4075"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        if False:
            line_name = "O-II-4152"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
            
        if False: # This might exist, but is extremely hard to fit because it lies in NIII, Balmer cluster!
            line_name = "O-II-4111"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        if False:
            self.add_multiplet_to_model(positions=[411.942], branching_ratios=[1], intensity=1e17, width=self.width, tag="OV")
        # Ar
        if not pec_lines:
            if not argon_seeding:
                self.add_multiplet_to_model(
                    positions=[401.3856], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
            else:
                #self.add_multiplet_to_model(
                #    positions=[396.83589 ], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                #self.add_multiplet_to_model(
                #    positions=[397.44764], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                #self.add_multiplet_to_model(
                #    positions=[397.47582], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                #self.add_multiplet_to_model(
                #    positions=[397.93554], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[401.3856], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[407.20043], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[409.94554], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[410.38323], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[410.39118], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII")
        if pec_lines:
            # PEC lines
            debug=False
            cache=True
            save = False
            multiplets = []
            data = adf15.ADF15()
            fn = ground_path + 'adas/adf15/dcg01_ic_pju#ar1.dat'
            data.read(fn,v=1,debug=False,headerlength=8)
            indices = []
            for idx in range(100):
                    if utils.wl_vac_to_air(data.dat.wvl[idx])/10 > 395 and utils.wl_vac_to_air(data.dat.wvl[idx])/10 < 417:
                        multiplets.append(spectral_line_classes.Multiplet(positions=np.array([utils.wl_vac_to_air(data.dat.wvl[idx])])/10,
                                                                    intensity=1e17,branching_ratios=[1],width=self.width,tag="ArII"))
                        indices.append(idx)
            print(indices)
            print(len(multiplets))
            ne_count = 100
            Te_count = 101
            ne_range = [5e19,1e21]
            Te_range = [1,10]
            electron_density = 1e20
            electron_temperature = 3
            impurity_density = 1e17
            self.add_pec_lines_to_model(multiplets=multiplets,element="ar",ion=1,indices=indices,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,
                                        Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,
                                        impurity_density=impurity_density,emission_length=0.1,save=save,cache=cache,debug=False)
            element = "n"
            if element == "n":
                if zeeman:
                    line_name = "N-II-3995"
                    multiplets = [spectral_line_classes.Multiplet(self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, width=self.width, tag="NII"),
                        spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, width=self.width, tag="NII"),
                        spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, width=self.width, tag="NII")]
                else:
                    multiplets = [spectral_line_classes.Multiplet(positions=[399.5],branching_ratios=[1], intensity=1e17, width=self.width, tag="NII"),
                        spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, width=self.width, tag="NII"),
                        spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, width=self.width, tag="NII")]
                ne_count = 100
                Te_count = 101
                ne_range = [1e19,1e21]
                Te_range = [1,10]
                electron_density = 1e20
                electron_temperature = 3
                impurity_density = 1e17
                self.add_pec_lines_to_model(multiplets=multiplets,element=element,ion=1,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=0.1,save=save,cache=cache,debug=debug)
        # Balmer lines
        if stark_density:
            magnetic_field = None
            self.add_balmer_lines_to_model(
                lines=["H_delta"], intensities=[1e18], electron_density=1e20,electron_temperature=3, width_gauss=self.width, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
        else:
            self.add_multiplet_to_model(positions=[410.1734], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        self.build_popt_description()
        self.clear_tag_multiplicity()