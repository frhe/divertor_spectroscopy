"""
This is to really fit and apply the NII fits
Functionalities:
    - fit experiment data
    - from that apply line ratios to extract plasma parameters!
"""
import matplotlib.pyplot as plt
import numpy as np
import importlib

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)


class Test_spectrum(spectrum.Spectrum):
    """
    class to test if different line-shapes work
    """

    def __init__(self, wavelength, background=0, debug=False, cache=False, save=False):
        self.background = background
        self.wavelength = wavelength
        self.multiplets = []
        self.lorentzians = []
        self.voigtians = []
        self.add_multiplet_to_model(
            positions=[400], branching_ratios=[1], width=0.04, intensity=10, tag="HI")
        self.add_multiplet_to_model(
            positions=[413], branching_ratios=[1], width=0.04, intensity=10, tag="HI")
        self.add_voigtian_to_model(
            position=399.007, intensity=10, width=0.04, broadening=1, tag="HI")
