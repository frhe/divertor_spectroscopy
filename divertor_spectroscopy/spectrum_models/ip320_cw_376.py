import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_376(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman=False,variable_width=False, pec_lines=False,he_main_gas=False,stark_density=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.045
        self.variable_width = variable_width
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        
         # Multiplets
         # HI - I don't use Stark Broadening code here as I don't have broadening data from Lomanowski paper lower than Balmer Eta (383nm)
        self.add_multiplet_to_model(
            positions=[383.5397], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        self.add_multiplet_to_model(
            positions=[379.7909], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        self.add_multiplet_to_model(
            positions=[377.0633], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        self.add_multiplet_to_model(
            positions=[375.0151], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        # C
        self.add_multiplet_to_model(
            positions=[383.1726], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        
        # O
        line_name = "O-II-3736"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,width=self.width, tag="OII")
        line_name = "O-IV-3734"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,width=self.width, tag="OIV")
        line_name = "O-III-3706"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,width=self.width, tag="OIII")
        line_name = "O-III-3762"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,width=self.width, tag="OIII")
        self.add_multiplet_to_model(
            positions=[381.367],branching_ratios=[1],width=self.width,intensity=1,tag="NeIV")
        
        if not pec_lines:
            line_name = "Ne-II-3713"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,width=self.width, tag="NeII 3s2P-3p2D")
            line_name = "Ne-II-3717"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,width=self.width, tag="NeII 3s2P-3p2D")
            line_name = "Ne-II-3824"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,width=self.width, tag="NeII 3s2P-3p2D")
        
        else:
            # pec lines
            debug = True
            cache = False
            save = False
            element = "ne"
            multiplets = [spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,width=self.width, tag="NeII 3s2P-3p2D"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,width=self.width, tag="NeII 3s4P-3p4P"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3824"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3824"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,width=self.width, tag="NeII 3p2P-3d2D")]
            ne_count = 100
            Te_count = 101
            ne_range = [5e19,1e21]
            Te_range = [1,10]
            electron_density = 1e20
            electron_temperature = 5
            impurity_density = 1e17
            self.add_pec_lines_to_model(multiplets=multiplets,element=element,ion=1,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=0.1,save=save,cache=cache,debug=debug)
        self.build_popt_description()
        self.clear_tag_multiplicity()