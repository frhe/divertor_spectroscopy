import importlib
import json

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Sopraberlin_cw_436_HgAr_calibration(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self,zeeman=False,stark_density=False,exclude_wavelength_from_fit=None,special_tags=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        self.recommended_background_position = 435
        self.strongest_line =434.74945# 435.83350
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        #self.angle_to_magnetic_field = 110
        self.angle_to_magnetic_field = 90
        # Multiplets
        self.add_multiplet_to_model(
                    positions=[434.74945], branching_ratios=[1], intensity=1e17, doppler_temperature=0.1, tag="HgI")
        self.add_multiplet_to_model(
                    positions=[434.5168], branching_ratios=[1], intensity=1e17, doppler_temperature=0.1, tag="ArI")
        
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()