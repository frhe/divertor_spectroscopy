import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
importlib.reload(spectral_line_classes)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_709(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, he_main_gas=False,stark_density=False,zeeman=False,
                 pec_lines=False,neon=False,argon_seeding=False,
                 exclude_wavelength_from_fit=None,special_tags=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.2
        self.variable_width=False
        self.recommended_background_position =700
        self.strongest_line = 667.8151
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        if False:
            self.add_multiplet_to_model(
                positions=[656.279], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HI")
        if True:
            self.add_multiplet_to_model(
                positions=[667.8151], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
            self.add_multiplet_to_model(
                positions=[706.5190], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
            self.add_multiplet_to_model(
                positions=[728.1349], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
        line_name = "B-II-7032"
        self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="BII")
        line_name = "C-III-6741"
        self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=6, tag="CIII")
        if False:
            self.add_multiplet_to_model(
                positions=[661.056], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="NII")
        if False:
            self.add_multiplet_to_model(
                positions=[664.1054], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
            self.add_multiplet_to_model(
                positions=[672.1398], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
            self.add_multiplet_to_model(
                positions=[689.5109], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
        if neon:
            line_name = "Ne-II-7235"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=4, tag="NeII")
            self.add_multiplet_to_model(
            positions=[717.39381], branching_ratios=[1], intensity=1e17, doppler_temperature=1, tag="NeI")
            self.add_multiplet_to_model(
            positions=[689], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="NeIII")
        # pec lines
        if pec_lines:
            debug = True
            cache = False
            save = False
            element = "c"
            line_names = ["C-II-6786","C-II-7118","C-II-7235"]
            multiplets = [spectral_line_classes.Multiplet(
                positions=self.zeeman_lines[line_names[0]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_names[0]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag="CII"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines[line_names[1]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_names[1]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,doppler_temperature=3, tag="CII"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines[line_names[2]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_names[2]][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,doppler_temperature=3, tag="CII")]
            ne_count = 100
            Te_count = 101
            ne_range = [1e18,1e21]
            Te_range = [1,10]
            electron_density = 1e20
            electron_temperature = 4
            impurity_density = 1e17
            self.add_pec_lines_to_model(multiplets=multiplets,element=element,ion=1,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=0.1,save=save,cache=cache,debug=debug)
        else:
            line_name = "C-II-6786"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
            line_name = "C-II-7118"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
            line_name = "C-II-7235"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()