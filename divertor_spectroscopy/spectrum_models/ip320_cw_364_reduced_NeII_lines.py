import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_364_reduced_NeII_lines(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    
    Todo: 
    I want to say in the specral model which lines should be fitted and it will be saved in the "fit_description"
    Options for every line are: 
        - Intensity (default to make it backwards compatible - line intensities/ratios can contain physics information like density or temperature)
        - width (this might contain physics information like temperature or density)
        - position (can also contain physics information)
    I clearly want to seperate pysics information (i.e. density and temperature) from calibration information (like instrument function)
    """

    def __init__(self, zeeman=False,pec_lines_argon=False,pec_lines_neon=False,fit_electron_temperature_neon=True,
                 ne_range_NeII=[5e18,1e20],pec_guess=[1e19,4.1,1e17,0.1],stark_density=False,special_tags=False,
                 cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.zeeman=zeeman
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.recommended_background_position = 357.95#,367.6]
        self.strongest_line = 369.421
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        
        
         # Multiplets
        # He
        self.add_multiplet_to_model(
                        positions=[370.501], branching_ratios=[1], intensity=1e17,doppler_temperature=2,tag="HeI")
        # C
        # there is maybe a CII line at 359 nm but the multiplet info makes no sense
        if False:
            line_name = "C-III-3609"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=6, tag="CIII")
        # O
        
        if True:
            line_name = "O-II-3736"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag="OII")
        if True:
            line_name = "O-III-3706"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=7, tag="OIII")
        if True:
            line_name = "O-IV-3734"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=12, tag="OIV")
        
        
        # Ne
        mode = "default"
        if special_tags:
            self.ne_tags = ["NeII3s2D_3p2F","NeII3s2P_3p2D","NeII3s4P_3p4P"]
        else: 
            self.ne_tags = ["NeII","NeII","NeII","NeII"]
        if not pec_lines_neon:
            branching_ratio_analysis = False
            if branching_ratio_analysis:
                ### This is going to be used to fit the lines of multiplets independently
                positions = [356.85021066, 357.41826136, 357.46121346,364.39289916, 371.30824818, 372.71081181,366.40740524, 369.42144631, 370.96222012, 373.49388158]
                for position in positions:
                    self.add_multiplet_to_model(
                        positions=[position], branching_ratios=[1], intensity=1e17,doppler_temperature=3,fit_broadening=False,  tag="NeII")
            else:
                line_name = "Ne-II-3571"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                    intensity=1e17, doppler_temperature=3,tag=self.ne_tags[0])
                line_name = "Ne-II-3713"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                    intensity=1e17,doppler_temperature=3, tag=self.ne_tags[1])
                line_name = "Ne-II-3717"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                    intensity=1e17,doppler_temperature=3, tag=self.ne_tags[2])
                m = self.multiplets[-1]
                measured_positions = np.array([366.40740524, 369.42144631, 370.96222012, 373.49388158,
                                             375.12458933, 376.62612898, 377.71357821])
                measured_ratios = np.array([0.17404514, 0.43402778, 0.10503472, 0.05251736, 0.01736111,
                                            0.11545139, 0.1015625 ])# these are the ratios measured by Stuart Henderson - mine differ only very slightly
                if False:
                    measured_ratios = np.array([0.17782190369171746,
                                                0.43289656205473404,
                                                0.08441136069921629,
                                                0.04567105320647649,
                                                0.010446500324578983,
                                                0.14285652638272842,
                                                0.10589609364054815]) # What I measured myself
                m.positions,m.branching_ratios=spectrum.redistribute_branching_ratios(predicted_positions=m.positions,predicted_ratios=m.branching_ratios,measured_positions=measured_positions,measured_ratios=measured_ratios)
                
        else:
            if mode == "pec_temperatures_fixed":
                line_name = "Ne-II-3571"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                    intensity=1e17, tag=self.ne_tags[0])
        #line_name = "Ne-II-3559" # not used in PEC
        #self.add_multiplet_to_model(
        #    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
        #    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
        #    intensity=1e17, tag=self.ne_tags[3])

        #Ar
        if not pec_lines_argon:
            ar_positions = [ #354.5627304638842, # ??? # index 21 in pec file
                            #354.58431, # strong
                            # 354.8536498335498, # weak but visible
                             #355.57944867333424, # not visible
                             #355.5994431317305, # not visible
                             #355.69341708619294, # not visible
                             #355.9543447682642, # strong
                             #356.10303, # strongish
                             #356.50519209708125, # weak but visible
                             #357.6648706840646, # strong
                             #358.16273269813166, # these three lines might be difficult to separate
                             #358.2387116400374, # these three lines might be difficult to separate
                             #358.2617052671932, # these three lines might be difficult to separate
                             #358.23544, 
                             #358.8475428982037, # strong
                             #360.591059670358, # weak but visible
                             #362.2156094150545, # weak but visible
                             #363.48425780029754, # weak but visible
                             #365.53069061715524, # weak but visible
                             #367.8300533327257, # # weak but visible
                             #368.2569350194859, # weak but visible
                             #371.719975213719, # weak but visible
                             #371.82061, 
                             372.9336388383723 # strong # index 40 in pec file
                            ]
            for position in ar_positions:
                if True:
                    self.add_multiplet_to_model(
                        positions=[position], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
            
        #self.add_multiplet_to_model(
        #    positions=[356.10303], branching_ratios=[1], intensity=1e17,doppler_temperature=2,  tag="ArII")
        if False:
            self.add_multiplet_to_model(
                positions=[363.78731], branching_ratios=[1], intensity=1e17,doppler_temperature=3.5,  tag="ArIII")

        # W
        if False:
            self.add_multiplet_to_model(
                positions=[361.752], branching_ratios=[1], intensity=1e17,  tag="WI")
            self.add_multiplet_to_model(
                positions=[363.194], branching_ratios=[1], intensity=1e17,  tag="WI")
            self.add_multiplet_to_model(
                positions=[368.806], branching_ratios=[1], intensity=1e17,  tag="WI")
            self.add_multiplet_to_model(
                positions=[364.141], branching_ratios=[1], intensity=1e17,  tag="WII")
        if pec_lines_argon:
            # pec lines
            multiplets = []
            data = adf15.ADF15()
            fn = ground_path + 'adas/adf15/dcg01_ic_pju#ar1.dat'
            data.read(fn,v=1,debug=False,headerlength=8)
            indices = []
            wavelengths = []
            if False: # this includes all lines in pec file, of which some might be not visible
                for idx in range(100):
                        if utils.wl_vac_to_air(data.dat.wvl[idx])/10 > 354 and utils.wl_vac_to_air(data.dat.wvl[idx])/10 < 374:
                            multiplets.append(spectral_line_classes.Multiplet(positions=np.array([utils.wl_vac_to_air(data.dat.wvl[idx])])/10,
                                                                        intensity=1e17,branching_ratios=[1],tag="ArII"))
                            indices.append(idx)
                            wavelengths.append(utils.wl_vac_to_air(data.dat.wvl[idx])/10)
            else:
                ar_positions = [[354.5627304638842,21], # strong # index 21 in pec file
                            [354.8536498335498,22], # weak but visible
                            #[355.57944867333424,23], # not visible but should be bright, this is the reason this is taken out!
                            [355.5994431317305,24], # not visible
                            [355.69341708619294,25], # not visible
                            [355.9543447682642,26], # strong
                            [356.50519209708125,27], # weak but visible
                            [357.6648706840646,28], # strong
                            [358.16273269813166,29], # these three lines might be difficult to separate
                            [358.2387116400374,30], # these three lines might be difficult to separate
                            [358.2617052671932,31], # these three lines might be difficult to separate
                            [358.8475428982037,32], # strong
                            [360.591059670358,33], # weak but visible
                            [362.2156094150545,34], # weak but visible
                            [363.48425780029754,35], # weak but visible
                            [365.53069061715524,36], # weak but visible
                            [367.8300533327257,37], # # weak but visible
                            [368.2569350194859,38], # weak but visible
                            [371.719975213719,39], # weak but visible
                            [372.9336388383723,40] # strong # index 40 in pec file
                ]
                for line in ar_positions:
                    indices.append(line[1])
                    wavelengths.append(line[0])
                    multiplets.append(spectral_line_classes.Multiplet(positions=[line[0]],
                                                                        intensity=1e17,branching_ratios=[1],tag="ArII"))
            ne_count = 100
            Te_count = 101
            ne_range = [5e19,1e21]
            Te_range = [1,10]
            electron_density = 5e19
            electron_temperature = 1.8
            impurity_density = 1e15
            self.add_pec_lines_to_model(multiplets=multiplets,element="ar",ion=1,indices=indices,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,
                                        Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,
                                        impurity_density=impurity_density,emission_length=0.1,cache=cache,save=save,debug=debug)
        if pec_lines_neon:
            element = "ne"
            if mode == "default":
                if fit_electron_temperature_neon:
                    multiplets = [
                    #spectral_line_classes.Multiplet(
                    #positions=self.zeeman_lines["Ne-II-3559"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    #branching_ratios=self.zeeman_lines["Ne-II-3559"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                    #intensity=1e17, tag="NeII 3p4S-3d4P"),
                
                        spectral_line_classes.Multiplet(
                        positions=self.zeeman_lines["Ne-II-3571"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        branching_ratios=self.zeeman_lines["Ne-II-3571"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                        intensity=1e17,doppler_temperature=3, tag=self.ne_tags[0]),
                        spectral_line_classes.Multiplet(
                        positions=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        branching_ratios=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                        intensity=1e17,doppler_temperature=3, tag=self.ne_tags[1]),
                        spectral_line_classes.Multiplet( # I have measured the ratios and for this line experiment and theory vary - I replace this here hardcoded!
                        positions=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        branching_ratios=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                        intensity=1e17,doppler_temperature=3, tag=self.ne_tags[2])]
                    indices = [16,23,24]
                else:
                    multiplets = [
                    #spectral_line_classes.Multiplet(
                    #positions=self.zeeman_lines["Ne-II-3559"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    #branching_ratios=self.zeeman_lines["Ne-II-3559"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                    #intensity=1e17, tag="NeII 3p4S-3d4P"),
                
                        #spectral_line_classes.Multiplet(
                        #positions=self.zeeman_lines["Ne-II-3571"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        #branching_ratios=self.zeeman_lines["Ne-II-3571"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                        #intensity=1e17,doppler_temperature=3, tag=self.ne_tags[0]),
                        spectral_line_classes.Multiplet(
                        positions=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        branching_ratios=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                        intensity=1e17,doppler_temperature=3, tag=self.ne_tags[1]),
                        spectral_line_classes.Multiplet( # I have measured the ratios and for this line experiment and theory vary - I replace this here hardcoded!
                        positions=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                        branching_ratios=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                        intensity=1e17,doppler_temperature=3, tag=self.ne_tags[2])]
                    #spectral_line_classes.Multiplet( # I have measured the ratios and for this line experiment and theory vary - I replace this here hardcoded!
                    #positions=[366.40740524, 369.42144631, 370.96222012, 373.49388158, 375.12458933, 376.62612898, 377.71357821], 
                    #branching_ratios=[0.14880706235835076,0.4408851181598541, 0.08650080393701925, 0.05012048733774482, 0.014814117210781356, 0.13999737281237043, 0.11887503818387933], 
                    #intensity=1e17, tag=self.ne_tags[2])]
                    indices = [23,24]
                m = multiplets[-1]
                measured_positions = np.array([366.40740524, 369.42144631, 370.96222012, 373.49388158,
                                            375.12458933, 376.62612898, 377.71357821])
                measured_ratios = np.array([0.17404514, 0.43402778, 0.10503472, 0.05251736, 0.01736111,
                                            0.11545139, 0.1015625 ])
                m.positions,m.branching_ratios=spectrum.redistribute_branching_ratios(predicted_positions=m.positions,predicted_ratios=m.branching_ratios,measured_positions=measured_positions,measured_ratios=measured_ratios)
                
            ne_count = 100
            Te_count = 101
            Te_range = [1,10]
            electron_density =pec_guess[0]
            electron_temperature = pec_guess[1]
            impurity_density = pec_guess[2]
            emission_length = pec_guess[3]
            self.add_pec_lines_to_model(multiplets=multiplets,indices=indices,element=element,ion=1,ne_range=ne_range_NeII,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,fit_electron_temperature=fit_electron_temperature_neon,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=emission_length,save=save,cache=cache,debug=debug)
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()