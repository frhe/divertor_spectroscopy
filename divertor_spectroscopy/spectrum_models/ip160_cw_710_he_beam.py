import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
importlib.reload(spectral_line_classes)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_710_he_beam(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, he_main_gas=False,stark_density=False,zeeman=False,pec_lines=False,neon=False,argon_seeding=False,special_tags=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.2
        self.variable_width=False
        self.recommended_background_position =700
        self.strongest_line = 667.8151
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        if True:
            self.add_multiplet_to_model(
                positions=[667.8151], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI2p1P_3d1D")
            self.add_multiplet_to_model(
                positions=[706.5190], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI2p3P_3s3S")
            self.add_multiplet_to_model(
                positions=[728.1349], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI2p1P_3s1s")
        self.clear_tag_multiplicity()