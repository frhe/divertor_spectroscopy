import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
importlib.reload(spectral_line_classes)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Synthetic_spectrum(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, he_main_gas=False,stark_density=False,zeeman=False,pec_lines=False,neon=False,
                 argon_seeding=False,special_tags=False,exclude_wavelength_from_fit=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        self.width = 0.2
        self.variable_width=False
        self.recommended_background_position = 402
        self.strongest_line = 400
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        if True:
            self.add_multiplet_to_model(
                positions=[395], branching_ratios=[1], intensity=0.5, doppler_temperature=1, tag="HI_example1")
                
            self.add_multiplet_to_model(
                positions=[400], branching_ratios=[1], intensity=0.7, doppler_temperature=1, tag="HI_example2")
            self.add_multiplet_to_model(
                positions=[405], branching_ratios=[1], intensity=1, doppler_temperature=1, tag="HI_example3")

        self.clear_tag_multiplicity()
        self.get_all_model_parameters()