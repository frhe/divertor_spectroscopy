import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_700(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman=False,he_main_gas=False,pec_lines_nitrogen=False,fit_electron_temperature_nitrogen=True,electron_temperature_NII=3.5,emission_length_NII=0.1,pec_lines_argon=False,stark_density=False,special_tags=False,cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 690
        self.strongest_line = 703.24	
        # Multiplets
        
        if True:
            self.add_multiplet_to_model(
                positions=[656.279], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HI")
        if True:
            self.add_multiplet_to_model(
                positions=[667.8151], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
            self.add_multiplet_to_model(
                positions=[706.5190], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
            self.add_multiplet_to_model(
                positions=[728.1349], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="HeI")
        line_name = "B-II-7032"
        self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="BII")
        line_name = "C-II-6786"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
        line_name = "C-II-7118"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
        line_name = "C-II-7235"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
    
        line_name = "C-III-6741"
        self.add_multiplet_to_model(
                    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                    branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=6, tag="CIII")
        if False:
            self.add_multiplet_to_model(
                positions=[661.056], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="NII")
        if False:
            self.add_multiplet_to_model(
                positions=[664.1054], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
            self.add_multiplet_to_model(
                positions=[672.1398], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
            self.add_multiplet_to_model(
                positions=[689.5109], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="OII")
        if False:
            line_name = "Ne-II-7235"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=4, tag="NeII")
            self.add_multiplet_to_model(
            positions=[717.39381], branching_ratios=[1], intensity=1e17, doppler_temperature=1, tag="NeI")
            self.add_multiplet_to_model(
            positions=[689], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="NeIII")
        
        
        self.clear_tag_multiplicity()