import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_430_balmer_lines(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self, zeeman=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.4
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 2.5
        self.angle_to_magnetic_field = 90
        # Multiplets

        # Balmer lines
        if zeeman:
            magnetic_field = self.magnetic_field
        else:
            magnetic_field = None
        self.add_balmer_lines_to_model(
            lines=["H_eta", "H_zeta", "H_epsilon", "H_delta", "H_gamma"], intensities=[0.5e17, 0.7e17, 1e17, 2e17, 4e17], electron_density=1e20, electron_temperature=3, width_gauss=self.width, tag="HI", magnetic_field=magnetic_field, angle_to_magnetic_field=self.angle_to_magnetic_field)
        self.build_popt_description()
