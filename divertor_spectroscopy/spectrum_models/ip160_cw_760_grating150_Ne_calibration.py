import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_760_grating150_Ne_calibration(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,variable_width=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.25
        self.variable_width = variable_width
        
        self.recommended_background_position = 700
        self.strongest_line = 703.241
        
        known_wavelengths = np.array([568.981,571.887,576.441,580.444,587.282,591.363,593.445,597.462,604.613,609.616,614.25,616.359,621.728,624.672,633.442,635.185,642.17,650.652,653.288,724.516, ])
        for w in known_wavelengths:
            self.add_multiplet_to_model(positions=[w],branching_ratios=[1],intensity=1,tag="NeI",doppler_temperature=1)
        self.clear_tag_multiplicity()