import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_405_reduced_Balmer_lines(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman=False,he_main_gas=False,pec_lines_nitrogen=False,fit_electron_temperature_nitrogen=True,
                 electron_temperature_NII=3.5,emission_length_NII=0.1,pec_lines_argon=False,stark_density=False,
                 exclude_wavelength_from_fit=None,special_tags=False,cache=False,save=False,debug=False,grating="1800"):
        self.zeeman = zeeman
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 401.695
        self.strongest_line = 410.173
        # Multiplets
        #self.add_multiplet_to_model(positions=[410.015], branching_ratios=[1], intensity=1e17, doppler_temperature=15, tag="HeI")
            
        if not stark_density:
            line_name = "H-I-3970"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e18,doppler_temperature=1, tag="HI")
            line_name = "H-I-4102"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e18,doppler_temperature=1, tag="HI")
       
            #self.add_multiplet_to_model(positions=[397.007], branching_ratios=[1], intensity=1e17, doppler_temperature=1, tag="HI")
            #self.add_multiplet_to_model(positions=[410.1734], branching_ratios=[1], intensity=1e18, doppler_temperature=3, tag="HI")
        
        #self.add_multiplet_to_model(
        #    positions=[396.47291], branching_ratios=[1], intensity=1e17, doppler_temperature=4, tag="HeI")
        line_name = "N-III-4099"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=5e17,doppler_temperature=6, tag="NIII")
       
        #self.add_multiplet_to_model(
        #    positions=[409.94554], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
        #self.add_multiplet_to_model(
        #    positions=[410.38323], branching_ratios=[1], intensity=1e17, doppler_temperature=20, tag="ArII")
        self.add_multiplet_to_model(
            positions=[410.39118], branching_ratios=[1], intensity=1e17, doppler_temperature=20, tag="ArII")
        # O
        line_name = "O-II-3967"
        #self.add_multiplet_to_model(
        #    positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=4, tag="OII")
        
        if True:
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="OII")
            self.add_multiplet_to_model(
                positions=[396.83589 ], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
            #self.add_multiplet_to_model(
            #    positions=[397.44764], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
            #self.add_multiplet_to_model(
            #    positions=[397.47582], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
            #self.add_multiplet_to_model(
            #    positions=[397.93554], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
                
       # Balmer lines
        if stark_density:
            magnetic_field = self.magnetic_field
            #magnetic_field = None
            #self.add_balmer_lines_to_model(
            #    lines=["H_epsilon","H_delta"], intensities=[0.5e18,1e18], electron_density=1e20,electron_temperature=1, doppler_temperature=2.2, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
            self.add_balmer_lines_to_model(
               lines=["H_epsilon"], intensities=[1e17], electron_density=1e19,electron_temperature=3, doppler_temperature=2.2, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field,fit_doppler_shift=False,doppler_shift=0)
            self.add_balmer_lines_to_model(
                lines=["H_delta"], intensities=[1e18], electron_density=1e19,electron_temperature=3, doppler_temperature=2.2, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field,fit_doppler_shift=False,doppler_shift=0)
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()