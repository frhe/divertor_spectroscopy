import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

# from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_262(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    
    Todo: 
    I want to say in the specral model which lines should be fitted and it will be saved in the "fit_description"
    Options for every line are: 
        - Intensity (default to make it backwards compatible - line intensities/ratios can contain physics information like density or temperature)
        - width (this might contain physics information like temperature or density)
        - position (can also contain physics information)
    I clearly want to seperate pysics information (i.e. density and temperature) from calibration information (like instrument function)
    """

    def __init__(self, zeeman=False,pec_lines_argon=False,pec_lines_neon=False,fit_electron_temperature_neon=True,
                 ne_range_NeII=[5e18,1e20],pec_guess=[1e19,4.1,1e17,0.1],stark_density=False,special_tags=False,
                 cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.zeeman=zeeman
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.recommended_background_position = 260.5
        self.strongest_line = 266.57
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90

        # W 
        self.add_multiplet_to_model(positions=[255.14], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="WI")
        
        # C
        self.add_multiplet_to_model(positions=[257.48], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="CII")
        self.add_multiplet_to_model(positions=[264.14], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="CII")
        
        self.add_multiplet_to_model(positions=[252.441], branching_ratios=[1], intensity=1e17, doppler_temperature=20, tag="CIV")
        self.add_multiplet_to_model(positions=[252.998], branching_ratios=[1], intensity=1e17, doppler_temperature=20, tag="CIV")
        
         # Multiplets
        # O
        self.add_multiplet_to_model(positions=[266.57], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="OII")
        self.add_multiplet_to_model(positions=[255.79], branching_ratios=[1], intensity=1e17, doppler_temperature=10, tag="OIII")
        
        if False: # not observed
            line_name = 'O-III-2539'
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=4, tag="OII")
        self.add_multiplet_to_model(positions=[259.52], branching_ratios=[1], intensity=1e17, doppler_temperature=20, tag="OIV")
          
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()