import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_404_HgAr_calibration(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman=False,he_main_gas=False,pec_lines_nitrogen=False,fit_electron_temperature_nitrogen=True,electron_temperature_NII=3.5,emission_length_NII=0.1,pec_lines_argon=False,stark_density=False,special_tags=False,cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 410
        self.strongest_line = 404.65650  	
        # Multiplets
        #Hg
        self.add_multiplet_to_model(positions=[404.65643], branching_ratios=[1], intensity=1e17, doppler_temperature=0.1, tag="HgI")
        self.add_multiplet_to_model(positions=[407.78370], branching_ratios=[1], intensity=1e17, doppler_temperature=0.1, tag="HgI")
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()