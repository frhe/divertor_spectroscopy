import importlib
import json

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Soprawega_cw_400(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self,stark_density=False,zeeman=False,special_tags=[],exclude_wavelength_from_fit=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        self.recommended_background_position = 399.2
        self.strongest_line = 399.5
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        #self.angle_to_magnetic_field = 110
        self.angle_to_magnetic_field = 90
        # Multiplets
        
        if True:
            line_name = "N-II-3995"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"],
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,doppler_temperature=4, tag="NII")       
        
        
        self.clear_tag_multiplicity()
