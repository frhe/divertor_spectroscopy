import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_410_balmer_lines(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self, zeeman=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.4
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 2.5
        self.angle_to_magnetic_field = 90
        # Multiplets
        lines = [434.047,410.173,397.007,388.906,383.54,379.7909,377.0633,375.0151]
        for l in lines:
            self.add_multiplet_to_model(
                positions=[l], branching_ratios=[1], intensity=1e17, width=self.width, tag="HI")
        
        self.build_popt_description()
        self.clear_tag_multiplicity()
