import importlib
import json

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Sopra_wega_cw_397(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.009

        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 2.5
        self.angle_to_magnetic_field = 90
        # Multiplets
        #self.add_multiplet_to_model(
        #    positions=[396.47291], branching_ratios=[1], intensity=1, #width=self.width, tag="HeI")
        #self.add_multiplet_to_model(
        #    positions=[396.836], branching_ratios=[1], intensity=1, width=self.width, #tag="ArII")
        #self.add_multiplet_to_model(
        #    positions=[396.562], branching_ratios=[1], intensity=1, width=self.width, #tag="NeIII")
        line_name = "O-II-3967"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, width=self.width, tag="O-II")
        # Balmer lines
        self.add_balmer_lines_to_model(
            lines=["H_epsilon"], intensities=[1], electron_density=1e20, electron_temperature=3, width_gauss=self.width, tag="HI")

        self.build_popt_description()
