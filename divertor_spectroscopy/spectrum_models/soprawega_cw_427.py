import importlib
import json

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Soprawega_cw_427(spectrum.Spectrum):
    """
    testing the methods (ie spectral_line_classes) in spectrum
    - does pec_line work to create a spectrum?
    - can I then fit this?
    """

    def __init__(self):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.008

        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 2.5
        self.angle_to_magnetic_field = 90
        # Multiplets
        if True:
            line_name = "C-II-4267"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, width=self.width, tag="CII")
        else:
            self.add_multiplet_to_model(
                positions=[426.7], 
                branching_ratios=[1], intensity=1, width=self.width, tag="CII Nist without Zeeman")
        self.build_popt_description()
        self.clear_tag_multiplicity()