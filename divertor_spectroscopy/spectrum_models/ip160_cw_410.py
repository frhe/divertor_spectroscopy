import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_410(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.3
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        # He - todo look at active he beam spectrum
        if True:
            self.add_multiplet_to_model(
                positions=[396.47291], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
            self.add_multiplet_to_model(
                positions=[388.8648], branching_ratios=[1], intensity=1e17, width=self.width, tag="HeI")
        # B - there could very well be more lines
        line_name = "B-II-4122"
        self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="BII")
        
        # C - this will be a lot
        self.add_multiplet_to_model(
            positions=[387.619], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[387.641], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[387.666], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[391.898], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        self.add_multiplet_to_model(
            positions=[392.069], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        line_name = "C-II-4075" 
        # core electrons have one excited electron, which I had to workaround in John Heys Zeeman code
        # validated with Sopra in experiment 20181018.038
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="CII")
        self.add_multiplet_to_model( # This line is not understood with the zeeman code as an energy level is probably incorret!
            positions=[426.700], branching_ratios=[1], intensity=1e17, width=self.width, tag="CII")
        line_name = "C-III-4069"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="CIII")
        line_name = "C-III-4649"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="CIII")
        # N - maybe only strong enough if there is seeding?
        self.add_multiplet_to_model(
            positions=[383.837], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[391.900], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[395.585], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[399.5], branching_ratios=[1], intensity=1e17, width=self.width, tag="NII")
        self.add_multiplet_to_model(
            positions=[403.509, 404.132, 404.354, 404.479, 405.692], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, width=self.width, tag="NII")
        
        if True:
            self.add_multiplet_to_model(positions=[412.408,413.367,414.577],branching_ratios=[0.192,0.303,0.505],intensity=1e17,width=self.width,tag="NII") # double excited
        self.add_multiplet_to_model(
            positions=[399.863, 400.358], branching_ratios=[0.375, 0.625], intensity=1e17, width=self.width, tag="NIII")
        line_name = "N-III-4099"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="NIII")
        # O
        line_name = "O-II-3967"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        line_name = "O-II-4075"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        line_name = "O-II-4341"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        line_name = "O-II-4350"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="OII")
        # Balmer lines
        magnetic_field = None
        self.add_balmer_lines_to_model(
            lines=["H_gamma","H_delta","H_epsilon","H_zeta"], intensities=[8e17,6e17,5e17,4e17], electron_density=1e20,electron_temperature=3, width_gauss=self.width, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
            

        self.build_popt_description()
        self.clear_tag_multiplicity()