import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_390_balmer_lines(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.045
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        # Balmer lines
        if zeeman:
            magnetic_field = self.magnetic_field
        else:
            magnetic_field = None
        self.add_balmer_lines_to_model(
            lines=["H_eta", "H_zeta", "H_epsilon"], intensities=[0.1e17, 0.2e17, 0.5e17], electron_density=1e20, electron_temperature=3, width_gauss=self.width, tag="HI", magnetic_field=magnetic_field, angle_to_magnetic_field=self.angle_to_magnetic_field)

        self.build_popt_description()
