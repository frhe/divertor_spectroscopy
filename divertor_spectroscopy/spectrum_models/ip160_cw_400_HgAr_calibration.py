import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_400_HgAr_calibration(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,variable_width=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.25
        self.variable_width = variable_width
        
        self.recommended_background_position = 360
        
        self.add_multiplet_to_model(positions=[365.01580],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.add_multiplet_to_model(positions=[365.48420],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.add_multiplet_to_model(positions=[366.32840],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.add_multiplet_to_model(positions=[404.65650],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.add_multiplet_to_model(positions=[407.78370],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.add_multiplet_to_model(positions=[435.83350],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="HgI")
        self.clear_tag_multiplicity()