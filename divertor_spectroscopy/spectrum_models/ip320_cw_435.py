import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_435(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman,stark_density=False,exclude_wavelength_from_fit=None,special_tags=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.recommended_background_position = 440
        self.strongest_line = 434.0472	
        self.strongest_line = 441.4905
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        # Multiplets
        if not stark_density:
            self.add_multiplet_to_model( # This line is not understood with the zeeman code as an energy level is probably incorret!
            positions=[434.0472], branching_ratios=[1], intensity=1e17, doppler_temperature = 1, tag="HI")
        #self.add_multiplet_to_model( # This line is not understood with the zeeman code as an energy level is probably incorret!
        #    positions=[426.700], branching_ratios=[1], intensity=1e17, doppler_temperature = 2, tag="CII")
        line_name = "C-II-4267"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="CII")
        
        self.add_multiplet_to_model(
            positions=[432.556], branching_ratios=[1], intensity=1e17, doppler_temperature = 6, tag="CIII")
        line_name = "O-II-4341"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        line_name = "O-II-4350"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        line_name = "O-II-4418"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature = 4, tag="OII")
        
        # Balmer lines
        if zeeman:
            magnetic_field = self.magnetic_field
        else:
            magnetic_field = None
        if stark_density:
            self.add_balmer_lines_to_model(
                lines=["H_gamma"], intensities=[1e18], electron_density=1e20, electron_temperature=3, doppler_temperature=1, tag="HI", magnetic_field=magnetic_field, angle_to_magnetic_field=self.angle_to_magnetic_field)

        self.clear_tag_multiplicity()
        self.get_all_model_parameters()