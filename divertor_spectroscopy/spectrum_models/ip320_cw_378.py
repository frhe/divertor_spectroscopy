import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_378(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """

    def __init__(self, zeeman=False,pec_lines_argon=False,pec_lines_neon=False,fit_electron_temperature_neon=True,
                 ne_range_NeII=[5e18,1e21],pec_guess=[1e20,4.1,1e18,0.1],stark_density=False,special_tags=False,
                 cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.zeeman=zeeman
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.recommended_background_position = 371.75
        self.strongest_line = 369.421
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
         # Multiplets
         # HI - I don't use Stark Broadening code here as I don't have broadening data from Lomanowski paper lower than Balmer Eta (383nm)
        self.add_multiplet_to_model(
            positions=[383.5397], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="HI")
        self.add_multiplet_to_model(
            positions=[379.7909], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="HI")
        self.add_multiplet_to_model(
            positions=[377.0633], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="HI")
        self.add_multiplet_to_model(
            positions=[375.0151], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="HI")
        # C
        self.add_multiplet_to_model(
            positions=[383.1726], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="CII")
        
        # O
        line_name = "O-II-3736"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,doppler_temperature=3, tag="OII")
        line_name = "O-III-3706"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,doppler_temperature=6, tag="OIII")
        line_name = "O-III-3762"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,doppler_temperature=6, tag="OIII")
        line_name = "O-IV-3734"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
            branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
            intensity=1e17,doppler_temperature=10, tag="OIV")
        
        self.add_multiplet_to_model(
            positions=[381.367],branching_ratios=[1],doppler_temperature=20,intensity=1,tag="NeIV")
        if special_tags:
            self.ne_tags = ["NeII3s2P_3p2D","NeII3s4P_3p4P","NeII3p2P_3d2D",]
        else: 
            self.ne_tags = ["NeII","NeII","NeII"]
        if not pec_lines_neon:
            # weird order just that we have the same special colors as the other plot
            line_name = "Ne-II-3824"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag=self.ne_tags[2])
            line_name = "Ne-II-3713"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag=self.ne_tags[0])
            line_name = "Ne-II-3717"
            self.add_multiplet_to_model(
                positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag=self.ne_tags[1])
            m = self.multiplets[-1]
            measured_positions = np.array([366.40740524, 369.42144631, 370.96222012, 373.49388158,
                                            375.12458933, 376.62612898, 377.71357821])
            measured_ratios = np.array([0.17404514, 0.43402778, 0.10503472, 0.05251736, 0.01736111,
                                        0.11545139, 0.1015625 ])
            m.positions,m.branching_ratios=spectrum.redistribute_branching_ratios(predicted_positions=m.positions,predicted_ratios=m.branching_ratios,measured_positions=measured_positions,measured_ratios=measured_ratios)
                
            
        else:
            # pec lines
            debug = True
            cache = False
            save = False
            element = "ne"
            multiplets = [spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3713"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"],
                intensity=1e17,doppler_temperature=3, tag="NeII 3s2P-3p2D"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3717"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,doppler_temperature=3, tag="NeII 3s4P-3p4P"),
                spectral_line_classes.Multiplet(
                positions=self.zeeman_lines["Ne-II-3824"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], 
                branching_ratios=self.zeeman_lines["Ne-II-3824"][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], 
                intensity=1e17,doppler_temperature=3, tag="NeII 3p2P-3d2D")]
            ne_count = 100
            Te_count = 101
            ne_range = [5e19,1e21]
            Te_range = [1,10]
            electron_density = 1e20
            electron_temperature = 5
            impurity_density = 1e17
            self.add_pec_lines_to_model(multiplets=multiplets,element=element,ion=1,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=0.1,save=save,cache=cache,debug=debug)
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()