"""
This is to really fit and apply the NII fits
Functionalities:
    - fit experiment data
    - from that apply line ratios to extract plasma parameters!
"""
import matplotlib.pyplot as plt
import numpy as np
import json
import importlib

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_405_active_argon(spectrum.Spectrum):
    """
    Spectrum is the used to analyze spectra
    Expects calibrated spectra which can now be analyzed
    """

    def __init__(self, background=0, debug=False, cache=False, save=False):
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.background = background
        self.voigtians = []
        self.lorentzians = []
        self.multiplets = []
        self.balmer_lines = []
        self.pec_lines = []
        self.all_tags = []
        self.width = 0.045

        element = "ar"
        if element == "ar":
            ion = 1
            ne_count = 100
            Te_count = 101
            ne_range = [5e19, 1e21]
            Te_range = [1, 10]
            multiplets = [spectral_line_classes.Multiplet(positions=[396.838], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[399.207], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[401.389], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[403.883], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[404.291], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[407.202], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[410.393], branching_ratios=[
                                                          1], intensity=1e17, width=self.width, tag="ArII"),
                          spectral_line_classes.Multiplet(positions=[413.173], branching_ratios=[1], intensity=1e17, width=self.width, tag="ArII"), ]
            electron_density = 1e20
            electron_temperature = 4
            impurity_density = 1e18
            self.add_pec_lines_to_model(multiplets=multiplets, element=element, ion=ion, ne_range=ne_range, Te_range=Te_range, ne_count=ne_count, Te_count=Te_count,
                                        electron_density=electron_density, electron_temperature=electron_temperature, impurity_density=impurity_density, emission_length=0.1, save=save, cache=cache, debug=debug)
        self.clear_tag_multiplicity()
        self.build_popt_description()

    def clear_tag_multiplicity(self):
        all_tags = []
        for tag in self.all_tags:
            if not tag in all_tags:
                if not tag == None:
                    all_tags.append(tag)
        self.all_tags = all_tags
