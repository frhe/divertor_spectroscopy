"""
This is to really fit and apply the NII fits
Functionalities:
    - fit experiment data
    - from that apply line ratios to extract plasma parameters!
"""
import matplotlib.pyplot as plt
import numpy as np
import json
import importlib

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_462(spectrum.Spectrum):
    """
    Spectrum is the used to analyze spectra
    Expects calibrated spectra which can now be analyzed
    This should probably be implemented as an extension of class 405
    """

    def __init__(self,zeeman=False,he_main_gas=False,pec_lines_nitrogen=False,fit_electron_temperature_nitrogen=True,
                 electron_temperature_NII=3.5,emission_length_NII=0.1,pec_lines_argon=False,stark_density=False,
                 special_tags=False,cache=False,save=False,debug=False,exclude_wavelength_from_fit=[464.5,465.3],grating="1800"):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        #self.recommended_background_position = 456.1
        self.recommended_background_position = 462
        self.strongest_line = 468.58
        self.strongest_line = 464.7418236987857
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        self.exclude_wavelength_from_fit = None
        if grating == "1800":
            # He
            name = "He-II-4686"
            self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=10, tag=name.split("-")[0]+name.split("-")[1])
            # B 
            # # there is no physics behind this whatsoever - I inserted the wavelength and branching ratios from measurements!
            # todo: is this really only boron? Candidates are carbon and argon
            if False: # these boron lines are not visible I think
                self.add_multiplet_to_model(positions=[461.113],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="BII")
                name = "B-III-4632"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=10, tag=name.split("-")[0]+name.split("-")[1])
            #self.add_multiplet_to_model(positions=[461.832],branching_ratios=[1],intensity=1e17,doppler_temperature=3,tag="X")
            self.add_multiplet_to_model( 
                positions=[465.815], branching_ratios=[1], intensity=1e17, doppler_temperature=100, tag="BIV")# not sure about this!
            if False: # not observed
                name = "C-II-4619"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                        self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            if True:
                name = "C-III-4649"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            # N
            name = "N-II-4623"
            self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, doppler_temperature=3, tag=name.split("-")[0]+name.split("-")[1])
            # NII multiplet with a spin flip is visible in line identification - this was a quick branching ratio fit of active spectrum, not very accurate
            if True: 
                self.add_multiplet_to_model(positions=[465.4531,466.7208,467.4908],branching_ratios=[0.4,0.23,0.37],intensity=1e17,doppler_temperature=3,tag="NII")
            if False: # for line identification
                self.add_multiplet_to_model(positions=[465.4531],branching_ratios=[1],intensity=1e17,doppler_temperature=3,tag="NII")
                self.add_multiplet_to_model(positions=[466.7208],branching_ratios=[1],intensity=1e17,doppler_temperature=3,tag="NII")
                self.add_multiplet_to_model(positions=[467.4908],branching_ratios=[1],intensity=1e17,doppler_temperature=3,tag="NII")
            name = "N-III-4543"
            self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            name = "N-III-4517"
            self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            if True:
                name = "N-III-4639"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            else:
                self.add_multiplet_to_model(positions=[464.0644193568886],branching_ratios=[1],intensity=1e17,doppler_temperature=6,tag="NIII")
                self.add_multiplet_to_model(positions=[464.18508560021456],branching_ratios=[1],intensity=1e17,doppler_temperature=6,tag="NIII")
                self.add_multiplet_to_model(positions=[463.41261526680955],branching_ratios=[1],intensity=1e17,doppler_temperature=6,tag="NIII")
            
            
        
            
            #self.add_multiplet_to_model(positions=[453.086],branching_ratios=[1],intensity=1e17,doppler_temperature=6,tag="NIII")
            #self.add_multiplet_to_model(positions=[453.458],branching_ratios=[1],intensity=1e17,doppler_temperature=6,tag="NIII")
            self.add_multiplet_to_model(positions=[460.633],branching_ratios=[1],intensity=1e17,doppler_temperature=15,tag="NIV")
            name = "N-V-4609"
            self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, doppler_temperature=30, tag=name.split("-")[0]+name.split("-")[1])
            # O
            if True:
                name = "O-II-4593"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=name.split("-")[0]+name.split("-")[1])
            if True:
                name = "O-II-4652"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=name.split("-")[0]+name.split("-")[1])
            # Ar
            
            self.add_multiplet_to_model(positions=[454.50516],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
            self.add_multiplet_to_model(positions=[454.38702],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
            self.add_multiplet_to_model(positions=[457.93493],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
            self.add_multiplet_to_model(positions=[458.98976],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
            self.add_multiplet_to_model(positions=[460.95669],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
            self.add_multiplet_to_model(positions=[463.72324],branching_ratios=[1],intensity=1e17,doppler_temperature=2,tag="ArII")
        if grating == "2400":
            if True:
                name = "C-III-4649"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=6, tag=name.split("-")[0]+name.split("-")[1])
            if True:
                name = "O-II-4593"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=name.split("-")[0]+name.split("-")[1])
            if True:
                name = "O-II-4652"
                self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=name.split("-")[0]+name.split("-")[1])
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()
