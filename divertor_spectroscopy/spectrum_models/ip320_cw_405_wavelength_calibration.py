import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_405_wavelength_calibration(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman=False,he_main_gas=False,pec_lines_nitrogen=False,fit_electron_temperature_nitrogen=True,electron_temperature_NII=3.5,emission_length_NII=0.1,
                 pec_lines_argon=False,stark_density=False,special_tags=False,cache=False,save=False,debug=False,exclude_wavelength_from_fit=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 401.695
        self.strongest_line = 410.1734
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        # Multiplets
        self.add_multiplet_to_model(positions=[410.015], branching_ratios=[1], intensity=1e17, doppler_temperature=15, tag="HeI")
         
        if not stark_density:
            self.add_multiplet_to_model(positions=[410.1734], branching_ratios=[1], intensity=1e17, doppler_temperature=1, tag="HI")
        # He
        # There are different amounts of Helium in the spectrum. This can range from very little, over He-Beam up to he-main-gas plasmas
        # For he-main gas you might want to switch all available lines on!
        
        # B
        line_name = "B-II-4122"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="BII")
        
        
        # N
        # The NI line at 410 and 411 nm is prominent but not seen in the spectrum. Come back to this, when nitrogen is in the machine!
        self.add_multiplet_to_model(
            positions=[395.585], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="NII")
        if not pec_lines_nitrogen:
            nitrogen_seeding = False
            if nitrogen_seeding:
                if special_tags:
                    self.n_tags = ["NII 3s1P-3p1D","NII 3d3F-4f1G","NII 3d3F-4f3G"]
                else:
                    self.n_tags = ["NII","NII","NII"]
                self.add_multiplet_to_model(positions=[399.5], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag=self.n_tags[0])
                self.add_multiplet_to_model(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, doppler_temperature=3, tag=self.n_tags[1])
                self.add_multiplet_to_model(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, doppler_temperature=3,tag=self.n_tags[2])
                if True: # double excited line: ratio is measured, statistical weigths are similar: 0.2019,0.3336,0.4620
                        self.add_multiplet_to_model(positions=[412.408,413.367,414.577],branching_ratios=[0.192,0.303,0.505],intensity=1e17,doppler_temperature=3,tag="NII") # double excited
                    #self.add_multiplet_to_model(
                    #        positions=[407.304, 407.691, 408.227,408.730], branching_ratios=[0.403, 0.131, 0.371,0.095],
                    #        intensity=1e17, doppler_temperature=3, tag="NII")
                self.add_multiplet_to_model(
                    positions=[399.863, 400.358], branching_ratios=[0.375, 0.625], intensity=1e17, doppler_temperature=6, tag="NIII")
            else:
                    self.add_multiplet_to_model(positions=[399.5], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="NII")
        line_name = "N-III-4099"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=6, tag="NIII")
        if False:
            self.add_multiplet_to_model(
                positions=[413.163,413.383,414.613,414.678], branching_ratios=[0.157,0.509,0.208,0.126],
                intensity=1e17, doppler_temperature=6, tag="NIII")
        
        # O
        line_name = "O-II-3967"
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="OII")
        
        # Ar
        if not pec_lines_argon:
            if False:
                self.add_multiplet_to_model(
                    positions=[401.3856], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
            else:
                self.add_multiplet_to_model(
                    positions=[401.3856], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[409.94554], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[410.38323], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
                self.add_multiplet_to_model(
                    positions=[410.39118], branching_ratios=[1], intensity=1e17, doppler_temperature=2, tag="ArII")
        # W
        #self.add_multiplet_to_model(
        #            positions=[400.87506], branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag="WI")
        if pec_lines_argon:
            # PEC lines
            multiplets = []
            data = adf15.ADF15() # todo replace this with colradpy
            fn = ground_path + 'adas/adf15/dcg01_ic_pju#ar1.dat'
            data.read(fn,v=1,debug=False,headerlength=8)
            indices = []
            wavelengths = []
            if False:
                for idx in range(100):
                        if utils.wl_vac_to_air(data.dat.wvl[idx])/10 > 395 and utils.wl_vac_to_air(data.dat.wvl[idx])/10 < 417:
                            multiplets.append(spectral_line_classes.Multiplet(positions=np.array([utils.wl_vac_to_air(data.dat.wvl[idx])])/10,
                                                                        intensity=1e17,branching_ratios=[1],doppler_temperature=2,tag="ArII"))
                            indices.append(idx)
                            wavelengths.append(utils.wl_vac_to_air(data.dat.wvl[idx])/10)
            else:
                ar_positions = [#[ 395.2744469274388 , 68 ], #weak + overlapping
                                [ 396.83589 , 69 ], #strongly overlapping
                                #[ 397.93770878582126 , 70 ], #weak
                                #[ 399.20735689398407 , 71 ], #stronger in experiment
                                #[ 399.481280974013 , 72 ], #weak
                                [ 401.38561 , 73 ],#strong
                                #[ 403.3821998071242 , 74 ],#weak
                                #[ 403.5471540888934 , 75 ],#weak
                                [ 403.88039 , 76 ],#weak
                                [ 404.2190 , 77 ],#strong
                                #[ 405.2946697527269 , 78 ],#weak
                                [ 407.20043 , 79 ],#strong, overlapping with OII
                                [ 407.23843 , 80 ],#or this one
                                #[ 407.66401307268427 , 81 ],#weak
                                #[ 408.2398534744967 , 82 ],#weak, but there is a line
                                [ 410.38323 , 83 ],#completely in HDelta
                                [ 410.39118 , 84 ],#or this one
                                #[ 411.2830100424083 , 85 ],#weak
                                #[ 412.8645717015528 , 86 ],#weak
                                #[ 413.17348608377506 , 87 ],#weak
                                #[ 415.61081056227977 , 88 ]#strong
                ]
                for line in ar_positions:
                    indices.append(line[1])
                    wavelengths.append(line[0])
                    multiplets.append(spectral_line_classes.Multiplet(positions=[line[0]],
                                                                        intensity=1e17,branching_ratios=[1],doppler_temperature=2,tag="ArII"))
            
            print(indices)
            print(wavelengths)
            print(len(multiplets))
            ne_count = 100
            Te_count = 101
            ne_range = [5e19,1e21]
            Te_range = [1,10]
            electron_density = 1e20
            electron_temperature = 2
            impurity_density = 1e15
            self.add_pec_lines_to_model(multiplets=multiplets,element="ar",ion=1,indices=indices,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,
                                        Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,
                                        impurity_density=impurity_density,emission_length=0.1,cache=cache,save=save,debug=debug)
        if special_tags:
            self.n_tags = ["NII 3s1P-3p1D","NII 3d3F-4f1G","NII 3d3F-4f3G"]
        else:
            self.n_tags = ["NII","NII","NII"]
        if pec_lines_nitrogen:
            element = "n"
            if element == "n":
                if fit_electron_temperature_nitrogen:
                    if zeeman:
                        line_name = "N-II-3995"
                        multiplets = [spectral_line_classes.Multiplet(self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=self.n_tags[0]),
                            spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, doppler_temperature=3, tag=self.n_tags[1]),
                            spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, doppler_temperature=3, tag=self.n_tags[2])]
                    else:
                        multiplets = [spectral_line_classes.Multiplet(positions=[399.5],branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag=self.n_tags[0]),
                            spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, doppler_temperature=3, tag=self.n_tags[1]),
                            spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, doppler_temperature=3, tag=self.n_tags[2])]
                    indices = [14, 18, 20]
                else:
                    if zeeman:
                        line_name = "N-II-3995"
                        multiplets = [spectral_line_classes.Multiplet(self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17, doppler_temperature=3, tag=self.n_tags[0]),
                            #spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, doppler_temperature=3, tag=self.n_tags[1]),
                            spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, doppler_temperature=3, tag=self.n_tags[2])]
                    else:
                        multiplets = [spectral_line_classes.Multiplet(positions=[399.5],branching_ratios=[1], intensity=1e17, doppler_temperature=3, tag=self.n_tags[0]),
                            #spectral_line_classes.Multiplet(positions=[402.609, 403.935], branching_ratios=[0.944, 0.056], intensity=1e17, doppler_temperature=3, tag=self.n_tags[1]),
                            spectral_line_classes.Multiplet(positions=[403.509, 404.132, 404.354, 404.479, 405.692, ], branching_ratios=[0.205, 0.562, 0.175, 0.029, 0.029], intensity=1e17, doppler_temperature=3, tag=self.n_tags[2])]
                    indices = [14, 20]
                ne_count = 100
                Te_count = 101
                ne_range = [1e19,1e21]
                Te_range = [1,10]
                electron_density = 1e20
                electron_temperature = electron_temperature_NII
                impurity_density = 1e17
                self.add_pec_lines_to_model(indices=indices,multiplets=multiplets,element=element,ion=1,ne_range=ne_range,Te_range=Te_range,ne_count=ne_count,Te_count=Te_count,electron_density=electron_density,electron_temperature=electron_temperature,impurity_density=impurity_density,emission_length=emission_length_NII,fit_electron_temperature=fit_electron_temperature_nitrogen,cache=cache,save=save,debug=debug)
        # Balmer lines
        if stark_density:
            magnetic_field = self.magnetic_field
            #magnetic_field = None
            #self.add_balmer_lines_to_model(
            #    lines=["H_epsilon","H_delta"], intensities=[0.5e18,1e18], electron_density=1e20,electron_temperature=1, doppler_temperature=1, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
            self.add_balmer_lines_to_model(
                lines=["H_delta"], intensities=[1e18], electron_density=0.5e20,electron_temperature=1, doppler_temperature=1, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
             
        self.clear_tag_multiplicity()