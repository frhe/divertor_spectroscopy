import matplotlib.pyplot as plt
import numpy as np
import json
import importlib

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Line_identification(spectrum.Spectrum):

    def __init__(self, element="nitrogen",central_wavelength=405,variable_width=False,
                 special_tags=False,mode="default",comment=None,camid=None,exclude_wavelength_from_fit=None):
        self.mode = mode
        self.variable_width = variable_width
        self.special_tags = special_tags
        self.exclude_wavelength_from_fit=exclude_wavelength_from_fit
        if False: # use instrument function calibration
            with open(ground_path + "qss_analysis/json_files/instrument_function_calibration.json") as f:
                self.instrument_function = json.load(f)[camid]["30um"]["width"]
        if False:
            with open(ground_path + "qss_analysis/json_files/numeric_instrument_function_calibration.json") as f:
                self.numeric_instrument_function = json.load(f)[camid]
        
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.voigtians = []
        self.lorentzians = []
        self.multiplets = []
        self.all_tags = []
        self.pec_lines = []
        self.balmer_lines = []
        
        fixed_ratios = False
        if element == "argon":
            if fixed_ratios:
                name = "Ar-II-3968"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, width=self.width, tag="3d4D-4p4D")
                name = "Ar-II-4060"
                self.add_multiplet_to_model(
                    positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, width=self.width, tag="4s2D-4p2D")
            else:
                # here all lines need to be added manually and the ratios will be part of the output
                self.add_line_for_identification(
                    positions=[396.836, 399.205, 401.386, 403.880], tag="3d4D-4p4D")
                self.add_line_for_identification(
                    positions=[403.546, 404.289, 407.200, 407.957], tag="4s2D-4p2D")
                self.add_line_for_identification(
                    positions=[403.381, 407.238, 407.663, 410.391, 415.609], tag="4p4D-5s4P")
                self.add_line_for_identification(
                    positions=[399.479, 405.292], tag="4s2S-4p2P")
                self.add_line_for_identification(
                    positions=[413.172], tag="4s2D-4p2P")
                self.add_line_for_identification(
                    positions=[412.864], tag="3d2F-4p2D")
                self.add_line_for_identification(
                    positions=[397.936], tag="4p4S-4d4P")
            self.add_line_for_identification(
                positions=[397.476, 408.239, 411.282], tag="4s4P-4p2D")
        if element == "nitrogen":
            # NI
            self.add_line_for_identification(
                positions=[409.994, 410.995], tag="NI 3s2P-3p2D")
            # NII
            #self.add_line_for_identification(
            #    positions=[391.900], tag="NII 3p1P-3d1P")
            #self.add_line_for_identification(
            #    positions=[395.585, 397.731], tag="NII 3s3P-3p1D")  # , 397.731 should be too dark
            self.add_line_for_identification(
                positions=[399.5], tag="NII 3s1P-3p1D")
            self.add_line_for_identification(
                positions=[403.509, 404.132, 404.354, 404.479, 405.692], tag="NII 3d3F-4f3G")
            self.add_line_for_identification(
                positions=[402.609, 403.935], tag="NII 3d3F-4f1G")
            self.add_line_for_identification(
                positions=[412.408, 413.367, 414.577], tag="NII 2s2p2 3s5P-3p5S")
            self.add_line_for_identification(
                positions=[407.304, 407.691, 408.227, 408.730], tag="NII unidentified")
            self.add_line_for_identification(positions=[417.616],tag="NII unidentified 2")
            #self.add_line_for_identification(positions=[423.691,423.705,424.178],tag="NII unidentified 2")
            #self.add_line_for_identification(
            #    positions=[422.774], tag="NII 3p1D-4s1P")
            #self.add_line_for_identification(positions=[443.274],tag="NII unidentified 2")
            #self.add_line_for_identification(
            #    positions=[444.703], tag="NII 3p1P-3d1D")
            #zeeman = False
            #if zeeman:
            #    name = "N-II-4623"
            #    self.add_multiplet_to_model(positions=self.zeeman_lines[name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[name][str(
            #        self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1, width=self.width, tag="NII 3s3P-3p3P")
            #else:
            #    self.add_line_for_identification(positions=[
            #        460.148,
            #        460.715,
            #        461.387,
            #        462.139,
            #        463.054,
            #        464.309,
            #    ], tag="NII 3s3P-3p3P")
            #self.add_line_for_identification(
            #    positions=[477.4244, 477.9722, 478.1190, 478.8138, 479.3648], tag="NII 3p3D-3d3D")
            # NIII
            self.add_line_for_identification(
                positions=[399.863, 400.358], tag="NIII 5f2F-4d2D")
            self.add_line_for_identification(
                positions=[409.733, 410.343], tag="NIII 3p2P-3s2S")
            self.add_line_for_identification(
                positions=[413.163, 413.383, 414.613, 414.678], tag="NIII 2s2p 4p4D-5s4P")
            #self.add_line_for_identification(
            #    positions=[419.574, 420.007, 421.577], tag="NIII 2s2p(3P) 3s2P-3p2D")
            #self.add_line_for_identification(
            #    positions=[433.291, 434.568], tag="NIII 2s2p3P 3p4D-3d4D")
            #self.add_line_for_identification(positions=[437.911],tag="NIII unidentified")
            #self.add_line_for_identification(
            #    positions=[451.088, 451.096, 451.485, 451.814, 452.356, 453.086, 453.458, 454.730], tag="NIII 2s2p(3P) 3s4P-3p4D")
            #self.add_line_for_identification(
            #    positions=[453.970, 454.484], tag="NIII 4p4S-5s2S")
            #self.add_line_for_identification(
            #    positions=[463.414, 464.064, 464.185], tag="NIII 3p2P-3d2D")
            #self.add_line_for_identification(positions=[485.870,485.898,486.127,486.712,486.717,487.360,488.178,488.414,489.658],tag="NIII 2s2p3P 3p4D-3d4F")
            # NIV
            self.add_line_for_identification(
                positions=[405.776], tag="NIV 4f3G-3d3F")
        if element == "neon" and central_wavelength==376:
            self.add_line_for_identification(
                positions=[366.407,369.421,370.962,373.494,375.125,376.626,377.714], tag="NeII 3s4P-3p4P")
            self.add_line_for_identification(
                positions=[364.393,371.308,372.711], tag="NeII 3s2P-3p2D")
            self.add_line_for_identification(
                positions=[379.996,381.842,382.975], tag="NeII 3p2P-3d2D")
            self.add_line_for_identification(
                positions=[381.367], tag="NeIV 3s2P-3p2S")
        if element == "neon" and central_wavelength == 691:
            self.add_line_for_identification(
                positions=[703.24128,724.51665], tag="NeI A")
            #self.add_line_for_identification(
            #    positions=[640.22480,650.65277], tag="NeI B")
            self.add_line_for_identification(
                positions=[665.20925,743.88981], tag="NeI C")
            self.add_line_for_identification(
                positions=[717.39381], tag="NeI D")
            self.add_line_for_identification(
                positions=[692.94672,702.40500], tag="NeI E")# took out 653.28824, too close to balmer alpha
            self.add_line_for_identification(
                positions=[667.82766,671.70430], tag="NeI F")
            #self.add_line_for_identification(
            #    positions=[659.89528], tag="NeI G") # too close to balmer alpha
            #self.add_line_for_identification(
            # positions=[753.57739,754.40439], tag="NeI H")
            self.add_line_for_identification(
                positions=[706.4762], tag="NeI I")
            self.add_line_for_identification(
                positions=[705.12922,705.91079], tag="NeI J")
            self.add_line_for_identification(
                positions=[721.3210,723.51978,734.3945], tag="NeII 3d2D - 4p2P")
            self.add_line_for_identification(
                positions=[688.694, 689.541], tag="NeIII 4s3S - 4p3P")
        
        if element == "neon" and central_wavelength == 378:
            self.recommended_background_position = 370
            #self.add_line_for_identification(positions=[356.850219,357.41826,357.461224],tag="NeII 2D3s-2F3p")
            self.add_line_for_identification(positions=[371.308260,372.710810],tag="NeII 2P3s-2D3p")#364.392908,
            self.add_line_for_identification(positions=[369.421454,370.962218,373.493884,375.124597,376.6259,377.713589],tag="NeII 4P3s-4P3p")#366.407402,
        if element == "neon" and central_wavelength == 364:
            self.recommended_background_position = 370
            self.add_line_for_identification(positions=[356.850219,357.41826,357.461224],tag="NeII 2D3s-2F3p")
            self.add_line_for_identification(positions=[364.392908,371.308260,372.710810],tag="NeII 2P3s-2D3p")
            self.add_line_for_identification(positions=[366.407402,369.421454,370.962218,373.493884],tag="NeII 4P3s-4P3p")#,375.124597,376.6259,377.713589
        if element == "neon" and central_wavelength < 400 and comment == "patched_spectrum":
            self.recommended_background_position = 370
            self.add_line_for_identification(positions=[356.850219,357.41826,357.461224],doppler_temperature=4,tag="NeII 2D3s-2F3p")
            self.add_line_for_identification(positions=[364.392908,371.308260,372.710810],doppler_temperature=4,tag="NeII 2P3s-2D3p")
            self.add_line_for_identification(positions=[366.407402,369.421454,370.962218,373.493884,375.124597,376.6259,377.713589],doppler_temperature=4,tag="NeII 4P3s-4P3p")#

        if element is None and central_wavelength == 405:
            self.add_line_for_identification(positions=[402.5,402.65,402.725,402.84],doppler_temperature=10,tag="OII")
            self.recommended_background_position = 401.695
            
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()
    def add_line_for_identification(self, positions,doppler_temperature, tag):
        # use the tag later to get line ratios
        for p in positions:
            self.add_multiplet_to_model(positions=[p], branching_ratios=[
                                        1],tag=tag,doppler_temperature=doppler_temperature)
        self.all_tags.append(tag)

    def print_line_ratios(self, popt,save=False):
        self.transitions = {}
        self.branching_ratios = {}
        for tag in self.all_tags:
            self.transitions[tag] = {}
            self.transitions[tag]["summed_intensities"]=0
            self.transitions[tag]["single_intensity"]={}
        for i in range(len(self.multiplets)):
            self.transitions[self.multiplets[i].tag]["summed_intensities"]+=10**(popt[i])
            self.transitions[self.multiplets[i].tag]["single_intensity"][str(self.multiplets[i].positions[0])]=10**(popt[i])
        for t in self.transitions.keys():
            self.branching_ratios[t]={}
            print(t)
            for wl in self.transitions[t]["single_intensity"].keys():
                self.branching_ratios[t][wl]=self.transitions[t]["single_intensity"][wl]/self.transitions[t]["summed_intensities"]
                print(wl,self.transitions[t]["single_intensity"][wl]/self.transitions[t]["summed_intensities"])
