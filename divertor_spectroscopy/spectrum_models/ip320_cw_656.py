import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip320_cw_656(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,zeeman,stark_density=False,exclude_wavelength_from_fit=None,special_tags=None):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.exclude_wavelength_from_fit = exclude_wavelength_from_fit
        self.strongest_line = 656.279
        self.width = 0.03
        if False: # use instrument function calibration
            with open(ground_path + "qss_analysis/json_files/instrument_function_calibration.json") as f:
                self.instrument_function = json.load(f)["IP320_2"]["30um"]["width"]
        with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
            self.zeeman_lines = json.load(f)
        if zeeman:
            self.magnetic_field = 2.5
        else:
            self.magnetic_field = 0
        self.angle_to_magnetic_field = 90
        self.recommended_background_position = 659
        # Multiplets
        
        # C
        line_name = "C-II-6580" 
        self.add_multiplet_to_model(
            positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=3, tag="CII")
        # O
        #self.add_multiplet_to_model(
        #    positions=[650.024], branching_ratios=[1], intensity=1e17,width=self.width, tag="OV")
        
        # Balmer lines
        if stark_density:
            magnetic_field = self.magnetic_field
            if not zeeman:
                magnetic_field = None
            self.add_balmer_lines_to_model(
                lines=["H_alpha"], intensities=[1e18], electron_density=1e20,electron_temperature=1, doppler_temperature=1, tag="HI",magnetic_field=magnetic_field,angle_to_magnetic_field=self.angle_to_magnetic_field)
            line_name = "H-I-6563"
            #self.add_multiplet_to_model(positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="HI_10eV")
            #self.add_multiplet_to_model(positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,width=self.width, tag="HI_50eV")
        else:
            line_name = "H-I-6563"
            self.add_multiplet_to_model(positions=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"], branching_ratios=self.zeeman_lines[line_name][str(self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"], intensity=1e17,doppler_temperature=1, tag="HI")
        
        self.clear_tag_multiplicity()
        self.get_all_model_parameters()