import importlib
import json
from scipy.interpolate import interp2d
import numpy as np
import matplotlib.pyplot as plt

from atomdat.adas import adf15

from divertor_spectroscopy import spectrum
importlib.reload(spectrum)
from divertor_spectroscopy import spectral_line_classes
from divertor_spectroscopy import utilities as utils

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/../"+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Ip160_cw_700_Ne_calibration(spectrum.Spectrum):
    """
    A spectrum class should be build in the following order:
    1:
    - normal multiplets (no physical interetation besides intensity)
    - pec lines (lines fitted by local plasma parameters and impurity density)
    - balmer lines (hydrogen lines, which have electron density information in line shape)
    2: Periodic table (H before He)
    3: Charge state: (I before II)
    4: Wavelength: (400 nm before 500nm)
    Rules:
    - Use Zeeman code, if available!
    """
    def __init__(self,variable_width=False):
        self.multiplets = []
        self.pec_lines = []
        self.balmer_lines = []
        self.all_tags = []
        self.width = 0.25
        self.variable_width = variable_width
        
        self.recommended_background_position = 700
        self.strongest_line = 703.241
        
        #self.add_multiplet_to_model(positions=[650.65277],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        #self.add_multiplet_to_model(positions=[653.28824],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[659.89528],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[665.20925],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[667.82766],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[671.70430],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[692.94672],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[702.40500],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[703.24128],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[705.12922],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[705.91079],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[717.39380],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[724.51665],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[743.88981],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[747.24383],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.add_multiplet_to_model(positions=[748.88712],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        #self.add_multiplet_to_model(positions=[753.57739],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        #self.add_multiplet_to_model(positions=[754.40439],branching_ratios=[1],doppler_temperature=0.1,intensity=1,tag="NeI")
        self.clear_tag_multiplicity()