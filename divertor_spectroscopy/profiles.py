import numpy as np

from w7xdia import thomson as ts
from w7xdia.fits import fit_profile_lowess
from w7xdia import interferometer
import w7xdia.fits as w7xdia_fits
import w7xdia.cxrs
import w7xdia.campaigns as campaign
import w7xdia.equilibrium as equi
from scipy.interpolate import RegularGridInterpolator as RGI
from scipy.interpolate import interp1d
import logging
import matplotlib.pyplot as plt
from copy import deepcopy

"""
Module to load raw Thomson ne/Te + CXRS Ti profile data from the archive and create fits of the raw
data. Can compute MC samples and MC error using the w7xdia package.
"""

logger = logging.getLogger("PTA")

#! important parameters, changes gradients! need sophisticated way of setting it automatically!
# if binning is used
bin_edges = np.arange(0.0, 0.60, 0.07)
bin_centers = bin_edges[1:] - (bin_edges[1:] - bin_edges[:-1])/2
# if lowesss fit is used
lowess_window = 0.05
lowess_window_cxrs = 0.04

near_neighbours = None

# cxrs Ti channel rejects for every shot
channel_rejects = ["AEA21_A:40", "AEA21_A:36", "AEA21_A:34","AEA21_A:32","AEA21_A:33","AEM21_S7:51", "AEM21_S7:44", "AEA21_B:38", "AEA21_B:40"]

# use this path to obtain interferometer data for absolute density correction
corrected_interf_path = "ArchiveDB/raw/W7XAnalysis/QMJ_IEDDI/IED_LIA_DATASTREAM/V2/1/density"

# thomson volumes rejects per shot ID
# volumeRejects = {
# 	'20180919.039' : [16, 1006, 1018, 1070, 1073, 1074, 1075],
#     '20180919.051' : [1006, 1018, 1064, 1070, 1073, 1074, 1075, 1076],
# 	'20180919.055' : [16, 1006, 1018, 1064, 1070, 1073, 1074],
# 	'20181009.024' : [15, 1006, 1018, 1064, 1069, 1070, 1073, 1074, 1075, 1076],
# 	'20181009.034' : [16, 1006, 1018],
# 	'20181009.043' : [16, 1006, 1018, 1073, 1075],
# 	'20181009.018' : [16, 1006, 1018, 1075],
# }

def get_thomson_profiles(shotID, rho, time_points, vmecIDs, fitting_mode, num_samples, delta_t_forced=None,branch=None,
                         exclude_laser=None, exclude_volumes=None, remove_outliers=False):
    """
    Get Thomson electron density and temperature profile fits using w7xdia package. Return fits and 
    raw data loaded from archive. Density profile is rescaled using interferometer data.
    There are now several versions and places of Thomson data. For OP1.2B standard datastream is available and should
    be used. For OP2.1 standard only available for some discharges. Standard refers to the Minerva analysis of Thomson
    data. An alternative analysis of raw Thomson data is performed on the 'boz' branch which is available since OP2.
    All data loading is done using w7xdia, so to see the used data streams check out w7xdia. In this routine use the
    branch parameter to use other data then the standard data stream or to use an automated choosing of data.

    Args:
        shotID (_type_): Discharge ID
        rho (_type_): Normalised effective radius [m]
        time_points (_type_): time points [s]
        vmecIDs (_type_): Vmec ID of every time point
        fitting_mode (_type_): 'linear_interpolation', 'binning_linear_interpolation', 'lowess',
        'binning_lowess'
        num_samples (int): Number of profile samples for error analysis
        delta_t_forced (double): Force time window to average thomson data
        branch(str): None==standard, "fast", "prelim", "boz", "standard", None (auto choose)
        exclude_laser(int): Laser ID to be excluded
        exclude_volumes(int array): List of thomson volumes which are excluded
        remove_outliers(bool): Remove outlier data points from the raw data before fitting
    Returns:
        dict: \\
        'ne_profile_fits' : ne[iTimePoint][iRho][iSample] \\
        'reff_raw' : thomson raw data reff \\
        'ne_profile_raw' : raw thomson density data \\
        'te_profile_fits' : te[iTimePoint][iRho][iSample] \\
        'te_profile_raw' : raw thomson electron temp data
    """
    shape = (len(time_points), len(rho), num_samples)
    if len(vmecIDs) != len(time_points):
        raise RuntimeError("Number of timepoints must equal number of vmec IDs and minor radii")

    if delta_t_forced is not None:
        delta_t = delta_t_forced
    elif len(time_points) > 1:
        delta_t = (time_points[1] - time_points[0])
    else:
        delta_t = 0.05
        logger.info("Single time point --> using delta_t = 50ms")

    version, ts_vols, time_ts, ne_orig, te_orig = get_thomson_data(shotID, time_points, delta_t, branch, exclude_laser,
                                                exclude_volumes,remove_outliers)

    # array to store results of each time point
    ne_fits, te_fits = np.zeros(shape), np.zeros(shape)
    ne_raw, te_raw, ts_reff = [], [], []

    for iT, tp in enumerate(time_points):
        # get reff of thomson volumes
        reff_ts = None
        if branch is not None:
            reff_ts = ts.get_volume_reffs(shotID, vmecIDs[iT], version=version, useRecommendedVersion=False,
                                          volumes=ts_vols,
                                          branch=branch)
        else:
            reff_ts = ts.get_volume_reffs(shotID, vmecIDs[iT], version=version, useRecommendedVersion=False,
                                          volumes=ts_vols)

        # average data over time window and take time scatter as uncertainty estimate
        iT_ts_1 = np.argmin(np.abs(time_ts - (tp-delta_t/2)))
        iT_ts_2 = np.argmin(np.abs(time_ts - (tp+delta_t/2)))+1

        ne_sigma = np.std(ne_orig[iT_ts_1:iT_ts_2, :], axis=0)
        te_sigma = np.std(te_orig[iT_ts_1:iT_ts_2, :], axis=0)
        ne = np.average(ne_orig[iT_ts_1:iT_ts_2, :], axis=0)
        te = np.average(te_orig[iT_ts_1:iT_ts_2, :], axis=0)

        # order in ascending reff
        order = np.argsort(reff_ts)
        reff_ts = reff_ts[order]
        ne = ne[order]
        te = te[order]
        ne_sigma = ne_sigma[order]
        te_sigma = te_sigma[order]
        
        # remove nans
        valid = ~np.isnan(ne)
        valid = ~np.isnan(te) * valid
        reff_ts = reff_ts[valid]
        ne = ne[valid]
        te = te[valid]
        ne_sigma = ne_sigma[valid]
        te_sigma = te_sigma[valid]

        # for now we are not using error supplied by data source at is too small for some vals
        #ne_sigma = data_ts[iT_ts_1, valid, 13][order]
        #te_sigma = data_ts[iT_ts_1, valid, 12][order]

        # set density error to minimum of 10% or 0.5e19
        ne_sigma[ne_sigma<0.5] = 0.5
        ne_sigma[ne_sigma<0.1*ne] = 0.1*ne[ne_sigma<0.1*ne]
        # set electron temp error to minimum of 100eV
        te_sigma[te_sigma<0.1] = 0.1

        # reff to interpolate everything to
        minor_radius = equi.get_minor_radius(vmecIDs[iT])
        reff = rho * minor_radius

        ne_fits[iT] = fit_profile_MC_error(reff, reff_ts, ne, ne_sigma, fitting_mode,
                                           near_neighbours=near_neighbours, window=lowess_window,
                                           num_mc_samples=num_samples, return_mc_samples=True)
        
        te_fits[iT] = fit_profile_MC_error(reff, reff_ts, te, te_sigma, fitting_mode,
                                            near_neighbours=near_neighbours, window=lowess_window,
                                           num_mc_samples=num_samples, return_mc_samples=True)

        # store raw data
        ne_raw.append(ne)
        te_raw.append(te)
        ts_reff.append(reff_ts)

    # fill raw data with 0s until they have same length (as we excluded nans before)
    reff_raw, ne_raw = _equalize_raw_data_length(deepcopy(ts_reff), ne_raw)
    reff_raw, te_raw = _equalize_raw_data_length(deepcopy(ts_reff), te_raw)

    ret = {}
    ret['ne_profile_fits'] = ne_fits * 1e19
    ret['reff_raw'] = np.stack(reff_raw, axis=0)
    ret['ne_profile_raw'] = np.array(ne_raw) * 1e19
    ret['te_profile_fits'] = te_fits
    ret['te_profile_raw'] = np.array(te_raw)
    return ret


def get_thomson_data(shotID, time_points, delta_t, branch=None,exclude_laser=None, exclude_volumes=None,
                     remove_outliers=False):
    """
    Get Thomson scattering profile data for shot. The data is corrected using the line integrated density data from the
    interferometer. Data returned can still contain NaNs. Time points where all volumes are NaN are removed entirely.
    @param shotID:
    @param time_points:
    @param delta_t: time averaging window
    @param branch: If not set tries to use standard if available otherwise 'boz' branch
    @param exclude_laser: integer for laser
    @param exclude_volumes: Can exclude thomson volumes
    @param remove_outliers: Uses a 2D z score (z=1.5) outlier detection and replacement
    @return: version, volumes, time axis, electron density , electron temp
    """
    ts_vols = None
    if branch is None:
        if campaign.is_shot_in_op12b(shotID):
            ts_vols = ts.get_list_of_volumes(shotID, None)
            d = ts.get_data_from_archive(shotID, volumes=ts_vols, branch=None, useRecommendedVersion=True,
                                         returnVersion=True)
        elif campaign.is_shot_in_op21(shotID) or campaign.is_shot_in_op22(shotID):
            try:
                ts_vols = ts.get_list_of_volumes(shotID, None, branch='standard', useLastVersion=True, timeout=10)
                d = ts.get_data_from_archive(shotID, volumes=ts_vols, branch='standard', useLastVersion=True,
                                             returnVersion=True)
                branch = 'standard'
                logger.info("Using thomson data from 'standard' branch")
            except:
                ts_vols = ts.get_list_of_volumes(shotID, None, branch='boz', useLastVersion=True, timeout=10)
                d = ts.get_data_from_archive(shotID, volumes=ts_vols, branch='boz', useLastVersion=True,
                                             returnVersion=True)
                branch = 'boz'
                logger.warning("Using thomson data from 'boz' branch")
    else:
        ts_vols = ts.get_list_of_volumes(shotID, None, branch=branch, useLastVersion=True)
        d = ts.get_data_from_archive(shotID, volumes=ts_vols, branch=branch, returnVersion=True, useLastVersion=True)

    data, version = d[0], d[1]
    time_ts = data[0]
    data_ts = data[1]

    # exclude specified volumes
    if exclude_volumes is not None:
        mask = ~np.isin(ts_vols, exclude_volumes)
        data_ts = data_ts[:, mask, :]

    # in OP2 exclude data from laser 4
    if exclude_laser is not None:
        ls = ts.read_laser_stream(shotID)  # ls[0] : time points ; ls[1][:,0] is the actual laser stream
        if np.sum(ls[0] - time_ts) > 1e-5: raise RuntimeError(
            "Time axis of laser stream and thomson data are not equal!")

        mask = ls[1][:, 0] != exclude_laser
        time_ts = time_ts[mask]
        data_ts = data_ts[mask]

    # Slice out data and correct electron density values using interferometer
    t_min, t_max = time_points[0] - delta_t / 2, time_points[-1] + delta_t / 2
    i_min, i_max = np.argmin(np.abs(time_ts - t_min)), np.argmin(np.abs(time_ts - t_max))

    time_ts = time_ts[i_min:i_max]
    ne = data_ts[i_min:i_max, :, 1]
    te = data_ts[i_min:i_max, :, 0]

    # remove time points and data where all volumes are NaN
    mask = ~np.all(np.isnan(ne), axis=1)
    time_ts = time_ts[mask]
    ne = ne[mask]
    te = te[mask]

    ne_correction = get_density_correction_factors(shotID, time_ts, ne, branch=branch)
    ne *= ne_correction[:, np.newaxis]

    if remove_outliers:
        time_intervals = np.empty((len(time_points) + len(time_points)))
        time_intervals[0::2] = np.array(time_points) - delta_t / 2
        time_intervals[1::2] = np.array(time_points) + delta_t / 2
        ne = replace_outliers_2d(ne, time_ts, None, x_intervals=time_intervals , num_y_intervals=int(ne.shape[1]/7), z_threshold=1.5)
        te = replace_outliers_2d(te, time_ts, None, x_intervals=time_intervals, num_y_intervals=int(te.shape[1] /6), z_threshold=1.5)

    return version, ts_vols, time_ts, ne , te



def get_cxrs_ti_profile(shotID, rho, time_points, vmecIDs, minor_radius, fitting_mode, num_samples, remove_outliers=False):
    ti_fits = np.zeros((len(time_points), len(rho), num_samples))
    
    # spectrometers = ['ILS_Green', 'AUG1', 'AUG2']
    spectrometers = ['ILS_Green']
    # op2 nifs_c
    
    reff_raw, Ti_raw, los_names = [], [], []
    useBranch = None
    for iT, t in enumerate(time_points):

        reff_fixed = rho * minor_radius[iT]

        # lists to fill in data of each used spectrometer
        reff_raw_iT, Ti_raw_iT, los_names_iT = np.array([]), np.array([]), np.array([])
        Ti_sigma_iT = np.array([])
        for iS, spectrometer in enumerate(spectrometers):
            # load data from archive, determine branch only once
            if iT == 0:
                try:
                    cx_data = w7xdia.cxrs.get_all(shotID, spectrometer=spectrometer, head=None, analysisBranch="Preferred", timeout=10)
                    useBranch = "Preferred"
                except:
                    try:
                        logger.info("CXRS: Trying BGSubtract branch for Ti data")
                        cx_data = w7xdia.cxrs.get_all(shotID, spectrometer=spectrometer, head=None, analysisBranch="BGSubtract", timeout=10)
                        useBranch = "BGSubtract"
                    except:
                        logger.debug("CXRS: Trying DualGauss branch for Ti data")
                        try:
                            cx_data = w7xdia.cxrs.get_all(shotID, spectrometer=spectrometer, head=None, analysisBranch="DualGauss", timeout=10)
                            useBranch = "DualGauss"
                        except:
                            logger.warning("No CXRS data found")
                            return None
            else:
                try:
                    cx_data = w7xdia.cxrs.get_all(shotID, spectrometer=spectrometer, head=None,analysisBranch=useBranch, timeout=10)
                except:
                    logger.warning("No CXRS data found for spec" + spectrometer + " @ time point " + str(t))
                    return None

            # determine time interval for averaging data
            if len(time_points) > 1:
                delta_t = (time_points[1] - time_points[0])
            else:
                delta_t = 0.05
            
            iTCX = np.argmin(np.abs(cx_data['time'] - t))
            iTCX1 = np.argmin(np.abs(cx_data['time'] - (t-delta_t/2)))
            iTCX2 = np.argmin(np.abs(cx_data['time'] - (t+delta_t/2)))
            
            # if time resolution of CXRS Ti profiles is low 
            if iTCX2 == iTCX1: iTCX2+=1
            
            # write beta vmec reff into cx_data dict
            w7xdia.cxrs.get_rEff(cx_data, vmecIDs[iT])
            
            # line of sight names
            losnames = cx_data['losNames']
            
            # index valid for valid data points
            valid = np.where(np.isfinite(cx_data['nominalREff']))[0]
            # filter out points outside R=6.0 that didn't measure anything
            valid=valid[cx_data['nominalR'][valid] < 6.0 ]
            # exclude T port channels and rejected channels as defined at beginning of script
            used_channels = np.ones(len(valid))
            for i in range(0, len(valid)):
                if "AET" in losnames[valid][i][0:3]: used_channels[i] = 0
                # if "AEM" in losnames[valid][i][0:3]: used_channels[i] = 0
                for chan_rej in channel_rejects:
                    if chan_rej in losnames[valid][i]: used_channels[i] = 0
                
            valid = valid[used_channels==1]
            
            # now we average over time interval and get rid of NaN channels in the process
            Ti = np.zeros_like(cx_data['Ti'][iTCX,valid])
            num_vals = np.zeros_like(Ti)
            for i in range(iTCX1, iTCX2+1):
                Tvalid = cx_data['Ti'][i,valid]
                ids = np.where(np.isnan(cx_data['Ti'][i,valid]))[0]
                Tvalid[ids] = 0.
                Ti += Tvalid
                num_vals += np.ones_like(num_vals) * (~np.isnan(cx_data['Ti'][i,valid]))
                
            num_vals[num_vals == 0] = 1  # if a channel was NaN all the time still need to divide by 1
            Ti /= num_vals
         
            # average time interval and get Ti and reff for spectrometer
            # Ti = (np.average(cx_data['Ti'][iTCX1:iTCX2,valid], axis=0)).squeeze()
            Ti_sigma = (np.average(cx_data['TiErr'][iTCX1:iTCX2,valid], axis=0)).squeeze()
            
            reff = (cx_data['nominalREff'][valid]).squeeze()
            
            # flatten out outliers
            if remove_outliers:
                Ti = moving_outlier_rejection(Ti, 1, 2)
            
            Ti_raw_iT = np.append(Ti_raw_iT, Ti[:])
            Ti_sigma_iT = np.append(Ti_sigma_iT, Ti_sigma[:])
            
            reff_raw_iT = np.append(reff_raw_iT, reff[:])
            los_names_iT = np.append(los_names_iT, losnames[valid][:])
        
        # order values with increasing reff
        order = np.argsort(reff_raw_iT)
        Ti_raw_iT = np.array(Ti_raw_iT)[order]
        Ti_sigma_iT = np.array(Ti_sigma_iT)[order]
        reff_raw_iT = np.array(reff_raw_iT)[order]
        los_names_iT = np.array(los_names_iT)[order]
        
        #! should be unneccessary now
        # we should have gotten rid of all nans but sometimes one sneaks through and we remove it here
        # ids = np.where(~np.isnan(Ti_raw_iT))[0]
        # Ti_raw_iT = Ti_raw_iT[ids]
        # Ti_sigma_iT = Ti_sigma_iT[ids]
        # reff_raw_iT = reff_raw_iT[ids]
        
        Ti_raw.append(Ti_raw_iT)
        reff_raw.append(reff_raw_iT)
        los_names.append(los_names_iT)
        
        # use monte carlo error estimation to fit profiles from data
        Ti_sigma_iT = None
        
        if len(reff_raw_iT) == 0 or len(Ti_raw_iT) == 0:
            logger.warning("No CXRS Ti data @ " + shotID + '@' + str(t))
            return None

        # fit profile samples
        ti_fits[iT] = fit_profile_MC_error(reff_fixed, reff_raw_iT, Ti_raw_iT, Ti_sigma_iT, fitting_mode, 
                                            window=lowess_window_cxrs, num_mc_samples=num_samples, return_mc_samples=True)
            
    # fill raw data with 0 until they have same length
    reff_raw, Ti_raw = _equalize_raw_data_length(reff_raw, Ti_raw)
    
    ret = {}
    ret['reff_raw'] = np.stack(reff_raw, axis=0)
    ret['ti_profile_raw'] = np.stack(Ti_raw) / 1000
    ret['ti_profile_fits'] = ti_fits / 1000
    ret['los_names'] = los_names
      
    return ret


def get_xics_Ti_profile(shotID, rho, time_points, minor_radius, fitting_mode, num_samples, remove_outliers=False, correction=150):
    """
    Get Ti profile XICS data inversion. As data is already processed no vmecID can be set explcitly
    and the data points are interpolated to specified time grid

    Args:
        shotID (_type_): Discharge ID
        rho (_type_): Fitting grid for profiles
        time_points (_type_): XICS data is interpolated 
        minor_radius (_type_): Minor radius for each time point
        fitting_mode (_type_):'linear_interpolation', 'binning_linear_interpolation', 'lowess',
        'binning_lowess'
        num_samples (_type_): Monte Carlo samples generated for error propagation
        remove_outliers (bool, optional): _description_. Defaults to False.
        correction (int, optional): _description_. Defaults to 150 eV

    Returns:
        Dic : fitted profiles
    """
    import w7xdia.xics as xics  # requires to have mdsplus installed
    
    ti_fits = np.zeros((len(time_points), len(rho), num_samples))
    
    # get inverted xics Ti profiles, reff is given for each time point
    data = xics.get_inverted_Ti(shotID)
    t_xics = data[0]

    reff_xics_interp = interp1d(t_xics, data[1], axis=1)
    sigma_xics_interp = interp1d(t_xics, data[3], axis=1)
    ti_xics_interp = interp1d(t_xics, data[2], axis=1)
    reff_raw, Ti_raw = [], []
    
    for iT, tp in enumerate(time_points):
        reff = reff_xics_interp(tp)
        sigma = sigma_xics_interp(tp)
        Ti = ti_xics_interp(tp) - correction / 1000
        sigma=None
        ti_fits[iT] = fit_profile_MC_error(rho*minor_radius[iT], reff, Ti, sigma, fitting_mode, window=lowess_window_cxrs,
                                           num_mc_samples=num_samples, return_mc_samples=True)
        reff_raw.append(reff)
        Ti_raw.append(Ti)
        
    ret = {}
    ret['reff_raw'] = np.stack(reff_raw, axis=0)
    ret['ti_profile_raw'] = np.stack(Ti_raw)
    ret['ti_profile_fits'] = ti_fits
    return ret
    

def fit_profile_MC_error(reff, reff_prof, prof, prof_sigma, fitting_mode, num_mc_samples, **kwargs):
    
    # remove outlier data points
    # prof = moving_outlier_rejection(prof, m=0.5, window=int(len(reff)/52 * 3))
      
    if fitting_mode == 'linear_interpolation':
        prof_fit = np.interp(reff, reff_prof, prof)[:,np.newaxis] * np.ones((len(reff), num_mc_samples))
        
    elif fitting_mode == 'binning_linear_interpolation':
        prof_binned = bin_data(reff_prof, prof, bin_edges)

        prof_fit = np.interp(reff, bin_centers , prof_binned)[:,np.newaxis] * np.ones((len(reff), num_mc_samples))
        
    elif fitting_mode == 'lowess':
        # lowess fits with monte carlo error estimation
        prof_fit = w7xdia_fits.estimate_fit_error_montecarlo(reff, reff_prof, prof, prof_sigma, method=fit_profile_lowess,num_mc_samples=num_mc_samples, **kwargs)[1]
        
    elif fitting_mode == 'binning_lowess':
        # same as lowess but data is first binned
        prof_binned = bin_data(reff_prof, prof, bin_edges)
        
        prof_fit = w7xdia_fits.estimate_fit_error_montecarlo(reff, bin_centers, prof_binned, None, method=fit_profile_lowess,num_mc_samples=num_mc_samples, **kwargs)[1]

    return prof_fit
         

def get_density_correction_factors(shotID, ts_time, ts_ne, branch=None):
    """
    Get Thomson electron density correction factor from interferometer line integrated density
    @param ts_time: time axis of thomson data
    @param ts_ne: thomson data [iTime, iChannel]
    @param branch: branch used to obtain thomson volume positions
    @return: correction factor for each Thomson time point (thomson time axis)
    """
    # real space volume positions
    ts_vols_pos = ts.get_volume_positions(shotID, branch=branch, useLastVersion=True)

    # calc relative spacing of thomson volumes
    ts_R = np.linalg.norm(ts_vols_pos - ts_vols_pos[0], axis=1)

    # get interferometer data which we need for correction
    try:
        tLID, dLID = interferometer.get_line_integrated_density(shotID, path=corrected_interf_path)
    except:
        logger.warning("Corrected interferometer datastream not available @ " + corrected_interf_path)
        tLID, dLID = interferometer.get_line_integrated_density(shotID)

    # interferometer
    i2_min, i2_max = np.argmin(np.abs(tLID - ts_time[0])), np.argmin(np.abs(tLID - ts_time[-1]))+1

    # estimate ne correction
    ne_correction = ts.estimate_ne_correction(ts_time, ts_ne, ts_R, tLID[i2_min:i2_max], dLID[i2_min:i2_max])

    return ne_correction


def moving_outlier_rejection(data, m=1., window=1):
    """
    Moving window algorithm to remove outlier data points. Value is rejected if the absolute 
    difference to the average over the window is greater than m*mean. Rejected values are replaced
    by the mean of the considered values left and right.

    Args:
        data (_type_): 1D array of data
        m (_type_, optional): The smaller the more values will be rejected. Defaults to 1..
        window (int, optional): Number of values left AND right of data[i] considered. Defaults to 1.

    Returns:
        _type_: 1D array of data with outliers replaced
    """
    for i in range(0, len(data)):
        win_vals = []
        for j in range(-window, window+1):
            if j == 0: continue
            if (i+j)>= 0 and (i+j) < len(data):
                k = i+j 
            else: continue 
            win_vals.append(data[k])
            
        win_mean = np.mean(win_vals)
            
        if abs(data[i] - win_mean) > m * win_mean:
            logger.warning("Outlier removed: " + str(data[i]) + " mean: " + str(win_mean))
            data[i] = win_mean
    return data


def _equalize_raw_data_length(reff_input, data):
    num_vals = 0
    reff = reff_input.copy()
    for iT in range(0, len(reff)):
        if len(reff[iT])>num_vals: num_vals = len(reff[iT])
        
    for iT in range(0, len(reff)):
        if len(reff[iT]) < num_vals:
            if len(reff[iT]) != len(data[iT]):
                raise RuntimeError("Arrays do not have same length\n" + reff[iT] + " \n<->\n" + data[iT])
            reff[iT] = np.append(reff[iT], np.zeros((num_vals-len(reff[iT]))))
            data[iT] = np.append(data[iT], np.zeros((num_vals-len(data[iT]))))
    return reff, data



def bin_data(rho, data, bin_edges):
    ret = np.zeros(len(bin_edges)-1)
    for j in range(1, len(bin_edges)):
        idx = np.where(np.logical_and(rho<=bin_edges[j], rho>bin_edges[j-1]))[0]
        if len(idx) == 0 : 
            logger.warning("No data in bin interval! " + str(np.round(bin_edges[j-1],2)) +"< rho < "+  str(np.round(bin_edges[j],2)))
            continue
        ret[j-1] = np.sum(data[idx] / len(idx))
    return ret





def replace_outliers_2d(arr, x, y, x_intervals=None, y_intervals=None, num_x_intervals=None,
                                       num_y_intervals=None, z_threshold=3):
    """
    Replace outliers in a 2D array using Z-score thresholding within specified intervals.

    This function divides a 2D array into sub-arrays based on specified x and y intervals or
    a specified number of intervals. It calculates the Z-score for each element in the sub-arrays
    and replaces elements that are considered outliers (based on the Z-score threshold) with the
    mean of the non-outlier elements within the same sub-array.

    Parameters:
    - arr (np.ndarray): The 2D array in which to replace outliers.
    - x (np.ndarray): The x-coordinates corresponding to the rows of `arr`.
    - y (np.ndarray): The y-coordinates corresponding to the columns of `arr`.
    - x_intervals (list, optional): Specific intervals along the x-axis to define sub-arrays.
    - y_intervals (list, optional): Specific intervals along the y-axis to define sub-arrays.
    - num_x_intervals (int, optional): Number of equal intervals along the x-axis.
    - num_y_intervals (int, optional): Number of equal intervals along the y-axis.
    - z_threshold (float, optional): The Z-score threshold to identify outliers. Default is 3.

    Returns:
    - np.ndarray: The modified 2D array with outliers replaced by the mean of non-outliers
                  within each sub-array.
    """
    # indices
    if x_intervals is not None:
        x_idx_min = np.searchsorted(x, x_intervals, side='left')
        x_idx_max = np.searchsorted(x, x_intervals, side='right')
    elif num_x_intervals is not None:
        inds = np.linspace(0, arr.shape[0], num_x_intervals + 1, dtype=int)
        x_idx_min = inds
        x_idx_max = inds
    else:
        x_idx_min, x_idx_max = [0], [0, arr.shape[0]]

    if y_intervals is not None:
        y_idx_min = np.searchsorted(y, y_intervals, side='left')
        y_idx_max = np.searchsorted(y, y_intervals, side='right')
    elif num_y_intervals is not None:
        inds = np.linspace(0, arr.shape[1], num_y_intervals + 1, dtype=int)
        y_idx_min = inds
        y_idx_max = inds
    else:
        y_idx_min, y_idx_max = [0], [0, arr.shape[1]]

    # Find and replace outliers
    for i in range(0, len(x_idx_max)-1):
        for j in range(0, len(y_idx_max)-1):
            sub_arr = arr[x_idx_min[i]:x_idx_max[i+1], y_idx_min[j]:y_idx_max[j+1]]

            if sub_arr.shape[0] == 1 and sub_arr.shape[1] == 1:
                continue

            # Compute Z-scores for the subset of the array
            mean = np.mean(sub_arr)
            std = np.std(sub_arr)
            z_scores = (sub_arr - mean) / std

            # Detect outliers based on the Z-score threshold
            outliers = np.abs(z_scores) > z_threshold

            # Replace outliers with the mean of non-outlier data
            non_outlier_mean = np.mean(sub_arr[~outliers])
            sub_arr[outliers] = non_outlier_mean

            # Place the modified subset back into the original array
            arr[x_idx_min[i]:x_idx_max[i+1], y_idx_min[j]:y_idx_max[j+1]] = sub_arr

    return arr


def adjust_outside_LCFS(profile_samples, rho, from_rho, min_val):
    """
    Linearily decrease plasma profiles from a given rho value to the max tho value
    @param profile_samples: Plasma profiles
    @param rho: Rho vector of given profiles
    @param from_rho: Decrease from here to rho max
    @param min_val: Value at the last rho value
    @return: Tweaked plasma profiles
    """
    rho_max = rho[-1]
    idx = np.argmin(np.abs(rho - from_rho))
    num_new_vals = len(rho) -idx
    avg_last_val = np.average(profile_samples[:,idx,:], axis=1)

    if min_val is None:
        min_val = avg_last_val / 10

    new_vals = np.linspace(avg_last_val, min_val, num_new_vals)
    profile_samples[:, idx:, :] = new_vals.T[:,:,np.newaxis]
    return profile_samples


# if __name__ == '__main__':
#     shotID = "20181009.034"
#     rho = np.linspace(0., 1, 100)
#     time_points = [2.8]
#     vmecIDs = ["w7x_ref_352"]
#     a = [0.52]
#
#     ti = get_xics_Ti_profile(shotID, rho, time_points, a, 'linear_interpolation', 80)
#
#     fig, axes = plt.subplots(2,1)
#     ne, te = get_thomson_profiles(shotID, rho, time_points, vmecIDs, a, 'linear_interpolation', 80)
#     axes[0].plot(rho, ne[0], color='blue', alpha=0.5)
#     axes[1].plot(rho, np.gradient(ne[0], axis=0), color='blue', alpha=0.5)
#
#
#     ne, te = get_thomson_profiles(shotID, rho, time_points, vmecIDs, a, 'lowess', 80)
#     axes[0].plot(rho, ne[0], color='orange', alpha=0.5)
#     axes[1].plot(rho, np.gradient(ne[0], axis=0), color='orange', alpha=0.5)
#
#     plt.show()
    

if __name__ == '__main__':
    from particle_transport.profiles import replace_outliers_2d_with_intervals
    # Example usage:
    arr = np.array([
        [1, 2, 100, 4],
        [5, 6, 7, 150],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ])

    # 1D vectors representing the axis values (e.g., time or some other parameter)
    x = np.array([0.0, 0.2, 0.4, 0.6])  # for rows
    y = np.array([0.0, 0.2, 0.4, 0.6])  # for columns

    # Specify intervals on x and y axes
    x_interval = (0., 0.3, 0.6)
    y_interval = (0., 0.6)
    num_x=2
    num_y=2

    print(arr)
    #print(replace_outliers_2d_with_intervals(np.copy(arr), x, y, x_interval, y_interval, z_threshold=2) - arr)
    print(replace_outliers_2d_with_intervals(np.copy(arr), x, y, num_x_intervals=num_x, num_y_intervals=num_y, z_threshold=2) - arr)
