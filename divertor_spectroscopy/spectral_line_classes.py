"""
Class to access different spectral features:
To-do-list:
    - Gaussian lines
    - Multiplets
    - Nitrogen including PECs
    - Balmer lines
"""
# official python packages
import numpy as np
import json
from scipy.interpolate import interp2d
import importlib
import logging
logger = logging.getLogger(__name__)

# my python packages
from divertor_spectroscopy import atomic_data
from divertor_spectroscopy import utilities as utils
importlib.reload(atomic_data)
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

class Multiplet():
    def __init__(self, positions=[402.509, 403.935], branching_ratios=[0.944, 0.056], doppler_temperature=0, intensity=1, fit_broadening=False, tag=None):
        self.positions = np.array(positions)
        self.branching_ratios = np.array(branching_ratios)
        self.intensity = intensity
        self.doppler_temperature = doppler_temperature
        self.fit_broadening = fit_broadening
        self.fit_intensity = True
        self.tag = tag
        self.multiplicity = len(positions)
        if len(positions) != len(branching_ratios):
            logger.critical("Should have same dimension -> something is wrong")
        if np.sum(branching_ratios) > 1.00001 or np.sum(branching_ratios)<0.99999:
            logger.warning("{} Sum of brancing ratios is incorrect: {}".format(positions,
                                                                      np.sum(branching_ratios)))
class Lorentzian():
    def __init__(self, position=397.007, broadening=0.04, intensity=1, tag=None):
        self.position = position
        self.intensity = intensity
        self.broadening = broadening
        self.tag = tag


class Voigtian():
    def __init__(self, position=397.007, width=0.04, broadening=0.05, intensity=1, tag=None):
        self.position = position
        self.intensity = intensity
        self.broadening = broadening
        self.width = width
        self.tag = tag


class Balmer_lines():
    def __init__(self, lines=["H_epsilon"], intensities=[1], electron_density=0.5e20, electron_temperature=1,doppler_temperature=3, tag=None, magnetic_field=None, angle_to_magnetic_field=90,fit_broadening=False,fit_doppler_shift=False,doppler_shift=0):
        self.lines = lines
        self.intensities = intensities
        self.electron_density = electron_density
        self.electron_temperature = electron_temperature
        self.doppler_temperature=doppler_temperature
        self.tag = tag
        self.magnetic_field = magnetic_field
        self.angle_to_magnetic_field = angle_to_magnetic_field
        self.fit_broadening = fit_broadening
        self.fit_doppler_shift = fit_doppler_shift
        self.doppler_shift = doppler_shift
        self.lomanowski_conversion_coefficients = atomic_data.get_lomanowski_conversion_coefficients(
            lines)
        self.positions = []
        self.branching_ratios = []
        for line in self.lines:
            if magnetic_field is None:
                self.positions.append(
                    [self.lomanowski_conversion_coefficients[line]["p"]])
                self.branching_ratios.append([1])
            else:
                logger.debug("Using Zeeman split lines")
                with open(ground_path + "qss_analysis/json_files/zeeman_split_lines.json") as f:
                    self.zeeman_lines = json.load(f)
                if line == "H_epsilon":
                    zeeman_name = "H-I-3970"
                if line == "H_delta":
                    zeeman_name = "H-I-4102"
                if line == "H_gamma":
                    zeeman_name = "H-I-4341"
                if line == "H_alpha":
                    zeeman_name = "H-I-6563"
                self.positions.append(self.zeeman_lines[zeeman_name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["wavelength"])
                self.branching_ratios.append(self.zeeman_lines[zeeman_name][str(
                    self.magnetic_field)][str(self.angle_to_magnetic_field)]["branching_ratio"])
        self.branching_ratios = np.array(self.branching_ratios)
        self.positions = np.array(self.positions)
        self.fwhm = self.get_fwhm(
            ne=self.electron_density, Te=self.electron_temperature)

    def get_fwhm(self, ne, Te):
        self.fwhm = []
        for line in self.lines:
            self.fwhm.append(self.lomanowski_conversion_coefficients[line]["C"] * ne **
                             self.lomanowski_conversion_coefficients[line]["a"] / Te**self.lomanowski_conversion_coefficients[line]["b"])
        return self.fwhm


class Pec_line():
    def __init__(self, multiplets, element="n", ion=1, indices=None, ne_range=[1e19, 1e21], Te_range=[1, 10], ne_count=100, Te_count=101, electron_density=1e20, electron_temperature=4, impurity_density=1e19, emission_length=0.1,fit_electron_density=True,fit_electron_temperature=True,fit_impurity_density=True, cache=False, save=False, debug=False):
        # all multiplets of a pec line should have saved their own intensity

        # where do I get add_multiplet_to_model from? Similar purpose
        # sollte ich hier schon die richtige Intensität der linie eintragen?
        self.multiplets = multiplets
        self.element = element
        self.ion = ion
        self.indices = indices
        self.ne_range = ne_range
        self.Te_range = Te_range
        self.ne_count = ne_count
        self.Te_count = Te_count
        self.electron_density = electron_density
        self.electron_temperature = electron_temperature
        self.impurity_density = impurity_density
        self.emission_length = emission_length
        self.fit_electron_density = fit_electron_density
        self.fit_electron_temperature = fit_electron_temperature
        self.fit_impurity_density = fit_impurity_density
        self.tag = element
        self.tag = self.tag.title()
        for i in range(ion+1):
            self.tag = self.tag + "I"
        self.ne_array, self.Te_array, self.frac, self.pecs, self.line_intensities = atomic_data.get_line_intensities(
            element=element, ion=ion, indices=indices,ne_count=ne_count, Te_count=Te_count, ne_range=ne_range, Te_range=Te_range, save=save, cache=cache, debug=debug)
        self.interpolated_line_intensity_functions = []
        for m in range(len(self.multiplets)):
            self.interpolated_line_intensity_functions.append(interp2d(
                self.ne_array, self.Te_array, self.line_intensities[m].T, bounds_error=True))  # I want this function to have same dimension as always: f(n,T)
            self.multiplets[m].intensity = self.interpolated_line_intensity_functions[m](
                self.electron_density, self.electron_temperature) * self.electron_density * self.impurity_density * self.emission_length / (4 * np.pi)