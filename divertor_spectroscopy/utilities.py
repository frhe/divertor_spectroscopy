"""
Utility functions
"""
import numpy as np
import matplotlib.pyplot as plt
import json
import copy
import os
import scipy.constants as const
from scipy.special import wofz
import pandas as pd
import re
import datetime
import logging
logger = logging.getLogger(__name__)

def val2idx(x, x0):
    """
    Returns the nearest index of a value in an array
        - first, if there are multiple
        - input of searched values as integer or list
        - multiple indices of values can be searched if x0 is list
    2d is not implemented yet

    """
    if isinstance(x0, list):
        idx = []
        for item in x0:
            idx.append(int(np.abs(np.array(x) - item).argmin()))
        return idx
    else:
        return int(np.abs(np.array(x) - x0).argmin())
def get_campaign_from_pid_or_day(pid_or_day):
    if pid_or_day[:4] == "2018":
        return "OP1.2b"
    elif pid_or_day[:4] in {"2022", "2023"}:
        return "OP2.1"
    elif pid_or_day[:4] in {"2024", "2025"}:
        return "OP2.2"
    return None  # In case none of the conditions are met
def gauss(x, offset=0, position=399.5, intensity=1, width=0.04):
    """Gaussian, characterized by intensity, which is the area under the curve, not the amplitude!"""
    amplitude = intensity / (np.sqrt(2 * np.pi) * width)
    return offset + np.abs(amplitude) * np.exp(-(x - position)**2 / (2 * width**2))


def gauss_amplitude(x, offset=0, position=399.5, amplitude=1, width=0.04):
    """Gaussian, characterized by the amplitude!"""
    return offset + amplitude * np.exp(-(x - position)**2 / (2 * width**2))


def lorentz(x, offset=0, position=397.007, intensity=1, fwhm=0.4):
    """Lorentz function as found in Griem, intensity is area under curve"""
    y = offset + intensity / (2 * np.pi) * (fwhm /
                                    ((x - position)**2 + (fwhm / 2)**2))
    return y

def lomanowski_amplitude(x,offset=0,position=397.007,amplitude=1,fwhm=0.05):
    """This is also a Lorentz-function, but with a different power. It taken from Lomanowskis 2015 paper and the 2.5 power is chosen for physics reason which can be found in the paper, the 2.5 makes it harder to integrate this function so I chose to just work with the amplitude"""
    shape = 1/(np.abs((x-position))**2.5 + (fwhm/2)**2.5)
    return  amplitude * shape/np.max(shape) + offset

def lomanowski(x,offset=0,position=397.007,intensity=1,fwhm=0.05):
    """This is also a Lorentz-function, but with a different power. It taken from Lomanowskis 2015 paper and the 2.5 power is chosen for physics reason which can be found in the paper, the 2.5 makes it harder to integrate this function so I chose to just work with the amplitude"""
    y = 1/(np.abs((x-position))**2.5 + (fwhm/2)**2.5)
    area = np.abs(np.trapz(y, x))
    return  intensity * y/area + offset


def lomanowskigauss(x,offset=0,position=397.007,intensity=1,width_gauss=0.04,fwhm_lorentz=0.05):
    """A Lomanowskigauss is a voigtian, which is convolution of a gaussian and a lorentzian (power 2.5 for Lomanowski case). 
    This is implemented for Balmer line broadening from Lomanowskis 2015 paper and should be used when electron density is extracted from Balmer lines, 
    the intensity is the area under the curve. One should be careful, when one of the broadenings gets negligible, ie pure Lorentzian
    I had a hard time with the convolution. I learned the following: dx of both functions needs to be inherently the same. 
    One of the funtions needs to be symmetric on the wavlength array. Otherwise the peak position will be shifted. This is now both done by the method. 
    If buggy, pick up here
    Currently the intensity is orders of magnitude too small!
    Todo: the "len(x)/2-1" could lead to shifts. Lets see
    """
    x_lomanowski = np.arange(len(x))
    dispersion = np.abs(x[0]-x[-1])/len(x) # todo: this is going to fail, when there is something in the center of the line missing! 
    #print(dispersion)
    y_lomanowski = lomanowski(x_lomanowski,position=len(x)/2-1,fwhm=fwhm_lorentz/dispersion) # this is now as in pixels on the spectrometer
    #x_gauss = np.linspace(position-spectral_range/2,position+spectral_range/2,len(x+1))
    #y_gauss = gauss(x_gauss,position=position,intensity=1,width=width_gauss)
    y_gauss = gauss(x,position=position,intensity=1,width=width_gauss)
    y = np.convolve(y_lomanowski,y_gauss,"same")
    area = np.abs(np.trapz(y, x))
    return intensity*y/area + offset

def lomanowski_instrument(x,instrument,offset=0,position=397.007,intensity=1,width_gauss=0.04,fwhm_lorentz=0.05):
    """A Lomanowskigauss is a voigtian, which is convolution of a gaussian and a lorentzian (power 2.5 for Lomanowski case). 
    This is implemented for Balmer line broadening from Lomanowskis 2015 paper and should be used when electron density is extracted from Balmer lines, 
    the intensity is the area under the curve. One should be careful, when one of the broadenings gets negligible, ie pure Lorentzian
    I had a hard time with the convolution. I learned the following: dx of both functions needs to be inherently the same. 
    One of the funtions needs to be symmetric on the wavlength array. Otherwise the peak position will be shifted. This is now both done by the method. 
    If buggy, pick up here
    Currently the intensity is orders of magnitude too small!
    """
    spectral_range = x[-1]-x[0]
    y_lomanowski = lomanowskigauss(x,position=position,width_gauss=width_gauss,fwhm_lorentz=fwhm_lorentz)
    y = np.convolve(instrument,y_lomanowski,"same")
    area = np.abs(np.trapz(y, x))
    return intensity*y/area + offset

def voigt_amplitude(x, offset=0, amplitude=1, position=397.007, width=0.04, broadening=0.4):
    """I found this formula in some forum and it works surpisingly well. Voigtian, which is a convolution of Gauss and Lorentz. The broadening should be fwhm, but check before physics interpretation.
    I chose to work with this first to avoid the convolution"""
    width = width * 2 * np.sqrt(2 * np.log(2))
    tmp = 1 / wofz(np.zeros((len(x))) + 1j *
                   np.sqrt(np.log(2.0)) * broadening).real
    result = amplitude * tmp * wofz(2 * np.sqrt(np.log(2.0)) * (
        x - position) / width + 1j * np.sqrt(np.log(2.0)) * broadening).real
    return np.abs(result) + offset


def voigt_intensity(x, offset=0, intensity=1, position=397.007, width=0.04, broadening=0.4):
    """The integration of a pure Lorentzian fails - otherwise seems fine. This basically only normalizes voigt_intensity to intensity 1 and multiplies wished intensity"""
    y = voigt_amplitude(x, offset=0, amplitude=1, position=position, width=width, broadening=broadening)
    x_fine = np.linspace(x[0], x[-1], 10000)
    y_fine = voigt_amplitude(x_fine, offset=0, amplitude=1,
                             position=position, width=width, broadening=broadening)
    area = np.abs(np.trapz(y_fine, x_fine))
    return intensity * y / area + offset

def doppler_broadening_nist(T,m,lambda_0):
    
    """
    Formula found on Nist https://www.nist.gov/pml/atomic-spectroscopy-compendium-basic-ideas-notation-data-and-formulas/atomic-spectroscopy-6
    T in eV (formula uses K, so K*11604 = eV)
    m in amu 
    returns in sigma of a gauss (formula gives you fwhm)
    """
    return 7.16e-7*lambda_0*np.sqrt(11604*T/m)/(2*np.sqrt(2*np.log(2)))

def sigma_to_fwhm(sigma):
    return 2*np.sqrt(2*np.log(2))*sigma
def fwhm_to_sigma(fwhm):
    return fwhm/(2*np.sqrt(2*np.log(2)))

def doppler_shift(lambda_0,v):
    return lambda_0 * np.sqrt((const.c+v)/(const.c-v))

def split_tag(tag):
    """A tag is atom name + ion stage - return this split"""
    ions = ["VIII","VII","VI","IV","V","III","II","I"]
    for ion in ions:
        if ion in tag:
            element = tag.split(ion)[0]
            return element,ion

def polynominal(x, *parameters):
    """Implemented originally for wavelength calibration"""
    order = len(parameters)
    result = 0
    for i in range(order):
        result += parameters[i] * x**i
    return result
def bremsstahlung(wavelength,Zeff=1.,Te=1000.,ne=1e19,debug=False):
    """Bremsstahlung as found in Maciejs Diplomarbeit
    Args:
        wavelength (np.array): Wavelength in nm
        Zeff (float, optional): Zeff of the plasma. Defaults to 1.
        Te (float, optional): Electron temperature in eV. Defaults to 1000.
        ne (float, optional): Electron density in m^-3. Defaults to 1e19.

    Returns:
        spectrum in W/m^3/nm/str
    """
    ne_cm = ne*1e-6
    wavelength_A = wavelength*10
    result_Wcmm3Am3 = Zeff*(2.045e-29*(ne_cm)**2*np.exp(-12400/(wavelength_A*Te)))/(wavelength_A**2*Te**0.35)
    result = result_Wcmm3Am3*1e6*10 # transform from nm to A and cm^3 
    return result

def planck_law(wavelength,T,a):
    """Implemented for intensity calibration"""
    return  a * 2 * const.h * const.c**2/(wavelength**5) * 1/((np.exp(const.h*const.c/(wavelength*const.k*T)))-1)

def get_branching_ratios_from_quantum_numbers(wl_0 = 404,
                        A_0 = 1.24e8,
                        g_0 = 6,
                        A = [2.01e7,8.57e7,1.02e8,4.16e7],
                        g_upper = [4,2,4,2],
                        E_upper = [214229.671,214169.920,214229.671,214169.920],
                        E_lower = [188888.543,188888.543,189068.514 ,189068.514]):
    """Takes the typical input from Nist and calculates branching ratios of a multiplet
    Default values are from the OII 396.7 line and are meant as a test

    Args:
        wl_0 (float, optional): center of mass wavelength. Defaults to 396.6938.
        A_0 (_type_, optional): Center of mass??? Einstein value. Defaults to 1.24e8.
        g_0 (int, optional): _description_. statistical weight.
        A (list, optional): _description_. Einstein values per line [2.01e7,8.57e7,1.02e8,4.16e7].
        g_upper (list, optional): statistical weights per line. Defaults to [4,2,4,2].
        E_upper (list, optional): Upper Energy level in cm-1. Defaults to [214229.671,214169.920,214229.671,214169.920].
        E_lower (list, optional): Lower Energy level in cm-1. Defaults to [188888.543,188888.543,189068.514 ,189068.514].

    Returns:
        list: branching ratios
    """
    E_difference = []
    wl = []
    for i in range(len(E_upper)):
        E_difference.append(E_upper[i]-E_lower[i])
        wl.append(wl_vac_to_air(1/E_difference[i]/100*1e9))
    branching_ratio = []
    for i in range(len(wl)):
        branching_ratio.append(A[i]*g_upper[i]*wl_0/(A_0*g_0*wl[i]))
    return branching_ratio
def set_log_axis_imshow(axis_array, number_entries=5, axis="x", multiple_axs=None, scientific=True):
    """
    This method expects the spacing of the displayed data to be logarithmic
    if this
    axis_array: spacing of your axis
    number_entries: how many numbers do you want on your axis?
    axis: x or y?
    multiple_axs: if you use subplots, give this the axs array
    """
    x = np.linspace(axis_array[0], axis_array[-1], number_entries)
    if scientific:
        my_xticks = np.around(np.logspace(np.log10(axis_array[0]), np.log10(
            axis_array[-1]), number_entries), decimals=1)
        for i in range(len(my_xticks)):
            my_xticks[i] = np.format_float_scientific(
                my_xticks[i], precision=2)
    else:
        my_xticks = np.around(np.logspace(np.log10(axis_array[0]), np.log10(
            axis_array[-1]), number_entries), decimals=1)
    if multiple_axs is None:
        if axis == "x":
            plt.xticks(x, my_xticks)
        if axis == "y":
            plt.yticks(x, my_xticks)
    else:
        for i in range(len(multiple_axs)):
            if axis == "x":
                multiple_axs[i].set_xticks(x)
                multiple_axs[i].set_xticklabels(my_xticks)
            if axis == "y":
                multiple_axs[i].set_yticks(x)
                multiple_axs[i].set_yticklabels(my_xticks)
def detect_peaks(spectrum, number_peaks, clearance=10,debug=False):
    """
    Identifies the positions of the highest peaks in a spectrum.

    Parameters
    ----------
    spectrum : numpy.ndarray
        1D array of spectrum data.
    number_peaks : int
        Number of peaks to detect.
    clearance : int, optional
        Number of elements to clear around each detected peak (default is 10).

    Returns
    -------
    numpy.ndarray
        Sorted indices of the detected peaks.
    """
    spectrum_copy = copy.deepcopy(spectrum)
    peak_positions = []
    for i in range(number_peaks):
        peak_position = np.nanargmax(spectrum_copy)
        peak_positions.append(peak_position)
        spectrum_copy[peak_position-clearance:peak_position+clearance] = np.nan
    if debug:
        plt.figure(figsize=(8,6))
        plt.plot(spectrum)
        for peak_position in peak_positions:
            plt.axvline(peak_position,c="r",ls="--")
        plt.show()
    return np.sort(peak_positions)
def ROIs_configString_to_df(ROIs):
    pattern = re.compile(r"{(.*?)}")
    matches = pattern.findall(ROIs)
    # Split each ROI into components
    data = [match.split(",") for match in matches]
    # Convert to a DataFrame
    df_rois = pd.DataFrame(data, columns=["names", "enabled","x","width",  "ones", "y", "y_binning","height"])
    return df_rois
def get_date_from_nano(nano):
    """_summary_

    Args:
        nano (_type_): _description_
    """
    seconds = nano/1e9
    return datetime.datetime.fromtimestamp(seconds)
    
    
def log_to_lin_value(log_array, value):
    """this method is used to use axvline in imshow with logarithmic axis"""
    lin_array = np.linspace(log_array[0], log_array[-1], len(log_array))
    idx = val2idx(log_array, value)
    return lin_array[idx]
def detect_scale(array):
    """Detects whether the scaling of the array is logarithmic or linear based on the ratio of differences."""
    ratios = np.diff(array) / array[:-1]
    if np.allclose(ratios, ratios[0], atol=1e-2):
        return "linear"
    else:
        return "log"

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def wl_vac_to_air(wl_vac):
    """Method to convert wavelength in vacuuum to wavelength observed in air"""
    #n_air = 1.000273 # stp refractive index of air found: https://en.wikipedia.org/wiki/List_of_refractive_indices
    n_air = 1.0002771569814664
    wl_air = wl_vac/n_air
    return wl_air

def smooth_noisy_data(noisy_data, window=2):
    """1d array is smoothed by averaging the window next points
    edge points are done by only the inner neighbors
    """
    smoothed_data = np.zeros(len(noisy_data))
    # edge cases:
    for i in range(window):
        smoothed_data[i] = np.mean(noisy_data[0:i + window + 1])
        smoothed_data[-(i + 1)] = np.mean(noisy_data[-(i + window):-1])
    for i in range(len(noisy_data) - window * 2):
        smoothed_data[i + window] = np.mean(noisy_data[i:i + 2 * window + 1])
    return smoothed_data

def unsigned_integer_correction(data_uncorrected):
    """
    Unsigned integer correction is necessary when reading spectroscopy data from the archive.
    input:
        - data_uncorrected: uncorrected data (dimension time, channel, wavelength)
    output:
        - corrected data
    """
    # Correct the data in one operation without deep copying
    data_corrected = np.where(data_uncorrected < 0, data_uncorrected + 2**16, data_uncorrected)
    
    return data_corrected


def saturation_correction(data_uncorrected, saturation=6e4, camid=None, time_id=None, debug=True):
    """
    Saturation correction is necessary when the spectrometer has saturated pixels.
    input:
        - data_uncorrected: uncorrected data (dimension time, channel, wavelength)
        - saturation: threshold value for saturation
        - camid, time_id: identifiers for debugging (optional)
        - debug: whether to display a debug plot
    output:
        - corrected data with saturated values set to NaN
    """
    # Create a copy and apply saturation correction
    data_corrected = np.where(data_uncorrected > saturation, np.nan, data_uncorrected).astype(float)
    
    # Generate a debug array indicating where saturation occurred
    debug_array = np.any(data_uncorrected > saturation, axis=2).astype(float)
    debug_array[debug_array == 1] = np.nan  # Set saturated time-channel pairs to NaN
    
    # Plot if debug is enabled
    if debug:
        plt.figure(figsize=(8, 6))
        plt.imshow(debug_array.T, aspect="auto", origin="lower", interpolation="none")
        plt.xlabel("Time index")
        plt.ylabel("Channel index")
        plt.title(f"{time_id} {camid} nan Channels have saturated frames")
        plt.show()

    return data_corrected
def correct_smearing(measured_signal, shift_time=1e-6, exp_time = 0.025, bin_width=32, shift_direction="both"):
    """
    This has to be applied to a background corrected signal!
    Corrects smearing, which happens during vertical readout of ccd camera in frame transfer mode
    Todo: Does the loop over the time have to happen in the datasource? 
    Todo: if I implement it here in the class it should happen in the class context with self. everything!
    
    Parameters:
    - measured_signal: np array of dimensons (channels, pixels) - this is the signal including real signal and smearing signal
    - shift_time: Time that is needed to shift one pixel
    - exp_time: Exposure time set at the camera and read by the datasource. Todo: here we might have to thing about subtracting the shifting time then
    - shift_direction: options "up", "down" and "both". I don't understand why both should be possible but in Kevin Verhaeghs thesis it looks like every channel is affected by smearing
    
    Returns:
    - real_signal: numpy-Array of shape (channels, pixels), signal with smearing effect subtracted
    """
    num_channels, pixels = measured_signal.shape  # 27 Kanäle, 1024 Wellenlängen    
    smearing_matrix = np.eye(num_channels)
    for i in range(num_channels):
        for j in range(num_channels):
            if shift_direction == "down" and j > i:
                # Downshift:
                smearing_matrix[i, j] = shift_time * bin_width / exp_time
            elif shift_direction == "up" and j < i:
                # Upshift: 
                smearing_matrix[i, j] = shift_time * bin_width / exp_time
            elif shift_direction == "both" and j != i:
                smearing_matrix[i, j] = shift_time * bin_width / exp_time
    smearing_matrix_inverted = np.linalg.inv(smearing_matrix)
    real_signal = np.zeros((num_channels,pixels))*np.nan
    # Apply correction for pixel
    for pixel in range(pixels):
        real_signal[:,pixel] = np.dot(smearing_matrix_inverted,measured_signal[:,pixel])
    return real_signal
def get_valid_pixels(spectrum):
    """This method returns the valid indices of an array
    Only works for 1d arrays!
    todo: write exception
    """

    try:
        return np.concatenate(np.argwhere(np.isfinite(spectrum)))
    except:
        return []

def average_signal_on_time_array(new_time_array,old_time_array,old_signal,axis=None):
    """For example for juice timing, 200 ms averaging is needed
    This method averages the signal on the new time array

    Args:
        new_time_array (numpy array): _
        old_time_array (numpy array): _description_
        signal (numpy array): _description_
    """
    # Test if the old array is long enough!
    if new_time_array[-1] > old_time_array[-1]:
        logger.warning("Your data is not acquired long enough!")
        return
    else:
        interval_length = new_time_array[1]-new_time_array[0]
        new_signal = []
        for time in new_time_array:
            idx_start = val2idx(old_time_array,time)
            idx_end = val2idx(old_time_array,time+interval_length)
            new_signal.append(average_signal(old_signal=old_signal[idx_start:idx_end+1],axis=axis))
        return np.array(new_signal)
def average_signal(old_signal, axis=None):
    """This method is supposed to average a signal of an array to one timepoint (or other axis)
    without ignoring NaNs. If any value in the segment is NaN, the average should be NaN.

    Args:
        old_signal (numpy array): The signal to be averaged.
        axis (int, optional): The axis along which the means are computed. Defaults to None.

    Returns:
        numpy array: The averaged signal, with NaNs if any element in the slice is NaN.
    """
    if axis is None:
        # Flatten the array
        if np.isnan(old_signal).any():
            return np.nan
        else:
            return np.mean(old_signal)
    else:
        if np.nan in old_signal:
            # I don't find a better way then creating the array in a for loop - this is probably quite slow
            new_signal = []
            for i in range(len(old_signal[axis])):
                if np.nan in old_signal[axis,i]:
                    new_signal.append(np.nan)
                else:
                    new_signal.append(np.mean(old_signal[axis,i]))
            return np.array(new_signal)
        else:
            return np.mean(old_signal, axis=axis)

def pid_format_to_mds_plus(pid):
    """Some methods want weird mds plus format

    Args:
        pid (str): yyyymmdd.nnn
    """
    pid_mds = ""
    pid_mds += pid[2:-4]
    pid_mds += pid[-3:]
    return pid_mds
    

def pid_format_to_logbook(pid):
    """Logbook wants no leading zeros in the program number

    Args:
        pid (str): yyyymmdd.nnn

    Returns:
        str: logbook style
    """
    number = int(pid[-3:])
    return pid[:9]+str(number)
import numpy as np 
import matplotlib.pyplot as plt

def multicolor_ylabel(ax,list_of_strings,list_of_colors,axis='x',anchorpad=0,bbox_to_anchor=(1.25, 0.3),**kw):
    """this function creates axes labels with multiple colors
    ax specifies the axes object where the labels should be drawn
    list_of_strings is a list of all of the text items
    list_if_colors is a corresponding list of colors for the strings
    axis='x', 'y', or 'both' and specifies which label(s) should be drawn
    ax = plt.subplot(111)
    example
    x = [[0,1,2],[0,1,2],[0,1,2]]
    plt.contourf(x)
    cbar1 = plt.colorbar()
    multicolor_ylabel(ax,("359","and","371"),('r','b','g'),axis='z',size=15)

    plt.show()
    
    """
    from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, VPacker

    # x-axis label
    if axis=='x' or axis=='both':
        boxes = [TextArea(text, textprops=dict(color=color, ha='left',va='bottom',**kw)) 
                    for text,color in zip(list_of_strings,list_of_colors) ]
        xbox = HPacker(children=boxes,align="center",pad=0, sep=5)
        anchored_xbox = AnchoredOffsetbox(loc=3, child=xbox, pad=anchorpad,frameon=False,bbox_to_anchor=(0.2, -0.11),
                                          bbox_transform=ax.transAxes, borderpad=0.)
        ax.add_artist(anchored_xbox)

    # y-axis label
    if axis=='y' or axis=='both':
        boxes = [TextArea(text, textprops=dict(color=color, ha='left',va='bottom',rotation=90,**kw)) 
                     for text,color in zip(list_of_strings[::-1],list_of_colors) ]
        ybox = VPacker(children=boxes,align="center", pad=0, sep=5)
        anchored_ybox = AnchoredOffsetbox(loc=3, child=ybox, pad=anchorpad, frameon=False, bbox_to_anchor=(-0.15, 0.2), 
                                          bbox_transform=ax.transAxes, borderpad=0.)
        ax.add_artist(anchored_ybox)
    if axis == "z":
        boxes = [TextArea(text, textprops=dict(color=color, ha='left',va='bottom',rotation=90,**kw)) 
                     for text,color in zip(list_of_strings[::-1],list_of_colors) ]
        zbox = VPacker(children=boxes,align="center", pad=0, sep=5)
        anchored_zbox = AnchoredOffsetbox(loc=3, child=zbox, pad=anchorpad, frameon=False, bbox_to_anchor=bbox_to_anchor, 
                                          bbox_transform=ax.transAxes, borderpad=0.)
        ax.add_artist(anchored_zbox)

def create_folder(folder_path):
    """
    Create a folder at the specified path.

    Parameters:
    - folder_path (str): The path where the folder should be created.

    Returns:
    - None
    """
    try:
        # Create the folder
        os.makedirs(folder_path)
        logger.debug("Folder '{}' created successfully.".format(folder_path))
    except FileExistsError:
        logger.debug("Folder '{}' already exists.".format(folder_path))

def convert_lists_to_arrays(data):
    """
    Recursively converts all lists in a dictionary to numpy arrays.

    Args:
    data (dict or list): Dictionary or list containing data.

    Returns:
    dict or ndarray: Processed dictionary with lists converted to numpy arrays,
                     or numpy array if the input was a list.
    """
    if isinstance(data, dict):
        return {key: convert_lists_to_arrays(value) for key, value in data.items()}
    elif isinstance(data, list):
        return np.array([convert_lists_to_arrays(item) for item in data])
    else:
        return data