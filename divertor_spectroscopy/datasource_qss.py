import numpy as np
import matplotlib.pyplot as plt
import json
import copy
import logging
logger = logging.getLogger(__name__)

import archivedb
import w7xarchive

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import wavelength_calibration
from divertor_spectroscopy import intensity_calibration

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
fontsize=10
plt.rcParams.update({'font.size': fontsize})
class Datasource_qss():
    
    def __init__(self, time_id, camid, central_wavelength=None, broken_channels=None,
                 straylight_correction_channels=None,experiment_length=None,disabled_channels=None,
                 saturation=6e4,exclude_pixels=None, ical=True, wcal=True,instrument_cal=False,
                 numeric_instrument_function=False,use_off_center_numeric_instrument_function=None,
                 wcal_comment=None,ical_comment=None,
                 port=None,software_binning=False,software_binning_shift=None,correct_smearing=False,
                 database_path=None,read_metadata=True,campaign=None,exposure_time=None,debug=False):
        """Method to load divertor spectroscopy data (qss)
        I had some confusion in this class because of the order of channels, which I had solved by flipping some calibration factors and so on. 
        This is not wanted! The datasource is responsible to correctly order the channels of your camera:
            - in unbinned cases
            - in incorrectly binned cases
            - and in correctly binned cases!
        Then you use this datasource to create the calibration files (instrument, intensity)
        Nothing will have to be flipped in a hardcored way!
        Rules: 
            - AEI spectrometers have channel 0 closest to the target
            - AEF spectrometers have channel 0 to the side of the horizontal divertor plate
        todo: 
            make wcal oneliner and handle rest in wcal class
            make ical oneliner and handle rest in ical class
            make proper software binning and give it as an option
        Args:
            time_id (String or list of two ints): 
                pid format "yearmmdd.num"
                timestamp for out of program measurements format [start_nano,end_nano]
            camid (String): short name of your spectrometer i.e. "IP320_2"
            central_wavelength (int, optional): only an option for OP1.2, when saved wavelength was sometimes wrong. Defaults to None.
            broken_channels (list, optional): Channels of your spectrometer might be broken, these will be set to np.nan. Defaults to [].
            saturation (int, optional): You will get a warning if your data has saturated pixels. Defaults to 6e4.
            ical (bool, optional): Apply intensity calibration. Defaults to True.
            wcal (bool, optional): Apply wavelength calibration. Defaults to True.
            wcal_comment (String, optional): There might be multiple possible calibrations - this choses one. Defaults to None.
            ical_comment (String, optional): There might be multiple possible calibrations - this choses one. Defaults to None.
            database_path (str,optional): There are different Archives to chose from: Sandbox, Test, ArchiveDB. Defaults to "ArchiveDB"
            read_metadata (bool, optional): Turns on metadata readout. Defaults to True
            debug (bool, optional): Debugging option. Defaults to False.
        """
        self.ground_path = "http://archive-webapi.ipp-hgw.mpg.de/"
        self.qss_path = "/raw/W7X/QSS_DivertorSpectroscopy/"
        if camid == "IP320_UV":
            self.qss_path = "/raw/W7X/QSK06_PassiveSpectroscopy/"
        self.camid=camid
        self.time_id = time_id
        self.saturation=saturation
        self.exposure_time=exposure_time
        if campaign is None:
            if type(time_id) == str:
                self.campaign = utils.get_campaign_from_pid_or_day(time_id)
            else:
                logger.error("Your time_id input is a nanotime, specify the keyword argument campaign to avoid this")
        else:
            self.campaign=campaign
        try: # try to read some settings file created using the automated fits (automated_analysis_qss.py). Not a problem if it is not available
            save_path = ground_path+"qss_analysis/discharges/"+self.campaign+"/"+time_id[:8]+"/"+time_id+"/"
            with open(save_path+"settings_200_version1.json") as f:
                settings_load = json.load(f)
            self.experiment_length = settings_load["discharge_length"]
            self.config = settings_load["config"]
        except:
            try:
                with open(save_path+"settings_25_version1.json") as f:
                    settings_load = json.load(f)
                    self.experiment_length = settings_load["discharge_length"]
                    self.config = settings_load["config"]
            except:
                logger.info("Did not find settings for this discharge")            
        if experiment_length is not None:
            self.experiment_length = experiment_length
        
        self.software_binning_shift = software_binning_shift
        # reading some of the settings for the camera and spectrometer
        with open(__location__+"/datasource_settings.json") as f:
            self.datasource_settings = json.load(f)[self.campaign][camid]
        if port is None:
            self.port = self.datasource_settings["port"]
        else:
            self.port = port
        self.gratings = self.datasource_settings["gratings"]
        self.number_channels = self.datasource_settings["binning"]["number_channels"]
        # printing yout the date, if the time input is a nano
        if type(time_id)==str:
            self.pid = time_id
        else:
            self.date_from = utils.get_date_from_nano(time_id[0])
            self.date_to = utils.get_date_from_nano(time_id[1])
            logger.info("Time of measurement from: {}".format(self.date_from))
            logger.info("Time of measurement to: {}".format(self.date_to))
            self.pid = None
        if database_path is None: # set default values
            if int(time_id[:8])<20230101: # todo: here we should actually distinguish between nano and pid
                database_path = "Test"
            else:
                database_path = "ArchiveDB"
        self.database_path = database_path
        if disabled_channels is None:
            self.disabled_channels = self.datasource_settings["disabled_channels"]
        else:
            self.disabled_channels = disabled_channels
        if broken_channels is None: # this is escecially implemented for OP2.1 as we had channels repaired during the campaign there
            self.broken_channels = np.array(self.datasource_settings["broken_channels"])
            self.uncertain_channels = self.datasource_settings["uncertain_channels"]
            # Changes during OP2.1 - some channels were repaired during the campaign by swapping out fibers
            if self.campaign == "OP2.1":
                self.repaired_channels = np.array(self.datasource_settings["repaired_channels"])
                if int(time_id[:8])>20230202:# replacement of AEF51 fibersbroken long fibers
                    if camid == "IP320_1" or camid == "IP320_2":
                        logger.info("AEF51 channels have been repaired during campaign")
                        self.broken_channels = np.setdiff1d(self.broken_channels,self.repaired_channels)
                        if camid == "IP320_1":
                            ical_comment = "OP2.1, out vessel, after channel recovery, straylight_corrected"
                    if camid == "IP320_3":
                        logger.info("AEI51 channels have been repaired during campaign")
                        self.broken_channels = np.setdiff1d(self.broken_channels,self.repaired_channels)
            if self.campaign == "OP2.1":
                self.repaired_channels = np.array(self.datasource_settings["repaired_channels"])
                if int(time_id[:8])>20230124:# replacement of AEI51 fibersbroken long fibers
                    if camid == "IP320_3":
                        logger.info("AEI51 channels have been repaired during campaign")
                        self.broken_channels = np.setdiff1d(self.broken_channels,self.repaired_channels)
        else:
            self.broken_channels = broken_channels
            self.uncertain_channels = self.datasource_settings["uncertain_channels"]
        
        if read_metadata: # todo: this needs to properly distinguish between IP spectrometers and others
            self.get_camera_meta_data(time_id,camid)
            if central_wavelength is None:
                if self.campaign == "OP1.2b":
                    central_wavelength = int(np.round(self.metadata["Central_Wavelength"]))
                else:
                    central_wavelength = int(np.round(self.metadata["centralWavelength"]))
            self.central_wavelength = central_wavelength
            logger.info("{} nm is the central wavelength".format(self.central_wavelength))
            if "IP" in camid:
                if self.campaign == "OP1.2b":
                    self.grating_number = 0
                else:
                    self.grating_number = self.metadata["gratingNumber"]
                self.grating = self.gratings[int(self.grating_number)]
                logger.info("grating number {} is used: it has {} lines/mm".format(self.grating_number,self.grating))
                if self.campaign == "OP1.2b":
                    self.slit_width = 30
                else:
                    self.slit_width = self.metadata["slitWidth"]
                logger.info("slit width {} is used".format(self.slit_width))
                if self.campaign == "OP1.2b":
                    self.filter_number = 0
                else:
                    self.filter_number = self.metadata["filterNumber"]
                logger.info("filter number {} is used".format(self.filter_number))
            else:
                self.grating_number=0
                self.slit_width = None
                self.filter_number = None
            self.gain = self.metadata["AdcEMGain"]
            logger.info("EM gain {} is used".format(self.gain))
            if exposure_time is None:
                self.exposure_time = self.metadata["ExposureTime"]/1000 # is saved in ms by camera
            logger.info("Exposure time {} is used".format(self.exposure_time))
        self.data = self.get_camera_raw_data(time_id, camid, saturation,exclude_pixels,useCache=True,correct_smearing=correct_smearing,debug=debug)
        logger.info("Frame Time {} is used".format(self.frame_time))
        # There might be times, when your the order of your channels was inverted. Then you can correct that here
        # As I want to work binned and unbinned images the same way, I will compare the bin of the first channel with the software binning
        if camid == "SopraBerlin" and self.data["signal_counts"].shape[1] == 28:
            self.data["signal_counts"] = self.data["signal_counts"][:,:27]
        if self.campaign == "OP2.1" and camid == "SopraWega_AEF30" and self.data["signal_counts"].shape[1] != 1024:
            if float(time_id)<20230214.045:
                channels = np.arange(*self.datasource_settings["binned_channels_arange_input"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
                logger.info("Old binning used: "+str(channels))
            else:
                channels = np.arange(*self.datasource_settings["binned_channels_arange_input_corrected"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
                logger.info("New binning used: "+str(channels))
        
        elif self.campaign == "OP2.2" and camid == "IP320_8" and self.data["signal_counts"].shape[1] != 1024:
            spectrometer_array = np.zeros((len(self.data["time_s"]),self.datasource_settings["binning"]["number_channels"],1024))
            projection_array = np.array([0, 1, 2, 3, None, 4, 5, 6, 7, None, 8, 9, 10, 11, None, 12, 13, 14, 15, None, 16, 17, 18, 19, None, 20, None, 21, 22, 23, 24, 25, 26, 27, None, 28, 29, 30, 31, None, 32, 33, 34, 35, None, 36, 37, 38, 39, None, 40, None, 41])
            for field_idx,field in enumerate(projection_array):
                if field is None:
                    pass
                else:
                    spectrometer_array[:,field_idx] = self.data["signal_counts"][:,field] 
            self.data["signal_counts"] = spectrometer_array
        else:
            if self.data["signal_counts"].shape[1] != 1024: # binned data
                if np.abs(self.metadata["Rois_arrays"]["y"]["[0]"]-self.datasource_settings["binning"]["position_channel_0"])<100:
                    logger.info("Binning seems to be in the natural order: y_0 binning: {}, y_0 software binning {}".format(self.metadata["Rois_arrays"]["y"]["[0]"],self.datasource_settings["binning"]["position_channel_0"]))
                    if self.disabled_channels is not None:
                        for channel in self.disabled_channels:
                            new_channel = np.ones((len(self.data["signal_counts"]),1024))*np.nan
                            self.data["signal_counts"] = np.insert(self.data["signal_counts"], channel, new_channel, axis=1)
                            logger.info("You have a disabled channel, that we add")
                    channels = np.arange(*self.datasource_settings["binned_channels_arange_input"])
                else:
                    logger.warning("Binning seems to be in the alternative order: y_0 binning: {}, y_0 software binning {}".format(self.metadata["Rois_arrays"]["y"]["[0]"],self.datasource_settings["binning"]["position_channel_0"]))
                    if self.disabled_channels is not None:
                        for channel in self.disabled_channels:
                            new_channel = np.ones((len(self.data["signal_counts"]),1024))*np.nan
                            self.data["signal_counts"] = np.insert(self.data["signal_counts"], 27-channel, new_channel, axis=1)
                            logger.info("You have a disabled channel, that we add")
                    channels = np.arange(*self.datasource_settings["binned_channels_arange_input_corrected"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
                if correct_smearing:
                    self.data["signal_counts_without_smearing_correction"] = np.array(self.data["signal_counts_without_smearing_correction"])[:,channels]
        
        # add in the disabled channel into the raw data - this is important if channels have been left out during measurements but where there during calibrations for example
        # copy the signal counts into a new numpy array, that has one channel more and set it to nan
        
        self.software_binned = False
        if self.data["signal_counts"].shape[1] == 1024:
            logger.warning("attention,  data is not binned!")
            if software_binning:
                self.software_binned = True
                self.get_software_binning(time_id,camid,debug)
            if self.campaign == "OP2.1" and camid == "SopraWega_AEF30":
                channels = np.arange(*self.datasource_settings["binned_channels_arange_input"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
            if self.campaign == "OP2.2" and camid == "SopraWega_AEF30":
                channels = np.arange(*self.datasource_settings["binned_channels_arange_input"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
            if self.campaign == "OP2.2" and camid == "SopraWega_AEI51":
                channels = np.arange(*self.datasource_settings["binned_channels_arange_input"])
                self.data["signal_counts"] = np.array(self.data["signal_counts"])[:,channels]
            
        if straylight_correction_channels is not None:
            # take average spectrum of the straylight correction channels:
            if experiment_length is None:
                end = 1
            else:
                end = experiment_length
            self.t_idx_start = utils.val2idx(self.data["time_s"],0)
            print(self.t_idx_start)
            self.t_idx_end = utils.val2idx(self.data["time_s"],end)
            print(self.t_idx_end)
            # todo: this takes only the average of the entire discharge. In principle should be done for every frame, but that might be too noisy
            straylight = np.nanmean(self.data["signal_counts"][self.t_idx_start:self.t_idx_end,straylight_correction_channels],(0,1))
            if debug:
                plt.figure(figsize=(8,6))
                plt.plot(straylight)
                plt.show()
            logger.info("Straylight is corrected")
            self.data["signal_counts"] -= straylight
        self.data["signal_counts_including_broken_channels"]=copy.deepcopy(self.data["signal_counts"])
        for broken_channel in self.broken_channels:
            self.data["signal_counts"][:,broken_channel] = self.data["signal_counts"][:,broken_channel]*np.nan
        if debug:
            fontsize = 14
            plt.rcParams.update({'font.size': fontsize})  
            plt.figure(figsize=(8, 6))
            plt.title(
                "Uncalibrated signal {} {} {}".format(camid, time_id, port))
            plt.plot(np.arange(len(self.data["signal_counts_including_broken_channels"][0])), np.nanmean(
                self.data["signal_counts_including_broken_channels"], (0, 2)), "+-",label="Signal")
            if correct_smearing:
                plt.plot(np.arange(len(self.data["signal_counts_without_smearing_correction"][0])), np.nanmean(
                    self.data["signal_counts_without_smearing_correction"], (0, 2)), "+-",label="without smearing correction")
            for broken_channel in self.broken_channels:
                plt.axvline(broken_channel,ls="--",c="black")
            plt.xlabel("Channel")
            plt.ylabel("Average counts")
            plt.grid()
            plt.legend()
            plt.show()
        if wcal:
            self.order = self.datasource_settings["wavelength_calibration"]["order"]
            if wcal_comment is None:
                self.data["wavelength_nm"] = wavelength_calibration.get_wavelength(comment=self.campaign+"_gauss",
                    grating_number=self.grating_number,camid=camid, central_wavelength=central_wavelength, order=self.order,campaign=self.campaign)
            else:
                logger.debug("The wavelength calibration with the following comment is used: "+wcal_comment)
                self.data["wavelength_nm"] = wavelength_calibration.get_wavelength(comment=wcal_comment,grating_number=self.grating_number,
                    camid=camid, central_wavelength=central_wavelength, order=self.order,campaign=self.campaign)
            self.wavelength_coefficients_guess = None
            self.lower_bounds_wl = None
            self.upper_bounds_wl = None
        else:
            self.data["wavelength_nm"] = None
            logger.debug("You need to produce your wavelength vector yourself")
            try:
                self.wavelength_coefficients_guess = self.datasource_settings["wavelength_calibration"]["wavelength_coefficients_guess"][str(self.central_wavelength)]
                self.lower_bounds_wl = self.datasource_settings["wavelength_calibration"]["lower_bounds_wl"][str(self.central_wavelength)]
                self.upper_bounds_wl = self.datasource_settings["wavelength_calibration"]["upper_bounds_wl"][str(self.central_wavelength)]
            except:
                logger.debug("You don't have a wavelength guess for this camera/cw")
        self.data["signal_counts_per_second"] = self.data["signal_counts"]  / (self.exposure_time)/self.metadata["AdcEMGain"]
        if ical:
            if ical_comment is None:
                self.ical_comment = self.datasource_settings["ical_comment"]
            else:
                self.ical_comment = ical_comment
            if camid == "SopraWega_AEF30" and self.campaign == "OP2.1":
                self.data["intensity_calibration"] = intensity_calibration.get_intensity_calibration(
                    camid,grating=self.grating_number+1,ical_comment=self.ical_comment,central_wavelength=465,broken_channels=self.broken_channels,campaign=self.campaign,debug=debug)
            else:
                self.data["intensity_calibration"] = intensity_calibration.get_intensity_calibration(
                    camid,grating=self.grating_number+1,ical_comment=self.ical_comment,central_wavelength=int(np.round(self.central_wavelength)),broken_channels=self.broken_channels,campaign=self.campaign,debug=debug)
            if False:
                if camid == "IP320_4" and type(time_id)==str and self.data["signal_counts"].shape[1] != 1024:
                    if int(time_id[:8]) > 20230116 and int(time_id[:8]) < 20230126:
                        with open(ground_path + "qss_analysis/json_files/recalibration_factors_NII_lines_IP320_4.json".format(campaign)) as f:
                            recalibration_data = json.load(f)
                            recalibration_factors = np.array(recalibration_data["recalibration_factors"])
                            channels = np.array(recalibration_data["channels"])
                            self.data["intensity_calibration"][channels] = recalibration_factors[:,np.newaxis]*self.data["intensity_calibration"][channels]
            # it is possible that your intensity calibration file is the wrong way around. This can be corrected here: todo: not implemented yet
            self.data["signal_photons"] = self.data["signal_counts"] * \
                self.data["intensity_calibration"] / \
                (self.exposure_time)/self.metadata["AdcEMGain"]
            if debug:
                plt.figure(figsize=(8, 6))
                plt.title(
                    "QSS 12.3° {} {} {}".format(camid, time_id, self.port))
                plt.plot(np.arange(len(self.data["signal_photons"][0])), np.nanmean(
                    self.data["signal_photons"], (0, 2)), "+-")
                plt.xlabel("Channel")
                plt.ylabel("Average calibrated signal over wavelenth and time")
                plt.gca().set_ylim(bottom=0)
                # Add arrow pointing to the left
                plt.arrow(0.6, 0.2, -0.1, 0, head_width=0.05, head_length=0.05, fc='red', ec='red', width=0.02,
                    transform=plt.gca().transAxes)
                if self.port[:3]=="AEF":
                    plt.text(0.2, 0.19, 'Horizontal Target', fontsize=12, color='red',
                    transform=plt.gca().transAxes)
                if self.port[:3]=="AEI":
                    plt.text(0.2, 0.19, 'Divertor Target', fontsize=12, color='red',
                    transform=plt.gca().transAxes)
                # Add arrow pointing right
                plt.arrow(0.65, 0.2, 0.1, 0, head_width=0.05, head_length=0.05, fc='blue', ec='blue', width=0.02,
                        transform=plt.gca().transAxes)
                if self.port[:3]=="AEF":
                    plt.text(0.8, 0.19, 'Vertical Target', fontsize=12, color='blue',
                        transform=plt.gca().transAxes)
                if self.port[:3]=="AEI":
                    plt.text(0.8, 0.19, 'Confined Plasma', fontsize=12, color='blue',
                        transform=plt.gca().transAxes)
                plt.grid()
                plt.show()
        else:
            logger.debug("You don't have an intensity calibration")
        if instrument_cal: # use instrument function calibration # todo: this should have the added layer central wavelength and grating
            with open(ground_path + "qss_analysis/json_files/instrument_function_calibration_{}.json".format(self.campaign)) as f:
                    self.instrument_function = json.load(f)[camid]["30um"]["width"]
        if numeric_instrument_function: 
                if use_off_center_numeric_instrument_function is not None: # these one are implemented specifically for Stark broadening - the instrument function can be quite different at the pixel, where Balmer epsilon and Balmer delta are measured
                    logger.info("Using an off center instrument function!")
                    numeric_instrument_function_path = "qss_analysis/json_files/numeric_instrument_function_off_center_calibration_{}.json".format(self.campaign)
                    with open(ground_path + numeric_instrument_function_path) as f:
                        self.numeric_instrument_function = json.load(f)[camid]["30um"]["numeric_instrument_function"][use_off_center_numeric_instrument_function]
                else:
                    numeric_instrument_function_path = "qss_analysis/json_files/numeric_instrument_function_calibration_{}.json".format(self.campaign)
                    with open(ground_path + numeric_instrument_function_path) as f:
                        self.numeric_instrument_function = json.load(f)[camid]["30um"]["numeric_instrument_function"]
        else:
            self.numeric_instrument_function = None
        

    def get_camera_raw_data(self, time_id, camid, saturation=6e4,exclude_pixels=None,useCache=True,correct_smearing=False,debug=False):
        """This method gives you the raw data of a princeton instruments camera including:
            Unsigned integer correction
            Saturation warning/correction
            Background correction (electronic noise subtracted) - only done when time_id is a pid!
        Args:
            time_id (String or list of two ints): 
                pid format "yearmmdd.num"
                timestamp for out of program measurements format [start_nano,end_nano]
            camid (String): short name of your spectrometer i.e. "IP320_2"
            saturation (int, optional): You will get a warning if your data has saturated pixels. Defaults to 6e4.
        Returns:
            dict: dictionary with time and raw_data in counts/exp_time
        """
        return_data = {}
        self.path = self.ground_path+self.database_path + self.qss_path + self.datasource_settings["archive_path"]+"_DATASTREAM/0/Images"
        print(self.path)
        use_get_image = True
        logger.info("Start reading data")
        if use_get_image:
            if type(time_id) == str:
                # convert to nanotimestamps
                time_intervals = archivedb.get_time_intervals_for_program(self.path,time_id,timeout=10)[::-1]
                time_from,time_to = archivedb.get_program_from_to(time_id)
                t1 = archivedb.get_program_t1(time_id)
                timestamps = []
                images = []
                for time_interval in time_intervals:
                    timestamp, image = w7xarchive.get_image_raw(self.path, *time_interval)
                    timestamps.append(timestamp)
                    if len(image.shape) == 2:  # Only 1 image in this block, so need to manually add the block axis
                        image = image[np.newaxis, :, :]
                    images.append(image)
                timestamps = np.concatenate(timestamps)
                images = np.concatenate(images)
                raw_data = [timestamps, images]
                return_data["time_s"] = (raw_data[0] - t1) / 1e9
            else:
                raw_data = archivedb.get_signal(self.path, *time_id, timeout=10)
                return_data["time_nansoseconds_ts"] = raw_data[0]
                return_data["time_s"] = (raw_data[0] - raw_data[0][0]) / 1e9
        else:
            if type(time_id) == str:
                raw_data = archivedb.get_signal_for_program(self.path, time_id,useCache=useCache,timeout=10)
                return_data["time_s"] = raw_data[0]
            else:
                raw_data = archivedb.get_signal(self.path, *time_id, timeout=10)
                return_data["time_nansoseconds_ts"] = raw_data[0]
                return_data["time_s"] = (raw_data[0] - raw_data[0][0]) / 1e9
        logger.info("Finish reading data")
        logger.info("Start unsigned integer correction")
        self.frame_time = (return_data["time_s"][-1] - return_data["time_s"][0])/len(return_data["time_s"])
        return_data["signal_counts"] = utils.unsigned_integer_correction(
            raw_data[1])
        logger.info("Finish unsigned integer correction")
        return_data["signal_counts"] = np.array(return_data["signal_counts"],dtype=float)
        
        if exclude_pixels is not None:
            region = slice(*exclude_pixels)
            for t_idx in range(len(return_data["signal_counts"])):
                for c_idx in range(len(return_data["signal_counts"][0])):
                    return_data["signal_counts"][t_idx,c_idx,region] = return_data["signal_counts"][t_idx,c_idx,region]*np.nan
        return_data["signal_counts_original"] = copy.deepcopy(return_data["signal_counts"])
        if not camid == "IP320_8":
            if np.nanmax(return_data["signal_counts"]) > saturation:
                logger.warning("Attention, saturation occurs during this discharge. You find nans where > {} counts are found".format(
                    saturation))
                return_data["signal_counts"] = utils.saturation_correction(
                    return_data["signal_counts"], saturation, camid=camid,time_id=time_id)
        # electronic noise correction - only done for timeid == pid
        if type(time_id)==str:
            if camid == "SopraWega_AEF30" or camid == "SopraBerlin" or camid == "SopraWega_AEI51": # todo this looks super hardcoded :o
                if False:
                    time_idx_from = -3
                    time_idx_to = -1
                    self.offset_spectrum = np.nanmean(return_data["signal_counts"][time_idx_from:], 0)
                    # trying to make this automatic: either take first 2 frames or last three frames
                else:
                    first_frame_average = np.nanmean(return_data["signal_counts"][:3])
                    last_frame_average = np.nanmean(return_data["signal_counts"][-3:])
                    if first_frame_average > last_frame_average: # take last 3 frames 
                        time_idx_from = -3
                        time_idx_to = -1
                        self.offset_spectrum = np.nanmean(return_data["signal_counts"][time_idx_from:], 0)
                    else:
                        time_idx_from = 0
                        time_idx_to = 1
                        self.offset_spectrum = np.nanmean(return_data["signal_counts"][:time_idx_to], 0)
                        
            else:
                time_idx_from = 0
                time_idx_to = utils.val2idx(return_data["time_s"], 0) - 1 
                self.offset_spectrum = np.nanmean(
                    return_data["signal_counts"][:time_idx_to], 0)
            if debug:
                logger.info("Shape of data[signal_counts]: {}".format(return_data["signal_counts"].shape))
                logger.info("Shape of offset_spectrum: {}".format(self.offset_spectrum.shape))
            return_data["signal_counts"] = return_data["signal_counts"] - \
                self.offset_spectrum
            
            if correct_smearing:
                return_data["signal_counts_without_smearing_correction"] = copy.deepcopy(return_data["signal_counts"])
                for t_idx in range(len(return_data["signal_counts"])):
                    return_data["signal_counts"][t_idx] = utils.correct_smearing(measured_signal=return_data["signal_counts"][t_idx],shift_time=1.2e-6,exp_time=self.exposure_time,bin_width=32,shift_direction="both")
            if debug: # plot average signal on entire chip over time
                average_signal_over_time = np.nanmean(return_data["signal_counts"],(1,2))
                plt.figure(figsize=(8,6))
                plt.plot(return_data["time_s"],average_signal_over_time,label="Average signal")
                if correct_smearing:
                    average_signal_over_time_without_smearing_correction = np.nanmean(return_data["signal_counts_without_smearing_correction"],(1,2))
                    plt.plot(return_data["time_s"],average_signal_over_time_without_smearing_correction,label="without smearing correction")
                    plt.legend()
                plt.xlabel("Time [s]")
                plt.ylabel("Average signal entire chip [Counts/exp time]")
                plt.axvspan(xmin=return_data["time_s"][time_idx_from],xmax=return_data["time_s"][time_idx_to],label="Electronic background window",color="grey",alpha=0.5)
                if self.experiment_length is not None:
                    plt.axvline(self.experiment_length,c="r",label="Experiment length",ls="--")
                plt.grid()
                
                plt.show()
        return return_data
    def get_camera_meta_data(self,time_id,camid):
        self.metadata = {}
        self.path_metadata_camera =self.ground_path+self.database_path+self.qss_path+self.datasource_settings["archive_path"]+ "_PARLOG/parms/PICam/config/"
        if self.campaign == "OP1.2b":
            self.path_metadata_spec = self.ground_path+self.database_path+self.qss_path+self.datasource_settings["archive_path_spectrometer"]+"_PARLOG/"
            print(self.path_metadata_spec)
        else:
            self.path_metadata_spec = self.ground_path+self.database_path+self.qss_path+self.datasource_settings["archive_path"]+"_PARLOG/parms/IsoPlaneControl/atTrigger/"
        self.path_metadata = self.ground_path+self.database_path+self.qss_path+self.datasource_settings["archive_path"]+"_PARLOG/"
        if type(time_id)==str:
            self.metadata_full = archivedb.get_parameters_box_for_program(self.path_metadata,time_id,timeout=10)["values"][0]
        else:
            self.metadata_full = archivedb.get_parameters_box(self.path_metadata,*time_id,timeout=10)["values"][0]
        self.metadata = {}
        metadata_dict_cam = ["AdcEMGain","Rois_arrays","ROIs_configString","ExposureTime"]
        if self.campaign == "OP1.2b":
            metadata_dict_spec = ["Central_Wavelength"]
        else:
            metadata_dict_spec = ["centralWavelength","gratingNumber","slitWidth","filterNumber"]
        for key in metadata_dict_cam:
            try:
                self.metadata[key] = self.metadata_full["PICam"]["config"][key]
            except:
                self.metadata[key] = None
        if self.campaign == "OP1.2b":
            if type(time_id)==str:
                self.metadata_spec = archivedb.get_parameters_box_for_program(self.path_metadata_spec,time_id,timeout=10)["values"][0]
            else:
                self.metadata_spec = archivedb.get_parameters_box(self.path_metadata_spec,*time_id,timeout=10)["values"][0]
        for key in metadata_dict_spec:
            try:
                if self.campaign == "OP1.2b":
                    self.metadata[key] = self.metadata_spec[key]
                else:
                    self.metadata[key] = self.metadata_full["IsoPlaneControl"]["atTrigger"][key]
            except:
                self.metadata[key] = np.nan
    def get_software_binning(self,pid,camid,debug):
            """When the image is not hardware binned, we want to use software to make the channels
            Here I want to include the possibility to count from the top of from the bottom

            Args:
                pid (_type_): _description_
                camid (_type_): _description_
                debug (_type_): _description_
            """
            logger.debug("your data is not binned")
            logger.debug(self.data["time_s"])
            logger.debug(self.data["signal_counts"].shape)
            x0 = self.datasource_settings["binning"]["position_channel_0"]
            width_fiber = self.datasource_settings["binning"]["width_fiber"]
            number_channels = self.datasource_settings["binning"]["number_channels"]
            if self.software_binning_shift is not None:
                x0+=self.software_binning_shift
            self.data["signal_counts_unbinned"] = copy.deepcopy(
                self.data["signal_counts"])
            self.data["signal_counts"] = np.zeros(
                (len(self.data["signal_counts"]), number_channels, 1024))
            rois_from_archive = False
            try:
                rois = self.datasource_settings["binning"]["ROIs"]
                df_rois = utils.ROIs_configString_to_df(rois)
                rois_from_archive = True
            
            except:
                logger.info("No ROI config string specified")
            rois_from_archive = False
            if rois_from_archive:
                for c_idx,channel in df_rois.iterrows():
                    row_from = int(channel["y"])
                    row_to = int(int(channel["y"])+int(channel["y_binning"]))
                    self.data["signal_counts"][:, c_idx] = np.sum(self.data["signal_counts_unbinned"][:,row_from:row_to ],axis=1)
            else:
                for c_idx in range(number_channels):
                    if width_fiber > 0:
                        row_from = int(x0+width_fiber*c_idx)
                        row_to = int(x0+width_fiber*(c_idx+1))
                    if width_fiber < 0:
                        row_to = int(x0+width_fiber*c_idx)
                        row_from = int(x0+width_fiber*(c_idx+1))
                    #print(row_from,row_to)
                    if row_from < 0:
                        row_from = 0
                    if row_to > 1024:
                        row_to = 1024
                    self.data["signal_counts"][:, c_idx] = np.nansum(self.data["signal_counts_unbinned"][:,row_from:row_to ],axis=1)
            if debug:
                self.spectrum_averaged_time_and_wavelength = np.nanmean(self.data["signal_counts_unbinned"], (0,2))
                plt.figure(figsize=(12,6))
                plt.plot(np.arange(1024),self.spectrum_averaged_time_and_wavelength)
                if False:
                    for peak_position in peak_positions:
                        plt.axvline(peak_position,ls="--",c="r")
                for channel in range(number_channels+1):
                    plt.axvline(np.round(x0+channel*width_fiber),ls="--",c="green")
                plt.title("Linear binning {} with Channel width {:.2f} pixels".format(camid,width_fiber))
                plt.xlabel("Y position camera [Pixel]")
                plt.ylabel("Average intensity")
                plt.yscale("log")
                plt.show()
                plt.figure(figsize=(12, 12))
                if type(self.time_id) == str:
                    plt.title("Averaged signal over entire discharge: "+str(pid)+" "+camid)
                else:
                    plt.title("Averaged signal over entire discharge: "+str(self.date_from)+" "+camid)
                plt.imshow(np.mean(self.data["signal_counts_unbinned"], 0),
                            aspect="auto", interpolation="none")
                if rois_from_archive:
                    for idx,channel in df_rois.iterrows():
                        plt.axhline(int(channel["y"]),c="r",ls="--")
                        plt.axhline(int(int(channel["y"])+int(channel["y_binning"])),c="g",ls="--")
                else:
                    for i in range(number_channels+1):
                        position = x0 + width_fiber * i
                        plt.axhline(int(position), c="r", ls="--")
                    plt.axhline(int(x0),c="g",ls="--",label="Channel 0 {}".format(self.port))
                if self.camid == "SopraWega_AEF30":
                    plt.axhline(54*width_fiber+x0+15,c="orange",ls="--",label="Overview fiber 1")
                    plt.axhline(54*width_fiber+x0+35,c="orange",ls="--")
                    plt.axhline(54*width_fiber+x0+40,c="orange",ls="--",label="Overview fiber 2")
                    plt.axhline(54*width_fiber+x0+80,c="orange",ls="--")
                    
                plt.colorbar()
                plt.legend()
                plt.show()
                plt.figure(figsize=(12, 8))
                plt.title("Binned - every available channel")
                plt.imshow(np.mean(self.data["signal_counts"], 0),
                            aspect="auto", interpolation="none")
                
                plt.colorbar()
                plt.show()