import unittest
import numpy as np
import matplotlib.pyplot as plt

import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import analysis
from divertor_spectroscopy.spectrum_models import ip320_cw_404_HgAr_calibration
from divertor_spectroscopy.spectrum_models import synthetic_spectrum
from divertor_spectroscopy import instrument
from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import spectrum
class Test_analysis(unittest.TestCase):
    """Tests for analysis class"""
    def test_fit_spectrum(self,debug=True):
       """
       Collection of tests for the fit_spectrum method

       Args:
           debug (bool, optional): _description_. Defaults to True.
       """
       
       # Simplest test: compare a spectrum with a central peak with gauss and numeric instrument function
       model = synthetic_spectrum.Synthetic_spectrum()
       wavelength = np.linspace(395,415,1024)
       inst_gauss = instrument.Instrument(wavelength=wavelength,instrument_function_mode="gauss",width=0.04)
       x = np.arange(31)
       dispersion = 20/1024
       numeric_instrument_function = utils.gauss(x=x,offset=0,position=50,width=0.03/dispersion)
       inst_numeric = instrument.Instrument(wavelength=wavelength,instrument_function_mode="convolution_entire_spectrum",numeric_instrument_function=numeric_instrument_function)
       analyse_gauss = analysis.Analysis(model=model,instrument=inst_gauss)
       analyse_numeric = analysis.Analysis(model=model,instrument=inst_numeric)
       params = [1] # Intensities of the peaks
       params_numeric = [1]

       spectrum_gauss = analyse_gauss.build_spectrum(wavelength,*params,filter=None)
       spectrum_numeric = analyse_numeric.build_spectrum(wavelength,*params_numeric,filter=None)
       
       plt.figure(figsize=(8,6))
       plt.plot(wavelength,spectrum_gauss,label="Gauss data")
       plt.plot(wavelength,spectrum_numeric,label="Numeric data")
       plt.legend()
       plt.xlabel("Wavelength [nm]")
       plt.ylabel("Intensity [a.u.]")
       plt.show()

       
       #Test if fitting, simplest case. Two peaks, gaussian instrumentfunction, linear wavelength
       model = ip320_cw_404_HgAr_calibration.Ip320_cw_404_HgAr_calibration() 
       wavelength = np.linspace(395,415,1024)
       inst_gauss = instrument.Instrument(wavelength=wavelength,instrument_function_mode="gauss",width=0.04)
       x = np.arange(101)
       dispersion = 20/1024
       numeric_instrument_function = utils.gauss(x=x,offset=0,position=50,width=0.04/dispersion)
       inst_numeric = instrument.Instrument(wavelength=wavelength,instrument_function_mode="convolution_entire_spectrum",numeric_instrument_function=numeric_instrument_function)
       analyse_gauss = analysis.Analysis(model=model,instrument=inst_gauss)
       analyse_numeric = analysis.Analysis(model=model,instrument=inst_numeric)
       params = [1,2] # Intensities of the peaks
       params_numeric = [np.log10((10**params[0])*1/2),np.log10((10**params[1])*1/2)]
       params_numeric = params
       spectrum_gauss = analyse_gauss.build_spectrum(wavelength,*params,filter=None)
       spectrum_numeric = analyse_numeric.build_spectrum(wavelength,*params_numeric,filter=None)
       
       plt.figure(figsize=(8,6))
       plt.plot(wavelength,spectrum_gauss,label="Gauss data")
       plt.plot(wavelength,spectrum_numeric,label="Numeric data")
       plt.legend()
       plt.xlabel("Wavelength [nm]")
       plt.ylabel("Intensity [a.u.]")
       plt.show()

if __name__ == '__main__':
    unittest.main()
