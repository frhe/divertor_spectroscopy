import unittest
import numpy as np
import matplotlib.pyplot as plt

import logging
logger = logging.getLogger(__name__)

from divertor_spectroscopy import utilities


class Test_utilities(unittest.TestCase):
    """Tests for utilities class"""
    def test_gauss(self,debug=True):
        """testing gauss function
        """
        x = np.linspace(395, 415, 1024)
        y = utilities.gauss(x)  # takes default values
        if debug:
            plt.figure(figsize=(8,6))
            plt.plot(x,y)
            plt.show()
        intensity = np.sum(y) / 1024 * 20
        logger.debug("Comparison of intensity 1 with sum of spectrum {}".format(intensity))
        self.assertEqual(np.round(intensity), 1)

    def test_val2idx(self):
        liste = np.array([3, 8, 9, 2, 8])  # testing input
        self.assertEqual([1], utilities.val2idx(
            liste, [8]))  # always takes first
        # rounding doesn't change this
        self.assertEqual(0, utilities.val2idx(liste, 2.5))
        self.assertEqual([3, 1], utilities.val2idx(liste, [2, 7.5]))

    def test_polinominal(self):
        coefficients = [3, 2, 1]  # should lead to 3+2x+1x**2
        result = utilities.polynominal(2, *coefficients)
        coefficients = [4, 3, 2, 1]  # should lead to 4+3+2+1
        self.assertEqual(result, 11)
        result = utilities.polynominal(0, *coefficients)
        self.assertEqual(result, 4)


if __name__ == '__main__':
    unittest.main()
