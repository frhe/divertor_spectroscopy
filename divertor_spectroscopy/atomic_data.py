"""
This class would optimally run with the adas scripts -> hopefully available later

but right now I am just going to use Felix Reimolds scripts for convenience
    adf11 for fracitonal abundance
    adf15 for reading PECs
"""
# official python packages
import numpy as np
import matplotlib.pyplot as plt
import json
import codecs
import copy
import periodictable
import logging
from scipy.interpolate import RegularGridInterpolator
from scipy.optimize import root
from scipy.optimize import minimize
logger = logging.getLogger(__name__)
#import colradpy

# Felix' python packages
try:
    from atomdat.adas import ibal
    from atomdat.adas import adf15
    

except ImportError as e:
    logger.error("{}: some functionality of the code will be missing! ".format(e))
# my python packages
try: 
    from hebeam_analysis import crm_solver
except ImportError as e:
    logger.error("{}: some functionality of the code will be missing! ".format(e))
from divertor_spectroscopy import utilities as utils
import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

def get_frac_abundance(element="n", ions=[1, 2], ne_count=100, Te_count=101, ne_range=[1e19, 1e21],ne_scale="log", Te_range=[1, 10],Te_scale="linear", year="96", debug=False):
    """
    for NII model I am interested in calculating fractional abundance on a very fine grid of ne and Te
    the gridpoints are set by input-parameters
    the fractional abundance of singly and double ionized nitrogen is returned with the shape [n,T], because usually we plot for specific n against the temperature

    The electron density vector in the pec files is log
    The electron temperature vector in the pec files is lin
    The electron density vector in the fractional abundance is log
    Since this is from a method of Felix, that I don't understand, I need to map it somehow
    """

    # this is the n and T-array, that I want the analysis happening - for now it is ne_array log and Te_array lin - in the adas files that might be different or even nonuniform
    if ne_scale == "log":
        ne_array = np.logspace(np.log10(ne_range[0]), np.log10(ne_range[1]), ne_count)
    if ne_scale == "linear":
        ne_array = np.linspace(ne_range[0],ne_range[1],ne_count)
    if Te_scale == "linear":
        Te_array = np.linspace(Te_range[0], Te_range[1], Te_count)
    if Te_scale == "log":
        Te_array = np.logspace(np.log10(Te_range[0]), np.log10(Te_range[1]), Te_count)
    frac = np.zeros((len(ions), ne_count, Te_count))
    #Te_range_log = [np.log10(Te_range[0]), np.log10([Te_range[1]])]
    # needed for fractional abundance
    #Te_array_log = np.logspace(Te_range_log[0], Te_range_log[1], Te_count * 10)
    for n in range(len(ne_array)):
        te, fz, lz, lzl = ibal.calc_fz(
            elem=element, te=Te_array, ne=ne_array[n], year=year,pn=ground_path+"adas/adf11/")
        # now map everything to a linear Te scale
        # for T_idx_lin in range(len(Te_array)):
        #    T_idx_log = utils.val2idx(Te_array_log, Te_array[T_idx_lin])
        #    for i in range(len(ions)):
        #        frac[i, n, T_idx_lin] = fz[T_idx_log, i + ions[0]]+9
        if (te != Te_array).all():
            logger.critical("something is wrong here!")
        for i in range(len(ions)):
            frac[i, n] = fz[:, i + ions[0]]
        if np.mod(n, ne_count / 10) == 0 and n != 0:
            logger.info("{} % done".format(n / ne_count * 100))

    if debug:
        extent = [Te_array[0], Te_array[-1], ne_array[0], ne_array[-1]]
        for i in range(len(ions)):
            plt.figure(figsize=(8,6))
            plt.imshow(frac[i], aspect="auto", origin="lower",
                       extent=extent, interpolation="nearest")
            plt.xlabel("Temperature [eV]")
            plt.ylabel("density [m$^{-3}$]")
            plt.title("Fractional abundance {} {}+".format(element, ions[i]))
            if ne_scale == "log":
                utils.set_log_axis_imshow(
                    axis_array=ne_array, axis="y")
            if Te_scale == "log":
                utils.set_log_axis_imshow(
                    axis_array=Te_array, axis="x")
            plt.colorbar()
            plt.show()
    return frac

def get_frac_abundance_colradpy(element="n", ions=[1, 2], ne_count=100, Te_count=101, ne_range=[1e19, 1e21],ne_scale="log", Te_range=[1, 10], year="96", debug=False):
    """
    for NII model I am interested in calculating fractional abundance on a very fine grid of ne and Te
    the gridpoints are set by input-parameters
    the fractional abundance of singly and double ionized nitrogen is returned with the shape [n,T], because usually we plot for specific n against the temperature

    The electron density vector in the pec files is log
    The electron temperature vector in the pec files is lin
    The electron density vector in the fractional abundance is log
    Since this is from a method of Felix, that I don't understand, I need to map it somehow
    """

    # this is the n and T-array, that I want the analysis happening - for now it is ne_array log and Te_array lin - in the adas files that might be different or even nonuniform
    if ne_scale == "log":
        ne_array = np.logspace(np.log10(ne_range[0]), np.log10(ne_range[1]), ne_count)
    if ne_scale == "linear":
        ne_array = np.linspace(ne_range[0],ne_range[1],ne_count)
    Te_array = np.linspace(Te_range[0], Te_range[1], Te_count)
    frac = np.zeros((len(ions), ne_count, Te_count))
    #Te_range_log = [np.log10(Te_range[0]), np.log10([Te_range[1]])]
    # needed for fractional abundance
    #Te_array_log = np.logspace(Te_range_log[0], Te_range_log[1], Te_count * 10)
    for n in range(len(ne_array)):
        te, fz, lz, lzl = ibal.calc_fz(
            elem=element, te=Te_array, ne=ne_array[n], year=year,pn=ground_path+"adas/adf11/")
        # now map everything to a linear Te scale
        # for T_idx_lin in range(len(Te_array)):
        #    T_idx_log = utils.val2idx(Te_array_log, Te_array[T_idx_lin])
        #    for i in range(len(ions)):
        #        frac[i, n, T_idx_lin] = fz[T_idx_log, i + ions[0]]
        if (te != Te_array).all():
            logger.critical("something is wrong here!")
        for i in range(len(ions)):
            frac[i, n] = fz[:, i + ions[0]]
        if np.mod(n, ne_count / 10) == 0 and n != 0:
            logger.info("{} % done".format(n / ne_count * 100))

    if debug:
        extent = [Te_array[0], Te_array[-1], ne_array[0], ne_array[-1]]
        for i in range(len(ions)):
            plt.figure(figsize=(8,6))
            plt.imshow(frac[i], aspect="auto", origin="lower",
                       extent=extent, interpolation="nearest")
            plt.xlabel("Temperature [eV]")
            plt.ylabel("density [m$^{-3}$]")
            plt.title("Fractional abundance {} {}+".format(element, ions[i]))
            if ne_scale == "log":
                utils.set_log_axis_imshow(
                    axis_array=ne_array, axis="y")
            plt.colorbar()
            plt.show()
    return frac


def get_pecs(element="n",ion=1,indices=None,ne_count=100, Te_count=101, ne_range=[1e19, 1e21],ne_scale="log", Te_range=[1, 10],all_pecs=False,comment=None, debug=False):
    """
    On the same grid as the fractional abundance I want to save the PEC values for all 3 used lines:
        - exc 399.5, exc 402.6, exc 404.1, rec 399.5, rec 402.6, rec 404.1
    grid is input and other than frac_abundance the grid is [T,n], because we plot against density for specific T
    """
    reading_object = adf15.ADF15()
    if element == "h" and ion ==0:
        fn = ground_path + 'adas/adf15/pec12#h_pju#h0.dat'
        idx = [31,32,39,50,53]
        idx = [16,11] # epsilon und delta
        idx = [7,2] # gamma und alpha
        number_lines = len(idx)
        if indices is not None:
            idx = indices
        for index in copy.deepcopy(idx):
            idx.append(index+66)
        headerlength = 0
    elif element == "he" and ion ==0:
        fn = ground_path + 'adas/adf15/pec96#he_pjr#he0.dat'
        idx = [4,12,7,11,34,42,37,41]
        if indices is not None:
            idx = indices
        number_lines = int(len(idx)/2)
        headerlength = 0
    elif element == "c" and ion == 1:
        fn = ground_path + 'adas/adf15/pec96#c_vsu#c1.dat'
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        idx = [8,32,1]
        if all_pecs:
            idx = list(range(50))
        number_lines = len(idx)
        if indices is not None:
            idx = indices
        for index in copy.deepcopy(idx):
            idx.append(index+50)
        headerlength = 0
    elif element == "c" and ion == 2:
        fn = ground_path + 'adas/adf15/c/pec96#c_vsu#c2.dat'
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        idx = [21]
        if all_pecs:
            idx = list(range(100))
        number_lines = len(idx)
        if indices is not None:
            idx = indices
        for index in copy.deepcopy(idx):
            idx.append(index+50)
        headerlength = 0
    elif element == "n" and ion == 1:
        fn = ground_path + 'adas/adf15/pec98#n_ssh_pju#n1.dat'
        idx = [14, 18, 20] # indices Stuart Henderson is using for his Line ratio model - 402.9 nm line difficult to fit due to overlapping lines!
        # idx = [14, 20, 24] # possibly new line 408.7 line must be identified!
        if all_pecs:
            idx = list(range(50))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            idx.append(index+50)
        headerlength = 5
    elif element == "n" and ion == 2:
        fn = ground_path + 'adas/adf15/n/pec96#n_vsu#n2.dat'
        idx = [0] # indices Stuart Henderson is using for his Line ratio model - 402.9 nm line difficult to fit due to overlapping lines!
        # idx = [14, 20, 24] # possibly new line 408.7 line must be identified!
        if all_pecs:
            idx = list(range(50))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            idx.append(index+50)
        headerlength = 0
    elif element == "ne" and ion == 1 and comment =="old":
        fn = ground_path + 'adas/adf15/ne/ssh01_ls_pju#ne1.dat'
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        #idx = [31,32,53] # indices measured in OP1.2b
        idx = [23,31,32] # indices picked by Victoria Winters for OP2
        if all_pecs:
            idx = list(range(100))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            idx.append(index+100)
        headerlength = 6
    elif element == "ne" and ion == 1 and comment=="recreated":
        fn = ground_path + 'adas/adf15/ne/ssh01_ls_pju#ne1_recreated.dat'
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        #idx = [31,32,53] # indices measured in OP1.2b
        #idx = [4,9,10] # indices picked by Victoria Winters for OP2
        idx = [16,23,24]
        if all_pecs:
            idx = list(range(100))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            #idx.append(index+100)
            idx.append(index+50)
        headerlength = 6
    elif element == "ne" and ion == 1 and comment=="extended_temperature":
        fn = ground_path + 'adas/adf15/ne/ssh01_ls_pju#ne1_extended_temperature.dat'
        
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        #idx = [31,32,53] # indices measured in OP1.2b
        #idx = [4,9,10] # indices picked by Victoria Winters for OP2
        idx = [15,22,23]
        if all_pecs:
            idx = list(range(100))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            #idx.append(index+100)
            idx.append(index+50)
        headerlength = 6
    elif element == "ne" and ion == 1:
        fn = ground_path + 'adas/adf15/ne/ssh01_ls_pju#ne1_extended_density.dat'        
        #idx = [75,89,90,91,92] # indices between 396 and 416 nm
        #idx = [31,32,53] # indices measured in OP1.2b
        #idx = [4,9,10] # indices picked by Victoria Winters for OP2
        idx = [16,23,24]
        if all_pecs:
            idx = list(range(100))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            #idx.append(index+100)
            idx.append(index+50)
        headerlength = 6
    
    elif element == "ar" and ion ==1:
        fn = ground_path + 'adas/adf15/dcg01_ic_pju#ar1.dat'
        idx = [79,73,77]
        if all_pecs:
            idx = list(range(100))
        if indices is not None:
            idx = indices
        number_lines = len(idx)
        for index in copy.deepcopy(idx):
            idx.append(index+100)
        headerlength = 8
    logger.debug("indices: {}".format(idx))
    _ = reading_object.read(fn, v=1,headerlength=headerlength)  # was macht das v=1 für einen Unterschied?
    wavelength = []
    for index in idx:
        wavelength.append(utils.wl_vac_to_air(reading_object.dat.wvl[index]))
    # similar to the fractional abundance I want this to be on my chosen grid, which is log in ne space and lin in Te space
    if ne_scale == "log":
        ne_array = np.logspace(np.log10(ne_range[0]), np.log10(ne_range[1]), ne_count)
    if ne_scale == "linear":
        ne_array = np.linspace(ne_range[0],ne_range[1],ne_count)
    Te_array = np.linspace(Te_range[0], Te_range[1], Te_count)
    nn = ne_array.shape[0]  # number of density entries
    nt = Te_array.shape[0]  # number of temperature entries
    # create nt times n_e array and nt times
    neg, teg = np.meshgrid(ne_array, Te_array)
    neg = neg.flatten()
    teg = teg.flatten()
    pecs = np.zeros((number_lines*2, ne_count, Te_count))
    for i in range(2*number_lines):
        # reads data from file and interpolates to flattened array
        pecs[i] = np.reshape(reading_object.interpolate(neg, teg, idx=idx[i]), (nt, nn)).T
    if False:  # will plot line-ratios
        titles = wavelength
        fig, axs = plt.subplots(number_lines, 2, figsize=(12, 3*number_lines))
        axs = axs.flatten()
        order = [0, 3, 1, 4, 2, 5]
        order = []
        for i in range(number_lines):
            order.append(i)
            order.append(i+number_lines)
        for i in range(2*number_lines):
            for T in range(len(Te_array)):
                axs[i].plot(ne_array, pecs[order[i], :, T])
            if np.mod(i, 2) == 0:
                axs[i].set_title("Excitation {:4.0f}".format(titles[int(i / 2)]))
            else:
                axs[i].set_title(
                    "Recombination {:4.0f}".format(titles[int((i - 1) / 2)]))
            axs[i].set_xscale("log")
        plt.tight_layout()
        plt.show()

    return wavelength,pecs


def get_line_intensities(element="n",ion=1, indices =None,ne_count=100, Te_count=101, ne_range=[1e19, 1e21],ne_scale="log", Te_range=[1, 10],use_recom=True,use_fractional_abundance=True,comment=None, debug=False, cache=False, save=False,title=None,add_points=None,save_path=None):
    """
    Function to get line intensities for plasma conditions
    There is a major difference if you enable use_recom or not.
    If you enable this, a fractional abundance is used to get the line intensities from both excitation and recombination contribution
    If you don't enable this, we don't need a fractional abundance but only devide the PECs by each other
    todo: But if you still want the absolute intensities through the fractional abundance, this should also be made possible
    
    """
    if cache:
        with open(ground_path+"qss_analysis//json_files//atomic_data.json") as f:
            atomic_data = json.load(f)
            # compare ne range, Te range, ne_and Te_count. If these are not the same, cache can't be allowed!
            if (ne_range != np.array(atomic_data["{}_{}".format(element,ion)]["ne_range"])).all():
                logger.error("ne_range " + str(ne_range)+" is not equal to "+str(atomic_data["{}_{}".format(element,ion)]["ne_range"]))
            if (Te_range != np.array(atomic_data["{}_{}".format(element,ion)]["Te_range"])).all():
                logger.error("Te_range " + str(Te_range)+" is not equal to "+str(atomic_data["{}_{}".format(element,ion)]["Te_range"]))
            if ne_count != np.array(atomic_data["{}_{}".format(element,ion)]["ne_count"]):
                logger.error("ne_count " + str(ne_count)+" is not equal to "+str(atomic_data["{}_{}".format(element,ion)]["ne_count"]))
            if Te_count != np.array(atomic_data["{}_{}".format(element,ion)]["Te_count"]):
                logger.error("Te_count " + str(Te_count)+" is not equal to "+str(atomic_data["{}_{}".format(element,ion)]["Te_count"]))
            if indices is None:
                logger.debug("This seems to ask for default lines as indices is None")
            else:
                if (np.array(indices) != np.array(atomic_data["{}_{}".format(element,ion)]["indices"])).all():
                    logger.error("indices " + str(indices)+" is not equal to "+str(atomic_data["{}_{}".format(element,ion)]["indices"]))
            wavelength = np.array(atomic_data["{}_{}".format(element,ion)]["wavelength"])
            ne_array = np.array(atomic_data["{}_{}".format(element,ion)]["ne_array"])
            Te_array = np.array(atomic_data["{}_{}".format(element,ion)]["Te_array"])
            frac = np.array(atomic_data["{}_{}".format(element,ion)]["frac"])
            pecs = np.array(atomic_data["{}_{}".format(element,ion)]["pecs"])
            line_intensities = np.array(atomic_data["{}_{}".format(element,ion)]["line_intensities"])
    else:
        if element == "he":
            pecs = [None,None,None,None,None,None]
            wavelength = [6678,7065,7281]
        elif element == "synthetic":
            pecs = [None,None,None,None,None,None]
            wavelength = [3950,4000,4050]
        else:
            wavelength,pecs = get_pecs(element=element,ion=ion,indices=indices,ne_count=ne_count,ne_scale=ne_scale, Te_count=Te_count,ne_range=ne_range, Te_range=Te_range, debug=debug,comment=comment)
        if use_fractional_abundance: 
            if element == "c" and ion == 1:
                year = "96"
            if element == "c" and ion == 2:
                year = "96"
            if element == "n" and ion == 1:
                year = "96"
            if element == "n" and ion == 2:
                year = "96"
            if element == "ne" and ion == 1:
                year = "89"
            if element == "ar" and ion == 1:
                year = "89"
            
            frac = get_frac_abundance(element=element, ions=[ion,ion+1],
                                    ne_count=ne_count, Te_count=Te_count, ne_range=ne_range,ne_scale=ne_scale, Te_range=Te_range,year=year, debug=debug)
        if ne_scale == "log":
            ne_array = np.logspace(np.log10(ne_range[0]), np.log10(ne_range[1]), ne_count)
        if ne_scale == "linear":
            ne_array = np.linspace(ne_range[0],ne_range[1],ne_count)
        Te_array = np.linspace(Te_range[0], Te_range[1], Te_count)
        # save this to json file
        # ratios are ordered as in Henderson2018 figure 6: 404/399, 402/399, 404/402
        number_lines = int(len(pecs)/2)
        if not element == "he" and not element == "synthetic":
            line_intensities = np.zeros((number_lines,ne_count, Te_count))
            for i in range(number_lines):
                for n_idx in range(len(ne_array)):
                    for T_idx in range(len(Te_array)):
                        if use_fractional_abundance:
                            if use_recom:
                                line_intensities[i,n_idx,T_idx] = frac[0,n_idx,T_idx] * pecs[i,n_idx,T_idx] + frac[1,n_idx,T_idx] * pecs[i+number_lines, n_idx, T_idx]
                            else:
                                line_intensities[i,n_idx,T_idx] = frac[0,n_idx,T_idx] * pecs[i,n_idx,T_idx] 
                        else:
                            line_intensities[i,n_idx,T_idx] = pecs[i,n_idx,T_idx] 
    if debug:
        fontsize = 16
        plt.rcParams.update({'font.size': fontsize})
        if False:
            fig, axs = plt.subplots(1,3, figsize=(16, 6))
            axs = axs.flatten()
            for T in range(len(Te_array)):
                axs[0].plot(ne_array, line_intensities[2,:,T]/line_intensities[0, :, T],label="{}eV".format(Te_array[T]))
                axs[1].plot(ne_array, line_intensities[1,:,T]/line_intensities[0, :, T])
                axs[2].plot(ne_array, line_intensities[2,:,T]/line_intensities[1, :, T])
            for i in range(3):
                if ne_scale == "log":
                    axs[i].set_xscale("log")
                axs[i].set_xlabel("Density [m$^{-3}$]")
                axs[i].set_xlim(*ne_range)
                axs[i].grid()
            axs[0].set_title("{:4.0f} / {:4.0f}".format(wavelength[2],wavelength[0]))
            axs[1].set_title("{:4.0f} / {:4.0f}".format(wavelength[1],wavelength[0]))
            axs[2].set_title("{:4.0f} / {:4.0f}".format(wavelength[2],wavelength[1]))
            axs[0].set_ylim(0,1)
            axs[0].legend()

            # axs[i].set_xlim(10**ne_range[0], 10**ne_range[1])
            axs[0].set_ylabel("Line ratio")
            plt.tight_layout()
            plt.show()
        
        fig = plt.figure(figsize=(10,6))
        special_colors = ["darkmagenta","darkred","darkblue","darkgreen"]
        #order = np.argsort([np.max(line_intensities[0]),np.max(line_intensities[1]),np.max(line_intensities[2])])
        # first ratio is weakest by medium and second ratio is medium by strongest
        ax = plt.subplot(111)
        #plt.fill_between(x=[1,10],y1=1.5e19,y2=2.5e19,alpha=0.2,color="grey")
        #plt.fill_between(x=[3.25,3.75],y1=5e18,y2=10e20,alpha=0.2,color="grey")
        #plt.fill_between(x=[1,10],y1=2.5e20,y2=4e20,alpha=0.2,color="grey")
        #plt.fill_between(x=[4.25,4.75],y1=5e18,y2=10e20,alpha=0.2,color="grey")
        cmap_temperature = "cool"
        cmap_density = "viridis"
        if element == "synthetic":
            ratio_density = np.zeros((len(ne_array),len(Te_array)))
            ratio_temperature = np.zeros((len(ne_array),len(Te_array)))
            for n_idx,n in enumerate(ne_array):
                ratio_density[n_idx]=n_idx/len(ne_array)
            for T_idx,T in enumerate(Te_array):
                ratio_temperature[:,T_idx]=T_idx/len(Te_array)
            if True: # more temperature dependent ratio
                vmin = None
                vmax = None
                #vmin=0
                #vmax=1
                # same output as Erik:
                #cs2 = plt.contour(Te_array,ne_array,line_ratios[1],15,linestyles="dashed",levels=[1,1.5,2,3,4.5,7.5,10,12.5],cmap=cmap_temperature,ls="--",vmin=vmin,vmax=vmax,)
                #utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[1]/10,1)),"/",str(np.round(wavelength[2]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[2],"black",special_colors[1]],axis="z",bbox_to_anchor=(1.5,0.1))
                levels=[0.1,0.3,0.5,0.7,0.9]
                #levels=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
                #levels = None
                cs2 = plt.contour(Te_array,ne_array,ratio_temperature,15,linestyles="dashed",levels=levels,cmap=cmap_temperature,ls="--",vmin=vmin,vmax=vmax,)
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[1]/10,1)),"/",str(np.round(wavelength[2]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[2],"black",special_colors[1]],axis="z",bbox_to_anchor=(1.5,0.1))
                cbar2 = plt.colorbar()
                #cbar2.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[0],wavelength[1]))
                plt.clabel(cs2, inline=1, fontsize=12)
            if True: # more density dependent
                vmin = None
                vmax = None
                #vmin=0
                #vmax=1
                # same output as Erik:
                levels=[0.1,0.3,0.5,0.7,0.9]
                #levels=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
                #levels=None
                cs1 = plt.contour(Te_array,ne_array,ratio_density,15,levels=levels,cmap=cmap_density,vmin=vmin,vmax=vmax,)

                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[0]/10,1)),"/",str(np.round(wavelength[2]/10,1))," n$_\mathrm{e}$-dependent -"],list_of_colors=["black",special_colors[2],"black",special_colors[0]],axis="z",bbox_to_anchor=(1.24,0.1))    
                cbar1 = plt.colorbar()
                #cbar1.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[1],wavelength[2]))
                plt.clabel(cs1, inline=1, fontsize=12)
        if element == "he": # usually we use the line intensities, but here we are going to use the line ratios from Eriks code
            
            ratio_707_728 = None # I just want to use Eriks calcultation of the PECs including magnetic field correction
            ratio_668_728 = None
            solver = crm_solver.CRM_solver(ratio_707_728, ratio_668_728, model = "Burgos_HR")
            te_ratio_interpolator, ne_ratio_interpolator = solver.helium_crm_forward_model()
            line_ratios = np.zeros((2,100,101))
            if ne_scale == "log":
                ne_array = np.logspace(np.log10(ne_range[0]), np.log10(ne_range[1]), ne_count)
            if ne_scale == "linear":
                ne_array = np.linspace(ne_range[0],ne_range[1],ne_count)
            Te_array = np.linspace(Te_range[0], Te_range[1], Te_count)
            # save this to json file
            # ratios are ordered as in Henderson2018 figure 6: 404/399, 402/399, 404/402
            number_lines = 3
            line_intensities = np.zeros((number_lines,ne_count, Te_count))
            for n_idx,n in enumerate(ne_array):
                for T_idx,T in enumerate(Te_array):
                        line_ratios[0,n_idx,T_idx] = ne_ratio_interpolator(T,n/1e18)
                        line_ratios[1,n_idx,T_idx] = te_ratio_interpolator(T,n/1e18)
            if True: # more temperature dependent ratio
                vmin = None
                vmax = None
                #vmin=0
                #vmax=1
                # same output as Erik:
                #cs2 = plt.contour(Te_array,ne_array,line_ratios[1],15,linestyles="dashed",levels=[1,1.5,2,3,4.5,7.5,10,12.5],cmap=cmap_temperature,ls="--",vmin=vmin,vmax=vmax,)
                #utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[1]/10,1)),"/",str(np.round(wavelength[2]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[2],"black",special_colors[1]],axis="z",bbox_to_anchor=(1.5,0.1))
                levels=[1,1.5,2,3,4.5,7.5,10,12.5]
                levels = None
                cs2 = plt.contour(Te_array,ne_array,1/line_ratios[1],15,linestyles="dashed",levels=levels,cmap=cmap_temperature,ls="--",vmin=vmin,vmax=vmax,)
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[1]/10,1)),"/",str(np.round(wavelength[2]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[2],"black",special_colors[1]],axis="z",bbox_to_anchor=(1.5,0.1))
                cbar2 = plt.colorbar()
                #cbar2.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[0],wavelength[1]))
                plt.clabel(cs2, inline=1, fontsize=12)
            if True: # more density dependent
                vmin = None
                vmax = None
                #vmin=0.1
                #vmax=0.45
                # same output as Erik:
                levels=[2,3,4,5,6,7,8,9]
                levels=None
                cs1 = plt.contour(Te_array,ne_array,line_ratios[0],15,levels=None,cmap=cmap_density,vmin=vmin,vmax=vmax,)
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[0]/10,1)),"/",str(np.round(wavelength[2]/10,1))," n$_\mathrm{e}$-dependent -"],list_of_colors=["black",special_colors[2],"black",special_colors[0]],axis="z",bbox_to_anchor=(1.24,0.1))    
                cbar1 = plt.colorbar()
                #cbar1.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[1],wavelength[2]))
                plt.clabel(cs1, inline=1, fontsize=12)
            
        if element == "ne":
            if True:#more temperature dependent ratio
                vmin = None
                vmax = None
                #vmin=0
                #vmax=1
                cs2 = plt.contour(Te_array,ne_array,line_intensities[0]/line_intensities[1],15,cmap=cmap_temperature,linestyles="dashed",vmin=vmin,vmax=vmax,)
                cbar2 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[0]/10,1)),"/",str(np.round(wavelength[1]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[1],"black",special_colors[0]],axis="z",bbox_to_anchor=(1.5,0.1))    
                #cbar1.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[1],wavelength[2]))
                plt.clabel(cs2, inline=1, fontsize=12)
            if True: # more density dependent ratio. In this case 371.3 /371.7
                vmin = None
                vmax = None
                #vmin=0.1
                #vmax=0.45
                cs1 = plt.contour(Te_array,ne_array,line_intensities[2]/line_intensities[1],15,cmap=cmap_density,vmin=vmin,vmax=vmax,)
                cbar1 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[2]/10,1)),"/",str(np.round(wavelength[1]/10,1))," n$_\mathrm{e}$-dependent -"],list_of_colors=["black",special_colors[1],"black",special_colors[2]],axis="z",bbox_to_anchor=(1.24,0.1))
                #cbar2.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[0],wavelength[1]))
                plt.clabel(cs1, inline=1, fontsize=12)
            
        if element == "n" and ion == 1: # order the ratios the same way as measurements are presented in Stuarts paper
            
            if True: # Temperature
                levels = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
                cs1 = plt.contour(Te_array,ne_array,line_intensities[2]/line_intensities[0],15,cmap=cmap_temperature,linestyles="--",levels=levels)
                cbar1 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[2]/10,1)),"/",str(np.round(wavelength[0]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[0],"black",special_colors[2]],axis="z",bbox_to_anchor=(1.5,0.1))
                #cbar2.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[0],wavelength[1]))
                plt.clabel(cs1, inline=1, fontsize=12)
            if True: # Density
                levels=None

                cs2 = plt.contour(Te_array,ne_array,line_intensities[1]/line_intensities[2],15,cmap=cmap_density,linesyles="-",levels=levels)
                cbar2 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[1]/10,1)),"/",str(np.round(wavelength[2]/10,1))," n$_\mathrm{e}$-dependent -"],list_of_colors=["black",special_colors[2],"black",special_colors[1]],axis="z",bbox_to_anchor=(1.24,0.1))    
                #cbar1.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[1],wavelength[2]))
                plt.clabel(cs2, inline=1, fontsize=12)
        if element == "c" and ion == 1: # order the ratios the same way as measurements are presented in Stuarts paper
            
            if True: # Temperature
                levels = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
                levels = None
                cs1 = plt.contour(Te_array,ne_array,line_intensities[0]/line_intensities[2],15,cmap=cmap_temperature,linestyles="--",levels=levels)
                cbar1 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[0]/10,1)),"/",str(np.round(wavelength[2]/10,1))," T$_\mathrm{e}$-dependent --"],list_of_colors=["black",special_colors[2],"black",special_colors[0]],axis="z",bbox_to_anchor=(1.5,0.1))
                #cbar2.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[0],wavelength[1]))
                plt.clabel(cs1, inline=1, fontsize=12)
            if True: # Density
                levels=None

                cs2 = plt.contour(Te_array,ne_array,line_intensities[2]/line_intensities[1],15,cmap=cmap_density,linesyles="-",levels=levels)
                cbar2 = plt.colorbar()
                utils.multicolor_ylabel(ax=ax,list_of_strings=[str(np.round(wavelength[2]/10,1)),"/",str(np.round(wavelength[1]/10,1))," n$_\mathrm{e}$-dependent -"],list_of_colors=["black",special_colors[1],"black",special_colors[2]],axis="z",bbox_to_anchor=(1.24,0.1))    
                #cbar1.ax.set_ylabel("{:4.0f}/{:4.0f}".format(wavelength[1],wavelength[2]))
                plt.clabel(cs2, inline=1, fontsize=12)
        plt.xlabel("Electron temperature [eV]")
        plt.ylabel("Electron density [m$^{-3}$]")
        if add_points is not None:
            for point in add_points:
                plt.scatter(x=point[0],y=point[1],s=100,linewidths=5,marker="o",label="Ratios from example")
            plt.legend()
        if ne_scale == "log":
            plt.yscale("log")
        #plt.tight_layout()
        plt.grid()
        if title is not None:
            plt.title(title)
        if save_path is not None:
            plt.savefig(save_path+".pdf")
            plt.savefig(save_path+".png",dpi=400)
        plt.show()
            
    if save:
        # opening the file without deleting anything
        path = ground_path+"qss_analysis//json_files//atomic_data.json"
        with open(path) as f:
            atomic_data = json.load(f)
        atomic_data["{}_{}".format(element,ion)] = {}
        atomic_data["{}_{}".format(element,ion)]["ne_range"] = ne_range
        atomic_data["{}_{}".format(element,ion)]["Te_range"] = Te_range
        atomic_data["{}_{}".format(element,ion)]["ne_count"] = ne_count
        atomic_data["{}_{}".format(element,ion)]["Te_count"] = Te_count
        atomic_data["{}_{}".format(element,ion)]["ne_array"] = ne_array
        atomic_data["{}_{}".format(element,ion)]["Te_array"] = Te_array
        atomic_data["{}_{}".format(element,ion)]["indices"] = np.array(indices[:int(len(indices)/2)])
        atomic_data["{}_{}".format(element,ion)]["wavelength"] = wavelength
        atomic_data["{}_{}".format(element,ion)]["frac"] = frac
        atomic_data["{}_{}".format(element,ion)]["pecs"] = pecs
        atomic_data["{}_{}".format(element,ion)]["line_intensities"] = line_intensities
        json.dump(atomic_data, codecs.open(path, 'w', encoding='utf-8'),
                  cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)
    if element == "he":
        return ne_array, Te_array, None, None, None
    if element == "synthetic":
        return None
    if not use_fractional_abundance:
        frac = None
    return ne_array, Te_array, frac, pecs, line_intensities

def get_ne_Te_from_tapete(ne_array, Te_array, line_ratio1_array, line_ratio2_array, line_ratio1_target, 
                          line_ratio2_target,ne_scale="linear",Te_scale="linear",element="Ne"):
    # Detect the scaling for ne and Te
    #ne_scale = utils.detect_scale(ne_array)
    #Te_scale = utils.detect_scale(Te_array) # this was buggy!

    # Convert arrays to logarithmic scale if necessary
    if ne_scale == "log":
        ne_interp_array = np.log10(ne_array)
    else:
        ne_interp_array = ne_array

    if Te_scale == "log":
        Te_interp_array = np.log10(Te_array)
    else:
        Te_interp_array = Te_array
    # Create interpolators
    method = "linear"
    interp_line_ratio1 = RegularGridInterpolator((ne_interp_array, Te_interp_array), line_ratio1_array,method=method)
    interp_line_ratio2 = RegularGridInterpolator((ne_interp_array, Te_interp_array), line_ratio2_array,method=method)

    # Define the function whose roots we want to find
    def equations(x):
        ne, Te = x
        return [
            interp_line_ratio1((ne, Te)) - line_ratio1_target,
            interp_line_ratio2((ne, Te)) - line_ratio2_target
        ]
     # Initial guess for optimization
    if element == "Ne":
        if np.max(Te_array) > 50: # active spectroscopy
            #if line_ratio2_target > 0.3 or line_ratio1_target < 0.25:
            #    initial_guess = [ne_interp_array[0], Te_interp_array[0]]
            #elif line_ratio2_target > 0.3 and line_ratio1_target > 0.25:
            #    initial_guess = [ne_interp_array[0], Te_interp_array[len(Te_interp_array) // 2]]
            #else:
            initial_guess = [ne_interp_array[1], Te_interp_array[1]]
        elif(np.max(ne_array)>1e20): # two solutions
            if line_ratio1_target < 0.37: # high density solution
                initial_guess = [ne_interp_array[-10], Te_interp_array[len(Te_interp_array) // 2]]
            else:
                initial_guess = [ne_interp_array[len(ne_interp_array) // 4], Te_interp_array[len(Te_interp_array) // 2]]
        else:
            if line_ratio2_target > 0.3 or line_ratio1_target < 0.25:
                initial_guess = [ne_interp_array[0], Te_interp_array[0]]
            elif line_ratio2_target > 0.3 and line_ratio1_target > 0.25:
                initial_guess = [ne_interp_array[0], Te_interp_array[len(Te_interp_array) // 2]]
            else:
                initial_guess = [ne_interp_array[len(ne_interp_array) // 2], Te_interp_array[len(Te_interp_array) // 2]]
        
    if element == "He":
        #if line_ratio1_target >5 and line_ratio2_target >5:
        #    initial_guess = [ne_interp_array[len(ne_interp_array) // 2], Te_interp_array[len(Te_interp_array) // 2]]
        if  line_ratio1_target > 5:# and line_ratio2_target > 1:
            initial_guess = [ne_interp_array[len(ne_interp_array) // 8], Te_interp_array[10]]
        
        #elif line_ratio1_target <8 and line_ratio2_target > 6:
        #    initial_guess = [ne_interp_array[10], Te_interp_array[-10]]
        else: # ratio1 < 6, ratio2 < 5
            initial_guess = [ne_interp_array[1], Te_interp_array[1]]
    # Numerical solution
    solution = root(equations, initial_guess)
    
    if solution.success:
        ne_solution = 10**solution.x[0] if ne_scale == "log" else solution.x[0]
        Te_solution = 10**solution.x[1] if Te_scale == "log" else solution.x[1]
        return ne_solution, Te_solution
    else:
        raise ValueError("No solution found")

if False:
    # this was my try to make this more stable using the help of chatgpt. It is quite unstable still and slow. So we are rolling back and come back to this maybe at some later point
    from scipy.optimize import minimize, differential_evolution
    from scipy.interpolate import RegularGridInterpolator
    from scipy.ndimage import gaussian_filter
    import numpy as np

    def get_ne_Te_from_tapete(ne_array, Te_array, line_ratio1_array, line_ratio2_array, 
                            line_ratio1_target, line_ratio2_target, 
                            ne_scale="linear", Te_scale="linear", element="Ne"):
        # Convert ne and Te arrays to logarithmic scale if necessary
        if ne_scale == "log":
            ne_interp_array = np.log10(ne_array)
        else:
            ne_interp_array = ne_array

        if Te_scale == "log":
            Te_interp_array = np.log10(Te_array)
        else:
            Te_interp_array = Te_array

        # Apply smoothing to line ratio arrays
        line_ratio1_array_smooth = gaussian_filter(line_ratio1_array, sigma=1)
        line_ratio2_array_smooth = gaussian_filter(line_ratio2_array, sigma=1)

        # Create interpolators for the smoothed line ratios
        interp_line_ratio1 = RegularGridInterpolator(
            (ne_interp_array, Te_interp_array), line_ratio1_array_smooth, method="linear"
        )
        interp_line_ratio2 = RegularGridInterpolator(
            (ne_interp_array, Te_interp_array), line_ratio2_array_smooth, method="linear"
        )

        # Define the cost function for optimization
        def cost_function(x):
            ne, Te = x
            try:
                ratio1_diff = (
                    interp_line_ratio1((ne, Te)) - line_ratio1_target
                ) ** 2
                ratio2_diff = (
                    interp_line_ratio2((ne, Te)) - line_ratio2_target
                ) ** 2
            except ValueError:  # Out of interpolation bounds
                return np.inf
            return ratio1_diff + ratio2_diff

        # Set bounds for the optimization based on the element and problem constraints
        bounds = [(ne_interp_array[0], ne_interp_array[-1]), 
                (Te_interp_array[0], Te_interp_array[-1])]

        # Hybrid Initialization: Coarse grid search for the best initial guess
        ne_grid = np.linspace(ne_interp_array[0], ne_interp_array[-1], 10)
        Te_grid = np.linspace(Te_interp_array[0], Te_interp_array[-1], 10)
        best_guess = None
        min_cost = float('inf')

        for ne in ne_grid:
            for Te in Te_grid:
                cost = cost_function([ne, Te])
                if cost < min_cost:
                    min_cost = cost
                    best_guess = [ne, Te]

        # Global Optimization: Use differential_evolution for robustness
        global_result = differential_evolution(cost_function, bounds, tol=1e-10)

        # Refine the solution using local optimization starting from the best guess
        local_result = minimize(
            cost_function, global_result.x if global_result.success else best_guess, 
            bounds=bounds, tol=1e-10
        )

        # Extract the solution if optimization was successful
        final_result = local_result if local_result.success else global_result

        if final_result.success:
            ne_solution = 10**final_result.x[0] if ne_scale == "log" else final_result.x[0]
            Te_solution = 10**final_result.x[1] if Te_scale == "log" else final_result.x[1]
            return ne_solution, Te_solution
        else:
            raise ValueError(f"Optimization failed to find a solution. Reason: {final_result.message}")



def get_ne_Te_nNe_from_tapete(ne_array, Te_array, line_intensity_array, line_intensity1, line_intensity2,line_intensity3,ne_scale="log",Te_scale="linear"):
        line_ratio1_target = line_intensity1/line_intensity2
        line_ratio2_target = line_intensity2/line_intensity3
        line_ratio1_array = line_intensity_array[0]/line_intensity_array[1]
        line_ratio2_array = line_intensity_array[1]/line_intensity_array[2]
        ne, Te = get_ne_Te_from_tapete(ne_array, Te_array, line_ratio1_array, line_ratio2_array, line_ratio1_target, line_ratio2_target,ne_scale,Te_scale)
        # I want to use the strongest line, but all lines should give the same value
        # todo: is this a problem, when my grid is log?
        interpolated_line_intensity_function = RegularGridInterpolator(
                (ne_array, Te_array),line_intensity_array[2])  # I want this function to have same dimension as always: f(n,T)
        nNe =   line_intensity3/(interpolated_line_intensity_function((ne,Te))*ne*0.1)*(4*np.pi)
        return ne,Te,nNe
        
def get_lomanowski_conversion_coefficients(lines):
    """This data is actually derived for deuterium, so hydrogen could be incorrect here.
    Paschen is also available, but at this point I am not too interested in it
    """
    lomanowski_data = {}
    lomanowski_data["H_alpha"] = {"p":656.279,"C":3.71e-18,"a":0.7665,"b":0.064}
    lomanowski_data["H_beta"] = {"p":486.135,"C":8.425e-18,"a":0.7803,"b":0.050}
    lomanowski_data["H_gamma"] = {"p":434.047,"C":1.31e-15,"a":0.6796,"b":0.03}
    lomanowski_data["H_delta"] = {"p":410.1734,"C":3.954e-16,"a":0.7149,"b":0.028}
    lomanowski_data["H_epsilon"] = {"p":397.007,"C":6.258e-16,"a":0.712,"b":0.029}
    lomanowski_data["H_zeta"] = {"p":388.906,"C":7.378e-16,"a":0.7159,"b":0.032}
    lomanowski_data["H_eta"] = {"p":383.54,"C":8.947e-16,"a":0.7177,"b":0.033}
    return_data = {}
    for line in lines:
        return_data[line]=lomanowski_data[line]
    return return_data
def get_mass_of_element_from_tag(tag):
    ions = ["VIII", "VII", "VI",  "IV","V", "III", "II", "I"]
    for ion in ions:
        if ion in tag:
            element_symbol = tag.split(ion)[0]
            element = periodictable.elements.symbol(element_symbol)  # Get the element
            element_mass = element.mass
            return element_mass
def get_mass_of_element(element):
    element = periodictable.elements.symbol(element)  # Get the element
    element_mass = element.mass
    return element_mass