"""
class to produce and apply wavelength calibrations
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json
import codecs
import copy
from scipy.optimize import curve_fit
import importlib
import logging
logger = logging.getLogger(__name__)
from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import analysis
from divertor_spectroscopy import instrument
importlib.reload(analysis)
importlib.reload(instrument)

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]

def calibrate_spectrometer_using_datasource_qss_class(ds_qss,model,wavelength_coefficients_guess,
                                                      lower_bounds_wl,upper_bounds_wl,wcal,
                                                      signal_type,instrument_function_mode="gauss",
                                                      mustache_correction=False,save_wavelength_calibration=False,
                                                      comment=None,fit_coefficients=False,
                                                      exclude_saturation_in_wl_calibration=True,c_idx_fixed=None,
                                                      xlim=None,save_path=None,special_tags=[],intensity_label="Intensity [a.u.]",debug=False):
    if type(ds_qss.time_id) == str:
        t_idx_start = utils.val2idx(ds_qss.data["time_s"],0)
        t_idx_end = utils.val2idx(ds_qss.data["time_s"],ds_qss.experiment_length)
    else:
        t_idx_start = 0
        t_idx_end = -1
    t_slice = slice(t_idx_start,t_idx_end)
    if not wcal: # if wcal: the wavelength calibration is read from a file
        if comment is None:
            comment = "OP2.2_gauss"
        highlight_tags = model.all_tags
        active_spectrum = False
        if exclude_saturation_in_wl_calibration:
            spectrum_saturated_frames_excluded = copy.deepcopy(ds_qss.data[signal_type][t_slice])
            for t_idx in range(len(spectrum_saturated_frames_excluded)):
                for c_idx in range(len(spectrum_saturated_frames_excluded[0])):
                    if np.nanmax(ds_qss.data["signal_counts_original"][t_slice][t_idx,c_idx])>=0.9*ds_qss.saturation:
                        logger.debug("Excluding frame {} of {}".format(t_idx,c_idx))
                        spectrum_saturated_frames_excluded[t_idx,c_idx] = spectrum_saturated_frames_excluded[t_idx,c_idx]*np.nan
            calibration_spectrum = np.nanmean(spectrum_saturated_frames_excluded,0)
        else:
            calibration_spectrum = np.nanmean(ds_qss.data[signal_type][t_slice],0)
        if active_spectrum:
            calibration_spectrum -= np.nanmean(ds_qss.data[signal_type][t_slice.start-3:t_slice.start],0)
        title_box = "Pid: {}\nCamid: {}\nPort: {}\nTime: {:.1f} to {:.1f}s".format(ds_qss.pid, ds_qss.camid, ds_qss.port, ds_qss.data["time_s"][t_idx_start], ds_qss.data["time_s"][t_idx_end])
        ds_qss.data["wavelength_nm"]=calibrate_spectrometer_using_analysis_class(
        spectrum=calibration_spectrum,wavelength_coefficients_guess=wavelength_coefficients_guess,
        lower_bounds=lower_bounds_wl,mustache_correction=mustache_correction,instrument_function=ds_qss.instrument_function,
        instrument_function_mode=instrument_function_mode,numeric_instrument_function=ds_qss.numeric_instrument_function,
        comment=comment,highlight_tags=highlight_tags,upper_bounds=upper_bounds_wl,model=model,camid=ds_qss.camid,
        central_wavelength=ds_qss.central_wavelength,save=save_wavelength_calibration,fit_coefficients=fit_coefficients,
        broken_channels=ds_qss.broken_channels,campaign=ds_qss.campaign,save_path=save_path,intensity_label=intensity_label,title_box=title_box,
        special_tags=special_tags,grating_number=ds_qss.grating_number,debug=debug)
    else:
        if c_idx_fixed is None:
            correction_channel = np.nanargmax(np.nanmean(
                        ds_qss.data[signal_type], (0, 2)))
        else:
            correction_channel = c_idx_fixed
        if exclude_saturation_in_wl_calibration:
            spectrum_saturated_frames_excluded = copy.deepcopy(ds_qss.data[signal_type][t_slice])
            for t_idx in range(len(spectrum_saturated_frames_excluded)):
                for c_idx in range(len(spectrum_saturated_frames_excluded[0])):
                    if np.nanmax(ds_qss.data["signal_counts_original"][t_slice][t_idx,c_idx])>=0.9*ds_qss.saturation:
                        logger.debug("Excluding frame {} of {}".format(t_idx,c_idx))
                        spectrum_saturated_frames_excluded[t_idx,c_idx] = spectrum_saturated_frames_excluded[t_idx,c_idx]*np.nan
            calibration_spectrum = np.nanmean(spectrum_saturated_frames_excluded,0)
        else:
            calibration_spectrum = np.nanmean(ds_qss.data[signal_type][t_slice],0)
        wavelength = ds_qss.data["wavelength_nm"][correction_channel]
        width = ds_qss.instrument_function[correction_channel]*(np.abs(wavelength[0]-wavelength[-1])/len(wavelength))
        if instrument_function_mode == "gauss":
            numeric_instrument_function = None
        else:
            numeric_instrument_function = ds_qss.numeric_instrument_function[correction_channel]
        lower_bounds_wl = [-0.1]
        upper_bounds_wl = [0.1]
        if ds_qss.camid[:5] == "IP320":
            lower_bounds_wl = [-0.2]
            upper_bounds_wl = [0.2]
        if ds_qss.camid[:5] == "IP160":
            lower_bounds_wl = [-1.5]
            upper_bounds_wl = [1.5]
        inst = instrument.Instrument(instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function,wavelength=wavelength,shift_guess=0,lower_bounds=lower_bounds_wl,upper_bounds=upper_bounds_wl,width=width)
        analyse = analysis.Analysis(model=model,instrument=inst)
        title = "{} {} Wavelength correction using channel {}".format(ds_qss.pid,ds_qss.camid,correction_channel)
        title = None
        ncol = 2
        show_fit = False
        show_background = False
        show_guess = False
        title_box = "Pid: {}\nCamid: {}\nPort: {}\nTime: {:.1f} to {:.1f}s\nChannel: {}".format(ds_qss.pid, ds_qss.camid, ds_qss.port, ds_qss.data["time_s"][t_idx_start], ds_qss.data["time_s"][t_idx_end], correction_channel)
        title_box_placement = "upper left"
        legend_loc = "upper right"
        analyse.fit_spectrum(experiment_data=calibration_spectrum[correction_channel],mode="wavelength_shift",exclude_wavelength_from_fit=model.exclude_wavelength_from_fit)
        if debug:
            highlight_tags = model.all_tags
            analyse.display_spectrum(title_box=title_box,ncol=2,background_guess=show_background,guess=show_guess,
                                     fit=show_fit,title_box_placement=title_box_placement,legend_loc=legend_loc,title=title,
                                     show_positions=False,xlim=xlim,save_path=save_path,highlight_tags=highlight_tags,special_tags=special_tags,ylabel=intensity_label)
        wavelength_correction = analyse.fit_parameters[0]
        ds_qss.data["wavelength_nm"] = ds_qss.data["wavelength_nm"] - wavelength_correction
        ds_qss.wl_calibration_fit_parameters = analyse.fit_parameters
        ds_qss.wl_calibration_fit_description = analyse.fit_description
        logger.info("Wavelength is corrected using channel {}, result {} ".format(str(correction_channel),wavelength_correction))

def calibrate_wavelength_using_analysis_class(spectrum,model,instrument,special_tags=[],highlight_tags=[],title=None,xlim=None,intensity_label="Intensity [a.u.]",title_box=None,save_path=None,debug=False):
    analyse = analysis.Analysis(model=model,instrument=instrument)
    analyse.fit_spectrum(spectrum,mode="wavelength_calibration",exclude_wavelength_from_fit=model.exclude_wavelength_from_fit)
    if debug:
            analyse.display_spectrum(guess=True,highlight_tags=highlight_tags,special_tags=special_tags,title=title,show_positions=True,xlim=xlim,ylabel=intensity_label,save_path=save_path,title_box=title_box,legend_loc="upper right")
            analyse.format_fit_output()
            display(analyse.fit_output)
    return analyse.fit_parameters[:len(analyse.instrument.wavelength_coefficients_guess)]

def calibrate_spectrometer_using_analysis_class(spectrum,model,wavelength_coefficients_guess,lower_bounds,upper_bounds,broken_channels=[],
                                                width_input=None,fit_coefficients=False,instrument_function=None,instrument_function_mode="gauss",
                                                numeric_instrument_function=None,mustache_correction=False,debug=False,highlight_tags=[],special_tags=[],
                                                save=False,comment=None,campaign="OP2.1",grating_number=None,central_wavelength=None,camid=None,intensity_label="Intensity [a.u.]",title_box=None,save_path=None):
    if mustache_correction:
        strongest_line = model.strongest_line
        mustache_correction = guess_mustache(spectrum=spectrum,broken_channels=broken_channels,strongest_line=strongest_line,wavelength_coefficients_guess=wavelength_coefficients_guess,debug=debug)
    order = len(wavelength_coefficients_guess)-1
    number_channels = len(spectrum)
    number_pixels = len(spectrum[0])
    coefficient_matrix = np.ones(
        (number_channels, order+1)) * np.nan
    for channel in range(number_channels):
        if channel in broken_channels:
            if debug:
                logger.info("Skipping broken channel "+str(channel))
        else:
            x = np.linspace(-511.5, 511.5, 1024)
            wavelength_guessed = utils.polynominal(x,*wavelength_coefficients_guess)
            if width_input is None:
                width = instrument_function[channel]*(np.abs(wavelength_guessed[0]-wavelength_guessed[-1]))/len(wavelength_guessed) 
            if numeric_instrument_function is not None:
                numeric_instrument_function_channel = numeric_instrument_function[channel]
            else:
                numeric_instrument_function_channel = None
            wavelength_coefficients_guess_input = copy.deepcopy(wavelength_coefficients_guess)
            if mustache_correction:
                wavelength_coefficients_guess_input[0] = wavelength_coefficients_guess_input[0]+mustache_correction[channel]
            inst = instrument.Instrument(instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function_channel,width=width,wavelength_coefficients_guess=wavelength_coefficients_guess_input,lower_bounds=lower_bounds,upper_bounds=upper_bounds)
            if save_path is None:
                save_path = None
            else: 
                save_path=str(save_path)+"wavelength_calibration_channel_"+str(channel)
            coefficient_matrix[channel]=calibrate_wavelength_using_analysis_class(spectrum=spectrum[channel],model=model,instrument=inst,highlight_tags=highlight_tags,special_tags=special_tags,title_box=title_box+"\nChannel: {}".format(channel),
                                                                                  save_path=save_path,intensity_label=intensity_label,debug=debug)
    if fit_coefficients:
        polynom_coefficients = np.zeros((order+1,3))*np.nan
        fitted_coefficients = np.zeros((number_channels,order+1))*np.nan
        
        for i in range(order+1):
            if i == 0: # only the 0th coefficient makes sense to fit. The others should be constant over the chip!
                guess = [np.nanmean(coefficient_matrix[:,i]),0,0]
                valid = np.isfinite(coefficient_matrix[:,i])
                polynom_coefficients[i] = curve_fit(utils.polynominal,np.arange(number_channels)[valid],coefficient_matrix[:,i][valid],p0=guess)[0]
                fitted_coefficients[:,i] = utils.polynominal(np.arange(number_channels),*polynom_coefficients[i]) 
            else:
                fitted_coefficients[:,i] = np.nanmean(coefficient_matrix[:,i])*np.ones(number_channels)
    wavelength = np.zeros((number_channels,number_pixels))
    
    if fit_coefficients:
        for channel in range(number_channels):
            wavelength[channel] = utils.polynominal(
                x, *fitted_coefficients[channel])
    else:
        for channel in range(number_channels):
            wavelength[channel] = utils.polynominal(
                x, *coefficient_matrix[channel])
    if save:
        path = ground_path + "qss_analysis/json_files/wavelength_calibration_coefficients_{}.json".format(campaign)
        with open(path) as f:
            wavelength_calibration_coefficients = json.load(f)
        if not camid in wavelength_calibration_coefficients.keys():
            wavelength_calibration_coefficients[camid] = {}
        if not str(grating_number) in wavelength_calibration_coefficients[camid].keys():
            wavelength_calibration_coefficients[camid][str(grating_number)] = {}
        if not str(central_wavelength) in wavelength_calibration_coefficients[camid][str(grating_number)].keys():
            wavelength_calibration_coefficients[camid][str(grating_number)][str(central_wavelength)] = {
            }
        if not comment in wavelength_calibration_coefficients[camid][str(grating_number)][str(central_wavelength)].keys():
            wavelength_calibration_coefficients[camid][str(grating_number)][str(central_wavelength)][comment] = {
            }
        if fit_coefficients:
            wavelength_calibration_coefficients[camid][str(grating_number)][str(
            central_wavelength)][comment][str(order)] = fitted_coefficients
        else:
            wavelength_calibration_coefficients[camid][str(grating_number)][str(
            central_wavelength)][comment][str(order)] = coefficient_matrix
        json.dump(wavelength_calibration_coefficients, codecs.open(path, 'w', encoding='utf-8'),
                  cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)
    if debug:
        fig, axs = plt.subplots(
            1, len(wavelength_coefficients_guess), figsize=(10, 4))
        for i in range(len(wavelength_coefficients_guess)):
            axs[i].plot(coefficient_matrix[:, i], "+-",label="Result from fitted spectrum")
            if fit_coefficients:
                axs[i].plot(fitted_coefficients[:,i],"+-",label="Fit coefficients over camera")
            axs[i].set_title("coefficient "+str(i))
            axs[i].set_xlabel("Channel")
        plt.legend()
        plt.tight_layout()
        plt.show()
    return wavelength
    
def guess_mustache(spectrum,wavelength_coefficients_guess,strongest_line,max_shift_pixel=50,broken_channels=[],debug=True):
    """_summary_

    Args:
        spectrum (_type_): _description_
        wavelength_coefficients_guess (_type_): _description_
        strongest_line (_type_): _description_
        broken_channels (list, optional): _description_. Defaults to [].
        debug (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """
    mustache = []
    x = np.linspace(-511.5,511.5,1024)
    guessed_wavelength = utils.polynominal(x,*wavelength_coefficients_guess)
    strongest_line_idx = utils.val2idx(strongest_line,guessed_wavelength)
    spectrum_copy = copy.deepcopy(spectrum)
    for channel in range(len(spectrum)):
        if channel in broken_channels:
            mustache.append(np.nan)
        else:
            spectrum_copy[channel][:strongest_line_idx-max_shift_pixel]=spectrum_copy[channel][:strongest_line_idx-max_shift_pixel]*np.nan
            spectrum_copy[channel][strongest_line_idx+max_shift_pixel:]=spectrum_copy[channel][strongest_line_idx+max_shift_pixel:]*np.nan
            mustache.append(strongest_line-guessed_wavelength[np.nanargmax(spectrum_copy[channel])])
    if debug:
        plt.figure(figsize=(8,6))
        plt.plot(mustache,"+-")
        plt.ylabel("Offset [nm]")
        plt.xlabel("Channel")
        plt.title("Mustache effect - correct for wavelength coef. guess")
        plt.show()
    return mustache
    
def get_wavelength(comment,camid="IP320_2", central_wavelength=405,order=2,grating_number=0, pixelcount=1024,campaign="OP2.1",debug=False):
    """
    method to apply the wavelength calibration. Reviewed wavelength calibration coefficients are saved in a json file "wavelength_calibration_coefficients.json"
    """
    path = ground_path + "qss_analysis/json_files/wavelength_calibration_coefficients_{}.json".format(campaign)
    with open(path) as f:
        wavelength_calibration_coefficients = json.load(f)
    x = np.linspace(-511.5, 511.5, 1024)
    number_channels = len(wavelength_calibration_coefficients[camid][str(grating_number)][str(
                (central_wavelength))][comment][str(order)])
    wavelength = np.zeros((number_channels, 1024)) * np.nan
    for c in range(number_channels):
        wavelength[c] = np.array(utils.polynominal(
            x, *wavelength_calibration_coefficients[camid][str(grating_number)][str(
                (central_wavelength))][comment][str(order)][c]))
    return wavelength

def get_wavelength_coefficient_matrix(comment,camid="IP320_2",grating_number=0, central_wavelength=405, order=2, pixelcount=1024):
    """
    Reviewed wavelength calibration coefficients are saved in a json file "wavelength_calibration_coefficients.json"
    """
    path = ground_path + "qss_analysis/json_files/wavelength_calibration_coefficients.json"
    with open(path) as f:
        wavelength_coefficient_matrix = json.load(f)
    return np.array(wavelength_coefficient_matrix[camid][str(grating_number)][str(
                (central_wavelength))][comment][str(order)])
    
def get_dispersion_sopra(central_wavelength):
        """The wavelength of the sopra is often difficult to 
        to calculate from a fit as there is sometimes only one line in the spectrum
        
        This is now the theoretical calculation of SopraWega during OP1.2b
        """
        F=1150                     #focal length in mm
        epsilon=6.5*np.pi/180        #Ebert angle in radians
        N=320                      #groove frequency in gr./mm
        lambda0=5570               #fundamental wavelength of the system in nm
        widthgrating=220           #width of the grating in mm    
        slitheight=20              #height of slit in mm
        
        #calculate the spectral order k:
        k=np.round(lambda0/central_wavelength)
        #calculate the angle between the grating normal & central axis of the
        #mirror in radians
        i=np.arcsin(k*N*central_wavelength*1e-6/(2*np.cos(epsilon)))
        
        #calculate the dispersion of the Sopra WEGA in nm/pix:
        dispersion=(central_wavelength)/(2*F*np.tan(i)*1e3/12.2)
        return dispersion
def linear_dispersion_manual(pixels,wavelengths):
    dispersion = (wavelengths[1]-wavelengths[0])/(pixels[1]-pixels[0])
    return dispersion