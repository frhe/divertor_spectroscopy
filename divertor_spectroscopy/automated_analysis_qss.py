
"""The idea is to get away from jupyter notebooks for analysis
The analysis is supposed to be more automated
Notebooks will then be used to visualize some of the results
"""

### for automated analysis some things need to be taken into account:

import numpy as np
import matplotlib.pyplot as plt
import json
import codecs
import copy
import importlib
from scipy import constants
from tqdm import tqdm
from functools import partial
from multiprocess import Pool
import scipy.io as sio
import logging
logger = logging.getLogger(__name__)

# todo: do I need to catch these errors here? The entire thing is not functional anyways without these imports - 
import archivedb
import w7x_overviewplot.query_programs as qp
import w7x_overviewplot.vmec as vmec
import w7x_overviewplot.logbookapi as LB
import w7x_overviewplot._datasources._datasources_cdx as cdx
import w7x_overviewplot.w7x_overviewplot
import w7xdia.ecrh

from divertor_spectroscopy import utilities as utils
from divertor_spectroscopy import datasource_qss
from divertor_spectroscopy import wavelength_calibration
from divertor_spectroscopy import analysis
from divertor_spectroscopy import instrument
from divertor_spectroscopy import spectrum

import os
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(__location__+"/.."+"/settings.json") as f:
    settings = json.load(f)
ground_path = settings["ground_path"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
colors = colors + colors[1:]
class Analysis_qss():
    def __init__(self):
        pass
    def analyse_shot(self,pid,camid,experiment_length=None,central_wavelength=None,average=200,broken_channels=None,
                     mode="full_fit",c_idx_fixed=None,version="0",numeric_instrument_function=False,use_off_center_numeric_instrument_function=None,ical=True,
                     new_wl_calibration="linear_correction",instrument_function_mode="gauss",
                     zeeman=False,stark_density=False,extra="",extra_wcal="",exclude_saturation_in_wl_calibration=True,
                     exclude_from_spectrum=None,wcal_comment=None,debug=False,show=False,xlim=None,doppler_temperature=None,correct_smearing=False):
        """fit an entire shot using default settings

        Args:
            pid (str): program id 
            camid (str): spectrometer name
            average (int, optional): time in ms you want to average over . Defaults to 200.
            version (str,optional): version 0: no saving, everything else is just attached to every saved file
            debug (bool, optional): debugging option. Defaults to False.
        """
        if pid[:4] == "2022" or pid[:4] == "2023":
            campaign = "OP2.1"
        if pid[:4] == "2018":
            campaign = "OP1.2b"
        if pid[:4] == "2024":
            campaign = "OP2.2"
        path = ground_path + "qss_analysis/discharges/{}/{}/{}".format(campaign,pid[:8],pid)
        print(path)
        utils.create_folder(path+"/Plots")
        # read settings file for shot
        with open(path+"/settings_{}_version{}.json".format(average,version)) as f:
            self.settings_discharge = json.load(f)
        self.discharge_length = self.settings_discharge["discharge_length_round_up"]
        self.time_s = self.settings_discharge["time_s"]
        self.config =self.settings_discharge["config"]
        if experiment_length is not None:
            self.discharge_length = experiment_length
            points_per_second = 1000/average
            self.time_s = np.linspace(0,experiment_length-average/1000,int(experiment_length*points_per_second))
       
        # loading qss data - the automated analysis is only planned to be run with intensity and wavelength calibrated data
        
        # broken channels - this should at some point be read from lightpath database - todo
        if ical:
            signal_type = "signal_photons"
        else:
            signal_type = "signal_counts_per_second"
        if new_wl_calibration == "full_calibration":
            wcal = False
        else:
            wcal = True
        self.ds_qss = datasource_qss.Datasource_qss(time_id=pid,camid=camid,central_wavelength=central_wavelength,wcal=wcal,broken_channels=broken_channels,ical=ical,
                                                    instrument_cal=True,numeric_instrument_function=numeric_instrument_function,use_off_center_numeric_instrument_function=use_off_center_numeric_instrument_function,wcal_comment=wcal_comment,
                                                    experiment_length=self.discharge_length,campaign=campaign,software_binning=True,software_binning_shift=16,correct_smearing=correct_smearing,debug=debug)
        self.port = self.ds_qss.port
        number_channels = len(self.ds_qss.data["signal_counts"][0])
        if exclude_from_spectrum is not None:
            for t_idx in range(len(self.ds_qss.data["time_s"])):
                for c_idx in range(number_channels):
                    self.ds_qss.data[signal_type][t_idx,c_idx,exclude_from_spectrum] = self.ds_qss.data[signal_type][t_idx,c_idx,exclude_from_spectrum]*np.nan
        if central_wavelength is None:
            try:
                if campaign == "OP1.2b":
                    self.central_wavelength = int(np.round(self.ds_qss.metadata["Central_Wavelength"]))
                else:
                    self.central_wavelength = int(np.round(self.ds_qss.metadata["centralWavelength"]))
            except:
                self.central_wavelength = np.nan
            if self.central_wavelength is None or np.isnan(self.central_wavelength):
                if camid == "IP160_4":
                    self.central_wavelength = 710
                if camid == "IP160_2":
                    self.central_wavelength = 711
        else:
            self.central_wavelength = central_wavelength
        print(central_wavelength,self.central_wavelength)
        self.t_idx_0 = utils.val2idx(self.ds_qss.data["time_s"],0)
        self.t_idx_end = utils.val2idx(self.ds_qss.data["time_s"],self.discharge_length)
        self.number_channels = len(self.ds_qss.data[signal_type][0])
        if self.ds_qss.data["time_s"][-1]<self.discharge_length:
            points_per_second = 1000/average
            self.qss_acquisition_length = np.floor(self.ds_qss.data["time_s"][-1]*points_per_second)/points_per_second
            self.time_s = np.linspace(0,self.qss_acquisition_length-average/1000,int(self.qss_acquisition_length*points_per_second))
            if debug:
                logging.warning("QSS acquisition {} is not long enough compared to discharge length {}".format(self.ds_qss.data["time_s"][-1],self.time_s[-1]))
        else:
            self.qss_acquisition_length = self.discharge_length    
        #load model: 
        
        if extra_wcal is not "":
            kwargs = {"extra":extra_wcal,"zeeman":zeeman,"stark_density":stark_density}
            self.model = spectrum.load_model(camid,self.central_wavelength,**kwargs)
        elif self.central_wavelength == 405:
            kwargs = {"extra":extra_wcal,"zeeman":zeeman,"stark_density":stark_density,"grating":self.ds_qss.grating}
            self.model = spectrum.load_model(camid,self.central_wavelength,**kwargs)
        else:
            kwargs = {"extra":extra_wcal,"zeeman":zeeman,"stark_density":stark_density}
            self.model = spectrum.load_model(camid,self.central_wavelength,**kwargs)
        
        # wcal 
        wavelength_calibration.calibrate_spectrometer_using_datasource_qss_class(
            instrument_function_mode=instrument_function_mode,ds_qss=self.ds_qss,
            model=self.model,
            wavelength_coefficients_guess=self.ds_qss.wavelength_coefficients_guess,lower_bounds_wl=self.ds_qss.lower_bounds_wl,
            upper_bounds_wl=self.ds_qss.upper_bounds_wl,mustache_correction=False,wcal=wcal,signal_type=signal_type,
            save_wavelength_calibration=False,comment=wcal_comment,fit_coefficients=False,intensity_label=r"Intensity [Photons/($\mathrm{m}^2$ nm sr s)]",
            exclude_saturation_in_wl_calibration=exclude_saturation_in_wl_calibration,debug=debug,xlim=xlim,
            c_idx_fixed=c_idx_fixed,save_path=path+"/Plots/wavelength_shift_{}_{}".format(camid,str(self.central_wavelength)))

        if self.central_wavelength == 405:
            kwargs = {"extra":extra,"zeeman":zeeman,"stark_density":stark_density,"grating":self.ds_qss.grating}
            self.model = spectrum.load_model(camid,self.central_wavelength,**kwargs)
        else:
            kwargs = {"extra":extra,"zeeman":zeeman,"stark_density":stark_density}
            self.model = spectrum.load_model(camid,self.central_wavelength,**kwargs)
        if doppler_temperature is not None:
            self.model.balmer_lines[0].doppler_temperature = doppler_temperature
            self.model.get_all_model_parameters()
        # automated analysis of the discharge 
        # average data to given time interval:
        self.analysed_spectrum = np.zeros((len(self.time_s),self.number_channels,1024))*np.nan
        for c_idx in range(self.number_channels):
            self.analysed_spectrum[:,c_idx] = utils.average_signal_on_time_array(new_time_array=self.time_s,old_time_array=self.ds_qss.data["time_s"],old_signal=self.ds_qss.data[signal_type][:,c_idx],axis=0)
            #print(self.analysed_spectrum[:c_idx])
        tomography_uncertainty = np.ones(self.number_channels)*0.05
        self.broken_channels = self.ds_qss.broken_channels
        self.uncertain_channels = self.ds_qss.uncertain_channels
        for c in range(self.number_channels):
            if c in self.uncertain_channels:
                tomography_uncertainty[c] = 0.1
            if c in self.broken_channels:
                tomography_uncertainty[c] = 1
        
        if mode == "brightest_channel":
            if "AEI" in self.port:
                if self.config[0] == "E" or self.config[0] == "D":
                    strikelines = ["horizontal"]
                else:
                    strikelines = ["no_strikeline"]
            else:
                strikelines = []
                if self.config[0] == "E":
                    strikelines = ["horizontal","vertical"]
                if self.config[0] == "K":
                    strikelines = ["vertical"]
                if self.config[0] == "D":
                    strikelines = ["horizontal"]
                if self.config[0] == "F":
                    strikelines = ["no_strikeline"]
            exclude_channels = {}
            exclude_channels["horizontal"] = np.arange(16,27)
            exclude_channels["vertical"] = np.arange(0,16)
            exclude_channels["no_strikeline"] = np.array([])
            ls = {"horizontal":"-","vertical":"--","no_strikeline":"-"}
            output = {}
            parallel = True
            for strikeline in strikelines:
                if not parallel:
                    output[strikeline] = analysis.fit_discharge_brightest_channel(instrument_function_mode=instrument_function_mode,numeric_instrument_function=self.ds_qss.numeric_instrument_function,
                                                        debug=debug,highlight_tags=self.model.all_tags,model=self.model,ds_qss=self.ds_qss,pid=pid,camid=camid,port=self.port,
                                                        special_tags=[],xlim=[409.5,410.5],intensity_label=None,scale="linear",exclude_channels=exclude_channels,strikeline=strikeline,
                                                        strongest_line=self.model.strongest_line,signal_type="signal_photons",analysis_start=0,c_idx_fixed=c_idx_fixed,
                                                        analysis_end=self.qss_acquisition_length,analysis_step=average/1000,show=show,save_path=path+"/Plots/") 
                else:
                    # Todo - prallelize this, I don't know if debugging is still possible here - maye is must be seperated from the fitting part
                    output[strikeline] = {}
                    output[strikeline]["time"] = self.time_s
                    output[strikeline]["channels"] = []
                    output[strikeline]["fit_parameters"] = {}
                    max_pool = 16
                    inst = instrument.Instrument(wavelength=self.ds_qss.data["wavelength_nm"][0],width=1) # I don't remember why I absolutely needed this
                    self.analyse = analysis.Analysis(instrument=inst,model=self.model)
                    output[strikeline]["fit_description"] = self.analyse.fit_description
                    self.fit_parameters = np.zeros((len(self.analysed_spectrum), 1+len(self.analyse.fit_description))) * np.nan
                    time_range = range(len(self.analysed_spectrum))
                    logger.debug("start fitting " + pid)
                    with Pool(max_pool) as p:
                            self.pool_outputs = list(
                            tqdm(
                                p.imap(partial(fit_spectrum_brightest_channel,data=self.analysed_spectrum,model=self.model,
                                            wavelength=self.ds_qss.data["wavelength_nm"],instrument_function_mode=instrument_function_mode,
                                            instrument_function=self.ds_qss.instrument_function,
                                            numeric_instrument_function=self.ds_qss.numeric_instrument_function,broken_channels=self.broken_channels,exclude_channels=exclude_channels[strikeline],c_idx_fixed=c_idx_fixed,use_nans=False),
                                                time_range),
                                                total=len(time_range)
                                                )
                                                )
                            self.fit_parameters=np.array(self.pool_outputs)
                    for parameter_idx,parameter in enumerate(output[strikeline]["fit_description"]):
                        output[strikeline]["fit_parameters"][parameter] = self.fit_parameters[:,1+parameter_idx]
                    output[strikeline]["channels"] = self.fit_parameters[:,0]
                    for parameter in output[strikeline]["fit_description"]:
                        output[strikeline]["fit_parameters"][parameter] = np.array(output[strikeline]["fit_parameters"][parameter])
                    output[strikeline]["channels"] = np.array(output[strikeline]["channels"])
                    output[strikeline]["time"] = np.array(output[strikeline]["time"])
            fontsize = 12
            plt.rcParams.update({'font.size': fontsize})
            if "IP320" in camid:
                parameters = ['Stark_density_H_delta', "Intensity_H_delta"]
            if "IP320" in camid and "one_density" in extra:
                parameters = ["Stark_density", "Intensity_H_delta","Intensity_H_epsilon"]
            if camid == "SopraBerlin" or "SopraWega" in camid:
                parameters = ["Stark_density_H_gamma", "Intensity_H_gamma"]
            fig, ax1 = plt.subplots(figsize=(8,6))
            ax2 = ax1.twinx()
            ax3 = ax1.twinx()
            ax3.spines['right'].set_position(('outward', 60))
            for parameter in parameters:
                for strikeline in strikelines:
                    if "density" in parameter:
                        ax1.plot(output[strikeline]["time"],10**output[strikeline]["fit_parameters"][parameter]/1e19,ls=ls[strikeline],marker="+",c=colors[parameters.index(parameter)],label=parameter+" "+strikeline[0] +" [10$^{19}$m$^{-3}$]")
                    if "Intensity" in parameter:
                        ax2.plot(output[strikeline]["time"],10**output[strikeline]["fit_parameters"][parameter],ls=ls[strikeline],marker="+",c=colors[parameters.index(parameter)],label=parameter+" "+strikeline[0])
                    ax3.plot(output[strikeline]["time"],output[strikeline]["channels"],ls=ls[strikeline],marker="+",c="grey",label=strikeline[0]+" Channels")
            ax1.plot(self.settings_discharge["time_s"],np.array(self.settings_discharge["density_interferometer"])/1e19,label="interferometer",color="r")
            try:
                ax1.plot(self.settings_discharge["time_s"],self.settings_discharge["prad_MW"],label="prad [MW]",color="green")
            except:
                logger.warning("PRAD data can't be plotted for some reason")
            try:
                ax1.plot(self.settings_discharge["time_s"],self.settings_discharge["power_ecrh_MW"],label="ecrh [MW]",color="brown")
            except:
                logger.warning("ECRH data can't be plotted for some reason")
            ax1.legend(loc="upper left")
            ax2.legend(loc="upper right")
            ax1.set_xlabel("Time [s]")
            title = pid+" "+camid+" "+self.ds_qss.port+" "+instrument_function_mode[:3]
            ax3.set_ylabel("Channel",color="grey")
            #ax1.set_ylim(0,15)
            plt.title(title)
            plt.tight_layout()
            name = "Fit_of_channels_max_intensity_intensities_"+str(self.model.strongest_line)+"_"+self.ds_qss.port+"_"+str(average)+"ms"+"_"+version
            fig.savefig(path+"/"+name+".png")
            plt.show()
            output["all_model_parameters"] = self.model.all_model_parameters
            json.dump(output, codecs.open(path+"/{}.json".format(name), 'w', encoding='utf-8'),
                        cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)  
        if mode == "fixed_channel_full_discharge_average":
            if self.central_wavelength == 405:
                output = {}
                strikelines = ["horizontal"]
                for strikeline in strikelines:
                    output[strikeline] = {}
                    output[strikeline]["channels"]=[c_idx_fixed]
                    output[strikeline]["time"]=[0]
                    output[strikeline]["fit_parameters"] = self.ds_qss.wl_calibration_fit_parameters
                    output[strikeline]["fit_description"] = self.ds_qss.wl_calibration_fit_description
                
                name = "Fixed_channel_full_discharge_average_intensities_"+port[camid]+"_"+version
                json.dump(output, codecs.open(path+"/{}.json".format(name), 'w', encoding='utf-8'),
                            cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)     
        # parallelized analyis analysis: 
        # used to parallelize channels - should be quicker to parallelize the single fits
        if mode == "full_fit":
            max_pool = 16
            inst = instrument.Instrument(wavelength=self.ds_qss.data["wavelength_nm"][0],width=1) # I don't remember why I absolutely needed this
            self.analyse = analysis.Analysis(instrument=inst,model=self.model)
            self.fit_parameters = np.zeros((len(self.analysed_spectrum), self.number_channels, 1+len(self.analyse.fit_description))) * np.nan
            time_range = range(len(self.analysed_spectrum))
            logger.debug("start fitting " + pid)
            with Pool(max_pool) as p:
                for c_idx in range(self.number_channels):
                    if c_idx in self.broken_channels:
                        logger.debug("Skipping channel because it has no data")
                    else:
                        self.pool_outputs = list(
                        tqdm(
                            p.imap(partial(fit_spectrum,c_idx=c_idx,data=self.analysed_spectrum,model=self.model,
                                           wavelength=self.ds_qss.data["wavelength_nm"][c_idx],instrument_function_mode=instrument_function_mode,
                                           instrument_function=self.ds_qss.instrument_function,
                                           numeric_instrument_function=self.ds_qss.numeric_instrument_function),
                                            time_range),
                                            total=len(time_range)
                                            )
                                            )
                        self.fit_parameters[:,c_idx]=np.array(self.pool_outputs)
            #saving
            if version!="0":
                # create more human readable files for tomography and matlab file for Maciej
                # First for Gabriele and everyone else a json file
                print(self.central_wavelength)
                # create folders:
                if os.path.exists(path+"/images_"+"version"+version):
                    pass
                else:
                    os.mkdir(path+"/images_"+"version"+version+"/")
                if os.path.exists(path+"/images_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version)):
                    pass
                else:
                    os.mkdir(path+"/images_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version))
                    
                if os.path.exists(path+"/matlab_files_"+"version"+version):
                    pass
                else:
                    os.mkdir(path+"/matlab_files_"+"version"+version)
                if os.path.exists(path+"/matlab_files_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version)):
                    pass
                else:
                    os.mkdir(path+"/matlab_files_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version))
                
                self.save_human_readable = {}
                self.save_human_readable["all_model_parameters"]=self.model.all_model_parameters
                self.save_human_readable["camid"] = camid
                self.save_human_readable["pid"] = pid
                self.save_human_readable["config"] = self.config
                self.save_human_readable["port"] = self.port
                self.save_human_readable["discharge_length_s"] = self.discharge_length
                self.save_human_readable["time_s"] = self.time_s
                self.save_human_readable["channels"] = np.arange(number_channels)
                self.save_human_readable["broken_channels"] = self.broken_channels
                self.save_human_readable["metadata"] = self.ds_qss.metadata
                self.analyse.fit_description.append("background_guess_{:.3f}".format(self.model.recommended_background_position))
                self.save_human_readable["fit_description"] = self.analyse.fit_description
                self.save_human_readable["tomography_uncertainty"] = tomography_uncertainty
                self.save_human_readable["fit_parameters_log"] = self.fit_parameters
                self.save_human_readable["fit_parameters"] = {}
                self.save_human_readable["fit_parameters_unit"] = {}
                if ical:
                    self.save_human_readable["fit_parameters_intensity_in_watts"] = {}
                    self.save_human_readable["fit_parameters_intensity_in_watts_unit"] = {}
                for p_idx in range(len(self.analyse.fit_description)):
                    if "Intensity" in self.analyse.fit_description[p_idx]:
                        self.save_human_readable["fit_parameters"][self.analyse.fit_description[p_idx]] = 10**self.fit_parameters[:,:,p_idx]
                        if ical:
                            self.save_human_readable["fit_parameters_unit"][self.analyse.fit_description[p_idx]] = "Photons/s/m2/sr"
                        else:
                            self.save_human_readable["fit_parameters_unit"][self.analyse.fit_description[p_idx]] = "Counts/s"
                    if "background" in self.analyse.fit_description[p_idx]:
                        self.save_human_readable["fit_parameters"][self.analyse.fit_description[p_idx]] = self.fit_parameters[:,:,p_idx]
                        self.save_human_readable["fit_parameters_unit"][self.analyse.fit_description[p_idx]] = "Photons/s/m2/sr"
                    if "Temperature" in self.analyse.fit_description[p_idx] or "temperature" in self.analyse.fit_description[p_idx]:
                        self.save_human_readable["fit_parameters"][self.analyse.fit_description[p_idx]] = self.fit_parameters[:,:,p_idx]
                        self.save_human_readable["fit_parameters_unit"][self.analyse.fit_description[p_idx]] = "eV"
                    if "Density" in self.analyse.fit_description[p_idx] or "density" in self.analyse.fit_description[p_idx]:
                        self.save_human_readable["fit_parameters_unit"][self.analyse.fit_description[p_idx]] = "m-3"
                        self.save_human_readable["fit_parameters"][self.analyse.fit_description[p_idx]] = 10**self.fit_parameters[:,:,p_idx]
                    if ical:#todo: this should not take out so much
                        if "Intensity" in self.analyse.fit_description[p_idx]:
                            try:
                                wavelength = float(self.analyse.fit_description[p_idx][-7:])
                            except:
                                if "epsilon" in self.analyse.fit_description[p_idx]:
                                    wavelength = 397.007
                                if "delta" in self.analyse.fit_description[p_idx]:
                                    wavelength = 410.1734
                            self.save_human_readable["fit_parameters_intensity_in_watts"][self.analyse.fit_description[p_idx]] = 10**self.fit_parameters[:,:,p_idx]*constants.h*constants.c/(wavelength*1e-9)
                            self.save_human_readable["fit_parameters_intensity_in_watts_unit"][self.analyse.fit_description[p_idx]] = "Watt/m2/sr"
                        else:
                            self.save_human_readable["fit_parameters_intensity_in_watts"][self.analyse.fit_description[p_idx]] = 10**self.fit_parameters[:,:,p_idx]
                            if "Temperature" in self.analyse.fit_description[p_idx]:
                                self.save_human_readable["fit_parameters_intensity_in_watts_unit"][self.analyse.fit_description[p_idx]] = "eV"
                            if "Density" in self.analyse.fit_description[p_idx] or "density" in self.analyse.fit_description[p_idx]:
                                self.save_human_readable["fit_parameters_intensity_in_watts_unit"][self.analyse.fit_description[p_idx]] = "m-3"
                    if ical:
                        intens = {}
                        intens["x"] = self.time_s
                        intens["y"] = np.linspace(1,number_channels,number_channels)
                        intens["z"] = self.save_human_readable["fit_parameters_intensity_in_watts"][self.analyse.fit_description[p_idx]]
                        intens["error"] = self.save_human_readable["tomography_uncertainty"] 
                        if False:
                            if "Intensity" in self.analyse.fit_description[p_idx]:
                                sio.savemat(path+"/matlab_files_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version)+'/{} {} {} calib data {} nm_version{}.mat'.format(pid,self.port,camid,self.analyse.fit_description[p_idx][10:],version), intens)
                    #create folder for sharing
                json.dump(self.save_human_readable, codecs.open(path+"/fit_parameters_camid{}_cw{}_port{}_average{}ms_{}.json".format(camid,int(np.round(self.central_wavelength)),self.port,average,"version"+version), 'w', encoding='utf-8'),
                    cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)
            #debugging           
                      
            display_fit_parameters(self.save_human_readable,path=path,central_wavelength=self.central_wavelength,average=average,version=version,debug=debug)
            
def display_fit_parameters(fit_parameters,path=None,central_wavelength=None,average=None,version=None,debug=False,xlim=None):
    #if np.isfinite(np.nanmean(fit_parameters)):
    time = fit_parameters["time_s"]
    for parameter in fit_parameters["fit_description"]:
        fig = plt.figure(figsize=(8,6))                    
        if xlim is not None:
            t_idx_start = utils.val2idx(time,xlim[0])
            t_idx_end = utils.val2idx(time,xlim[1])
            extent = [time[t_idx_start],time[t_idx_end]+time[-1]-time[-2],0.5,len(fit_parameters["fit_parameters"][parameter][0])+.5]
            plt.imshow(fit_parameters["fit_parameters"][parameter][t_idx_start:t_idx_end].T,aspect="auto",origin="lower",interpolation="none",extent=extent)
        else:
            extent = [time[0],time[-1]+time[-1]-time[-2],0.5,len(fit_parameters["fit_parameters"][parameter][0])+.5]
            plt.imshow(fit_parameters["fit_parameters"][parameter].T,aspect="auto",origin="lower",interpolation="none",extent=extent)
        title= parameter+" "+fit_parameters["pid"]+" "+fit_parameters["camid"]+" "+fit_parameters["port"]
        plt.title(title)
        
        plt.xlabel("Time [s]")
        plt.ylabel("Channel index")
        plt.tight_layout()
        #if xlim is not None:
        #    t_idx_start = utils.val2idx(time,xlim[0])
        #    t_idx_end = utils.val2idx(time,xlim[1])
        #    plt.xlim(xlim)
        cbar = plt.colorbar()
        cbar.set_label(fit_parameters["fit_parameters_unit"][parameter], rotation=270,labelpad=15)
        if path is not None:
            plt.savefig(path+"/images_"+"version"+version+"/camid{}_cw{}_port{}_average{}ms_{}".format(fit_parameters["camid"],int(np.round(central_wavelength)),fit_parameters["port"],average,"version"+version)+"/"+title+".png")
        if debug:
            plt.show()
        else:
            plt.close(fig)
def fit_spectrum(t_idx,c_idx,data,model,wavelength,instrument_function_mode,instrument_function,numeric_instrument_function):
        if instrument_function_mode == "gauss":
            numeric_instrument_function = None
        else:
            numeric_instrument_function = numeric_instrument_function[c_idx]
        width = instrument_function[c_idx]*np.abs(wavelength[0]-wavelength[-1])/len(wavelength)
        inst = instrument.Instrument(wavelength=wavelength,width=width,instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function)
        analyse = analysis.Analysis(model=model,instrument=inst)
        try:
            analyse.fit_spectrum(experiment_data=data[t_idx,c_idx],exclude_wavelength_from_fit=model.exclude_wavelength_from_fit)
            fit_parameters = analyse.fit_parameters
            fit_parameters = np.append(fit_parameters,[analyse.background_guess])
        except:
            fit_parameters = list(np.zeros(1+len(analyse.fit_description))*np.nan)
        return fit_parameters
def fit_spectrum_brightest_channel(t_idx,data,model,wavelength,instrument_function_mode,instrument_function,numeric_instrument_function,broken_channels,exclude_channels,c_idx_fixed,use_nans=False):
        experiment_data_all_channels = np.zeros(27)*np.nan
        for c_idx in range(len(wavelength)):
                if c_idx in broken_channels or c_idx in exclude_channels:
                    experiment_data_all_channels[c_idx] = np.nan
                else:
                    w_idx = utils.val2idx(wavelength[c_idx],model.strongest_line)
                    experiment_data_all_channels[c_idx] = np.nanmean(data[t_idx,c_idx,w_idx-10:w_idx+11])
        if c_idx_fixed is None:
            c_idx = np.nanargmax(experiment_data_all_channels)
        else:
            c_idx = c_idx_fixed
        wavelength = wavelength[c_idx]
        if instrument_function_mode == "gauss":
            numeric_instrument_function = None
        else:
            numeric_instrument_function = numeric_instrument_function[c_idx]
        width = instrument_function[c_idx]*np.abs(wavelength[0]-wavelength[-1])/len(wavelength)
        inst = instrument.Instrument(wavelength=wavelength,width=width,instrument_function_mode=instrument_function_mode,numeric_instrument_function=numeric_instrument_function)
        analyse = analysis.Analysis(model=model,instrument=inst)
        try:
            experiment_data = data[t_idx,c_idx]
            if not use_nans:
                if np.isnan(experiment_data).any():
                    experiment_data = experiment_data*np.nan
            analyse.fit_spectrum(experiment_data=experiment_data)
            fit_parameters = list(analyse.fit_parameters)
        except:
            fit_parameters = list(np.zeros(len(analyse.fit_description))*np.nan)
        return np.array([c_idx]+fit_parameters)

def load_model(camid,central_wavelength):
    """Loading the spectrum model depending on the camera type and the wavelength settings

    Args:
        camid (str): camera name - from this finds out the type
        central_wavelength (int): central wavelength of spectrometer
    """
    camid_name_model_class = camid.lower().split("_")[0] # the name how it will be found model clases
    camid_name_model_constructor = ""
    camid_name_model_constructor += camid_name_model_class[0].upper()
    for letter in camid_name_model_class[1:]:
        camid_name_model_constructor += letter
    model_class = importlib.import_module(name = "divertor_spectroscopy.spectrum_models.{}_cw_{}".format(camid_name_model_class,str(central_wavelength)))
    constructor_name = "{}_cw_{}".format(camid_name_model_constructor,str(central_wavelength))
    constructor = getattr(model_class,constructor_name)
    if central_wavelength == 405:
        model = constructor(zeeman=False)
    if central_wavelength == 364:
        model = constructor(zeeman=False)
    if central_wavelength == 462:
        model = constructor(zeeman=True)
    if central_wavelength == 710:
        model = constructor()
    if central_wavelength == 711:
        model = constructor()
    return model
    
def analyse_day(day,camids,start_pid=None,average=200,new_wl_calibration=None,wcal_comment=None,ical=True,central_wavelength=None,correct_smearing=False,
                version="0",debug=False,show=False,mode="full_fit",extra="",extra_wcal="",zeeman=False,
                stark_density=False,instrument_function_mode="gauss",minimum_discharge_length=2,
                numeric_instrument_function=False,exclude_from_spectrum=None,xlim=None,c_idx_fixed=None):
    """write setting file with parameters used for automatic fitting and analysis to folder for every
    discharge of day, which is longer than 3 seconds (my definition for successful)

    Args:
        day (str): day in which format? "2023-03-23"
        debug (bool, optional): debugging option. Defaults to False.
    """
    shot_list = archivedb.get_program_list_for_day(day,timeout=10)
    # Adjusted to check if `start_pid` is in the list of IDs within the dictionaries
    if start_pid is not None and any(shot["id"] == start_pid for shot in shot_list):
        # Find the index of the dictionary where "id" matches `start_pid`
        start_index = next(i for i, shot in enumerate(shot_list) if shot["id"] == start_pid)
        # Slice the list from this index onward
        shot_list = shot_list[start_index:]
    lb = LB.LogbookAPI()
    if day[:4] == "2022" or day[:4] == "2023":
            campaign = "OP2.1"
    if day[:4] == "2018":
            campaign = "OP1.2b"
    if day[:4] == "2024":
        campaign = "OP2.2"
    path = ground_path + "qss_analysis/discharges/{}/".format(campaign)+ day[:4]+day[5:7]+day[8:]
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
    for shot in shot_list:
        # pulse trains should be excluded:
        try:
            info = lb.getprograminfo(program=utils.pid_format_to_logbook(shot["id"]))
            if "train" in info["_source"]["name"].lower():
                logger.debug("Pulse Train")
            elif "gas seeding" in info["_source"]["name"].lower():
                logger.debug("Gas Seeding")
            else:
                logger.debug(shot["id"])
                settings = get_settings_for_shot(pid=shot["id"],average=average,version=version,minimum_discharge_length=minimum_discharge_length,debug=debug)
                for camid in camids:
                    #if settings["config"][0] == "E" or settings["config"][0] == "D":
                    analysis = Analysis_qss()
                    analysis.analyse_shot(show=show,mode=mode,pid=shot["id"],camid=camid,average=average,extra=extra,extra_wcal=extra_wcal,
                                zeeman=zeeman,stark_density=stark_density,version=version,ical=ical,
                                new_wl_calibration=new_wl_calibration,correct_smearing=correct_smearing,
                                instrument_function_mode=instrument_function_mode,
                                numeric_instrument_function=numeric_instrument_function,
                                wcal_comment=wcal_comment,
                                exclude_from_spectrum=exclude_from_spectrum,
                                debug=debug,xlim=xlim,c_idx_fixed=c_idx_fixed,central_wavelength=central_wavelength)
                    #else:
                    #    logger.debug(shot["id"]+"is not in standard or low iota config, but "+settings["config"])
        except:
            logger.warning(shot["id"]+" failed early, is there anything?")
        
def get_settings_for_shot(pid,average=200,version="0",minimum_discharge_length=2,load_from_cache=True,debug=False):
    """For automated analysis a few parameters need to be read, this could also have been done using w7xdia.extractor 
    which is also used for the juiceDB - but I was already almost done when I did this
    What is needed for automated fitting?
    - magnetic configuration - because of different strike-line positions
    - discharge length - this needs to be known rather accurately in order to not misinterpret the recombining plasma at the end of every discharge
    parameters saved for later analysis on 200 ms timebase
    - ecrh heating [MW]
    - density from inferometer [M-2]
    - prad [MW]
    todo - include NBI here
    
    Args:
        pid (str): program id
        average (int): ms averaging time for your analysis
        save (boolean, optional): writing file to discharge folder. defaults to false
        debug (boolean): debugging option
        
        returns (dict): settings including everything from description
    """
    if load_from_cache:
        try:
            path = ground_path + "qss_analysis/discharges/{}/{}/{}".format(campaign,pid[:-4],pid)
            with open(path+"/settings_{}_version{}.json".format(average,version)) as f:
                settings = json.load(f)
            return settings
        except:
            logger.info("Settings for "+pid+" could not be loaded from cache")
    # magnetic configuration
    if pid[:4] == "2022" or pid[:4] == "2023":
            campaign = "OP2.1"
    if pid[:4] == "2018":
            campaign = "OP1.2b"
    if pid[:4] == "2024":
        campaign = "OP2.2"
    pid_mds = utils.pid_format_to_mds_plus(pid)
    currents = qp.get_coilcurrents(pid_mds)
    three_letters = vmec.get_threeletter(currents)
    
    try:
        time_ecrh,power_ecrh = w7xdia.ecrh.read_ecrh_total_power(shot=pid,timeout=10)
    except:
        time_ecrh,power_ecrh = np.array([]),np.array([])
    larger_than_01MW_ecrh = np.where(power_ecrh>=.5)
    discharge_length = 0
    try:
        discharge_length = time_ecrh[larger_than_01MW_ecrh][-1]
        print(discharge_length)
    except:
        logger.warning("Discharge length could not be read")
    archive_path = "http://archive-webapi.ipp-hgw.mpg.de"
    if campaign == "OP1.2b" or campaign == "OP2.1":
        nbi_paths = {}
        nbi_paths['NBI_P8']= '/Test/raw/W7XAnalysis/NBI/S8_PNEUT_DATASTREAM/V1/0/Neutralized%20Power%20Source%20(S8)/'
        nbi_paths['NBI_P7']= '/Test/raw/W7XAnalysis/NBI/S7_PNEUT_DATASTREAM/V1/0/Neutralized%20Power%20Source%20(S7)/'
        nbi_paths['NBI_P4']= '/Test/raw/W7XAnalysis/NBI/S4_PNEUT_DATASTREAM/V1/0/Neutralized%20Power%20Source%20(S4)/'
        nbi_paths['NBI_P3']= '/Test/raw/W7XAnalysis/NBI/S3_PNEUT_DATASTREAM/V1/0/Neutralized%20Power%20Source%20(S3)/'
        nbi_signals = {}
        discharge_length_nbi = None
        for key in nbi_paths.keys():
            try:
                nbi_signals[key] = {}
                path = archive_path + nbi_paths[key]
                nbi_signals[key]["time_s"],nbi_signals[key]["power_W"] = archivedb.get_signal_for_program(path,pid,timeout=10,useCache=True)
                nbi_signals[key]["power_MW"] = nbi_signals[key]["power_W"]/1e6
                larger_than_1MW_nbi = np.where(nbi_signals[key]["power_MW"]>=1)
                if nbi_signals[key]["time_s"][larger_than_1MW_nbi][-1] > discharge_length:
                    discharge_length_nbi = nbi_signals[key]["time_s"][larger_than_1MW_nbi][-1]
                    discharge_length = discharge_length_nbi
            except:
                logger.info("Source {} not used".format(key))
    else:
        nbi_paths = {}
        nbi_paths['NBI_P8']= '/ArchiveDB/raw/W7X/ControlStation.2092/BE000_DATASTREAM/7/s8_Ptorus/'
        nbi_paths['NBI_P7']= '/ArchiveDB/raw/W7X/ControlStation.2092/BE000_DATASTREAM/6/s7_Ptorus/'
        nbi_paths['NBI_P4']= '/ArchiveDB/raw/W7X/ControlStation.2176/BE000_DATASTREAM/7/s4_Ptorus/'
        nbi_paths['NBI_P3']= '/ArchiveDB/raw/W7X/ControlStation.2176/BE000_DATASTREAM/6/s3_Ptorus/'
        nbi_signals = {}
        discharge_length_nbi = None
        for key in nbi_paths.keys():
            try:
                nbi_signals[key] = {}
                path = archive_path + nbi_paths[key]
                nbi_signals[key]["time_s"],nbi_signals[key]["power_MW"] = archivedb.get_signal_for_program(path,pid,timeout=10,useCache=True)#,nsamples=10000
                larger_than_1MW_nbi = np.where(nbi_signals[key]["power_MW"]>=1)
                if nbi_signals[key]["time_s"][larger_than_1MW_nbi][-1] > discharge_length:
                    discharge_length_nbi = nbi_signals[key]["time_s"][larger_than_1MW_nbi][-1]
                    discharge_length = discharge_length_nbi
            except:
                logger.info("Source {} not used".format(key))
    points_per_second = 1000/average
    discharge_length_round_up = np.ceil(discharge_length*points_per_second)/points_per_second
    time_s = np.linspace(0,discharge_length_round_up-average/1000,int(discharge_length_round_up*points_per_second))+average/1000/2
    try:
        interferometer_density_path = '/ArchiveDB/views/KKS/QMJ_Interferometer/Processed/Density/'
        time_interferometer,density_interferometer = archivedb.get_signal_for_program(archive_path+interferometer_density_path,pid,timeout=10,useCache=True)
        interferometer_density_average = utils.average_signal_on_time_array(new_time_array=time_s,old_time_array=time_interferometer,old_signal=density_interferometer)
        larger_than1e18_density = np.where(density_interferometer>1e18)
        if False:
            if time_interferometer[larger_than1e18_density][-1]>discharge_length+2 and density_interferometer[utils.val2idx(time_interferometer,discharge_length+1)]>5e18:
                discharge_length = time_interferometer[larger_than1e18_density][-1]-1 # one second before plasma density is gone seems to be safe
                discharge_length_round_up = np.ceil(discharge_length*points_per_second)/points_per_second
                time_s = np.linspace(0,discharge_length_round_up-average/1000,int(discharge_length_round_up*points_per_second))
                interferometer_density_average = utils.average_signal_on_time_array(new_time_array=time_s,old_time_array=time_interferometer,old_signal=density_interferometer)
    except:
        logger.warning("could not read interferometer")
        time_interferometer, density_interferometer,interferometer_density_average = np.array([]),np.array([]),np.nan*np.zeros(len(time_s))
    try:
        if campaign == "OP2.1" or campaign == "OP1.2b":
            prad_path = '/Test/raw/W7XAnalysis/QSB-Bolometry/Prad_HBC_DATASTREAM/V1/0/Prad_HBC/'
        else:
            prad_path = '/ArchiveDB/raw/W7XAnalysis/QSB-Bolometry/Prad_HBC_DATASTREAM/V1/0/Prad_HBC/'
        time_prad,prad = archivedb.get_signal_for_program(archive_path+prad_path,pid,timeout=10,useCache=True)
        prad_average = utils.average_signal_on_time_array(new_time_array=time_s,old_time_array=time_prad,old_signal=prad)
    except:
        logger.warning("could not read prad")
        time_prad,prad,prad_average =  np.array([]),np.array([]),np.zeros(len(time_s))
    try:
        power_ecrh_average = utils.average_signal_on_time_array(new_time_array=time_s,old_time_array=time_ecrh,old_signal=power_ecrh)
    except:
        power_ecrh_average = np.zeros(len(time_s))
    
    # end of discharge: last timepoint where heating is larger than 1 MW
    
    if discharge_length < minimum_discharge_length:
        logger.warning("{} failed".format(pid))
        return
    if time_ecrh[larger_than_01MW_ecrh][-1]+2 < discharge_length and discharge_length_nbi is None:
        logger.warning("{} failed".format(pid))
        discharge_length = None
    settings = {}
    settings["time_s"] = time_s
    settings["power_ecrh_MW"] = power_ecrh_average
    settings["density_interferometer"] = interferometer_density_average
    try:
        settings["prad_MW"] = prad_average/1e3
    except:
        time_prad,prad,prad_average =  np.array([]),np.array([]),np.zeros(len(time_s))
    settings["config"] = three_letters
    settings["pid"] = pid
    settings["discharge_length"] = discharge_length
    settings["discharge_length_round_up"] = discharge_length_round_up
    if version != "0":    
        path = ground_path + "qss_analysis/discharges/{}/{}/{}".format(campaign,pid[:-4],pid)
        if os.path.exists(path):
            pass
        else:
            os.mkdir(path)
        json.dump(settings, codecs.open(path+"/settings_{}_version{}.json".format(average,version), 'w', encoding='utf-8'),
                            cls=utils.NumpyEncoder, separators=(',', ':'), sort_keys=False, indent=4)
    if debug:
        plt.figure(figsize=(8,6))
        try:
            plt.plot(time_ecrh,power_ecrh,label="ecrh")
        except:
            pass
        try:
            plt.plot(time_interferometer,density_interferometer/1e19,label="Interferometer density [m-2]")
        except:
            pass
        try:
            plt.plot(time_prad,prad/1e3,label="Prad")
        except:
            pass
        try:
            plt.plot(time_s,power_ecrh_average,label="ecrh average")
        except:
            pass
        try:
            plt.plot(time_s,interferometer_density_average/1e19,label="Interferometer density average")
            plt.plot(time_s,prad_average/1e3,label="Prad_average")
        except:
            pass
        for key in nbi_paths.keys():
            try:
                plt.plot(nbi_signals[key]["time_s"],nbi_signals[key]["power_MW"],label=key)
            except:
                logger.debug("no data in"+key)
                #pass
        plt.xlim(-1,discharge_length+3)
        plt.xlabel("Time [s]")
        plt.ylabel("Power [MW]")
        plt.axvline(discharge_length,ls="--",c="r")
        plt.axvline(discharge_length_round_up,ls="--",c="g")
        plt.title(pid)
        plt.legend()
        path = ground_path + "qss_analysis/discharges/{}/{}/{}".format(campaign,pid[:8],pid)
        plt.savefig(path+"/settings_{}_version{}.png".format(average,version))
        plt.show()
    return settings