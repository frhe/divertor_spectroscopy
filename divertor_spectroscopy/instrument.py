import numpy as np
import logging
logger = logging.getLogger(__name__)
from divertor_spectroscopy import utilities as utils

class Instrument():
    """This class was introduced after a clear separation of physics in the spectrum class and instrument characteristics in this class
    
    The instrument object provides the instrument features of a spectrum
    This is meant to only describe one channel at a time!
    """
    def __init__(self,wavelength=None,shift_guess=None,width_guess=None,wavelength_coefficients_guess=None,lower_bounds=None,upper_bounds=None,instrument_function_mode="gauss",numeric_instrument_function=None,width=None):
        self.wavelength_coefficients_guess = wavelength_coefficients_guess
        self.wavelength = wavelength
        self.shift_guess = shift_guess
        self.width_guess = width_guess
        self.instrument_function_mode = instrument_function_mode
        if width is None:
            logger.warning("You have not given a width for your gaussian instrument function")
        else:
            self.width = width
        self.numeric_instrument_function = numeric_instrument_function
        if shift_guess is not None:
            self.shift_guess = shift_guess
            self.lower_bounds = lower_bounds
            self.upper_bounds = upper_bounds
        if width_guess is not None:
            self.width_guess = width_guess
        if wavelength_coefficients_guess is not None:
            self.wavelength_coefficients_guess = wavelength_coefficients_guess
            self.lower_bounds = lower_bounds
            self.upper_bounds = upper_bounds
            self.x = np.linspace(-511.5,511.5,1024)
            self.wavelength = utils.polynominal(self.x,*self.wavelength_coefficients_guess)
        self.dispersion = np.abs(self.wavelength[-1]-self.wavelength[0])/len(self.wavelength)
        
            