# divertor_spectroscopy

Goal of this project is to access and analyze divertor_spectroscopy (QSS)

## Getting started

### Prerequisites

Reduced version (enough for spectral fitting and wavelength calibration):
- archivedb

Full version:
- atomdat and utils from Felix Reimold for atomic data interface
- w7x-overviewplot and w7xdia for archive and logbook access

### Analysis folder
- you need to copy the following folder: \/\/share\groups\E3\Diagnostik\DIA-2\QSS Divertorspektroskopie\qss_analysis and place it at the location of your choice on your machine
- specify this location in divertor_spectroscopy/settings.json under the variable "ground_path"

### Installing

run: pip install -e .